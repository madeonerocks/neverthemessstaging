define([
        "log",
        "jquery",
        "backbone",
        "colorController"
    ],
    function (log, $, Backbone, ColorController) {
        "use strict";


        var HeaderMeta = Backbone.View.extend({

                events: {
                    "click .meta-search": "_openSearch",
                    "click .meta-basket": "_toggleMiniBasket",
                    "focus #search": "_searchMode",
                    "focusout #search": "_collapse"
                },

                basket: false,
                open: false,

                constructor: function (options) {
                    Backbone.View.apply(this, arguments);
                },

                initialize: function (options) {
                    this._checkBasket();
                },

                _collapse: function () {
                    $('#header-meta').removeClass();
                    $('#header-meta').addClass("tiny");
                },

                _openSearch: function () {
                    log.debug("open Search");
                    $('#header-meta').removeClass("tiny");
                    $('#search').focus();
                },

                _searchMode: function () {
                    $('#header-meta').addClass("search-mode");
                },

                _openBasket: function () {
                    var self = this;
                    $('#meta-sub .sub-cart').stop().slideDown(150, function() {
                        $('#header-meta .meta-basket').addClass("active");
                    });
                    if (! self.basket) {
                        $('#header-meta').toggleClass("tiny");
                    }
                    self.open = true;

                    //     this._openMetaSubItem();
                },

                _closeBasket: function () {
                    var self = this;
                    $('#meta-sub .sub-cart').stop().slideUp(350, function() {
                        $('#header-meta .meta-basket').removeClass("active");
                    });
                    if (! self.basket) {
                        $('#header-meta').toggleClass("tiny");
                    }
                    self.open = false;

                },

                _toggleMiniBasket: function () {


                    if (this.open) {
                        this._closeBasket();
                    } else {
                        this._openBasket();
                    }

                    //if (! this.basket) {
                    //    $('#header-meta').toggleClass("tiny");
                    //}
                    //$('#header-meta .meta-basket').toggleClass("active");
                    //$('#meta-sub .sub-cart').toggleClass("visible");
                },

                _openMetaSubItem: function (e) {
                    $("#meta-sub .sub-item a.close").click(this._closeSubItems);
                },

                _closeSubItems: function (e) {
                    e.preventDefault();
                    log.debug("close sub meta");
                    $('#meta-sub .sub-item').removeClass("visible");
                },

                _checkBasket: function () {

                    var $el = $("#cart-counter").hasClass("active");

                    if ( $el ) {
                        $('#header-meta').removeClass("tiny");
                        this.basket = true;
                    }

                },

                _checkMiniBasketCounter: function () {

                    var $el = $("#header-cart .minicart-wrapper p.empty");
                    log.debug("Check MiniBasket Counter");

                    if ( $el ) {

                        $("#cart-counter").removeClass("active");
                        log.debug("Counter: remove 'active'");

                    }

                }
            }
        );

        return HeaderMeta;
    }
);
define([
        "log",
        "jquery",
        "ScrollMagic",
        "TweenMax",
        "smoothScroll",
        "../util/GradientBoxController"
    ],
    function (log, $, ScrollMagic, TweenMax, SmoothScroll,GradientBoxController) {
        "use strict";

        var Filter = {

            init: function () {

                var self = this;
                var $filter = $(".list-filter");

                this.initHandler();

                var $dropDownButtons = $filter.find(".filter-button-drowdown");
				$dropDownButtons
                    .off("click", this.toggleDropDownHandler)
                    .on("click", this.toggleDropDownHandler);

                var $options = $filter.find(".filter-dropdown-option");
				$options
                    .off("click", this.selectOptionsHandler)
                    .on("click", this.selectOptionsHandler);

                /*var $clickFilter = $filter.find(".filter-button-results");
                $clickFilter.on("click", function () {
                    self.clickResult($(this));
                });*/

                if ($filter.length > 0) {
                    var checkGetParameter = window.location.search.substr(1);
                    var restoredSession = localStorage.getItem('filterData');
                    var restoredAjaxReloadSession = localStorage.getItem('filterReloadAjax');
                    var hash = null;

                    if(window.location.hash) {

                        var hash = window.location.hash.substring(1);

                        if(hash === "category-products"){
                            localStorage.removeItem('filterData');
                            restoredSession = null;
                        }
                    }


                    if(!restoredAjaxReloadSession){

                        // If local storage exist but no get parameters remove session
                        if (restoredSession && !checkGetParameter && !restoredAjaxReloadSession) {
                            localStorage.removeItem('filterData');
                            // If local storage exist and get parameter initialize filter
                        } else if (restoredSession && checkGetParameter) {
                            self.initializeProductFilter();
                            // If get parameter exist prepare data for initialize
                        } else if (checkGetParameter) {
                            self.prepareProductFilterData();
                        }
                    }


                    // Remove Ajax Filter Reload Marker
                    localStorage.removeItem('filterReloadAjax');


                    // Add click event for browser back
                    var $products = $(".category-products");
                    if($products.length < 1){
                        $products = $(".category-products-accessories");
                    }

                    var $productClick = $products.find(".item");

					$productClick
                        .off("click", this.buildBrowserBackHandler)
                        .on("click", this.buildBrowserBackHandler);

					$(window)
                        .off("scroll", this.checkStickynessHandler)
                        .on("scroll", this.checkStickynessHandler);
                    self.checkStickyness();
                }


            },

			/**
             * initialize event handler methods
			 * @returns {Filter}
			 */
			initHandler: function () {

                if ("undefined" !== typeof this.hasHandlers) return;


				this.toggleDropDownHandler = this.toggleDropdown.bind(this);
				this.selectOptionsHandler = this.selectOption.bind(this);
				this.buildBrowserBackHandler = this.buildBrowserBack.bind(this);
				this.checkStickynessHandler = this.checkStickyness.bind(this);

                this.hasHandlers = true;

                return this;

            },

            selectOption: function (e) {

                var $opt = $(e.currentTarget);
                var jsonDataArray = null;
                var jsonDataObj = {};
                var $btn = $opt.parents(".filter-button-drowdown").first();
                var $selection = $btn.find(".filter-selection");
                var $filterResults = $(".filter-button-results");
                var $options = $btn.find(".filter-dropdown-option");
                var name = $opt.html();
                var value = $opt.data("value");
                var code = $btn.data("code");
                var pos = $btn.data("pos");
                var restoredSession = localStorage.getItem('filterData');
                var $categoryId = $(".categoryId").html();
                var $pageId = $(".pageId").html();
                var self = this;

                $options.removeClass("selected");
                $opt.addClass("selected");
                $selection.html(name);
                if (value) {
                    $btn.addClass("active");
                } else {
                    $btn.removeClass("active");
                }

                // Find local storage data or build new object array
                if (!restoredSession) {
                    jsonDataArray = {};
                } else {
                    jsonDataArray = JSON.parse(restoredSession);
                }

                // If data exist in array rewrite property
                if (jsonDataArray.hasOwnProperty(code)) {

                    jsonDataArray[code] = {"value": value, "pos": pos};

                } else {

                    jsonDataObj[code] = {"value": value, "pos": pos};
                    $.extend(jsonDataArray, jsonDataObj);

                }

                // Set hierarchy

                var i;

                $.each(jsonDataArray, function (key, val) {

                    if(val.value === ""){

                        $.each(jsonDataArray, function (rkey, rval) {

                            // Reset Value
                            if(rval.pos > val.pos && rval.value){

                                delete jsonDataArray[rkey];

                            }

                        });


                        delete jsonDataArray[key];

                    }

                });

                //Write JSON string to local storage session
                var json = JSON.stringify(jsonDataArray);

                localStorage.setItem('filterData', json);

                // Update product filter data
                $.ajax({
                    method: "POST",
                    url: "/de/asyncfilter/filter/update",
                    data: {data: json, categoryId: $categoryId,pageId: $pageId}
                }).done(function (data) {

                    var ajaxReturnData = $.parseJSON(data);

                    $(".filter-result-count").html(ajaxReturnData.count);
                    $filterResults.attr('data-url', ajaxReturnData.filterUrl);


                    self.clickResult($opt);

                });

            },

            toggleDropdown: function (e) {

                var $btn = $(e.currentTarget);

                if ($btn.hasClass("open")) {
                    this.closeDropdown($btn);
                } else {
                    this.openDrowdown($btn);
                }
            },

            openDrowdown: function ($btn) {

                var $countOptions =  $btn.find(".filter-dropdown-option").filter(function() { return $(this).css('display') !== 'none'; }).length;

                this.closeAllDropdowns();

                if($countOptions > 0){
                    $btn.addClass("open");
                }
            },

            closeDropdown: function ($btn) {
                $btn.removeClass("open");
            },

            closeAllDropdowns: function () {
                var self = this;
                var $filter = $(".list-filter");
                var $dropDownButtons = $filter.find(".filter-button-drowdown");
                $dropDownButtons.each(function () {
                    self.closeDropdown($(this));
                });
            },

            checkStickyness: function () {
                var $filter = $(".list-filter");
                if ($filter.length > 0) {
                    var $container = $(".list-filter-container");
                    var scrollPos = $(window).scrollTop();
                    var containerPos = 0;
                    var containerHeight = 0;
                    if ($container.length > 0) {
                        containerPos = $container.offset().top;
                        containerHeight = $container.outerHeight();
                    }
                    var windowHeight = $(window).height();
                    var dif = containerPos - scrollPos;
                    if (dif <= 0) {
                        $filter.addClass("sticky-top").removeClass("sticky-bottom");
                    } else if (scrollPos + windowHeight - containerHeight < containerPos) {
                        $filter.removeClass("sticky-top").addClass("sticky-bottom");
                    } else {
                        $filter.removeClass("sticky-top").removeClass("sticky-bottom");
                    }
                }
            },

            clickResult: function ($opt) {

                var self = this;
                var $catUrl = $(".filter-button-results").data('url');
                var $categoryId = $(".categoryId").html();
                var $pageId = $(".pageId").html();
                var $filterCount = $(".list-filter .filter-result-count").html();
                var $categoryProducts = $(".category-products");
                var $filters = $(".filter-ajax-change");
                var $type = "normal";
                if($categoryProducts.length < 1){
                    $categoryProducts = $(".category-products-accessories");
                    $type = "accessoires";
                }

                var restoredSession = localStorage.getItem('filterData');

                var $trigger = $(".list-filter-container");
                if ($trigger.length < 1) $trigger = $(".category-products");

                // Scroll to top
                var $itemPosition = $trigger.offset().top;

                // Scroll to item
                $('html, body').animate({scrollTop: $itemPosition}, 1500);

                //SmoothScroll.scrollTo($categoryProducts.offset().top,function(){},1500);


                if ($filterCount > 0) {

                    // Update category products
                    $.ajax({
                        method: "POST",
                        url: "/de/asyncfilter/filter/updateproducts",
                        data: {categoryId: $categoryId, filter: restoredSession,ajaxRefresh: 1,url: $catUrl,type: $type,pageId: $pageId},
                        beforeSend: function(){

                            // Fade Out Category Products
                            $categoryProducts.animate({ opacity: 0.2 }, 300);
                            $categoryProducts.css({ pointerEvents: 'none' });
                            ias.destroy();
                        }
                    }).done(function (data) {

                        var ajaxReturnData = $.parseJSON(data);

                        // Fade In Category Products
                        $categoryProducts.animate({ opacity: 1 }, 300);
                        $categoryProducts.css({ pointerEvents: 'auto' });

                        // Prepare Blocks
                        $categoryProducts.empty();
                        $filters.empty();
                        $categoryProducts.html(ajaxReturnData.productlist);
                        $filters.html(ajaxReturnData.filter);
                        $(".toolbar").remove();
                        $categoryProducts.prepend(ajaxReturnData.toolbar);

                        localStorage.setItem('filterReloadAjax', 1);

                        // Reinitialize product filter
                        self.reinitializeProductFilter($opt);

                        ias.reinitialize();
                        Filter.init();
                        GradientBoxController.init();

                    });

                }

            },

            reinitializeProductFilter: function ($opt) {

                var json = localStorage.getItem('filterData');
                var jsonDataArray = JSON.parse(json);
                var $filterOptions = $(".filter-button-drowdown");
                var $filterItemLast = '';
                var $selection = '';

                // Set selected options
                $.each(jsonDataArray, function (key, val) {

                    var $filterBtn = $(".filter-button-drowdown[data-code=" + key + "]");


                    if ($filterBtn.length > 0) {
                        var $filterSelectedOption = $filterBtn.find("[data-value='" + val.value + "']");
                        var $filterOptions = $filterBtn.find(".filter-dropdown-option");
                        $selection = $filterBtn.find(".filter-selection");

                        $filterOptions.removeClass("selected");

                        if ($filterSelectedOption.length > 0) {
                            $filterSelectedOption.addClass("selected");
                            $selection.html($filterSelectedOption.html());

                        } else {
                            $filterOptions.first().addClass("selected");
                            $selection.html($filterOptions.first().html());
                        }

                    }

                });


                // Check if only one value set it to selected option
                $filterOptions.each(function (index) {

                    var $filterItems = $(this).find(".filter-dropdown-option");


                    // If last filter and only 1 result hide all
                    /*if(index === ($filterOptions.length - 1)){

                        if($filterItems.length === 2){
                            $filterItems.first().hide();
                        }

                    }*/

                    if ($filterItems.length > 0 && $filterItems.length < 3) {

                        $filterItemLast = $filterItems.last();
                        $filterItems.removeClass("selected");
                        $selection = $(this).find(".filter-selection");

                        $selection.html($filterItemLast.html());

                        $filterItemLast.addClass("selected");

                    }

                    if($(this).data("code") === "helmet_model"){

                        if($(this).find(".filter-selection").html() !== "All" && $(this).find(".filter-selection").html() !== "Alle"){

                            var $helmetType = $filterOptions.first();

                            if($helmetType.find(".filter-dropdown-option").length > 2){

                                var $helmetTypeOptions = $helmetType.find(".filter-dropdown-option");
                                $selection = $helmetType.find(".filter-selection");


                                if($helmetType.find(".filter-selection").html() !== "All" && $helmetType.find(".filter-selection").html() !== "Alle"){
                                    $helmetType.find(".filter-dropdown-option:gt(0)").hide();
                                }else{
                                    $helmetTypeOptions.removeClass("selected");
                                    $selection.html($helmetTypeOptions.eq(1).html());
                                    $helmetTypeOptions.eq(1).addClass("selected");
                                    $helmetType.find(".filter-dropdown-option:gt(1)").hide();
                                }


                            }

                        }
                    }


                });

            },

            prepareProductFilterData: function () {


                // Get all get parameters
                var vars = {};
                window.location.href.replace(location.hash, '').replace(
                    /[?&]+([^=&]+)=?([^&]*)?/gi,
                    function (m, key, value) {
                        vars[key] = value !== undefined ? {"value": value, "pos": ''} : '';
                    }
                );


                //console.log(vars);

                // Write to JSON string
                var json = JSON.stringify(vars);

                //Write JSON string to local storage session
                localStorage.setItem('filterData', json);

                // Initialize product filter
                this.initializeProductFilter();

            },

            initializeProductFilter: function () {

                var json = localStorage.getItem('filterData');
                var $categoryId = $(".categoryId").html();
                var $pageId = $(".pageId").html();
                var $filterResults = $(".filter-button-results");
                var jsonDataArray = JSON.parse(json);
                var $filterItemLast = '';
                var $selection = '';
                var $filterOptions = $(".filter-button-drowdown");
                var $filterSelectedOption = '';


                // Set selected options
                $.each(jsonDataArray, function (key, val) {

                    var $filterBtn = $(".filter-button-drowdown[data-code=" + key + "]");


                    //console.log(key);
                    if ($filterBtn.length > 0) {

                        if(val.value !== undefined){
                            $filterSelectedOption = $filterBtn.find("[data-value='" + val.value + "']");
                        }else{
                            $filterSelectedOption = $filterBtn.find("[data-value='" + val + "']");
                        }

                        var $filterOptions = $filterBtn.find(".filter-dropdown-option");
                        $selection = $filterBtn.find(".filter-selection");

                        $filterOptions.removeClass("selected");

                        if ($filterSelectedOption.length > 0) {

                            //console.log("find");

                            $filterSelectedOption.addClass("selected");
                            $selection.html($filterSelectedOption.html());

                        } else {
                            $filterOptions.first().addClass("selected");
                            $selection.html($filterOptions.first().html());
                        }

                    }

                });


                // Check if only one value set it to selected option
                $filterOptions.each(function (index) {

                    var $filterItems = $(this).find(".filter-dropdown-option");


                    // If last filter and only 1 result hide all
                    /*if(index === ($filterOptions.length - 1)){

                        if($filterItems.length === 2){
                            $filterItems.first().hide();
                        }

                    }*/

                    if ($filterItems.length > 0 && $filterItems.length < 3) {

                        $filterItemLast = $filterItems.last();
                        $filterItems.removeClass("selected");
                        $selection = $(this).find(".filter-selection");

                        $selection.html($filterItemLast.html());

                        $filterItemLast.addClass("selected");

                    }

                    if($(this).data("code") === "helmet_model"){

                        if($(this).find(".filter-selection").html() !== "All" && $(this).find(".filter-selection").html() !== "Alle"){

                            var $helmetType = $filterOptions.first();

                            if($helmetType.find(".filter-dropdown-option").length > 2){

                                var $helmetTypeOptions = $helmetType.find(".filter-dropdown-option");
                                $selection = $helmetType.find(".filter-selection");


                                if($helmetType.find(".filter-selection").html() !== "All" && $helmetType.find(".filter-selection").html() !== "Alle"){
                                    $helmetType.find(".filter-dropdown-option:gt(0)").hide();
                                }else{
                                    $helmetTypeOptions.removeClass("selected");
                                    $selection.html($helmetTypeOptions.eq(1).html());
                                    $helmetTypeOptions.eq(1).addClass("selected");
                                    $helmetType.find(".filter-dropdown-option:gt(1)").hide();
                                }


                            }

                        }
                    }

                });


                // Update product filter data

                //console.log("aja");
                $.ajax({
                    method: "POST",
                    url: "/de/asyncfilter/filter/update",
                    data: {data: json, categoryId: $categoryId,pageId: $pageId}
                }).done(function (data) {

                    var ajaxReturnData = $.parseJSON(data);

                    $(".filter-result-count").html(ajaxReturnData.count);
                    $filterResults.attr('data-url', ajaxReturnData.filterUrl);

                });

            },

            buildBrowserBack: function(e) {

                var $item = $(e.currentTarget);
                var $backUrl = $(".browserBack").text();
                var $productId = $item.data("id");
                var $productCount = $(".filter-result-count").text();

                var loc = $backUrl;
                if (loc.indexOf("?") === -1)
                    loc += "?";
                else
                    loc += "&";

                $backUrl = loc + "product="+$productId+"&limit="+$productCount;


                var jsonDataArray = '';

                localStorage.removeItem('backUrl');

                jsonDataArray = {"url": $backUrl, "productId": $productId,"productCount": $productCount};

                //Write JSON string to local storage session
                var json = JSON.stringify(jsonDataArray);

                localStorage.setItem('backUrl', json);
            }

        };

        return Filter;
    }
);
/**
 * Created by andi on 26.01.16.
 */

define([
        "log",
        "jquery"
    ],
    function (log, $) {
        "use strict";

        var BackLink = {

            init: function () {



                var restoredSession = localStorage.getItem('backUrl');

                if (restoredSession) {

                    history.replaceState(null, document.title, location.pathname+"#!/browseback");
                    history.pushState(null, document.title, location.pathname);

                    this._browserBack(restoredSession);

                    localStorage.removeItem('backUrl');

                }else{

                    $("div.back-link a").on("click",this._goBack);

                }

            },

            _goBack: function(e) {

                log.debug("_goBack");



                var $link = $("div.back-link a");

                if(!$link.data("category-url")){

                    e.preventDefault();

                    if (history.length < 2) {
                        var url = $link.data("category-url");
                        window.location.href = url;
                    } else {
                        var url = "javascript:history.back();";
                        window.location.href = url;
                    }

                }

            },

            _browserBack: function(data) {

                var jsonDataArray = JSON.parse(data);

                console.log(jsonDataArray.url);

                // Set browser back url

                window.addEventListener("popstate", function() {
                    if(location.hash === "#!/browseback") {
                        history.replaceState(null, document.title, location.pathname);
                        setTimeout(function(){
                            location.replace(jsonDataArray.url);
                        },0);
                    }
                }, false);


                // Set back link url

                $("div.back-link a").attr("href",jsonDataArray.url);




            }


        };

        return BackLink;
    }
);

define([
        "log",
        "jquery",
        "slick"
    ],
    function (log, $, slick) {
        "use strict";

        var Slider = {

            init: function () {
                var $sliders = $(".js-slider");

                $sliders.each(function(){

                    var options = {
                        infinite: true,
                        dots: true,
                        adaptiveHeight: false
                    };

                    if($(this).hasClass("js-slider-no-dots")) options.dots = false;

                    if($(this).hasClass("js-slider-autoplay")) {

                        options.autoplay = true;
                        options.autoplaySpeed = 10000;

                    }

                    if($(this).hasClass("js-slider-no-arrows")) options.arrows = false;
                    if($(this).hasClass("js-slider-adaptive")) options.adaptiveHeight = true;


                    $(this).slick( options );

                });



            }

        };

        return Slider;
    }
);
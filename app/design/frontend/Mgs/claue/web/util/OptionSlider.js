define([
        "log",
        "jquery",
        "touchSwipe"
    ],
    function (log, $, TouchSwipe) {
        "use strict";

        var OptionSlider = {

            $el: null,
            limit: 0,
            total: 0,
            pos: 0,
            $options: null,

            create: function ($el, buttonWidth, count) {

                log.debug($el, buttonWidth, count);

                buttonWidth = Math.floor(buttonWidth);

                this.$el = $el;

                this.pos = 0;
                this.$options = this.$el.children("div:not(.slider-button)");
                this.total = this.$options.length;

                var self = this;

                $el.addClass("sliderino");

                var $prevButton = $("<div>").addClass("slider-button").addClass("slider-button-prev").width(buttonWidth);
                var $nextButton = $("<div>").addClass("slider-button").addClass("slider-button-next").width(buttonWidth);
                var optionWidth = self.$options.first().width();
                var offset = 0;

                this.limit = count - 2;

                if( buttonWidth < optionWidth -1 ) {
                   //this.limit = count - 1;
                }


                $el.swipe({
                    threshold: optionWidth,
                    swipeStatus: function (event, phase, direction, distance, duration, fingers, fingerData, currentDirection) {

                        if(distance > optionWidth / 2){
                            event.stopPropagation();
                        }

                        if(phase == "start"){
                            offset = 0;
                        }

                        var realDistance = 0;

                        if( direction == "left" ){
                            realDistance = + distance;
                        } else if( direction == "right" ){
                            realDistance = - distance;
                        }

                        if ( Math.abs( realDistance + offset ) > optionWidth ) {
                            if (currentDirection == "right") {
                                self.prev(event);
                                offset += optionWidth;
                            } else if (currentDirection == "left") {
                                self.next(event);
                                offset -= optionWidth;
                            }
                        }
                    }
                });

                this.$el.prepend($prevButton);
                this.$el.append($nextButton);

                $(".slider-button-prev").on("click", this.prev);
                $(".slider-button-next").on("click", this.next);

                this.update(this.pos);

            },

            destroy: function ($el) {

                $el.removeClass("sliderino");
                $el.find(".slider-button").remove();
                this.$options.removeClass("hidden");

                $(".slider-button-prev").off("click");
                $(".slider-button-next").off("click");

            },

            prev: function (e) {

                e.preventDefault();

                var self = OptionSlider;

                self.pos = self.pos - 1;

                if (self.pos < 0) self.pos = 0;

                self.update();

            },

            next: function (e) {

                e.preventDefault();

                var self = OptionSlider;

                self.pos = self.pos + 1;

                if (self.pos > self.total - self.limit) self.pos = self.total - self.limit;

                self.update();

            },

            update: function () {

                var self = this;

                this.$options.each(function (index) {
                    if (index >= self.pos + self.limit || index < self.pos) {
                        $(this).addClass("hidden");
                    } else {
                        $(this).removeClass("hidden");
                    }
                });

            }

        };

        return OptionSlider;
    }
);
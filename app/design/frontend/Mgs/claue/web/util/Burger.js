define([
        "log",
        "jquery"
    ],
    function (log, $) {
        "use strict";

        var Burger = {

            init: function () {

                $("#burger-button").on('click', this.toggleBurger);
                // Tudock Helm-24: start
                    $(window).resize(function () {
                        Burger.hideMobileMenu();
                    });
                // Tudock Helm-24: End
            },

            toggleBurger: function() {
                var $overlay = $("#burger-overlay");
                var $header = $("#header");

                if($overlay.hasClass("open")){
                    $overlay.removeClass("open");
                    $header.removeClass("burger-hide-mode");
                } else {
                    $overlay.addClass("open");
                    $header.addClass("burger-hide-mode");
                }

            },
            // Tudock Helm-24: start
            hideMobileMenu: function() {
                var viewport = document.viewport.getDimensions();
                if (viewport.width >= 550) {
                    var overlay = jQuery("#burger-overlay");
                    if(overlay.hasClass('open')) {
                        jQuery('#burger-button').trigger('click');
                    }
                }
            }
            // Tudock Helm-24: end
        };

        return Burger;
    }
);

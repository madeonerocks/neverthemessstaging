define([
        "log",
        "jquery",
        "backbone",
        "colorUtil"
    ],
    function (log, $, Backbone, ColorUtil) {
        "use strict";

        var ColorController = {

            transitionDuration: "1s",

            texts: [
                {s: ".shipping-cost-details a"},
                {s: ".global-color.color-text"},
                {s: "#nav-container .subnav .featured a:hover"},
                {s: ".footer-container .footer .contact.mail a:hover"},
                {s: "a", t: false}
            ],

            backgrounds: [
                {s: "#configurator .visor-panel .visor-selector .visor-tile.active:before"},
                {s: ".button, .cart-table .product-cart-actions .button, #co-shipping-method-form .buttons-set .button, .footer .button"},
                {s: "#configurator-background .loader .loader-line"},
                {s: ".minicart-wrapper a.checkout-button"}
            ],

            textsPseudo: [
                {s: ".hover-color:hover:before"},
                {s: ".option .pager .icon:before"},
                {s: "#configurator .fitting-guide .icon:hover"},
                {s: ".footer-container .footer .contact .social .icon:hover"},
                {s: "#header-meta #header-meta-expandable .icon.active:before"},
                {s: "#burger-overlay .secondary-row .secondary-link .icon:before"}
            ],

            backgroundsPseudo: [
                {s: "#configurator .ui-view input[type='radio']:checked + label::after", t: false},
                {s: "#configurator .ui-view .size-panel .size-selector-container .size-selector .size.active:after"},
                {s: "#configurator .ui-view .size-panel .size-selector-container .size-selector .size.active:before"},
                {s: "#configurator-background:before"},
                {s: "#configurator .ui-view .signature-panel .font-selector .font-tile.active:before", t: false},
                {s: "#configurator .visor-panel .visor-selector .visor-tile.active:before", t: false},
                {s: "#configurator .visor-panel .visor-selector .visor-tile:hover:not(.active):before"},
                {s: "#configurator .signature-panel .font-selector .font-tile:hover:not(.active):before"}
            ],

            underlineBackground: [
                {s: "#header-nav nav ol li.active:before"},
                {s: ".underlined:before"},
                {s: ".hover-underline:before"},
                {s: "#nav .level0 span a:before", t: false},
                {s: ".footer-container .footer .links ul li:before", t: false},
                {s: "#nav-container .level1 .category-name:before"},
                {s: ".footer-container .term-container span:before", t: false},
                {s: "#burger-overlay nav .main-link:before", t: false}
            ],

            init: function () {
                this.changeColor("#42f092");
            },

            changeColor: function (hex) {

                hex = ColorUtil.levelColor(hex);

                var luminance = ColorUtil.getLuminance(hex);
                var opacity = 1;

                if (luminance < 0.6) {
                    opacity = 0.5;
                }

                var lastStyleSheet = document.styleSheets[document.styleSheets.length - 1];

                //console.log(lastStyleSheet);

                if (lastStyleSheet != null) {
                    var addRule = function (selector, styles) {
                        if (lastStyleSheet.cssRules != null) {
                            if (lastStyleSheet.insertRule) return lastStyleSheet.insertRule(selector + " {" + styles + "}", lastStyleSheet.cssRules.length);
                        } else {
                            //log.debug("lastStyleSheet.cssRules == null");
                        }
                        if (lastStyleSheet.addRule) return lastStyleSheet.addRule(selector, styles);
                    };
                } else {
                    // log.debug("could not find last stylesheet");
                }


                var applyRules = function (el, cssProperty, opacity) {
                    var selector = el.s;
                    var duration = ColorController.transitionDuration;

                    if (typeof el.t !== "undefined" && el.t !== false) duration = el.t;

                    addRule(selector, cssProperty + ": " + hex);

                    if (typeof opacity !== "undefined") {
                        addRule(selector, "opacity:" + opacity);
                    }


                    if (el.t !== false) {
                        addRule(selector, "transition: " + cssProperty + " " + opacity + " " + duration);
                        addRule(selector, "-webkit-transition: " + cssProperty + " " + opacity + " " + duration);
                        addRule(selector, "-moz-transition: " + cssProperty + " " + opacity + " " + duration);
                    }

                };

                // Set transition duration to what is set in CSS

                if ($('.global-color').first().css("transition-duration")) {
                    ColorController.transitionDuration = $('.global-color').first().css("transition-duration");
                }

                // Color Backgrounds

                var $backgrounds = $('.global-color.color-background');

                $backgrounds.each(function () {
                    $(this).css({backgroundColor: hex}).attr("data-lum",luminance);
                });

                var fontColor = ColorUtil.getForegroundForBackground(hex);

                $backgrounds.each(function () {
                    $(this).css({color: fontColor});
                });

                $.each(ColorController.backgrounds, function (index, selector) {
                    applyRules(this, "background-color");
                });


                // Color Texts

                var $texts = $('.global-color.color-text');

                $texts.each(function () {
                    $(this).css({color: hex});
                });

                $.each(ColorController.texts, function (index, selector) {
                    applyRules(this, "color");
                });

                // Color Pseudo Elements

                $.each(ColorController.textsPseudo, function (index, el) {
                    applyRules(el, "color");
                });

                $.each(ColorController.backgroundsPseudo, function (index, el) {
                    applyRules(el, "background-color");
                });

                $.each(ColorController.underlineBackground, function (index, el) {
                    applyRules(el, "background-color", opacity);
                });

            }

        };


        return ColorController;
    }
);
define([
        "log",
        "jquery"
    ],
    function (log, $) {
        "use strict";

        var ProductGallery = {

            init: function () {

                $(".product-img-box .product-image-thumbs a").on("click",function(e){
                    e.preventDefault();
                    ProductGallery.show($(this).attr("data-image-index"));
                });

            },

            show: function( index ) {
                $(".product-img-box .product-image-thumbs a").removeClass("active");
                $('.product-img-box .product-image-thumbs a[data-image-index="'+index+'"]').addClass("active");
                $('.product-image-gallery img').removeClass("visible");
                log.debug('.product-image-gallery .image-'+index);
                $('.product-image-gallery #image-'+index).addClass("visible");
            }

        };

        return ProductGallery;
    }
);
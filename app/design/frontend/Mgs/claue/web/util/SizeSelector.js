define([
        "log",
        "jquery"
    ],
    function (log, $) {
        "use strict";

        var SizeSelector = {

            init: function () {

                var $options = $(".size-selector .size-option");
                if($options.length > 0) this.deactivateBuyButton();
                $options.on('click', this.select);

            },

            select: function(e) {

                var $option = $( e.target );
                if(!$option.hasClass(".size-option")) $option = $option.parents(".size-option").first();

                if(!($option.hasClass("unavailable") || $option.hasClass("selected"))){
                    var value = $option.attr("data-id");
                    var $options = $option.parents(".size-selector").find(".size-option");
                    $options.removeClass("selected");
                    $option.addClass("selected");
                    $(".size-input").val(value);
                    SizeSelector.activateBuyButton();
                }

            },

            deactivateBuyButton: function() {
                $('.add-to-cart-buttons button').addClass("notActive");
            },

            activateBuyButton: function() {
                $('.add-to-cart-buttons button').removeClass("notActive");
                $('.add-to-cart-buttons button').addClass("noTransition");
            }

        };

        return SizeSelector;
    }
);

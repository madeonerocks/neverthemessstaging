define([
        "log",
        "jquery",
        "slick"
    ],
    function (log, $, slick) {
        "use strict";

        var DesignLineSlider = {

            init: function () {

                $(".js-slider-designline").on('afterChange', function(event, slick, currentSlide) {

                        DesignLineSlider.changeSlide();

                });

                this.changeSlide();

            },

            changeSlide: function(){

                var $this =  $(".js-slider-designline");

                var name = $this.find(".slick-current").data("name");


                var url = $this.find(".slick-current").data("url");
                var url_sk = $this.find(".slick-current").data("url-sk");
                var url_gp = $this.find(".slick-current").data("url-gp");
                var url_ck = $this.find(".slick-current").data("url-ck");

                $(".designline-slider-name").html(name);
                $(".designline-button.mssk").attr("href", url_sk);
                $(".designline-button.msgp").attr("href", url_gp);
                $(".designline-button.msck").attr("href", url_ck);
                $(".designline-button.notms").attr("href", url);


            }

        };

        return DesignLineSlider;
    }
);

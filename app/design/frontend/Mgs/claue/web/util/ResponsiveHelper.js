define([
        "log",
        "jquery"
    ],
    function (log, $) {
        "use strict";

        var ResponsiveHelper = {

            init: function () {

            },

            check: function( size ){

                var $el = null;

                if( size.indexOf("-") > -1 ){

                    var sizes = size.split("-");

                    var selector = "";

                    $.each(sizes,function(i,value){

                        selector += "#responsive-helper .show-for-"+value;

                        if(i < sizes.length -1 ) {
                            selector += ", ";
                        }

                    });

                    $el = $(selector);

                } else {
                    $el = $("#responsive-helper .show-for-"+size);
                }

                var isVisible = false;

                $el.each(function(){

                    if( $( this ).css("display") == "inline" ){
                        isVisible = true;
                    }
                });

                return isVisible;

            }

        };

        return ResponsiveHelper;
    }
);

define([
        "log",
        "jquery"
    ],
    function (log, $) {
        "use strict";

        var ResponsiveImageHelper = {

            init: function () {

                setInterval(function(){

                    var maxHeightImg = 0;
                    var maxHeightTitle = 0;
                    $('.post-title').css('height', '');


                    $('.post-image img').each(function(){
                        var thisH = $(this).height();
                        if (thisH > maxHeightImg) { maxHeightImg = thisH; }
                    });

                    $('.post-title').each(function(){
                        var thisHT = $(this).height();
                        if (thisHT > maxHeightTitle) { maxHeightTitle = thisHT; }
                    });

                    $('.post-image').height(maxHeightImg);
                    $('.post-title').height(maxHeightTitle);

                },200);
            }

        };

        return ResponsiveImageHelper;
    }
);
/**
 * Created by andi on 14.01.16.
 */

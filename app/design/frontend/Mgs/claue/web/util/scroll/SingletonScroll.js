/**
 * Created by andi on 08.01.16.
 */
define([
        "log",
        "jquery",
        "ScrollMagic",
        "TweenMax",
        "../../lib/animation.gsap"
    ],
    function (log, $, ScrollMagic, TweenMax) {
        "use strict";

        var SingletonScroll = {

            init: function () {

               var singleton_controller = new ScrollMagic.Controller();

                /*

                var $h1 = $(" .main-row .page-title h1");

                    var tween = TweenMax.fromTo($h1, 1,
                        {y: 0},
                        {y: -$h1.height()}
                    );

                    var scene = new ScrollMagic.Scene({
                        duration: $(document).height(),
                        triggerHook: 1
                    })
                        .setTween(tween)
                        .addTo(singleton_controller); */
            }

        };

        return SingletonScroll;
    }
);
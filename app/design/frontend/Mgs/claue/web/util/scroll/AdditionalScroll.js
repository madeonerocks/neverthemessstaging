/**
 * Created by andi on 08.01.16.
 */
define([
        "log",
        "jquery",
        "ScrollMagic",
        "TweenMax",
        "responsiveHelper",
        "../../lib/animation.gsap"
    ],
    function (log, $, ScrollMagic, TweenMax, ResponsiveHelper) {
        "use strict";

        var AdditionalScroll = {

            init: function () {

                if ( !ResponsiveHelper.check("xs")  && !ResponsiveHelper.check("s") ) {

                    //log.debug("Responsive TRUE");

                    var parallax_controller = new ScrollMagic.Controller();


                    $(".product-additional .image-container").each(function () {

                        var tween = TweenMax.fromTo($(this), 1,
                            {y: 0 },
                            {y: 100 }
                        );

                        var $column = $(this).parent(".image-column");

                        var scene = new ScrollMagic.Scene({
                            triggerElement: $column[0],
                            offset: "0%",
                            duration: "100%",
                            triggerHook: 1
                        })
                            .setTween(tween)
                            .addTo(parallax_controller);

                    });

                }

            }

        };

        return AdditionalScroll;
    }
);
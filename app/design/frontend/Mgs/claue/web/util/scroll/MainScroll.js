/**
 * Created by andi on 08.01.16.
 */
define([
        "log",
        "jquery",
        "ScrollMagic",
        "TweenMax",
        "responsiveHelper",
        "../../lib/animation.gsap"
    ],
    function (log, $, ScrollMagic, TweenMax, ResponsiveHelper) {
        "use strict";

        var MainScroll = {

            init: function () {

                if ( !ResponsiveHelper.check("xs")  && !ResponsiveHelper.check("s") ) {

                    //log.debug("Responsive TRUE");

                    var parallax_controller = new ScrollMagic.Controller();


                    $(".scroll-parallax").each(function () {

                        //log.debug($(this));
                        var factor = 1;

                        if ($(this).hasClass("level1")) {
                            factor = 2;
                        } else if ($(this).hasClass("level2")) {
                            factor = 1.25;
                        } else if ($(this).hasClass("level3")) {
                            factor = -1.1;
                        }

                        var tween = TweenMax.fromTo($(this), 1,
                            {y: 32 * factor},
                            {y: -32 * factor}
                        );

                        var $row = $(this).parents(".parallax-container");

                        var scene = new ScrollMagic.Scene({
                            triggerElement: $row[0],
                            offset: 0,
                            duration: $(window).height() + 650,
                            triggerHook: 1
                        })
                            .setTween(tween)
                            .addTo(parallax_controller);

                    });

                }

            }

        };

        return MainScroll;
    }
);
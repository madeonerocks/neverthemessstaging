define([
        "log",
        "jquery",
        "ScrollMagic",
        "TweenMax",
        "responsiveHelper",
        "../../lib/animation.gsap"
    ],
    function (log, $, ScrollMagic, TweenMax, ResponsiveHelper) {
        "use strict";

        var VideoScroll = {

            init: function () {

                if ( !ResponsiveHelper.check("xs")  && !ResponsiveHelper.check("s") ) {


                    var controller = new ScrollMagic.Controller();

                    $(".appear-on-scrolling").each(function () {

                        var tween = TweenMax.fromTo($(this), 1,
                            {y: 10},
                            {y: -35}
                        );

                        var scene = new ScrollMagic.Scene({
                            triggerElement: $(this)[0],
                            offset: "0%",
                            duration: "20%",
                            triggerHook: 1
                        })
                            .setTween(tween)
                            .addTo(controller);

                    });


                    $(".video-control-button").each(function () {

                        var controlTween = TweenMax.fromTo($(this), 1,
                            {y: 0},
                            {y: -90 }
                        );

                        var controlScene = new ScrollMagic.Scene({
                            triggerElement: $(this)[0],
                            offset: 46,
                            duration: "100%",
                            triggerHook: 1
                        })
                            .setTween(controlTween)
                            .addTo(controller);

/*                        var controlOpacityTween = TweenMax.fromTo($(this), 1,
                            {opacity: 1},
                            {opacity: 0, ease: Power4.easeIn }
                        );

                        var controlOpacityScene = new ScrollMagic.Scene({
                            triggerElement: $(this)[0],
                            offset: 46,
                            duration: "50%",
                            triggerHook: 1
                        })
                            .setTween(controlOpacityTween)
                            .addTo(controller);*/


                    });
                }

            }

        };

        return VideoScroll;
    }
);
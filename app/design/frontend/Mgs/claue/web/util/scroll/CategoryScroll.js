define([
        "log",
        "jquery",
        "ScrollMagic",
        "TweenMax",
        "responsiveHelper",
        "../../lib/animation.gsap"
    ],
    function (log, $, ScrollMagic, TweenMax, ResponsiveHelper) {
        "use strict";

        var CategoryScroll = {

            scrollHeight: 0,

            init: function () {

                //if (ResponsiveHelper.check("l") || ResponsiveHelper.check("xl")) {

                if ($('.sticky-header').length > 0 && $('#featured_slider').length > 0) {

                    $(window).on("scroll", this.check);
                    $(window).on("resize", this.calculateSizes);

                    this.check();
                    this.calculateSizes();

                }
                //}

                this.checkScrollToProduct();
                this.checkScrollToCategoryProductHead();

            },

            check: function () {

                if (ResponsiveHelper.check("l") || ResponsiveHelper.check("xl")) {

                    var scrollPos = $(window).scrollTop();
                    var $el = $('.sticky-header');
                    var $non = $(".non-sticky");

                    var advantageheight = 0;
                    if (ResponsiveHelper.check("l") || ResponsiveHelper.check("xl")) {
                        advantageheight = 40;
                    }

                    if ($el.length > 0) {
                        var $trigger = $(".list-filter-container");
                        if ($trigger.length < 1) $trigger = $(".category-products");
                        var triggerPos = 0;
                        if ($trigger.length > 0) {
                            triggerPos = $trigger.offset().top;
                        }
                        var windowHeight = $(window).height();
                        if (scrollPos + windowHeight > triggerPos) {
                            $el.css("position", "fixed");
                            $el.css("top", triggerPos - ( scrollPos + windowHeight ));
                        } else {
                            $el.css("position", "fixed");
                            $el.css("top", 0);
                        }
                    }

                    $non.css('-moz-transform', 'translateY(-' + scrollPos + 'px)');
                    $non.css('-webkit-transform', 'translateY(-' + scrollPos + 'px)');
                    $non.css('-o-transform', 'translateY(-' + scrollPos + 'px)');
                    $non.css('transform', 'translateY(-' + scrollPos + 'px)');

                }

            },

            calculateSizes: function () {
                if (ResponsiveHelper.check("l") || ResponsiveHelper.check("xl")) {
                    var $nextEl = $('.sticky-header').parent().next();
                    var sliderHeight = $('#featured_slider').innerHeight();
                    this.scrollHeight = ( $(window).height() ) + sliderHeight;// / 1.5;
                    $nextEl.css("margin-top", $(window).height() - $("#advantage-bar").height());
                    //var margin = 620 - sliderHeight;
                    $("#featured_slider").css("margin-bottom", 120);
                    CategoryScroll.check();
                }
            },


            checkScrollToCategoryProductHead: function () {

                // Check if Hash exist

                if(window.location.hash) {

                    var hash = window.location.hash.substring(1);

                    if(hash === "category-products"){

                        var $item =  $('.category-products');

                        if($item.length < 1){
                            $item =  $('.category-products-accessories');
                        }

                        if($item.length >= 1){

                            // Get top position of item
                            var $itemPosition = $item.offset().top;

                            // Scroll to item
                            $('html, body').animate({
                                scrollTop: $itemPosition - 80
                            }, 2000);
                        }

                    }

                }

            },


            checkScrollToProduct: function () {

                // Get get Parameters
                var vars = {};
                window.location.href.replace(location.hash, '').replace(
                    /[?&]+([^=&]+)=?([^&]*)?/gi,
                    function (m, key, value) {
                        vars[key] = value !== undefined ? value : '';
                    }
                );

                if(vars){

                    if(vars['product']){

                        var $item =  $('a[data-id="'+ vars['product']+'"]');

                        if($item.length >= 1){

                            // Get top position of item
                            var $itemPosition = $item.offset().top;

                            // Scroll to item
                            $('html, body').animate({
                                scrollTop: $itemPosition - 80
                            }, 2000);
                        }


                    }
                }

            }

        };

        return CategoryScroll;
    }
);
/**
 * Created by andi on 12.01.16.
 * Fixed by christopher on 26.06.17
 */

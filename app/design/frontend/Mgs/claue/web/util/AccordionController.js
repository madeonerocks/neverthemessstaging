define([
        "log",
        "jquery"
    ],
    function (log, $) {
        "use strict";

        var AccordionController = {

            init: function () {
                $(document).on("click",".accordion-button",this.toggle);
            },

            toggle: function (e) {
                log.debug("Details clicked")
                var $el = $(e.currentTarget);
                var $container = $el.parent(".accordion-container");
                var $accordion = $container.find(".accordion");
                $accordion.toggleClass("collapsed");
            }

        };

        return AccordionController;
    }
);
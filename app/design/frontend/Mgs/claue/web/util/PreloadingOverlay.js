define([
        "log",
        "jquery"
    ],
    function (log, $) {
        "use strict";

        var PreloadingOverlay = {

            hide: function () {
                var text_delay = $("#loading_overlay .redirect-body").length > 0 ? 5000 : 0;
                
                setTimeout(function() {
                    $("#loading_overlay img, #loading_overlay .redirect-head, #loading_overlay .redirect-body").animate({opacity: 0},400);

                    $("#loading_overlay").delay(400).animate({
                        opacity: 0
                    }, 400, function () {
                        $(this).css("display", "none");
                    });
                }, text_delay);
            }

        };

        return PreloadingOverlay;
    }
);
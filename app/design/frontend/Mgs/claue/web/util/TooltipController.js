define([
        "log",
        "jquery",
        "modernizr"
    ],
    function (log, $, Modernizr) {
        "use strict";

        var TooltipController = {

            init: function () {

                var self = this;

                $(document).on("mouseover",".no-touch .has-tooltip",function(e){
                    self.show(e);
                });

                $(document).on("mouseout",".no-touch .has-tooltip",function(e){
                    self.hide(e);
                });

                $(document).on("touchend",".touch .has-tooltip",function(e){
                    self.showAndFade(e);
                });

            },

            show: function(e) {

                var $box = $("#tooltip-box");

                var $el = $(e.target);

                if(!$el.hasClass("has-tooltip")){
                    $el = $(e.target).parent(".has-tooltip");
                }

                var text = $el.attr("data-tooltip");

                var xpos = $el.offset().left + Math.floor($el.width()/2);
                var ypos = $el.offset().top;

                $box.height( $(document).height() - $(".footer-container").height() );

                var $container = $("<div>").addClass("tooltip-container");
                var $tooltip = $("<div>").addClass("tooltip").html(text);

                $tooltip.appendTo($container);

                $container.appendTo($box);

                $container.offset({ top: ypos, left: xpos});
                $tooltip.css("left",-Math.floor($container.width()/2));

            },

            showAndFade: function(e){
                $(".tooltip-container").remove();
                this.show(e);
                this.fade();
            },

            hide: function(e) {

                var $el = $(e.target);

                if(!$el.hasClass("has-tooltip")){
                    $el = $(e.target).parent(".has-tooltip");
                }
                $("#tooltip-box .tooltip-container").remove();
            },

            fade: function(){
                var $c = $(".tooltip-container");
                $c.delay(2000).fadeOut(600,function(){
                    $c.remove();
                });
            }

        };

        return TooltipController;
    }
);
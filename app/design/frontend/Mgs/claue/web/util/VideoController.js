define([
        "log",
        "jquery"
    ],
    function (log, $) {
        "use strict";

        var VideoController = {

            init: function () {

                $('.video-button').on("click", this.playVideo);
                $('.video-control-button.button-close').on("click", this.stopVideo);
                $('.video-control-button.button-mute').on("click", this.toggleMute);
                $('.video-container .intro-video video').on("ended", function(){
                    VideoController.endVideo();
                });

            },

            playVideo: function (e) {

                var $button = $(e.target);

                var $introVideo = $(".video-container .intro-video");
                var $video = $introVideo.find("video");
                var $overlay = $(".video-container .intro-overlay");

                $introVideo.removeClass("hidden");
                $overlay.addClass("hidden");

                $video.get(0).play();

            },

            endVideo: function () {
                var $introVideo = $(".video-container .intro-video");
                var $overlay = $(".video-container .intro-overlay");
                $introVideo.addClass("hidden");
                $overlay.removeClass("hidden");
                var $video = $introVideo.find("video");
                $video.get(0).currentTime = 0;
            },

            stopVideo: function (e) {

                var $button = $(".video-control-button.button-close");

                var $introVideo = $(".video-container .intro-video");
                var $video = $introVideo.find("video");
                var $overlay = $(".video-container .intro-overlay");

                $introVideo.addClass("hidden");
                $overlay.removeClass("hidden");

                log.debug($video);

                $video.get(0).pause();
                $video.get(0).currentTime = 0;

            },

            toggleMute: function (e) {

                var $button = $(".video-control-button.button-mute");

                log.debug($button);

                var $video = $(".intro-video video");

                if ($button.hasClass("active")) {

                    $button.removeClass("active");
                    $video.prop('muted', false);

                } else {

                    $button.addClass("active");
                    $video.prop('muted', true);

                }


            }

        };


        return VideoController;
    }
);
define([
        "log",
        "jquery",
        "smoothScroll"
    ],
    function (log, $, SmoothScroll) {
        "use strict";

        var SmoothAnchor = {

            scrollTimer: null,

            init: function(){

                // Remove the # from the hash, as different browsers may or may not include it
                var hash = location.hash.replace('#','');

                if(hash != ''){

                    setTimeout(function() {

                        window.scrollTo(0, 0);
                    }, 1);

                    var $el = $("#"+hash);

                    if($el.length){
                        // Clear the hash in the URL
                        // location.hash = '';   // delete front "//" if you want to change the address bar

                        clearTimeout(this.scrollTimer);

                        this.scrollTimer = setTimeout(function(){
                            log.debug("SmoothAnchor", $el.first().offset().top);
                            SmoothScroll.scrollTo($el.first().offset().top,function(){},2500);
                        },200);

                    }

                }

            }

        };

        return SmoothAnchor;
    }
);

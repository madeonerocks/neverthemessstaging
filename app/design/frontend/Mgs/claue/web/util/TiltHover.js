define([
        "log",
        "jquery",
        "TweenMax",
        "TimelineMax"
    ],
    function (log, $, TweenMax, TimelineMax) {
        "use strict";

        var TiltHover = {

            init: function () {
                $(".tilt-hover").on('mousemove', this.hover);
                $(".tilt-hover").on('mouseleave', this.leave);
            },

            hover: function (e) {
                var $el = $(e.target).parents(".tilt-hover");
                var $front = $el.find(".tilt-hover-front-layer");
                var $back = $el.find(".tilt-hover-back-layer");

                var mousepos = TiltHover.getMousePos(e),
                    docScrolls = {left : document.body.scrollLeft + document.documentElement.scrollLeft, top : document.body.scrollTop + document.documentElement.scrollTop},
                    bounds = $el[0].getBoundingClientRect(),
                    relmousepos = { x : mousepos.x - bounds.left - docScrolls.left, y : mousepos.y - bounds.top - docScrolls.top };

                var xFactor = - TiltHover.calculateTransformFactor($el.innerWidth(), relmousepos.x);
                var yFactor = TiltHover.calculateTransformFactor($el.innerHeight(), relmousepos.y);
                TweenMax.killTweensOf($el);
                TweenMax.to($el, 0, { rotationX:yFactor*7,rotationY:xFactor*7});
                TweenMax.killTweensOf($front);
                TweenMax.to($front, 0, { x:xFactor*7,y:yFactor*-7});
                TweenMax.killTweensOf($back);
                TweenMax.to($back, 0, { x:xFactor*-7,y:yFactor*7});
            },

            leave: function (e) {
                var $el = $(e.target).parents(".tilt-hover");
                var $front = $el.find(".tilt-hover-front-layer");
                var $back = $el.find(".tilt-hover-back-layer");
                TweenMax.to($el, 1, { rotationX:0,rotationY:0});
                TweenMax.to($front, 1, { x:0,y:0});
                TweenMax.to($back, 1, { rotationX:0,rotationY:0,x:0,y:0});
            },

            getMousePos: function (e) {
                var posx = 0, posy = 0;
                if (!e) var e = window.event;
                if (e.pageX || e.pageY) {
                    posx = e.pageX;
                    posy = e.pageY;
                }
                else if (e.clientX || e.clientY) {
                    posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                    posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
                }
                return {x: posx, y: posy}
            },

            calculateTransformFactor: function (total, offset) {
                var mid = total / 2;
                var factor = 2 * ( offset - mid ) / total;
                return factor;
            },

        };

        return TiltHover;
    }
);
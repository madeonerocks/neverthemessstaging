define([
        "log",
        "jquery"
    ],
    function (log, $) {
        "use strict";

        var SmoothScroll = {

            pixelPerSecond: 2500,

            init: function () {

                $('.jumpButton').click(function (e) {
                    e.preventDefault();

                    var label = $(this).data('jump-target');
                    var target = $(label);

                    if (target.length) {

                        SmoothScroll.scrollTo(target.offset().top);

                        return false;
                    }
                });

            },

            // y = position, f = callback function, pps = pixel per second
            scrollTo: function(y, f, pps) {

                y = Math.round(y);

                pps = typeof  pps !== 'undefined' ?  pps : this.pixelPerSecond;

                var scrollPos = $(window).scrollTop();

                var scrollTime = Math.abs( 1000 * ( scrollPos - y ) / pps );

                if($('.desktopNavbar').is(":visible")){
                    y -= $('.desktopNavbar').outerHeight();
                }

                var $el = $("html");

                var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
                // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
                var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
                var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
                // At least Safari 3+: "[object HTMLElementConstructor]"
                var isChrome = !!window.chrome && !isOpera;              // Chrome 1+
                var isIE = /*@cc_on!@*/false || !!document.documentMode; // At least IE6

                if( isChrome || isSafari ) $el = $("body");

                y -= 50;

                $el.animate({
                    scrollTop: y
                }, scrollTime);

                //$("body").animate({scrollTop: 1000}, 1000);

            }
        };

        return SmoothScroll;
    }
);

define([
        "log",
        "jquery",
        "TweenMax",
        "TimelineMax",
        "colorUtil"
    ],
    function (log, $, TweenMax, TimelineMax, ColorUtil) {
        "use strict";

        var Button = {

            time: 0.5,

            init: function () {

                var self = this;

                var $buttons = $(".button").not('.no-hover');

                $buttons.on("mouseover touchstart", function () {
                    self.showHover($(this));
                });

                $buttons.on("mouseleave touchend", function () {
                    self.hideHover($(this));
                });

            },

            showHover: function ($btn) {

                var $hoverbox = $("<div>").addClass("hover-box");
                if ($btn.find(".hover-box").length > 0) {
                    $hoverbox = $btn.find(".hover-box");
                } else {
                    $btn.prepend($hoverbox);
                }

                var opacity = $btn.attr("data-lum")/8 + 0.12;
                $hoverbox.css({opacity:opacity});

                TweenMax.killTweensOf($hoverbox);

                var self = this;

                var timeline = new TimelineMax();
                if ($hoverbox.position().left <= -$hoverbox.width() / 2) {
                    timeline.to($hoverbox, self.time / 2, {
                        x: "-50%",
                        skewX: "30deg",
                        scaleY: 1.2,
                        ease: Power2.easeIn
                    });
                }
                timeline.to($hoverbox, self.time / 2, {x: "0%", skewX: "0deg", scaleY: 1, ease: Power2.easeOut});
                timeline.play();

            },

            hideHover: function ($btn) {

                var $hoverbox = $btn.find(".hover-box");

                var self = this;

                TweenMax.killTweensOf($hoverbox);
                //TweenMax.to($hoverbox, self.time, { x:"-100%", skewX:"0deg", scaleY:1, ease: Power2.easeOut});

                var timeline = new TimelineMax();
                if ($hoverbox.position().left > -$hoverbox.width() / 2) {
                    timeline.to($hoverbox, self.time / 2, {
                        x: "-50%",
                        skewX: "-30deg",
                        scaleY: 1.2,
                        ease: Power2.easeIn
                    });
                }
                timeline.to($hoverbox, self.time / 2, {x: "-100%", skewX: "0deg", scaleY: 1, ease: Power2.easeOut});
                timeline.play();

            }

        };

        return Button;
    }
);

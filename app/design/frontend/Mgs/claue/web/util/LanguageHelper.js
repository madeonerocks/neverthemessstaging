/**
 * Created by andi on 14.03.16.
 */
define([
        "log",
        "jquery"
    ],
    function (log, $) {
        "use strict";

        var LanguageHelper = {

            elementsToTranslate: [
                 "#accessories_navigation"
            ],

            init: function () {

                $.each(LanguageHelper.elementsToTranslate, function(index, el) {
                    LanguageHelper.check(el);
                });

            },

            check: function (el) {

                if(document.documentElement.lang == "en") {
                    $(el).addClass("en");
                }
                if(document.documentElement.lang == "de") {
                    $(el).addClass("de");
                }

            }

        };


        return LanguageHelper;
    }
);
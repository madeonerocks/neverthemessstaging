define([
		"jquery",
		"log",
		"json!https://cdn.helmade.com/configurator/color-config.txt"
	],
	function ($, log, configJSON) {
		"use strict";

		/**
		 * Converts a CSS hex colour string into an integer
		 * @method colorToNumber
		 * @param {string} colorString
		 * @return {number} An integer representing the CSS hex string
		 */
		function colorToNumber(colorString) {
			colorString = colorString.replace('#','');

			if (colorString.length == 3) {
				colorString = colorString.charAt(0) + colorString.charAt(0)
					+ colorString.charAt(1) + colorString.charAt(1)
					+ colorString.charAt(2) + colorString.charAt(2);
			}

			return parseInt(colorString, 16);
		}

		function getForegroundForBackground( backgroundHex ) {

			var grey = "#383838";
			var white = "#ffffff";
			var luminance = getLuminance( backgroundHex );

			if( luminance > 0.7 ){
				return grey;
			} else {
				return white;
			}
		}

		function getForegroundForBackgroundName( backgroundHex ) {

			var luminance = getLuminance( backgroundHex );

			if( luminance > 0.7 ){
				return "grey";
			} else {
				return "white";
			}
		}

		function skuToHex( sku ) {
			var hex = "#ffffff";

			$.each(configJSON.colors, function (key, value) {
				if (key == sku) hex = value.hex;
			});

			return hex;
		}

        function getType( sku ) {
            var type = "normal";

            $.each(configJSON.colors, function (key, value) {
                if (key == sku)
                {
                    type = value.type;
                }
            });

            if (type == "" || type == undefined)
                type = "normal";

            return type;
        }

		function getLuminance( hex ) {
			var rgb = hexToRGB( hex );
			var luminance = (rgb.r*0.299 + rgb.g*0.587 + rgb.b*0.114) / 256;
			return luminance;
		}

		function hexToRGB( hex ){

			hex = hex.replace('#','');

			var r = parseInt( hex.substr(0,2), 16 );
			var g = parseInt( hex.substr(2,2), 16 );
			var b = parseInt( hex.substr(4,2), 16 );

			return {r: r, g: g, b: b};

		}

		function rgbToHEX( rgb ){

			function componentToHex(c) {
				var hex = c.toString(16);
				return hex.length == 1 ? "0" + hex : hex;
			}

			return "#" + componentToHex(rgb.r) + componentToHex(rgb.g) + componentToHex(rgb.b);
		}

		function levelColor( hex ){

			var luminance = getLuminance( hex );

			var rgb = hexToRGB( hex );

			var levelFactor = 1;
			var whiteFactor = 1;

			if(luminance > 0.8) {
				levelFactor = 0.9;
			}

			function level( colorValue ) {
				var newValue = Math.floor( ( colorValue * levelFactor + 255 * whiteFactor ) - 255 );
				if (newValue > 255) {
					newValue = 255;
				}
				return newValue;
			}

			rgb.r = level(rgb.r);
			rgb.g = level(rgb.g);
			rgb.b = level(rgb.b);

			var newHex = rgbToHEX(rgb);

			return(newHex);

		}

		function lightenColor(hex, amount) {

			var usePound = false;

			if (hex[0] == "#") {
				hex = hex.slice(1);
				usePound = true;
			}

			var rgb = hexToRGB( hex );

			function lighten( color ){
				color += Math.floor( amount * 255 );
				if( color > 255 ) color = 255;
				return color;
			}

			rgb.r = lighten(rgb.r);
			rgb.g = lighten(rgb.g);
			rgb.b = lighten(rgb.b);

			return this.rgbToHEX(rgb);

		}

		var ColorUtil = function(){

			return {
				colorToNumber: colorToNumber,
				getForegroundForBackground: getForegroundForBackground,
				getForegroundForBackgroundName: getForegroundForBackgroundName,
				levelColor: levelColor,
				getLuminance: getLuminance,
				hexToRGB: hexToRGB,
				rgbToHEX: rgbToHEX,
				skuToHex: skuToHex,
				lightenColor: lightenColor,
                getType: getType
			};
		};

		return ColorUtil();
	}
);
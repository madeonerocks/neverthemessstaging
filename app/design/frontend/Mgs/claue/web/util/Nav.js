// TUDOCK: HELM-24 Start
define([
        "log",
        "jquery",
        "responsiveHelper"
    ],
    function (log, jq, ResponsiveHelper) {
        "use strict";

        var Nav = {
            /**
             * value for nav controlling
             */
            open: false,
            /**
             * Nav initialization
             */
            init: function () {
                Nav.createEvents();
                Nav.updateMenuBg();
                jq(window).resize(function () {
                    var viewport = document.viewport.getDimensions();
                    if (Nav.open === true) {
                        Nav.hideMenu();
                    }
                });
            },
            /**
             * Handles the Nav toggle
             * @param e
             */
            toggle: function (e) {
                //log.debug("Event Toggle: ", e);
                if(window.openedNavId !== undefined) {
                    if(window.openedNavId !== e.currentTarget.dataset.subnavid) {
                        Nav.jumpToOverlay(window.openedNavId, e);
                    }
                    else {
                        Nav.manager(e);
                    }
                }
                else {
                    Nav.manager(e);
                }
            },

            /**
             * handle the show and hide overlays
             * @param e
             */
            manager: function(e) {
                var currentNavId = e.currentTarget.dataset.subnavid;
                if(Nav.open) {
                    Nav.removeMenuHighLight(e);
                    Nav.hideOverlay(currentNavId, e);
                    window.openedNavId = undefined;
                    document.body.style.overflow = 'visible';

                }
                else{
                    Nav.addMenuHighLight(e);
                    Nav.showOverlay(currentNavId, e);
                    window.openedNavId = currentNavId;
                    document.body.style.overflow = 'hidden';
                }
            },
            /**
             * Jump to new overlay if one is already open
             * @param currentNavId
             * @param e
             */
            jumpToOverlay: function (currentNavId, e) {
                Nav.removeMenuHighLight(e);
                Nav.addMenuHighLight(e);
                var el = jq('#'+currentNavId);
                var navHeight = jq(window).height() + 1;
                el.innerHeight(navHeight);
                el.slideUp(250,'swing', function () {
                    Nav.open = false;
                    Nav.manager(e)
                });
            },
            /**
             * Show the overlay
             * @param currentNavId
             * @param e
             */
            showOverlay: function (currentNavId, e) {
                currentNavId = jq('#'+currentNavId);
                Nav.slideDownAnimation(currentNavId);
                Nav.open = true;
            },

            /**
             * hides the overlay
             * @param currentNavId
             * @param e
             */
            hideOverlay: function(currentNavId, e) {
                currentNavId = jq('#'+currentNavId);
                Nav.slideUpAnimation(currentNavId);
                Nav.open = false;
            },

            /**
             * Remove active class from all menu items
             */
            removeMenuHighLight: function() {
                var mainNavList = $$('ol.nav-primary span[id]');
                if(mainNavList.length >= 1) {
                    mainNavList.each(function(element){
                        if(element.hasClassName('active')) {
                            element.removeClassName('active')
                        }
                    });
                }
            },

            /**
             * Add active class to selected menu
             * @param e
             */
            addMenuHighLight: function(e) {
                var targetId = e.target.parentElement.parentElement.id
                    ? e.target.parentElement.parentElement.id
                    : e.target.parentElement.parentElement.down().id;
                var mainNavList = $$('ol.nav-primary span[id]');
                if(targetId !== '' && mainNavList.length >= 1) {
                    mainNavList.each(function(element){
                        if(element.id === targetId) {
                            element.addClassName('active')
                        }
                    });
                }
            },

            /**
             * Hide menu method
             */
            hideMenu: function() {
                Nav.hideOverlay(window.openedNavId);
            },

            /**
             * Trigger the slide down animation for the overlay
             * @param currentNavId
             */
            slideDownAnimation: function(currentNavId) {
                var el = jq(currentNavId);
                var navHeight = jq(window).height() + 1;
                el.innerHeight(navHeight);
                jq.when(Nav.addHeaderBackground())
                    .done(el.slideDown(260, 'swing'));
            },

            /**
             * Trigger the slide up animation for the overlay
             * @param currentNavId
             */
            slideUpAnimation: function(currentNavId) {
                var el = jq(currentNavId);
                var navHeight = jq(window).height() + 1;
                el.innerHeight(navHeight);
                el.slideUp(260, 'swing', function(){
                    Nav.removeHeaderBackground();
                });
            },

            /**
             * Create the events at the start
             */
            createEvents: function () {
                jq(document).on("click", ".nav-opener", Nav.toggle);
                //log.debug("EVENT CREATE");
            },

            /**
             * adds the background from header
             */
            addHeaderBackground : function () {
                var $header = jq("#header");
                $header.addClass("helmade-nav-active");
            },

            /**
             * removes the background from header
             */
            removeHeaderBackground: function () {
                var $header = jq("#header");
                $header.removeClass("helmade-nav-active");
            },

            /**
             * Old method, it handles the images hover on the overlay
             */
            updateMenuBg: function () {
                jq('.menu-hover-img-wrapper').each( function( index, element ){
                    var menuItemImgPathHv = jq(this).find('img').attr('src');
                    var menuItemImgPath = menuItemImgPathHv.replace('-hover', '');
                    jq(this).css('background-image', 'url(' + menuItemImgPath + ')');
                });
            }
        };
        return Nav;
    }
);
// TUDOCK: HELM-24 End
/**
 * Created by andi on 12.01.16.
 */
define([
        "log",
        "jquery",
        "colorUtil"
    ],
    function (log, $, ColorUtil) {
        "use strict";

        var FeaturedSliderController = {

            resizeTimer: null,

            init: function () {
                FeaturedSliderController.setHeight();
                $(window).on("resize", FeaturedSliderController.setHeight );

                var $fcs = $(".featured_container");
                var self = this;
                $fcs.each(function () {
                    var colorsSku = $(this).data("colors").split(",");
                    var colorsHex = [];
                    $.each(colorsSku, function (index, sku) {
                        var hex = ColorUtil.skuToHex(sku);
                        colorsHex.push(ColorUtil.levelColor( hex ));
                    });
                    if (colorsHex.length > 0) {
                        self.setColor($(this), colorsHex);
                    }
                });
            },

            setColor: function ($fc, colorsHex) {
                var $color = $fc;
                var $info = $fc.find(".featured_info");

                var basisColorHex = colorsHex[0];
                var fontColorHex = ColorUtil.getForegroundForBackground( basisColorHex );

                $color.css("background-color", basisColorHex);

                $info.css("color", fontColorHex);




            },


            setHeight: function() {

                var height = 0;

                var $slider_info = $("#featured_slider .slick-slide .featured_info");

                $slider_info.css("height","auto");

                clearTimeout(this.resizeTimer);

                this.resizeTimer = window.setTimeout(function(){

                    $slider_info.each(function(){

                        if($(this).height() > height) height = $(this).height();

                    });

                    $slider_info.height(height);

                },100);


            }

        };

        return FeaturedSliderController;
    }
);
define([
        "log",
        "jquery",
        "colorUtil"
    ],
    function (log, $, ColorUtil) {
        "use strict";

        var GradientBoxController = {

            init: function () {
                // $gcs = radient containters
                var $gcs = $('.gradient-container');
                var self = this;
                $gcs.each(function () {
                    var colorsSku = $(this).data("colors").split(",");
                    var colorsHex = [];
                    $.each(colorsSku, function (index, sku) {
                        var hex = ColorUtil.skuToHex(sku);
                        colorsHex.push(ColorUtil.levelColor(hex));
                    });
                    if (colorsHex.length > 0) {
                        self.setLocalColor($(this), colorsHex[0]);
                        self.setColors($(this), colorsHex);
                    }
                });
            },

            setLocalColor: function ($gc, hex) {
                var $item = $gc.parent(".item");
                var $borders = $item.find(".local-color.color-border");
                var borderHex = hex;
                var luminance = ColorUtil.getLuminance(borderHex);

                $borders.each(function () {

                    if (luminance < 0.4) {
                        borderHex = ColorUtil.lightenColor(borderHex, 0.5);
                    }

                    $(this).attr("style", "border-color:" + borderHex);

                });
                var $backgrounds = $item.find(".local-color.color-background");
                $backgrounds.each(function () {
                    $(this).attr("style", "background-color:" + hex).attr("data-lum",luminance);
                });

            },

            setColors: function ($gc, colorsHex) {
                var $cStart = $gc.find(".color-start");
                var $cContainer = $gc.find(".color-container");
                var $cEnd = $gc.find(".color-end");

                var colorStart = colorsHex[0];
                var colorStartRGBObject = ColorUtil.hexToRGB(colorStart);
                var colorStartRGB = colorStartRGBObject.r + "," + colorStartRGBObject.g + "," + colorStartRGBObject.b;

                var colorEnd = "";

                if (colorsHex.length > 1) {
                    colorEnd = colorsHex[colorsHex.length - 1];
                    var colorEndRGBObject = ColorUtil.hexToRGB(colorEnd);
                    var colorEndRGB = colorEndRGBObject.r + "," + colorEndRGBObject.g + "," + colorEndRGBObject.b;
                }

                if (colorsHex.length > 2) {
                    var colorMid = colorsHex[1];
                }

                $cStart.attr("style", "background: linear-gradient(0deg, rgba(" + colorStartRGB + ",1) 0%, rgba(" + colorStartRGB + ",0) 85%);");

                if (colorMid) {
                    $cContainer.attr("style", "background: linear-gradient(180deg, " + colorStart + " 0%," + colorMid + " 50%," + colorEnd + " 100%);");
                } else {
                    $cContainer.attr("style", "background: linear-gradient(180deg, " + colorStart + " 0%," + colorEnd + " 100%);");
                }

                $cEnd.attr("style", "background: linear-gradient(180deg, rgba(" + colorEndRGB + ",1) 0%, rgba(" + colorEndRGB + ",0) 85%);");
            }

        };

        return GradientBoxController;
    }
);
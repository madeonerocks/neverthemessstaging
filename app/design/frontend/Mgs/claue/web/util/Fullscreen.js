define([
        "log",
        "jquery",
        "responsiveHelper"
    ],
    function (log, $, ResponsiveHelper) {
        "use strict";

        var Fullscreen = {

            init: function () {

                this.resize();

                if(!ResponsiveHelper.check("xs")) {
                    $(window).on("resize", this.resize);
                }

            },

            resize: function () {

                var $video = $('.fullscreen-container').find("video");


                if ($video.length > 0) {

                    var advantageHeight = 40;

                    if($('.fullscreen-container').hasClass('homepage-intro')) {
                        advantageHeight = -40;
                    }

                    if(ResponsiveHelper.check("m") || ResponsiveHelper.check("xs") || ResponsiveHelper.check("s")){
                        advantageHeight = 0;
                    }

                    var wWidth = $(window).innerWidth();
                    var wHeight = $(window).innerHeight() - $("ul.messages").first().height() + advantageHeight;

                    $('.fullscreen-container').css("height", wHeight + "px");

                    if (( wWidth / wHeight ) <= ( 16 / 9 )) {
                        $video.height("100%");
                        $video.width("auto");
                    } else {
                        $video.height("auto");
                        $video.width("100%");
                    }

                    $video.css("margin-left", -Math.round($video.width() / 2) + "px");
                    $video.css("margin-top", -Math.round($video.height() / 2) + "px");

                    $video.removeClass("preDocReady");

                    var $overlay = $(".intro-overlay");
                    $overlay.css("margin-top", -Math.round($overlay.height() / 2) + "px");

                }

                // Check if image exist for video
                if($video.length < 1){

                    var $image = $('.fullscreen-container .intro-loop.image');

                    if($image.length > 0){

                        var advantageHeight = 40;

                        if($('.fullscreen-container').hasClass('homepage-intro')) {
                            advantageHeight = -40;
                        }

                        if(ResponsiveHelper.check("m") || ResponsiveHelper.check("xs") || ResponsiveHelper.check("s")){
                            advantageHeight = 0;
                        }

                        var wWidth = $(window).innerWidth();
                        var wHeight = $(window).innerHeight() - $("ul.messages").first().height() + advantageHeight;

                        $('.fullscreen-container').css("height", wHeight + "px");


                    }


                }

            }

        };


        return Fullscreen;
    }
);
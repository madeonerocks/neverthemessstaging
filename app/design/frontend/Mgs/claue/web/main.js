perfNow("main");

require.config({
    paths: {
        underscore: "lib/underscore",
        backbone: "lib/backbone",
        log: "lib/loglevel",
        hbs: "lib/hbs",
        text: "lib/text",
        json: "lib/json",
        modernizr: "lib/modernizr.custom.min",
        slick: "lib/slick",
        touchSwipe: "lib/jquery.touchSwipe",
        colorUtil: "util/ColorUtil",
        smoothScroll: "util/SmoothScroll",
        smoothAnchor: "util/SmoothAnchor",
        responsiveHelper: "util/ResponsiveHelper",
        languageHelper: "util/LanguageHelper",
        colorController: "util/ColorController",
        videoController: "util/VideoController",
        optionSlider: "util/OptionSlider",
        ScrollMagic: 'lib/ScrollMagic',
        TweenMax: 'lib/greensock/TweenMax',
        TimelineMax: 'lib/greensock/TimelineMax.min',
        hr: "/glview/js/hr",
        vendor: "/glview/js/vendor",
        cdn: "core/CDN",
        app: "/glview/js/app"
    },

    map: {
        "*": {
            "jquery": "jquery"
        },
        "jquery-private": {
            "jquery": "jquery"
        },
    },

    shim: {
        backbone: {
            deps: ["underscore", "jquery"],
            exports: function () {
                return Backbone.noConflict();
            }
        },

        underscore: {
            exports: "_"
        }
    },

    waitSeconds: 30,
});

require([
    "underscore",
    "jquery",
    "RegisterComponentsCommand",
    "configurator/view/ConfiguratorApp",
    "core/view/ComponentFactory",
    "util/Burger",
    "util/TooltipController",
    "util/GradientBoxController",
    "util/FeaturedSliderController",
    "util/AccordionController",
    "util/Fullscreen",
    "util/ResponsiveImageHelper",
    "util/scroll/VideoScroll",
    "util/ProductGallery",
    "util/scroll/MainScroll",
    "util/scroll/SingletonScroll",
    "util/scroll/AdditionalScroll",
    "util/Slider",
    "util/Button",
    "util/DesignLineSlider",
    "colorController",
    "videoController",
    "util/scroll/CategoryScroll",
    "util/Nav",
    "util/Logout",
    "util/BackLink",
    "util/PreloadingOverlay",
    "util/SizeSelector",
    "util/TiltHover",
    "util/Filter",
    "languageHelper",
    "smoothAnchor",
    "smoothScroll",
    "log"
], function (_, $, RegisterComponentsCommand, ConfiguratorApp, ComponentFactory, Burger, TooltipController, GradientBoxController,
             FeaturedSliderController, AccordionController, Fullscreen, ResponsiveImageHelper, VideoScroll, ProductGallery, MainScroll,
             SingletonScroll, AdditionalScroll, Slider, Button, DesignLineSlider, ColorController, VideoController, CategoryScroll, Nav, Logout,
             BackLink, PreloadingOverlay, SizeSelector, TiltHover, Filter, LanguageHelper, SmoothAnchor, SmoothScroll, log) {
				 
				 
    // enable logging
    try {
        log.enableAll();
    } catch (error) {
        if (console && console.log) {
            console.log("error: could not enable loglevel", error);
        }
    }



    perfNow("main require complete");

    // register all valid components
    RegisterComponentsCommand.execute();

    $(document).ready(function () {

        perfNow("dom ready");

        // TweenMax and TweenLite are written directly into the page via normal script tags
        // we need to set the tweenlite selector to internal jquery to prevent errors with prototype.js
        TweenLite.selector = $;

        SmoothAnchor.init();

        Fullscreen.init();
        ColorController.init();
        Burger.init();
        SmoothScroll.init();
        Button.init();
        ResponsiveImageHelper.init();
        AdditionalScroll.init();

        ComponentFactory.createComponent("main-application", $("body"));

        MainScroll.init();
        VideoScroll.init();
        SingletonScroll.init();
        TooltipController.init();
        GradientBoxController.init();
        AccordionController.init();
        VideoController.init();
        ProductGallery.init();
        Slider.init();
        DesignLineSlider.init();
        CategoryScroll.init();
        FeaturedSliderController.init();
        Nav.init();
        BackLink.init();
        Logout.init();
        SizeSelector.init();
        Filter.init();
        //TiltHover.init();

        LanguageHelper.init();

        if (ConfiguratorApp.isActive)
        {
            window.onConfiguratorComplete = function()
            {
                PreloadingOverlay.hide();
                perfNow("configurator ready");
            }
        }
        else
        {
            PreloadingOverlay.hide();
        }

        perfNow("dom ready complete");

    });

    // Refresh Gradient after Ajax Page Load
    ias.on('rendered', function(data, items) {
        GradientBoxController.init();
        Filter.init();
    });
});
define([
        "backbone",
        "log"
    ],
    function (Backbone, log) {
        "use strict";

        var PanelEvent = {
            TOGGLE_FITTING_GUIDE: "PanelEvent.TOGGLE_FITTING_GUIDE"
        }

        return PanelEvent;
    }
);
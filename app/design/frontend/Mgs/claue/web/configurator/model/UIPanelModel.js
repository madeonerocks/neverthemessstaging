define([
        "backbone"
    ],
    function (Backbone) {
        "use strict";

        var UIPanelModel = Backbone.Model.extend({
            defaults: {
                panelType: "",
                panelOptions: null,
                interest: ""
            },

            getLabel:function()
            {
                return this.get("panelOptions").first().get("label");
            }
        });

        return UIPanelModel;
    }
);
define([
        "backbone",
        "log"
    ],
    function (Backbone, log) {
        "use strict";

        var ProductOptionsCollection = Backbone.Collection.extend({
            comparator: "sort_order",

            getOptionById: function (id) {
                return this.findWhere({id: id});
            }
        });

        return ProductOptionsCollection;
    }
);
define([
        "backbone",
        "log"
    ],
    function (Backbone, log) {
        "use strict";

        var TranslationModel = Backbone.Model.extend({
            defaults: {
                labels: null
            },

            initialize: function (options) {

            },

            getLabel: function (key) {
                if (this.get("labels") === null) {
                    if ("translations" in window) {
                        this.set("labels", window.translations);
                    } else {
                        log.error("translation object NOT available!");
                    }
                }
                var l = this.get("labels")[key];
                if (l) return l;
                return "#" + key;
            }
        });

        /**
         * We are returning the translationmodel as a singleton here
         */
        var inst = new TranslationModel();
        return inst;
    }
);
define([
        "backbone",
        "cdn"
    ],
    function (Backbone, CDN) {
        "use strict";

        var ProductDetailsModel = Backbone.Model.extend({
            defaults: {
                name: "",
                sku: "",
                base_modell: "",
                design_line: "",
                customTemplate: "",
                manufacturer: "",
                price: "",
                manufacturer_img_src: "",
                is_toggle: "",
                custom_logo: "",
            },

            initialize: function (options) {

                var imgSource = CDN.path + "configurator/manufacturer/" + options.manufacturer.toLowerCase() + ".png";
                this.set("manufacturer_img_src", imgSource);

                jQuery("#manufacturer_img_src").attr("src",imgSource)

            }
        });

        return ProductDetailsModel;
    }
);
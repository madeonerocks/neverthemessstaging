define([
        "backbone",
        "log"
    ],
    function (Backbone, log) {
        "use strict";

        var ProductOptionModel = Backbone.Model.extend({
            defaults: {
                id: 0,
                screen: null,
                config: null,
                sort_order: 0,
                is_require: 0,
                label: "",
                options: null,
                value: "",
                sku_value: null,
                max_characters: 0
            },

            initialize: function (options) {
                var r = {};
                var d = options.design;
                if (d != null && d.indexOf("{") == 0) {
                    //log.warn(d);
                    r = JSON.parse(d);
                }
                this.set("config", r);
                this.unset("design", {silent: true});
            }
        });

        return ProductOptionModel;
    }
);
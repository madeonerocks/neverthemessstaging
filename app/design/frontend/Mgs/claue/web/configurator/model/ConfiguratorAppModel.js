define([
        "backbone",
        "jquery",
        "log",
        "colorUtil",
        "configurator/model/ProductDetailsModel",
        "configurator/model/ProductOptionsCollection",
        "configurator/model/ProductOptionModel",
        "configurator/model/UIPanelModel",
        "json!https://cdn.helmade.com/configurator/color-config.txt",
        "cdn"
    ],
    function (Backbone, $, log, ColorUtil, ProductDetailsModel, ProductOptionsCollection, ProductOptionModel, UIPanelModel, configJSON, CDN) {
        "use strict";

        var ConfiguratorAppModel = Backbone.Model.extend({

            defaults: {
                productDetails: null,
                productOptionsCollection: null,
                colorPalettesCollection: null,
                uiPanelsCollection: null,
                colors: null,
                totalPrice: 0,
                interest: "",
                skuMap: {},
                translation: null,
                glossyOnly: false
            },

            constructor: function () {
                Backbone.Model.apply(this, arguments);
            },

            initialize: function (attributes, options) {
                Backbone.Model.prototype.initialize.apply(this, arguments);

                this._initProductDetails();
                this._initColorPalettes();
                this._initProductOptions();
                this._initUIPanels();

                this.set("interest", this.get("uiPanelsCollection").first().get("interest"));
                this.get("productOptionsCollection").on("change", this._updateCalculatedPrice, this);

                this.save();
            },

            getSignatureColors: function () {
                var signatureColors = new Backbone.Collection();

                var baseModel = this.get("productDetails").get("base_modell");

                _.each(configJSON.config.signature_color_defaults[baseModel], function (sku) {
                    var id = this.getColorById(this.getIdBySku(sku));
                    signatureColors.add(id);
                }, this);


                //add layer colors for arai
                var colorOptions = new Backbone.Collection(this.get("productOptionsCollection").where({screen: "color"}));
                var values = colorOptions.pluck("value");
                _.each(values, function (colorId) {

                    var color = this.getColorById(colorId);

                    if (color != null)
                    {
                        var type = ColorUtil.getType(color.get("sku"));

                        if (type == "normal" || type == "metallic" || type == "neon")
                            signatureColors.add(this.getColorById(colorId));
                    }
                }, this);


                return signatureColors;
            },

            getColorById: function (colorId) {
                if (colorId > 0) {
                    var clr = this.get("colors").findWhere({id: colorId});
                    if (clr instanceof Backbone.Model) {
                        return clr;
                    }
                }
                return null;
            },

            getSkuByProductOptionModel: function (optionModel) {
                var f = _.findWhere(optionModel.get("options"), {id: optionModel.get("value")});
                if (f !== undefined) {
                    return f.sku;
                }

                return 0;
            },

            getFilteredColorPalettes: function (config) {
                var c = this.get("colorPalettesCollection");
                if (config.exclude !== undefined && config.exclude.length > 0) {
                    return c.filter(function (colorPalette) {
                        var m;
                        if (_.isArray(config.exclude)) {
                            m = _.contains(config.exclude, colorPalette.get("config").type);
                        } else if (_.isString(config.exclude)) {
                            m = config.exclude == colorPalette.get("config").type;
                        }
                        return !m;
                    });
                }
                return c.models;
            },

            getSkuById: function (id) {
                return this.get("skuMap")[id];
            },

            getIdBySku: function (sku) {
                var k = -1;

                _.find(this.get("skuMap"), function (item, key) {
                    if (sku == item) {
                        k = key;
                        return true;
                    }
                    return false;
                });

                return k;
            },

            fixAraiLabelColor: function() {
                //check if signature color is still within acceptable colors
                //if not so, choose black

                var colors = this.getSignatureColors();
                var colorOption = this.get("productOptionsCollection").findWhere({screen: "signature_color"});
                var signature_color = colorOption.get("value");

                var colorIsValid = false;
                var firstColor = null;
                colors.each(function (value, index, list) {
                    if (value.get("id") == signature_color)
                    {
                        colorIsValid = true;
                    }
                    if (firstColor == null)
                    {
                        firstColor = value.get("id");
                    }
                });

                if(!colorIsValid)
                {
                    colorOption.set("value", firstColor);
                }
            },

            /**
             * determines if we have a configuration that can only be glossy. This depends
             * on certain color palettes being used by the user when configuring the helmet.
             * This dependency can be set in config.json.
             */
            updateGlossyOnly: function () {
                // get all product options that are colors
                var colorOptions = new Backbone.Collection(this.get("productOptionsCollection").filter(function (item) {
                    return item.get("screen") == "color";
                }));

                // pluck only the color values
                var colorValues = colorOptions.pluck("value");

                // get actual color models by colorValues
                var colors = this.get("colors").filter(function (item) {
                    return _.contains(colorValues, item.get("id"));
                });

                // check for match against our configJSON.config.glossy_only
                var hasGlossyOnly = _.some(colors, function (item) {
                    return _.contains(configJSON.config.glossy_only, item.get("palette_model").get("config").type);
                });

                this.set("glossyOnly", hasGlossyOnly);
            },

            /**
             * We don't write to the server in this case, we just write data
             * in to hidden fields within the DOM of the page. Magento will
             * do the rest for us.
             */
            save: function () {
                var collection = this.get("productOptionsCollection");
                collection.each(function (element, index, list) {
                    var val = element.get("value");

                    switch(element.get("screen"))
                    {
                        case "number":
                        case "signature_text":
                            if (val == null || val == "")
                            {
                                val = element.get("sku_value");
                                element.set("value", val);
                            }
                            break;
                    }

                    var $field = $("input[type='hidden'][name='options[" + element.get("id") + "]']");
                    $field.val(val);
                }, this);

                this._updateCalculatedPrice();
            },

            _initProductDetails: function () {
                // productData is global and is being written by magento right into the page
                if ("productData" in window) {
                    this.set("productDetails", new ProductDetailsModel(window.productData));

                    var p = parseFloat(this.get("productDetails").get("price"));
                    p = p * window.currencyRate;
                    p = formatCurrency(p, window.priceFormat);
                    this.set("totalPrice", p);

                    //log.debug( this.get("productDetails").toJSON() );
                }
            },

            /**
             * List of product options
             *
             * @private
             */
            _initProductOptions: function () {
                // configData is global and is being written by magento right into the page
                if ("configData" in window) {
                    // transform object to array for collection
                    var c = [];
                    _.each(window.configData.options, function (value, key, list) {
                        var m = new ProductOptionModel(value);

                        // get the values out of the hidden fields in the DOM and set if necessary.
                        var $option = $("input[name='options[" + m.get("id") + "]'][type='hidden']");
                        if ($option.length > 0 && $option.val() !== null && $option.val() !== "") {
                            m.set("value", $option.val());
                        } else if (_.isString(value.sku_value) && value.sku_value.length > 0) {
                            var id = this.getIdBySku(value.sku_value);
                            if (id > -1) m.set("value", id);
                        }

                        if (_.isArray(m.get("options"))) {
                            _.each(m.get("options"), function (element, index, list) {
                                var match = _.findKey(this.get("skuMap"), function (val) {
                                    return val == element.sku;
                                });
                                if (match === undefined) {
                                    this.get("skuMap")[element.id] = element.sku;
                                }
                            }, this);
                        }

                        c.push(m);
                    }, this);
                    this.set("productOptionsCollection", new ProductOptionsCollection(c));

                    //log.debug("## productOptionsCollection ##");
                    //log.debug( this.get("productOptionsCollection").toJSON() );
                }
            },

            /**
             * group product options by type for use in UI palette setup
             *
             * @private
             */
            _initUIPanels: function () {
                var c = new Backbone.Collection();
                this.get("productOptionsCollection").each(function (value, index, list) {

                    var s = value.get("screen");
                    var m = new UIPanelModel();

                    if (s.indexOf("_") == -1) {
                        c.add(new UIPanelModel({
                            panelType: s,
                            panelOptions: new Backbone.Collection(value),
                            interest: value.get("config").interest
                        }));
                    } else {
                        var screenParts = s.split("_");
                        var existingNode = c.findWhere({panelType: screenParts[0]});
                        if (existingNode !== undefined) {
                            var ops = existingNode.get("panelOptions");
                            ops.add(value);
                            existingNode.set("panelOptions", ops);
                        } else {
                            c.add(new UIPanelModel({
                                panelType: screenParts[0],
                                panelOptions: new Backbone.Collection(value),
                                interest: value.get("config").interest
                            }));
                        }
                    }

                });

                this.set("uiPanelsCollection", c);
            },

            _initColorPalettes: function () {
                // configData is global and is being written by magento right into the page
                if ("configData" in window) {
                    // transform object to array for collection
                    var colorsList = [];
                    var c = [];
                    _.each(window.configData.color_palettes, function (value, key, list) {
                        var m = new ProductOptionModel(value);
                        c.push(m);

                        _.each(value.options, function (element, index, list) {

                            // merge config.json into model...
                            var elementConfig = configJSON.colors[element.sku];
                            if (_.isObject(elementConfig)) {
                                element.color = elementConfig.hex;
                                element.logo_color = elementConfig.logo_color;
                                element.palette_model = m;

                                if (elementConfig.type && elementConfig.type !== "neon") {
                                    element.color_thumbnail = CDN.path + "configurator/color_thumbnails/" + element.sku + ".png";
                                }

                            }

                            colorsList.push(element);
                            this.get("skuMap")[element.id] = element.sku;
                        }, this);

                    }, this);
                    this.set("colorPalettesCollection", new ProductOptionsCollection(c));
                    this.set("colors", new Backbone.Collection(colorsList));
                }
            },

            _updateCalculatedPrice: function () {
                var p = parseFloat(this.get("productDetails").get("price"));
                console.log(p);
                if (this.get("productOptionsCollection").length > 0) {
                    var optionsSum = _.chain(this.get("productOptionsCollection").models)
                        .filter(function (op) {
                            return _.isArray(op.get("options")) && op.get("options").length > 0 && op.get("value") !== null && op.get("value") !== "";
                        })
                        .map(function (op, index, list) {
                            var x = _.findWhere(op.get("options"), {id: op.get("value")});
                            return x;
                        })
                        .pluck("price")
                        .reduce(function (a, b) {
                            return a + parseFloat(b);
                        }, 0)
                        .value();

                    p += optionsSum;

                }

                p = p * window.currencyRate;
                p = formatCurrency(p, window.priceFormat);
                this.set("totalPrice", p);

                // Set Visor Price Dynamic
                if(optionsSum){

                    optionsSum = optionsSum * window.currencyRate;
                    optionsSum = formatCurrency(optionsSum, window.priceFormat);

                    if (!document.getElementsByClassName('visor_options-price-dynam').length){
                        var existHTML= jQuery(".price-box .price-product-options .option-table" ).html();
                        var existHTMLMobile = jQuery("#mobile-price-box .price-product-options" ).html();
                        jQuery(".price-box .price-product-options .option-table").html(existHTML + '<div class="visor_options-price-dynam table_row"><div class="cell"><strong>Visieroption</strong></div><div class="cell"><span>'+optionsSum+'</span></div>');
                        jQuery("#mobile-price-box .price-product-options").html(existHTMLMobile + '<span class="visor_options-price-dynam"><strong>Visieroption</strong><span>'+optionsSum+'</span></span>');
                    }else{
                        jQuery(".visor_options-price-dynam span").html(optionsSum);
                        jQuery(".visor_options-price-dynam").show();
                    }

                }else{

                    if (document.getElementsByClassName('visor_options-price-dynam').length) {
                        jQuery(".visor_options-price-dynam").remove();
                    }
                }
            }
        });

        return ConfiguratorAppModel;
    }
);
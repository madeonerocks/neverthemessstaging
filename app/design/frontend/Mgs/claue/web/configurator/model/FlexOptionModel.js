define([
        "backbone"
    ],
    function (Backbone) {
        "use strict";

        var FlexOptionModel = Backbone.Model.extend({
            defaults: {
                aspectRatio: 2,
                template: null,
                data: null
            }
        });

        return FlexOptionModel;
    }
);
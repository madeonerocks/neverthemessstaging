define([
        "backbone",
        "jquery",
        "log",
        "configurator/view/panel/option/FlexOptionView",
        "responsiveHelper"
    ],
    function (Backbone, $, log, FlexOptionView, ResponsiveHelper) {
        "use strict";

        /**
         * The abstract panel view
         */
        var AbstractPanelView = Backbone.View.extend({

                tagName: 'div',
                visible: false,
                flexOptionView: null,
                templateData: {},

                events: {
                    "click .next": "_onNextClick",
                    "click .prev": "_onPrevClick"
                },

                constructor: function (options) {
                    Backbone.View.apply(this, arguments);
                },

                initialize: function (options) {

                    this.panelModel = options.panelModel;

                    $(window).on("resize", _.bind(this._onResize, this));

                    this.render();

                },

                render: function () {
                    return this;
                },

                _updateTemplateData: function () {

                    var nextLabel = null;
                    var prevLabel = null;

                    var next = this.panelModel.get("next");
                    if (next)
                        nextLabel = next.get("panelOptions").first().get("label");

                    var prev = this.panelModel.get("prev");
                    if (prev)
                        prevLabel = prev.get("panelOptions").first().get("label");

                    this.templateData = {
                        nextLabel: nextLabel,
                        prevLabel: prevLabel,
                        panelModel: this.panelModel
                    };
                },

                _renderFlexView: function (flexOptionModel) {
                    var $flex = this.$(".flex").first();
                    if ($flex) {
                        if (this.flexOptionView !== null) {
                            this.flexOptionView.setElement($flex);
                            this.flexOptionView.render();
                        } else {
                            this.flexOptionView = new FlexOptionView({el: $flex, model: flexOptionModel});
                        }
                    }
                },

                show: function () {
                    this.visible = true;

                    this.$el.removeClass("hidden");
                    //this.$el.css({display: "block"});

                    this._updateTemplateData();
                    this.render();
                    this._onResize();
                },

                hide: function () {
                    this.visible = false;
                    this.$el.addClass("hidden");
                    //this.$el.css({display: "none"});
                },

                _onResize: function () {
                    if (this.visible) {

                        var panelWidth = this.$el.outerWidth();
                        var childrenWidth = 0;
                        var $flexEl = this.$el.children(".flex");

                        if (ResponsiveHelper.check("m-l-xl")) {
                            var $children = this.$el.children("div").not('.flex');
                            $children.each(function () {
                                childrenWidth += $(this).outerWidth();
                            });
                        }
                        var flexWidth = panelWidth - childrenWidth - 1;
                        $flexEl.width(flexWidth);

                        if (this.flexOptionView !== null) this.flexOptionView.layout();
                    }
                },

                _onNextClick: function () {
                    var next = this.panelModel.get("next");
                    if (next) {
                        console.log("Next Interest:" + next.get("interest"));
                        this.model.set("interest", next.get("interest"));
                    }
                },

                _onPrevClick: function () {
                    var prev = this.panelModel.get("prev");
                    if (prev) {
                        console.log("Prev Interest:" + prev.get("interest"));
                        this.model.set("interest", prev.get("interest"));
                    }
                }

            }
        );

        return AbstractPanelView;
    }
);
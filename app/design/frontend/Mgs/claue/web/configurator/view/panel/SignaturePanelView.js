define([
        "configurator/view/panel/AbstractPanelView",
        "configurator/model/FlexOptionModel",
        "backbone",
        "jquery",
        "log",
        "hbs!templates/panel/signaturePanel",
        "hbs!templates/panel/signatureList"
    ],
    function (AbstractPanelView, FlexOptionModel, Backbone, $, log, template, subTemplate) {
        "use strict";

        /**
         * The signature panel view
         */
        var SignaturePanelView = AbstractPanelView.extend({
                className: 'signature-panel panel',
                guideVisible: false,

                events: function () {
                    return _.extend({}, AbstractPanelView.prototype.events, {
                        "change input:radio": "_onRadioChange",
                        "input input[type=text]": "_onTextChange",
                        "click .font-tile": "_onFontClick",
                        "touchend .font-tile": "_onFontClick",
                        "click .color": "_onColorClick",
                        "touchend .color": "_onColorClick",
                    });
                },


                constructor: function (options) {
                    AbstractPanelView.apply(this, arguments);
                },

                initialize: function (options) {
                    this.flexOptionModel = new FlexOptionModel({template: subTemplate});
                    AbstractPanelView.prototype.initialize.apply(this, arguments);
                    this.panelModel.get("panelOptions").findWhere({screen: "signature_font"}).on("change", this._renderActiveOption, this);
                    this.hasProducerLogo = this.panelModel.get("panelOptions").findWhere({screen: "signature_producer_logo"}) != null;
                    this.producerLogoTitle = "";
                    if(this.hasProducerLogo) this.producerLogoTitle = this.panelModel.get("panelOptions").findWhere({screen: "signature_producer_logo"}).attributes.label;
                },

                _updateTemplateData: function () {

                    AbstractPanelView.prototype._updateTemplateData.apply(this, arguments);

                    if (this.hasProducerLogo)
                        this.templateData.producerLogos = this.panelModel.get("panelOptions").findWhere({screen: "signature_producer_logo"}).get("options");

                    var textOptions = this.panelModel.get("panelOptions").findWhere({screen: "signature_text"});
                    var driverName = textOptions.get("value");
                    if (driverName == "")
                        driverName = textOptions.get("sku_value");
                    if (driverName == "-")
                        driverName = "";

                    this.templateData.signatureText = driverName;
                    this.templateData.fonts = this.panelModel.get("panelOptions").findWhere({screen: "signature_font"}).toJSON();
                    this.templateData.colors = this.model.getSignatureColors().models;
                    this.templateData.maxChars = textOptions.get("max_characters");
                },

                render: function () {
                    var hide;
                    try{
                        hide = this.panelModel.get("panelOptions").findWhere({screen: "signature_producer_logo"}).attributes.config.hide;
                    }catch (e) {
                        console.log(e);
                        hide = false;
                    }
                    if(!hide) {
                        this.templateData.hasProducerLogo = this.hasProducerLogo;
                        this.templateData.title = this.producerLogoTitle;
                    }

                    this.$el.html(template(this.templateData));

                    var fonts = this.panelModel.get("panelOptions").findWhere({screen: "signature_font"}).get("options");
                    this.flexOptionModel.set("data", fonts);

                    this._renderFlexView(this.flexOptionModel);

                    this._renderActiveOption();

                    this._updateColorSelection();

                    this._renderActiveRadio();

                    return this;
                },

                _renderActiveRadio: function () {
                    var hide;
                    try{
                        hide = this.panelModel.get("panelOptions").findWhere({screen: "signature_producer_logo"}).attributes.config.hide;
                    }catch (e) {
                        console.log(e);
                        hide = false;
                    }
                    if (this.hasProducerLogo && !hide) {
                        var selectedLogo = this.panelModel.get("panelOptions").findWhere({screen: "signature_producer_logo"}).get("value");
                        this.$el.find("input:radio[value=" + selectedLogo + "]").prop("checked", true);
                    }
                },

                _updateColorSelection: function () {
                    var selectedColorId = this.panelModel.get("panelOptions").findWhere({screen: "signature_color"}).get("value");
                    this.$el.find('.color').removeClass("active");
                    if (selectedColorId !== undefined && selectedColorId > 0) {
                        this.$el.find('.color[data-id=' + selectedColorId + ']').addClass("active");
                    }
                },

                _renderActiveOption: function () {
                    var selectedFontId = this.panelModel.get("panelOptions").findWhere({screen: "signature_font"}).get("value");
                    this.$el.find('.font-tile').removeClass("active");

                    try {
                        this.$el.find('.font-tile[data-id=' + selectedFontId + ']').addClass("active");
                    }
                    catch (e) {
                        //cannot select
                    }
                },

                _onTextChange: function (e) {
                    var regex = new RegExp("^([a-zA-Z0-9äöüÄÖÜß#. ]{0,15})$");
                    var textModel = this.panelModel.get("panelOptions").findWhere({screen: "signature_text"});
                    var text = e.currentTarget;
                    var newText = e.currentTarget.value;
                    var $input = $(e.target);

                    if (newText == "")
                        newText = "-";

                    if (newText == "-")
                    {
                        textModel.set("value", newText);
                        $input.attr("data-save", "");
                    }
                    else if (newText.match(regex)) {
                        textModel.set("value", newText);
                        $input.attr("data-save", newText);
                    }

                    var start = text.selectionStart,
                        end = text.selectionEnd;

                    $input.val($input.attr("data-save"));

                    text.setSelectionRange(start, end);

                },

                _onRadioChange: function (e) {
                    var val = $(e.target).attr("value");
                    if (this.hasProducerLogo) {
                        var productOptionId = this.panelModel.get("panelOptions").findWhere({screen: "signature_producer_logo"}).get("id");
                        var productOpsCollection = this.model.get("productOptionsCollection");
                        productOpsCollection.findWhere({id: productOptionId}).set("value", val);
                    }
                },

                _onFontClick: function (e) {
                    var $fontEl = $(e.currentTarget);
                    var fontId = $fontEl.attr("data-id");
                    var productOptionId = this.panelModel.get("panelOptions").findWhere({screen: "signature_font"}).get("id");

                    var productOpsCollection = this.model.get("productOptionsCollection");
                    productOpsCollection.findWhere({id: productOptionId}).set("value", fontId);
                },

                _onColorClick: function (e) {
                    var $colorEl = $(e.currentTarget);
                    var colorId = $colorEl.attr("data-id");
                    var productOptionId = this.panelModel.get("panelOptions").findWhere({screen: "signature_color"}).get("id");

                    this.$el.find('.color').removeClass("active");
                    $colorEl.addClass("active");

                    var productOpsCollection = this.model.get("productOptionsCollection");
                    productOpsCollection.findWhere({id: productOptionId}).set("value", colorId);
                }
            }
        );

        return SignaturePanelView;
    }
);
define([
        "configurator/view/panel/AbstractPanelView",
        "backbone",
        "jquery",
        "log",
        "hbs!templates/panel/numberPanel"
    ],
    function (AbstractPanelView, Backbone, $, log, template) {
        "use strict";

        /**
         * The number panel view
         */
        var NumberPanelView = AbstractPanelView.extend({
                className: 'number-panel panel',

                events: function(){
                    return _.extend({},AbstractPanelView.prototype.events,{
                        "input input[type=number]": "_onNumberChange"
                    });
                },

                constructor: function (options) {
                    AbstractPanelView.apply(this, arguments);
                },

                initialize: function (options) {
                    AbstractPanelView.prototype.initialize.apply(this, arguments);
                },

                _updateTemplateData: function () {
                    var numberModel = this.panelModel.get("panelOptions").first();

                    var value = numberModel.get("value");
                    if (value == "")
                        value = numberModel.get("sku_value");

                    this.templateData = {
                        maxChars: numberModel.get("max_characters"),
                        value:value
                    };
                },

                render: function () {
                    this.$el.html(template(this.templateData));
                    return this;
                },

                _onNumberChange: function (e) {

                    var regex = new RegExp("^[0-9]{0,2}$");
                    var numberModel = this.panelModel.get("panelOptions").first();
                    var newValue = e.currentTarget.value;
                    var number = e.currentTarget;
                    var $input = $( e.target );

                    if (number.value.length > number.maxLength)
                        number.value = number.value.slice(0, number.maxLength);

                    if (newValue.match(regex)) {
                        log.debug(newValue,"matches",regex);
                        numberModel.set("value", newValue);
                        $input.attr("data-save",newValue);
                    }

                    if (newValue == "")
                    {
                        numberModel.set("value", "0");
                    }

                    $input.val($input.attr("data-save"));

                }


            }
        );

        return NumberPanelView;
    }
);
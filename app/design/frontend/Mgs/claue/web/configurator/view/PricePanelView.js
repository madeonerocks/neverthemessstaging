define([
        "backbone",
        "jquery",
        "log"
    ],
    function (Backbone, $, log) {
        "use strict";

        /**
         * The price panel view
         */
        var PricePanelView = Backbone.View.extend({

                events: {},

                constructor: function (options) {
                    Backbone.View.apply(this, arguments);
                },

                initialize: function (options) {
                    this.render();
                    this.model.on("change:totalPrice", this.render, this);
                },

                render: function () {
                    this.$el.find(".price").html(this.model.get("totalPrice"));
                    $("#mobile-price-box .price").html(this.model.get("totalPrice"));

                    return this;
                }
            }
        );

        return PricePanelView;
    }
);
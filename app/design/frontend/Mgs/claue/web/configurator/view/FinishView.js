define([
        "backbone",
        "jquery",
        "log",
        "hbs!templates/finish",
        "configurator/model/TranslationModel"
    ],
    function (Backbone, $, log, template, TranslationModel) {
        "use strict";

        /**
         * The ui view
         */
        var FinishView = Backbone.View.extend({
                events: {
                    "click .label": "_onLabelClick",
                    "click .toggle": "_onToggle"
                },
                isActive: false,


                constructor: function (options) {
                    Backbone.View.apply(this, arguments);
                },

                initialize: function (options) {
                    this.data = options.data;

                    this.model.on("change:glossyOnly", this._onGlossyStatusChanged, this);

                    this.render();
                },

                render: function () {
                    this.$el.empty();
                    var text = TranslationModel.getLabel("prod_configurator_notice");
                    this.$el.attr("data-tooltip", text);

                    this.$el.append(template(this.data.toJSON()));

                    this.$toggleIndicator = this.$el.find(".toggle-indicator");
                    this.$main = this.$el.find(".finish-view-main");

                    var productOptionModel = this.data.get("panelOptions").first();

                    var activeId = this.$el.find(".label[data-active]").attr("data-id");

                    var optionId = this.$main.attr("data-id",this.data.attributes.panelOptions.models[0].id);

                    this._setActive(activeId === productOptionModel.get("value"));

                    this.model.updateGlossyOnly();

                    return this;
                },

                _onLabelClick: function (e) {
                    var activeAttr = $(e.currentTarget).attr("data-active");

                    this._setActive(typeof activeAttr !== typeof undefined && activeAttr !== false);
                },

                _onToggle: function (e) {
                    this._setActive(!this.$toggleIndicator.hasClass("active"));
                },

                _setActive: function (b) {
                    var optionId = 0;

                    if (b) {
                        this.$toggleIndicator.addClass("active");
                        optionId = this.$el.find(".label[data-active]").attr("data-id");
                    } else {
                        this.$toggleIndicator.removeClass("active");
                        optionId = this.$el.find(".label:not([data-active])").attr("data-id");
                    }

                    this.isActive = b;

                    var productOptionModel = this.data.get("panelOptions").first();
                    productOptionModel.set("value", optionId);
                },

                _onGlossyStatusChanged: function (appModel, glossyOnly, options) {
                    if (glossyOnly) {
                        this.$main.addClass("disabled");
                        this.$el.addClass("has-tooltip");
                        this.undelegateEvents();
                        if (!this.isActive) this._setActive(true);
                    } else {
                        this.$main.removeClass("disabled");
                        this.$el.removeClass("has-tooltip");
                        this.delegateEvents();
                    }
                }
            }
        );

        return FinishView;
    }
);
define([
        "backbone",
        "jquery",
        "log",
        "slick",
        "optionSlider",
        "responsiveHelper"
    ],
    function (Backbone, $, log, slick, optionSlider, ResponsiveHelper) {
        "use strict";

        var FlexOptionView = Backbone.View.extend({

                tagName: 'div',
                numberOfOptions: 0,
                visible: false,

                constructor: function (options) {
                    Backbone.View.apply(this, arguments);
                },

                initialize: function (options) {
                    this.model.on("change:data", this.render, this);
                    this.render();
                },

                render: function () {
                    var template = this.model.get("template");
                    this.$el.html(template(this.model.get("data")));
                    return this;
                },

                layout: function () {

                    var $container = this.$el.find(".option-container");

                    var aspectRatio = this.model.get("aspectRatio");

                    if( this.$el.hasClass("square-for-mobile") && ResponsiveHelper.check("xs-s") ) aspectRatio = 1;

                    var $optionElements = $container.children("div");
                    var optionElementHeight = $optionElements.eq(0).height();

                    if (this.model.get("data") === null || optionElementHeight === 0) {
                        log.debug("model has no data");
                        return;
                    }

                    var optionElementMinWidth = Math.round(optionElementHeight / aspectRatio);
                    var numberOfVisibleElements = Math.floor(this.$el.width() / optionElementMinWidth);
                    var optionElementNewWidth = Math.floor(this.$el.width() / numberOfVisibleElements);

                    if (!$container.hasClass("slick-initialized")) {
                        $optionElements.width(optionElementNewWidth);
                        this.numberOfOptions = $optionElements.length;
                    }

                    if (this.numberOfOptions > numberOfVisibleElements) {

                        if ($container.hasClass("sliderino")) {
                            optionSlider.destroy($container);
                        }

                        var buttonWidth = optionElementNewWidth;

                        if( this.$el.hasClass("flex-big") ) {
                            buttonWidth = Math.floor(optionElementNewWidth / 2);
                        }

                        optionSlider.create($container, buttonWidth, numberOfVisibleElements);


                    } else if ($container.hasClass("sliderino")) {
                        optionSlider.destroy($container);
                    }

                }

            }
        );

        return FlexOptionView;
    }
);
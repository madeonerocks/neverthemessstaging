define([
        "backbone",
        "jquery",
        "log",
        "hbs!templates/ui",
        "configurator/view/UIPanelFactory",
        "configurator/view/PagerContainerView",
        "configurator/view/FinishView"
    ],
    function (Backbone, $, log, template, UIPanelFactory, PagerContainerView, FinishView) {
        "use strict";

        /**
         * The ui view
         */
        var UIView = Backbone.View.extend({
                tagName: 'div',
                className: 'ui-view',

                events: {},

                constructor: function (options) {
                    Backbone.View.apply(this, arguments);
                },

                initialize: function (options) {
                    this.model.on("change:interest", this._changeView, this);
                    //this.render();
                },

                render: function () {
                    this.$el.html(template(this.model.toJSON()));

                    this.panels = [];

                    var panelCollection = this.model.get("uiPanelsCollection");

                    panelCollection.each(function (element, index, list) {
                        if (element.get("panelType") == "finish") {
                            if(this.model.get("productDetails").attributes.is_toggle) {
                                this.finishView = new FinishView({
                                    data: element,
                                    model: this.model,
                                    el: this.$el.find(".finish-view")
                                });
                            }
                        } else {
                            var panel = ( UIPanelFactory.createUIPanel(element, this.model, this.$el.find(".panel-container")) );
                            if (panel) this.panels.push(panel);
                        }
                    }, this);

                    this.pagerContainer = new PagerContainerView({
                        list: this.panels,
                        model: this.model,
                        el: this.$el.find(".pager-container")
                    });

                    //create prev and next panel
/*                    for (var i = 0; i < this.panels.length; i++) {

                        var prev = null;
                        var next = null;

                        if (i > 0)
                            prev = this.panels[i - 1].panelModel;
                        if (i < this.panels.length - 1)
                            next = this.panels[i + 1].panelModel;

                        var current = this.panels[i].panelModel;
                        if (prev)
                            current.set("prev", prev);

                        if (next)
                            current.set("next", next);
                    }*/

                    return this;
                },

                start: function () {
                    this._changeView(this.model, this.model.get("interest"));
                },

                _changeView: function (model, value) {
                    // for debugging only! Remove eventually!
                    this.$el.find(".debug").html("interest: " + value);

                    _.each(this.panels, function (panel, index, list) {
                        if (panel.panelModel.get("interest") === value) {
                            panel.show();
                        } else {
                            panel.hide();
                        }
                    }, this);
                }
            }
        );

        return UIView;
    }
);
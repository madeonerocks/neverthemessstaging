define([
        "configurator/view/panel/AbstractPanelView",
        "backbone",
        "jquery",
        "log",
        "configurator/model/FlexOptionModel",
        "hbs!templates/panel/visorPanel",
        "hbs!templates/panel/visorList"
    ],
    function (AbstractPanelView, Backbone, $, log, FlexOptionModel, template, subTemplate) {
        "use strict";

        /**
         * The signature panel view
         */
        var VisorPanelView = AbstractPanelView.extend({
                className: 'visor-panel panel',

                events: function () {
                    return _.extend({}, AbstractPanelView.prototype.events, {
                        "change input:radio": "_onRadioChange",
                        "click .visor-tile": "_onVisorOptionClick",
                        "touchend .visor-tile": "_onVisorOptionClick"
                    });
                },

                constructor: function (options) {
                    AbstractPanelView.apply(this, arguments);
                },

                initialize: function (options) {
                    this.flexOptionModel = new FlexOptionModel({template: subTemplate});

                    AbstractPanelView.prototype.initialize.apply(this, arguments);

                    this.panelModel.get("panelOptions").findWhere({screen: "visor_options"}).on("change", this._renderActiveOption, this);

                    this.hasSticker = this.panelModel.get("panelOptions").findWhere({screen: "visor_sticker"}) != null;
                },

                _updateTemplateData: function () {

                    AbstractPanelView.prototype._updateTemplateData.apply(this, arguments);

                    if (this.hasSticker)
                        this.templateData.stickers = this.panelModel.get("panelOptions").findWhere({screen: "visor_sticker"}).get("options");
                },

                render: function () {
                    this.templateData.hasSticker = this.hasSticker;

                    this.$el.html(template(this.templateData));

                    var visors = this.panelModel.get("panelOptions").findWhere({screen: "visor_options"}).get("options");
                    this.flexOptionModel.set("aspectRatio", 1.13);
                    this.flexOptionModel.set("data", visors);

                    this._renderFlexView(this.flexOptionModel);

                    this._renderActiveRadio();
                    this._renderActiveOption();

                    return this;
                },

                _renderActiveRadio: function () {
                    if (this.hasSticker) {
                        var selectedStickerColor = this.panelModel.get("panelOptions").findWhere({screen: "visor_sticker"}).get("value");
                        this.$el.find("input:radio[value=" + selectedStickerColor + "]").prop("checked", true);
                    }
                },

                _renderActiveOption: function () {
                    var selectedVisorId = this.panelModel.get("panelOptions").findWhere({screen: "visor_options"}).get("value");

                    try {
                        this.$el.find('.visor-tile').removeClass("active");
                        this.$el.find('.visor-tile[data-id=' + selectedVisorId + ']').addClass("active");
                    }
                    catch (e) {
                        console.warn(e);
                    }

                },

                _onRadioChange: function (e) {
                    var val = $(e.target).attr("value");

                    if (this.hasSticker) {
                        var productOptionId = this.panelModel.get("panelOptions").findWhere({screen: "visor_sticker"}).get("id");
                        var productOpsCollection = this.model.get("productOptionsCollection");
                        productOpsCollection.findWhere({id: productOptionId}).set("value", val);
                    }
                },

                _onVisorOptionClick: function (e) {
                    var $visorEl = $(e.currentTarget);
                    var visorId = $visorEl.attr("data-id");
                    var productOptionId = this.panelModel.get("panelOptions").findWhere({screen: "visor_options"}).get("id");
                    var productOpsCollection = this.model.get("productOptionsCollection");
                    productOpsCollection.findWhere({id: productOptionId}).set("value", visorId);
                }
            }
        );

        return VisorPanelView;
    }
);
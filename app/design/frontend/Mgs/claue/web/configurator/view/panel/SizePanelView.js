define([
        "configurator/view/panel/AbstractPanelView",
        "backbone",
        "jquery",
        "log",
        "configurator/event/PanelEvent",
        "hbs!templates/panel/sizePanel"
    ],
    function (AbstractPanelView, Backbone, $, log, PanelEvent, template) {
        "use strict";

        /**
         * The signature panel view
         */
        var SizePanelView = AbstractPanelView.extend({
                className: 'size-panel panel',

                events: function(){
                    return _.extend({},AbstractPanelView.prototype.events,{
                        "click .size": "_onSizeClick",
                        "click .helper.guide": "_toggleGuide"
                    });
                },

                constructor: function (options) {
                    AbstractPanelView.apply(this, arguments);
                },

                initialize: function (options) {
                    AbstractPanelView.prototype.initialize.apply(this, arguments);
                },

                _updateTemplateData: function () {
                    AbstractPanelView.prototype._updateTemplateData.apply(this, arguments);

                    this.templateData.option = this.panelModel.get("panelOptions").first().get("options");
                },

                render: function () {

                    this.$el.html(template(this.templateData));

                    this._renderActiveOption();

                    return this;
                },

                _renderActiveOption: function () {
                    var selectedSizeId = this.panelModel.get("panelOptions").findWhere({screen: "size"}).get("value");

                    try {
                        this.$el.find('.size').removeClass("active");
                        this.$el.find('.size[data-id=' + selectedSizeId + ']').addClass("active");
                    }
                    catch (e) {
                        console.warn(e);
                    }
                },

                _onSizeClick: function (e) {
                    var $sizeEl = $(e.currentTarget);
                    this.$el.find('.size').removeClass("active");
                    $sizeEl.addClass("active");
                    var sizeId = $sizeEl.attr("data-id");

                    var productOptionId = this.panelModel.get("panelOptions").findWhere({screen: "size"}).get("id");
                    var productOpsCollection = this.model.get("productOptionsCollection");
                    productOpsCollection.findWhere({id: productOptionId}).set("value", sizeId);
                },

                _toggleGuide: function () {
                    Backbone.trigger(PanelEvent.TOGGLE_FITTING_GUIDE);
                }

            }
        );

        return SizePanelView;
    }
);
define([
        "log",
        "configurator/view/panel/ColorPanelView",
        "configurator/view/panel/SignaturePanelView",
        "configurator/view/panel/SizePanelView",
        "configurator/view/panel/SpoilerPanelView",
        "configurator/view/panel/VisorPanelView",
        "configurator/view/panel/NumberPanelView"
    ],
    function (log, ColorPanelView, SignaturePanelView, SizePanelView, SpoilerPanelView, VisorPanelView, NumberPanelView) {
        "use strict";

        var UIPanelFactory = function () {
            return {
                createUIPanel: function (panelModel, applicationModel, $container) {
                    var panel;
                    var viewOptions = {model: applicationModel, panelModel: panelModel};

                    switch (panelModel.get("panelType")) {
                        case "color":
                            panel = new ColorPanelView(viewOptions);
                            break;
                        case "signature":
                            panel = new SignaturePanelView(viewOptions);
                            break;
                        case "visor":
                            panel = new VisorPanelView(viewOptions);
                            break;
                        case "spoiler":
                            panel = new SpoilerPanelView(viewOptions);
                            break;
                        case "size":
                            panel = new SizePanelView(viewOptions);
                            break;
                        case "number":
                            panel = new NumberPanelView(viewOptions);
                            break;
                    }

                    if (panel) {
                        $container.append(panel.render().el);
                    }

                    return panel;
                }
            }
        };

        return UIPanelFactory();
    }
);
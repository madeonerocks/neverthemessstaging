define([
        "configurator/view/panel/AbstractPanelView",
        "configurator/model/FlexOptionModel",
        "backbone",
        "jquery",
        "log",
        "hbs!templates/panel/colorPanel",
        "hbs!templates/panel/colorList"
    ],
    function (AbstractPanelView, FlexOptionModel, Backbone, $, log, template, subTemplate) {
        "use strict";

        /**
         * The ui view
         */
        var ColorPanelView = AbstractPanelView.extend({
                className: 'color-panel panel',

                events: function(){
                    return _.extend({},AbstractPanelView.prototype.events,{
                        "change input:radio": "_onRadioChange",
                        "click .color": "_onColorClick",
                        "touchend .color": "_onColorClick"
                    });
                },

                constructor: function (options) {
                    AbstractPanelView.apply(this, arguments);
                },

                initialize: function (options) {
                    this.flexOptionModel = new FlexOptionModel({template: subTemplate});

                    AbstractPanelView.prototype.initialize.apply(this, arguments);

                    this.panelModel.get("panelOptions").first().on("change:value", this._updateActiveColor, this);
                },

                _updateTemplateData: function () {
                    AbstractPanelView.prototype._updateTemplateData.apply(this, arguments);

                    this.templateData.colorPalettes = this.model.getFilteredColorPalettes(this.panelModel.get("panelOptions").first().get("config"));
                },

                render: function () {
                    this.$el.html(template(this.templateData));

                    this._renderFlexView(this.flexOptionModel);

                    var initialColorId = this.panelModel.get("panelOptions").first().get("value");
                    var colorPalettes = this.model.get("colorPalettesCollection");

                    var matchedPalette = colorPalettes.find(function (paletteModel) {
                        var match = _.findWhere(paletteModel.get("options"), {id: initialColorId});
                        return match === undefined ? false : true;
                    });

                    if (matchedPalette === undefined) {
                        matchedPalette = colorPalettes.first();
                    }

                    this.$el.find("input:radio[value=" + matchedPalette.get("id") + "]").prop("checked", true);
                    this._updateFlexOption(matchedPalette);

                    this._updateActiveColor();

                    return this;
                },

                _onRadioChange: function (e) {
                    var val = $(e.target).attr("value");
                    var clrs = this.model.get("colorPalettesCollection").findWhere({id: val});

                    this._updateFlexOption(clrs);
                    this._updateActiveColor();
                },

                _updateFlexOption: function (colorPalette) {
                    this.flexOptionModel.set("aspectRatio", 2);
                    this.flexOptionModel.set("data", colorPalette.get("options"));

                    this._onResize();

                    this.delegateEvents();
                },

                _updateActiveColor: function () {
                    var selectedColorId = this.panelModel.get("panelOptions").first().get("value");
                    this.$el.find('.color').removeClass("active");
                    if (selectedColorId !== undefined && selectedColorId > 0) {
                        this.$el.find('.color[data-id=' + selectedColorId + ']').addClass("active");
                    }
                },

                _onColorClick: function (e) {
                    var $colorEl = $(e.currentTarget);
                    var colorId = $colorEl.attr("data-id");
                    var productOptionId = this.panelModel.get("panelOptions").first().get("id");

                    this.$el.find('.color').removeClass("active");
                    $colorEl.addClass("active");

                    var productOpsCollection = this.model.get("productOptionsCollection");
                    productOpsCollection.findWhere({id: productOptionId}).set("value", colorId);

                    this.model.updateGlossyOnly();
                    this.model.fixAraiLabelColor();
                }
            }
        );

        return ColorPanelView;
    }
);
define([
        "configurator/view/panel/AbstractPanelView",
        "backbone",
        "jquery",
        "log",
        "hbs!templates/panel/spoilerPanel"
    ],
    function (AbstractPanelView, Backbone, $, log, template) {
        "use strict";

        /**
         * The signature panel view
         */
        var SpoilerPanelView = AbstractPanelView.extend({
                className: 'spoiler-panel panel',

                events: function(){
                    return _.extend({},AbstractPanelView.prototype.events,{

                    });
                },

                constructor: function (options) {
                    AbstractPanelView.apply(this, arguments);
                },

                initialize: function (options) {
                    AbstractPanelView.prototype.initialize.apply(this, arguments);
                },

                _updateTemplateData: function () {
                    AbstractPanelView.prototype._updateTemplateData.apply(this, arguments);
                },

                render: function () {
                    this.$el.html(template(this.templateData));

                    return this;
                }
            }
        );

        return SpoilerPanelView;
    }
);
define([
        "core/view/ComponentContainer",
        "log",
        "jquery"

    ],
    function (ComponentContainer, log, $) {
        "use strict";

        /**
         * The Main Class of the Configurator
         */
        var ConfiguratorApp = ComponentContainer.extend({

                events: {
                    "click .add-to-cart-buttons button": "_onAddToCart",
                    "click .shareButton": "_onSocialShare",
                    "mouseover .shareButton": "_onSocialShare"
                },

                constructor: function (options) {
                    ComponentContainer.apply(this, arguments);
                },

                initialize: function (options) {
                    ConfiguratorApp.isActive = true;

                    var self = this;

                    $('#mobile-price-box button').on('click', function(e)
                    {
                        self._onAddToCart(e);
                    });

                    $('.shareButton').on('click', function(e)
                    {
                        self._onSocialShare(e);
                    });

                    require([
                            "configurator/view/GLView", "configurator/view/UIView", "configurator/model/ConfiguratorAppModel",
                            //"configurator/view/BasicInfoView",
                            "configurator/view/PricePanelView",
                            "configurator/view/FittingGuideView",
                            "colorUtil",
                            "colorController"
                        ],
                        function (GLView, UIView, ConfiguratorAppModel, PricePanelView, FittingGuideView, ColorUtil, ColorController) {
                            self.GLView = GLView;
                            self.UIView = UIView;
                            //self.BasicInfoView = BasicInfoView;
                            self.PricePanelView = PricePanelView;
                            self.FittingGuideView = FittingGuideView;
                            self.ColorController = ColorController;

                            self.model = new ConfiguratorAppModel();
                            //this.model.on("change:")
                            var firstColor = self.model.get("productOptionsCollection").findWhere({screen: "color"});
                            firstColor.on("change", self._onFirstColorChange, self);

   /*                         var colors = self.model.get("productOptionsCollection").where({screen: "color"});
                            log.debug("colors: ",colors);
                            $.each(colors,function(i, color){
                                color.on("change", function(){
                                    log.debug(color);
                                    var hex = ColorUtil.skuToHex(color.get("sku_value"));
                                    log.debug("change color"+i,hex);
                                    $('.pager-container .pager-button[data-id="color'+i+'"]').css({
                                        backgroundColor: hex,
                                        color: hex
                                    });

                                }, self);
                            });*/

                            // important to kick off the child bootstrapping process
                            ComponentContainer.prototype.initialize.apply(self, arguments);

                            // skip the rest, when no data is present
                            if (self.model.get("productOptionsCollection") === null) {
                                return;
                            }

                            function triggerSiteColorAnimation() {
                                self._onFirstColorChange(self.model.get("productOptionsCollection").findWhere({screen: "color"}));

                                if (window.onConfiguratorComplete != null)
                                    window.onConfiguratorComplete();

                            }

                            function initAndFirstRender() {
                                self._initViews();
                                self.render();
                                setTimeout(triggerSiteColorAnimation, 0);
                            }

                            initAndFirstRender();
                        });
                },

                _initViews: function () {
                    this.glView = new this.GLView({model: this.model});
                    this.$el.append(this.glView.render().el);
                    this.glView._onResize();

                    this.uiView = new this.UIView({model: this.model});
                    this.$el.append(this.uiView.render().el);

                    //this.basicInfoView = new this.BasicInfoView({model: this.model});
                    //this.$el.append(this.basicInfoView.render().el);

                    var ppEl = this.$el.find(".price-box");
                    this.pricePanelView = new this.PricePanelView({model: this.model, el: ppEl});

                    this.fittingGuideView = new this.FittingGuideView();
                    this.$el.append(this.fittingGuideView.render().el);

                    this.uiView.start();
                },

                render: function () {

                },

                _onFirstColorChange: function (model, val, options) {
                    var colorId = model.get("value");
                    var colorObject = this.model.get("colors").findWhere({id: colorId});

                    colorObject.get("color");

                    this.ColorController.changeColor(colorObject.get("color"));
                },

                _onAddToCart: function (e) {
                    e.preventDefault();

                    this.model.save();

                    console.log("Create Thumbnail...");
                    var pngBase64 = this.glView.createThumbnail();
                    console.log("Create Thumbnail Result: " + pngBase64.length + " bytes/");

                    var debugThumbnail = false;
                    if (debugThumbnail)
                    {
                        console.log(window.productAddToCartForm);
                        console.log("Thumbnail Value: " + pngBase64);
                        var image = pngBase64.replace("image/png", "image/octet-stream");
                        window.location.href = image;
                    }
                    else
                    {
                        $('#custom_image_stream').val(pngBase64);
                        window.productAddToCartForm.submit(e.currentTarget);
                    }

                },


            _onSocialShare: function (e) {
                    e.preventDefault();


                    if($('.shareButton').hasClass('active')){
                        $('.shareButton').removeClass('active');
                        $('.shareContainerContent').hide();
                    }else{

                        var socialHash = $("#socialShareHash").text();
                        var mediaPathUrl = $j("#mediaFolderUrl").text();
                        var signatureColor = '';
                        var signature = '';
                        var hashData = '';
                        var actualUrl = $("#productUrl").text();
                        var productPicture = $("meta[name='twitter:image:src']").attr("content");
                        var description = $j("meta[name='twitter:description']").attr("content").replace("#", "%23");
                        var productTitle = $j("meta[name='twitter:title']").attr("content");

                        if(socialHash){

                            var pngBase64 = this.glView.createThumbnail();
                            var postUrl = "https://"+window.location.hostname+"/de/custimg/index";
                            hashData = JSON.parse(atob(socialHash));

                            // Set Signature

                            if($j('input[title="Signature')) {
                                signature = $j('input[title="Signature"]').attr("name");
                            }else{
                                signature = $j('input[title="Signatur"]').attr("name");
                            }

                            if(signature){
                                var signatureId = signature.replace('[', "").replace(']',"").replace("options","");

                                // Add Signature Value
                                hashData[signatureId] = $j('input[name="signature_text"]').val();

                            }


                            // Set Signature Color
                            if($j('input[title="Signature Color"]')) {
                                signatureColor = "Signature Color";
                            }else{
                                signatureColor = "Signaturfarbe";
                            }

                            if(signatureColor){

                                var signatureColorId = signatureColor.replace('[', "").replace(']',"").replace("options","");

                                // Add Signature Value
                                hashData[signatureColorId] = $j('.signature-panel .color-selector .active').attr("data-id");
                            }

                            //Build new Option Hash
                            var newHashData = Base64EncodeUrl(btoa(JSON.stringify(hashData)));

                            //Build new Share URL and Media URL
                            var newShareUrl = actualUrl+'?custom='+newHashData;
                            var mediaPath = mediaPathUrl+'customImages/'+newHashData+'.png';

                            $j('#socialShareHash').text(newHashData);
                            $j('#socialShareUrl').text(newShareUrl);
                            $j('.fbShare').attr('href','https://www.facebook.com/sharer/sharer.php?u='+newShareUrl);
                            $j('.pnShare').attr('onclick', "window.open('http://pinterest.com/pin/create/button/?url="+newShareUrl+"&media="+mediaPath+"&description="+productTitle+' '+description+" :"+actualUrl+"','pinIt','toolbar=0,status=0,width=800,height=500');");
                            $j('.twShare').attr('onclick',"window.open('http://twitter.com/share?url="+newShareUrl+"&text="+productTitle+" - "+description+"','_blank','width=800,height=300')");
                            $j('.goShare').attr('href','https://plus.google.com/share?url='+newShareUrl);
                            $j('.copylink').attr('data-clipboard-text',newShareUrl);


                            // Show Social Share Container

                            $('.shareContainerContent').show();
                            $('.loadingSpinner').show();
                            $('.afterLoading').hide();



                            $.post(postUrl, {
                                custom_image_stream: pngBase64,
                                custom_image_hash: socialHash
                            }).done(function( data ) {
                                $('.shareButton').addClass('active');
                                $('.loadingSpinner').hide();
                                $('.afterLoading').show();
                            });

                        }else{

                            $('.fbShare').attr('href','https://www.facebook.com/sharer/sharer.php?u='+actualUrl);
                            $('.pnShare').attr('onclick', "window.open('http://pinterest.com/pin/create/button/?url="+actualUrl+"&media="+productPicture+"&description="+productTitle+' '+description+" :"+actualUrl+"','pinIt','toolbar=0,status=0,width=800,height=500');");
                            $('.twShare').attr('onclick',"window.open('http://twitter.com/share?url="+actualUrl+"&text="+productTitle+" - "+description+"','_blank','width=800,height=300')");
                            $('.goShare').attr('href','https://plus.google.com/share?url='+actualUrl);
                            $('.copylink').attr('data-clipboard-text',actualUrl);

                            $('.shareContainerContent').show();
                            $('.shareButton').addClass('active');
                            $('.loadingSpinner').hide();
                            $('.afterLoading').show();

                        }
                    }

                }
            }
        );

        return ConfiguratorApp;
    }
);

function Base64EncodeUrl(str){
    return str.replace(/\+/g, '-').replace(/\//g, '_').replace(/\=+$/, '');
}
define([
        "backbone",
        "jquery",
        "log",
        "configurator/event/PanelEvent",
        "hbs!templates/fittingGuide",
        "languageHelper"
    ],
    function (Backbone, $, log, PanelEvent, template, LanguageHelper) {
        "use strict";

        var FittingGuideView = Backbone.View.extend({

                tagName: 'div',
                className: "fitting-guide",

                visible: false,

                events: {
                    "click .closeButton": "_onToggle"
                },

                constructor: function (options) {
                    Backbone.View.apply(this, arguments);
                },

                initialize: function (options) {

                    Backbone.on(PanelEvent.TOGGLE_FITTING_GUIDE, this._onToggle, this);

                    this.render();

                    $(window).on("resize", _.bind(this._onResize, this));

                    this._onResize();

                },

                render: function () {
                    this.$el.html(template());
                    return this;
                },

                _show: function () {
                    LanguageHelper.check("h3.fitting-guide-headline");
                    LanguageHelper.check("div.fitting-guide-image");
                    this.$el.addClass("visible");
                },

                _hide: function () {
                    this.$el.removeClass("visible");
                },

                _onToggle: function () {

                    this.visible = !this.visible;

                    if (this.visible) {
                        this._show();
                    } else {
                        this._hide();
                    }

                },

                _onResize: function () {
                    this.$el.height($("#configurator .helmet-view").first().height());
                }

            }
        );

        return FittingGuideView;
    }
);
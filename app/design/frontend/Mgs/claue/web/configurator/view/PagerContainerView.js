define([
        "backbone",
        "jquery",
        "log",
        "configurator/model/TranslationModel",
        "hbs!templates/pagerContainer"
    ],
    function (Backbone, $, log, TranslationModel, template) {
        "use strict";

        /**
         * The price panel view
         */
        var PagerContainerView = Backbone.View.extend({

                events: {
                    "click .pager-button": "_onClick",
                    "touchend .pager-button": "_onClick"
                },

                constructor: function (options) {
                    this.list = options.list;
                    Backbone.View.apply(this, arguments);
                },

                initialize: function (options) {
                    this.render();

                    this.model.on("change:interest", this._onInterestChanged, this);
                },

                render: function () {

                    this.$el.html(template(this));
                    this._onInterestChanged(this.model, this.model.get("interest"));

                    return this;
                },

                tint: function( interest ){
                    var $button = this.$el.find(".pager-button[data-id='" + interest + "']");
                },

                _onClick: function (e) {
                    e.preventDefault();
                    this.model.set("interest", $(e.currentTarget).attr("data-id"));
                },

                _onInterestChanged: function (model, value) {
                    this.$el.find(".pager-button.active").removeClass("active");
                    this.$el.find(".pager-button[data-id='" + value + "']").addClass("active");

                    var instructionsDiv = this.$el.find(".instructions");
                    var text = TranslationModel.getLabel("prod_configurator_cta");

                    var self = this;
                    _.each(this.list, function(x){

                        if (x.panelModel.get('interest') == self.model.get("interest"))
                        {
                            instructionsDiv.html(text + x.panelModel.getLabel());
                        }
                    });
                }
            }
        );

        return PagerContainerView;
    }
);
define([
        "backbone",
        "jquery",
        "log",
        "hbs!templates/basicInfo"
    ],
    function (Backbone, $, log, template) {
        "use strict";

        /**
         * The ui view
         */
        var BasicInfoView = Backbone.View.extend({
                tagName: 'div',
                className: 'basic-info-view',

                events: {},

                constructor: function (options) {
                    Backbone.View.apply(this, arguments);
                },

                initialize: function (options) {
                    this.render();
                },

                render: function () {

                    $('#header-meta').removeClass("tiny");

                    this.$el.html(template(this.model.get("productDetails").toJSON()));

                    return this;
                }
            }
        );

        return BasicInfoView;
    }
);
define([
        "backbone",
        "jquery",
        "log",
        "app/GLComponent2/GLComponent2",
        "hr/MagentoFrame",
        "hbs!templates/helmet",
        "json!https://cdn.helmade.com/configurator/color-config.txt",
        "responsiveHelper"
    ],
    function (Backbone, $, log, GLComponent, Frame, template, configJSON, ResponsiveHelper) {
        "use strict";

        /**
         * The helmet webgl view
         */
        var GLView = Backbone.View.extend({
                tagName: 'div',
                className: 'helmet-view',
                glComponentDelegate: null,

                events: {
                    "click canvas": "_onClick"
                },

                constructor: function (options) {
                    Backbone.View.apply(this, arguments);
                },

                initialize: function (options) {

                    this.glComponentDelegate = {};
                    this.glComponentDelegate.interestChanged = _.bind(this._onGLComponentInterestChanged, this);
                    this.glComponentDelegate.setLogoColor = _.bind(this._onSetLogoColor, this);

                    this.model.get("productOptionsCollection").on("change:value", this._onOptionChanged, this);
                    this.model.on("change:interest", this._onInterestChanged, this);

                    var detailsModel = this.model.get("productDetails");

                    this.frame = new Frame();
                    this.component = new GLComponent(this.frame, configJSON, this.glComponentDelegate, detailsModel.get("base_modell"), detailsModel.get("design_line"));
                    this.component.debug = false;

                    this.$background = $("<div>").attr("id", "configurator-background");
                    this.$backgroundLineBox = $("<div>").addClass("loader").appendTo(this.$background);
                    this.$backgroundLine = $("<div>").addClass("loader-line").appendTo(this.$backgroundLineBox);

                    this.$gridBox = $("<div>").addClass("page-content").addClass("width-limited");

                    this.$gridBox.append(this.frame.renderer.domElement);
                    this.$gridBox.append( $("<div class='preloader' style='z-index: 200;position: absolute;'><div style='width:0%;' class='preloader_bar global-color color-background'>&nbsp;</div><div class='preloader_text invisible global-color-background'>Please wait...</div></div>").addClass("xxx"));

                    this.$el.append(this.$background);
                    this.$el.append(this.$gridBox);

                    this.frame.start();

                    this.model.get("productOptionsCollection").each(function (element, index, list) {
                        this._onOptionChanged(element);
                    }, this);

                    $(window).on("resize", _.bind(this._onResize, this));
                },

                render: function () {
                    this._onResize();
                    return this;
                },

                _onResize: function () {

                    var $header = $("#header");
                    var $ui = $(".ui-view");
                    var $messages = $('ul.messages').first();

                    var advantageHeight = 40;
                    if(ResponsiveHelper.check("m") || ResponsiveHelper.check("xs") || ResponsiveHelper.check("s")){
                        advantageHeight = 0;
                    }

                    var windowHeight = $(window).height();
                    var windowWidth = $(window).width();
                    
                    if (typeof window.parentHeight !== 'undefined' && typeof window.parentWidth !== 'undefined') {
                        if (window.parentHeight < windowHeight)
                            windowHeight = window.parentHeight;
                        if (window.parentWidth < windowWidth)
                            windowHeight = window.parentWidth;
                        advantageHeight = advantageHeight ? advantageHeight + $('header').height() + 40: 0;
                    }

                    if (windowWidth > 1280)
                        windowWidth = 1280;

                    var uiHeight = 178;

                    if(ResponsiveHelper.check("m") || ResponsiveHelper.check("xs") || ResponsiveHelper.check("s")){
                        uiHeight += 35;
                    }

                    var newHeight = windowHeight - $header.height() - uiHeight - $messages.height() - advantageHeight;

                    if (newHeight > 800) {
                        newHeight = 800;
                    } else if ( newHeight > ( windowWidth - 24 ) ) {
                        newHeight = windowWidth - 24 ;
                    } else if ( newHeight < 280 && windowWidth > 600) {
                        newHeight = windowHeight -83 ;
                    } else if (newHeight < 280 && windowWidth <= 600){
                        newHeight = windowWidth - 24 ;
                    }

                    if(newHeight >= 500)
                        newHeight -= 150;

                    $("#configurator canvas").height(windowWidth - 24);
                    $("#configurator canvas").width(newHeight);

                    this.frame.layout(windowWidth - 24, newHeight);

                },

                _onOptionChanged: function (optionModel, val, options) {
                    // console.log("_onOptionChanged: ", optionModel.attributes, this.model, optionModel.get("screen"));

                    switch (optionModel.get("screen")) {
                        case "color":
                            var layerIndex = (optionModel.get("config").layer);
                            var sku = this.model.getSkuById(optionModel.get("value"));
                            if (layerIndex !== undefined && sku !== undefined) {
                                this.component.setColor(layerIndex, sku);
                            }
                            break;
                        case "finish":
                            var sku = this.model.getSkuByProductOptionModel(optionModel);
                            this.component.setGlossy(sku === "finish_glossy");
                            break;
                        case "signature_text":
                            var value = optionModel.get("value");
                            if (value == "")
                                value = optionModel.get("sku_value");
                            if (value == null || value == "-")
                                value = "";
                            this.component.setDriverName(value);
                            break;
                        case "signature_font":
                            var sku = this.model.getSkuById(optionModel.get("value"));
                            if (sku !== undefined) this.component.setFont(sku);
                            break;
                        case "signature_color":
                            var sku = this.model.getSkuById(optionModel.get("value"));
                            if (sku !== undefined) this.component.setSignatureColor(sku);
                            break;
                        case "signature_producer_logo":
                            var activeOption = _.findWhere(optionModel.get("options"), {id: optionModel.get("value")});
                            var showProducerLogo = false;
                            if (activeOption !== undefined)
                                if (activeOption.sku == "with_producer_logo")
                                    showProducerLogo = true;
                            this.component.showProducerLogo(showProducerLogo);
                            break;
                        case "visor_sticker":
                            var activeOption = _.findWhere(optionModel.get("options"), {id: optionModel.get("value")});
                            if (activeOption !== undefined) {
                                this.component.setVisorStickerColor(activeOption.sku);
                            }
                            break;
                        case "visor_options":
                            var sku = this.model.getSkuById(optionModel.get("value"));
                            if (sku !== undefined) this.component.setVisor(sku);
                            break;
                        case "number":
                            var value = optionModel.get("value");
                            if (value == "")
                                value = optionModel.get("sku_value");
                            if (value == null)
                                value = 32;
                            this.component.setNumber(value);
                            break;
                    }
                },

                _onInterestChanged: function (appModel, val, options) {
                    this.component.setInterest(val);
                },

                _onGLComponentInterestChanged: function (interest) {
                    this.model.set("interest", null, {silent: true});
                    this.model.set("interest", interest);
                },

                _onSetLogoColor: function (logoColor) {
                    var logo = this.model.get("productOptionsCollection").where({screen: "logo_helmade"}).first();
                    var options = logo.get("options");
                    var colorOption = options[logoColor];
                    logo.set("value", colorOption.id);
                    this.model.save();
                },

                createThumbnail: function()
                {
                    console.log("Create Thumbnail");
                    return this.component.createThumbnail();
                }
            }
        );

        return GLView;
    }
);
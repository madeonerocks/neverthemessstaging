/**
 * for auto-loading of helpers
 * see https://github.com/SlexAxton/require-handlebars-plugin#helpers
 */

define("templates/helpers/panelIcon", ["hbs/handlebars"], function (Handlebars) {
	"use strict";

	function panelIcon(panelModel) {
        var label = "icon-visor-pure";
        try
        {
            var interest = panelModel.get("interest");

            switch(interest)
            {
                case "color0":
                case "color1":
                case "color2":
                case "color3":
                case "color4":
                case "color5":
                    label = "icon-paint-pure";
                    break;
                case "signature":
                    label = "icon-name-pure";
                    break;
                case "number":
                    label = "icon-number-pure";
                    break;
                case "visor":
                    label = "icon-addons-pure";
                    break;
                case "size":
                    label = "icon-size-pure";
                    break;
            }

        }
        catch(e)
        {
            console.log(e);
        }

        //return "icon-check-pure";
		return label;
	}

	Handlebars.registerHelper("panelIcon", panelIcon);
	return panelIcon;
});

/**
 * for auto-loading of helpers
 * see https://github.com/SlexAxton/require-handlebars-plugin#helpers
 */

define("templates/helpers/splitSizeLabel", ["hbs/handlebars"], function (Handlebars) {
    "use strict";

    function splitSizeLabel(label, part) {

        var result = "";

        if(part == "label"){
            var posSpace1 = label.indexOf(" ");
            console.log("pos space: "+posSpace1);
            if(posSpace1 > 0){
                result = label.substring(0,posSpace1);
            } else {
                result = label;
            }

        } else {
            var posBracket1 = label.indexOf("(");
            var posBracket2 = label.indexOf(")");
            label = label.substring(posBracket1+1 , label.length-1);
            var posSpace2 = label.indexOf(" ");
        }

        if(part == "measure"){
            result = label.substring(0,posSpace2);
        }

        if(part == "unit"){
            result = label.substring(posSpace2+1,label.length);
        }

        return result;

    }

    Handlebars.registerHelper("splitSizeLabel", splitSizeLabel);
    return splitSizeLabel;
});

/**
 * for auto-loading of helpers
 * see https://github.com/SlexAxton/require-handlebars-plugin#helpers
 */

define(
    "templates/helpers/foregroundForBackground",
    ["hbs/handlebars","colorUtil"],
    function (Handlebars, ColorUtil) {
    "use strict";

    function foregroundForBackground(hex) {
        var foregroundHex = ColorUtil.getForegroundForBackgroundName(hex);
        return foregroundHex;
    }

    Handlebars.registerHelper("foregroundForBackground", foregroundForBackground);
    return foregroundForBackground;
});

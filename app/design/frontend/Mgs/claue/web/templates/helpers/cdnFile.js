/**
 * for auto-loading of helpers
 * see https://github.com/SlexAxton/require-handlebars-plugin#helpers
 */

define("templates/helpers/cdnFile", ["hbs/handlebars", "cdn"], function (Handlebars, CDN) {
	"use strict";

	function cdnFile(path, filename, extension) {
		if (arguments.length < 3) {
			throw new Error("Handlerbars Helper 'cdnFile' needs three parameters!");
		}
		var p = CDN.path;
		return p + path + filename + extension;
	}

	Handlebars.registerHelper("cdnFile", cdnFile);
	return cdnFile;
});

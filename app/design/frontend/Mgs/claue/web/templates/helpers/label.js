/**
 * for auto-loading of helpers
 * see https://github.com/SlexAxton/require-handlebars-plugin#helpers
 */

define("templates/helpers/label", ["hbs/handlebars", "configurator/model/TranslationModel"], function (Handlebars, TranslationModel) {
	"use strict";

	function label(key) {
		if (arguments.length > 2) {
			throw new Error("Handlerbars Helper 'label' needs one parameter!");
		}
		return TranslationModel.getLabel(key);
	}

	Handlebars.registerHelper("label", label);
	return label;
});

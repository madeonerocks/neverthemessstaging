/**
 * for auto-loading of helpers
 * see https://github.com/SlexAxton/require-handlebars-plugin#helpers
 */

define("templates/helpers/getPrice", ["hbs/handlebars", "templates/helpers/label"], function (Handlebars, labelHelper) {
	"use strict";

	function getPrice(val) {
		if (arguments.length > 2) {
			throw new Error("Handlerbars Helper 'price' needs one parameter!");
		}
		var freeLabel = labelHelper("prod_free");
		var p = parseFloat(val);
		if (p == 0) {
			return freeLabel;
		} else {
            p = p * window.currencyRate;
			p = formatCurrency(p, window.priceFormat);
			return p;
		}
	}

	Handlebars.registerHelper("getPrice", getPrice);
	return getPrice;
});



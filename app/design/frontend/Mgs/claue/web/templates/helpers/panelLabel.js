/**
 * for auto-loading of helpers
 * see https://github.com/SlexAxton/require-handlebars-plugin#helpers
 */

define("templates/helpers/panelLabel", ["hbs/handlebars"], function (Handlebars) {
	"use strict";

	function panelLabel(panelModel) {
        var label = "";
        try
        {
            label = panelModel.getLabel();
        }
        catch(e)
        {
            console.log(e);
        }

		return label;
	}

	Handlebars.registerHelper("panelLabel", panelLabel);
	return panelLabel;
});

/**
 * for auto-loading of helpers
 * see https://github.com/SlexAxton/require-handlebars-plugin#helpers
 */

define("templates/helpers/oddOrEven", ["hbs/handlebars"], function (Handlebars) {
    "use strict";

    function oddOrEven(value, is, options) {

        if (arguments.length < 1) {
            throw new Error("Handlerbars Helper 'oddOrEven' needs 1 parameter");
        }

        var result =  value % 2 == 0;

        if(is == "odd"){
            result = !result;
        }

        if( result ) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    }

    Handlebars.registerHelper("oddOrEven", oddOrEven);
    return oddOrEven;
});

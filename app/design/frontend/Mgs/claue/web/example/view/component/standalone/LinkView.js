define( [
		"backbone",
		"jquery",
		"log"
	],
	function ( Backbone, $, log ) {
		"use strict";

		/**
		 * Just a usual Backbone.View
		 */
		var TestComponentContainer = Backbone.View.extend( {

				events : {
					"click a" : "_onLinkClick"
				},

				constructor : function ( options ) {
					Backbone.View.apply( this, arguments );
				},

				initialize : function ( options ) {
					log.debug( "link component initialized" );
				},

				_onLinkClick : function ( event ) {
					event.preventDefault();

					log.debug( "i was clicked: ", $( event.currentTarget ).text() );
				}

			}
		);

		return TestComponentContainer;
	}
);
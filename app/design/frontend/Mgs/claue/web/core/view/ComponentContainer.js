define( [
		"backbone",
		"jquery",
		"log"
	],
	function ( Backbone, $, log ) {
		"use strict";

		/**
		 * The ComponentContainer serves as a container/controller component, which
		 * bootstraps potential component children.
		 * There should be one top-level 'root' container, which is usually the
		 * MainApplication with the body as $el.
		 * This root container starts bootstrapping all other components from the
		 * outmost node-leaf back to the root. All child-container-components manage their children, so every
		 * component should have only 1 parent.
		 * Also: every DOM-Node can only have 1 component!
		 */
		var ComponentContainer = Backbone.View.extend( {

			events : {},

			/**
			 * @param options
			 *
			 * @param options.isRoot   should only be true for the topmost container component (a.k.a the application)
			 * @param options.factory  an instance of a ComponentFactory
			 */
			constructor : function ( options ) {
				if ( ! options.factory ) {
					throw new Error( "A component container needs a factory!" );
				}

				this._isRoot = options.isRoot === true;
				this._factory = options.factory;
				this._childComponents = [];

				Backbone.View.apply( this, arguments );
			},

			initialize : function ( options ) {
				this._parseChildComponents();
			},

			_parseChildComponents : function () {
				var componentNodes = this._extractComponentNodes();

				// reverse collection to loop from inside out
				componentNodes = $(componentNodes.get().reverse());

				// remove all nodes from the collection, which are already initialized.
				componentNodes = _.reject(componentNodes, function(node) {
					return $(node).data('initialized');
				});

				// loop through the list and create components
				_.each( componentNodes, function ( cnode ) {
					var cid = $( cnode ).data( "component" );

					// create the actual component
					var component = this._factory.createComponent( cid, cnode );

					if ( component ) {
						this._childComponents.push( component );
					} else {
						log.warn( "no component found for: ", cid );
					}
				}, this );
			},

			_extractComponentNodes : function () {
				// default selector - all components
				var defaultSelector = "*[data-component]";

				// if we are the root container, only containers and direct children should be matched.
				if (this._isRoot) {
					// only containers should be selected
					var containerTypes = this._factory.getContainerTypes();

					var containerSelectorList = [];
					_.each(containerTypes, function(cid) {
						containerSelectorList.push("[data-component='" + cid + "']");
					});
					var containerSelector = containerSelectorList.join(",");

					var containerNodes = this.$el.find( containerSelector );

					if ( containerNodes.length === 0 ) {
						// if no containers found, parse all descendant nodes in all depths
						return this.$el.find( defaultSelector );
					}

					// also standalone components outside the first found container
					var ancestors = containerNodes.first().parentsUntil(this.$el, defaultSelector);
					var siblings = containerNodes.first().siblings(defaultSelector);

					return $( _.unique(containerNodes.get().concat(ancestors.get()).concat(siblings.get())) );

				}
				return this.$el.find( defaultSelector );
			}
		} );

		return ComponentContainer;
	}
);
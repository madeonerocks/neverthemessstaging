define( [
		"jquery",
		"log"
	],
	function ( $, log ) {
		"use strict";

		return {

			_registeredComponents : [],
			_initalizedNodes      : [],

			getContainerTypes : function () {
				return _.pluck( _.where( this._registeredComponents, {isContainer : true} ), "id" );
			},

			registerComponent : function ( cid, componentClass, isContainer ) {
				isContainer = isContainer === true;

				if ( _.findWhere( this._registeredComponents, {id : cid} ) ) {
					log.warn( "component with cid '" + cid + "' already registered" );
					return;
				}

				var componentSetup = {
					id             : cid,
					componentClass : componentClass,
					isContainer    : isContainer
				};

				this._registeredComponents.push( componentSetup );
			},

			createComponent : function ( cid, el ) {
				var $el = $( el );

				if ( _.indexOf( this._initalizedNodes, $el.get( 0 ) ) > - 1 ) {
					log.warn( "node with cid '" + cid + "' was already initialized, check container component setup!" );
				}

				var componentSetup = _.findWhere( this._registeredComponents, {id : cid} );
				if ( ! componentSetup ) {
					log.warn( "could not find component setup for cid '" + cid + "'" );
					return;
				}

				var options = {
					el : $el
				};

				if ( componentSetup.isContainer ) {
					options.factory = this;
				}

				var component = new componentSetup.componentClass( options );

				if ( component ) {
					$el.data( "initialized", true );
					this._initalizedNodes.push( $el.get( 0 ) );
				}

				return component;
			}
		};
	}
);
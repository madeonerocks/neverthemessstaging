define( [
		"core/view/ComponentContainer",
		"log"
	],
	function ( ComponentContainer, log ) {
		"use strict";

		/**
		 * The MainApplication is also a ComponentContainer -
		 * it bootstraps it's children like all the other container components
		 */
		var MainApplication = ComponentContainer.extend( {

				events : {},

				constructor : function ( options ) {
					// important!
					options.isRoot = true;

					ComponentContainer.apply( this, arguments );
				},

				initialize : function ( options ) {
					// start child component parsing
					ComponentContainer.prototype.initialize.apply( this, arguments );
				}
			}
		);

		return MainApplication;
	}
);
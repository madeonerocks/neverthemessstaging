define( [
		"core/view/ComponentFactory",
		"core/view/ComponentContainer",
		"core/view/MainApplication",
		"configurator/view/ConfiguratorApp",
		"header/HeaderMeta",
		"example/view/component/standalone/LinkView",
		"log"
	],
	function ( ComponentFactory,
	           ComponentContainer,
	           MainApplication,
			   ConfiguratorApp,
			   HeaderMeta,
	           LinkView,
	           log ) {
		"use strict";

		var RegisterComponentsCommand = {

			execute : function () {
				// container components
				ComponentFactory.registerComponent("main-application", MainApplication, true);
				ComponentFactory.registerComponent("configurator-app", ConfiguratorApp, true);
				ComponentFactory.registerComponent("header-meta", HeaderMeta, true);

				// standalone components
				ComponentFactory.registerComponent("link", LinkView);

			}
		};

		return RegisterComponentsCommand;
	}
);
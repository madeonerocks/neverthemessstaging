/**
 *
 * @category    Magento
 * @package
 * @copyright    Copyright © 2017 Tudock GmbH (http://www.tudock.de/)
 */
/**
 * @category    Magento
 * @package
 * @copyright    Copyright © 2017 Tudock GmbH (http://www.tudock.de/)
 * @author        Henning Künne <henning.kuenne@tudock.de>
 */

var TD = TD || {};

/**
 * @constructor
 * @property {Array.<HTMLElement>} items
 */
TD.Accordion = function () {
    this.items = Array.prototype.slice.apply(document.querySelectorAll('a.category-link'));
    this.initHandler().addObserver();
    // this.expandAllItems();
};

/**
 * initialize handler
 * @returns {TD.Accordion}
 */
TD.Accordion.prototype.initHandler = function () {

    this.clickHandler = this.handleClick.bind(this);
    return this;
};

/**
 * add event listener
 * @returns {TD.Accordion}
 */
TD.Accordion.prototype.addObserver = function () {
    this.items.forEach(function (item) {

        item.addEventListener('click', this.clickHandler);
    }.bind(this));
    return this;
};

/**
 * handle click event
 * @param {Event} e
 */
TD.Accordion.prototype.handleClick = function (e) {
    var elem = e.currentTarget;
    elem.parentNode.classList.toggle('active');
};

/**
 * Expand all elements on load
 * @returns {TD.Accordion}
 */
TD.Accordion.prototype.expandAllItems = function () {
    this.items.forEach(function (item) {
        item.click();
    }.bind(this));
    return this;
};

document.observe("dom:loaded", function() {
    new TD.Accordion();
});
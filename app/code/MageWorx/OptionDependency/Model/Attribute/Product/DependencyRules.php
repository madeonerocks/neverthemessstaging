<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageWorx\OptionDependency\Model\Attribute\Product;

use Magento\Framework\App\ResourceConnection;
use MageWorx\OptionDependency\Model\Config;
use MageWorx\OptionDependency\Model\DependencyRules as DependencyRulesModel;
use MageWorx\OptionBase\Model\Product\AbstractProductAttribute;
use MageWorx\OptionBase\Api\ProductAttributeInterface;
use MageWorx\OptionBase\Helper\Data as BaseHelper;

class DependencyRules extends AbstractProductAttribute implements ProductAttributeInterface
{
    /**
     * @var DependencyRulesModel
     */
    protected $dependencyRulesModel;

    /**
     * @var BaseHelper
     */
    protected $baseHelper;

    /**
     * @param ResourceConnection $resource
     * @param DependencyRulesModel $dependencyRulesModel
     * @param BaseHelper $baseHelper
     */
    public function __construct(
        DependencyRulesModel $dependencyRulesModel,
        BaseHelper $baseHelper,
        ResourceConnection $resource
    ) {
        $this->dependencyRulesModel = $dependencyRulesModel;
        $this->baseHelper           = $baseHelper;
        parent::__construct($resource);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return Config::KEY_DEPENDENCY_RULES;
    }

    /**
     * Collect product attribute data
     *
     * @param \MageWorx\OptionBase\Model\Entity\Group|\MageWorx\OptionBase\Model\Entity\Product $entity
     * @example of structure:
     * [{"conditions":[{"values":["484","485"],"type":"!eq","id":122}],"condition_type":"and","actions":{"hide":{"123":{"values":{"486":486},"id":123}}}}]
     * @return array
     */
    public function collectData($entity)
    {
        $this->entity = $entity;
        $data         = [];

        if ($entity->getType() !== 'product') {
            return $data;
        }

        $linkField   = $entity->getDataObject()->getResource()->getLinkField();
        $linkFieldId = $entity->getDataObject()->getData($linkField);

        $dependencies  = [];
        $options       = [];
        $attributeData = $entity->getDataObject()->getData('mageworx_option_attributes');
        if (!empty($attributeData[$this->resource->getTableName(Config::TABLE_NAME)]['save'])) {
            $rawDependencies = $attributeData[$this->resource->getTableName(Config::TABLE_NAME)]['save'];
            $options         = $entity->getDataObject()->getData('merged_options');
            if ($rawDependencies) {
                $dependencies = $this->dependencyRulesModel->getPreparedDependencies($rawDependencies);
            }
        }

        $attributeValue = [];
        if ($dependencies && $options) {
            $attributeValue = $this->dependencyRulesModel->combineRules($dependencies, $options);
        }

        $data['save'][$linkFieldId][$this->getName()] = $this->baseHelper->jsonEncode($attributeValue);

        $data['delete'][$linkFieldId] = [
            'product_id' => $linkFieldId
        ];

        return $data;
    }
}

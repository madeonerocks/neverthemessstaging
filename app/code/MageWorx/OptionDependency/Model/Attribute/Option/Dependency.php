<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageWorx\OptionDependency\Model\Attribute\Option;

use MageWorx\OptionDependency\Model\Attribute\Dependency as DefaultDependency;

class Dependency extends DefaultDependency
{
    /**
     * @var string
     */
    protected $currentProductSku = '';

    /**
     * {@inheritdoc}
     */
    public function collectData($entity, array $options)
    {
        $this->entity  = $entity;
        $this->options = $options;
    }

    /**
     * Delete old dependencies
     *
     * @param $data
     * @return void
     */
    public function deleteOldData(array $data)
    {
        return;
    }

    /**
     * Collect system data (customer group ids, store ids) from Magento 1 product csv
     *
     * @param array $systemData
     * @param array $productData
     * @param array $optionData
     * @param array $valueData
     */
    public function collectOptionsSystemDataMageOne(&$systemData, $productData, $optionData, $valueData =[])
    {
        if ($this->currentProductSku === $productData['sku']) {
            return;
        }

        $this->currentProductSku = $productData['sku'];

        if (empty($productData['options']) || !is_array($productData['options'])) {
            return;
        }

        foreach ($productData['options'] as $optionDatum) {
            if (empty($optionDatum['values']) || !is_array($optionDatum['values'])) {
                continue;
            }

            foreach ($optionDatum['values'] as $valueDatum) {
                if (empty($valueDatum['_custom_option_row_dependent_ids'])) {
                    continue;
                }
                $childDependencyIds = explode(',', $valueDatum['_custom_option_row_dependent_ids']);
                foreach ($childDependencyIds as $childDependencyId) {
                    $systemData['dependencies'][$productData['sku']][] = [
                        'parent_option_id'      => $optionDatum['_custom_option_in_group_id'],
                        'parent_option_type_id' => $valueDatum['_custom_option_row_in_group_id'],
                        'child_option_id'       => '',
                        'child_option_type_id'  => '',
                        'in_group_id'           => $childDependencyId
                    ];
                }
            }
        }
    }

    /**
     * Prepare data from Magento 1 product csv for future import
     *
     * @param array $systemData
     * @param array $productData
     * @param array $optionData
     * @param array $preparedOptionData
     * @param array $valueData
     * @param array $preparedValueData
     * @return void
     */
    public function prepareOptionsMageOne($systemData, $productData, $optionData, &$preparedOptionData, $valueData = [], &$preparedValueData = [])
    {
        if (empty($systemData['dependencies']) || empty($systemData['dependencies'][$productData['sku']])) {
            return;
        }

        $dependencies = [];
        foreach ($systemData['dependencies'][$productData['sku']] as $key => $dependency) {
            if ($dependency['in_group_id'] != $optionData['_custom_option_in_group_id']) {
                continue;
            }
            $dependencies[] = [
                (int)$dependency['parent_option_id'],
                (int)$dependency['parent_option_type_id']
            ];
        }
        if ($dependencies) {
            $preparedOptionData[static::getName()] = $this->baseHelper->jsonEncode($dependencies);
        }
    }
}

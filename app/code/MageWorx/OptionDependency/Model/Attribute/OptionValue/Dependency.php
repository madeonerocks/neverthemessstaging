<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageWorx\OptionDependency\Model\Attribute\OptionValue;

use MageWorx\OptionDependency\Model\Attribute\Dependency as DefaultDependency;

class Dependency extends DefaultDependency
{
    /**
     * Prepare data from Magento 1 product csv for future import
     *
     * @param array $systemData
     * @param array $productData
     * @param array $optionData
     * @param array $preparedOptionData
     * @param array $valueData
     * @param array $preparedValueData
     * @return void
     */
    public function prepareOptionsMageOne($systemData, $productData, $optionData, &$preparedOptionData, $valueData = [], &$preparedValueData = [])
    {
        if (empty($systemData['dependencies']) || empty($systemData['dependencies'][$productData['sku']])) {
            return;
        }

        $dependencies = [];
        foreach ($systemData['dependencies'][$productData['sku']] as $key => $dependency) {
            if ($dependency['in_group_id'] != $valueData['_custom_option_row_in_group_id']) {
                continue;
            }
            $dependencies[] = [
                (int)$dependency['parent_option_id'],
                (int)$dependency['parent_option_type_id']
            ];
        }

        if ($dependencies) {
            $preparedValueData[static::getName()] = $this->baseHelper->jsonEncode($dependencies);
        }

    }
}

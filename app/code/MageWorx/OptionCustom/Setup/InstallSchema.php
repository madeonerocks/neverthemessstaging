<?php
namespace MageWorx\OptionCustom\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
   public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
   {
       $setup->startSetup();

       $setup->getConnection()->addColumn(
           $setup->getTable('catalog_product_option'),
           'design',
           [
               'type'     => Table::TYPE_TEXT,
               'unsigned' => true,
               'nullable' => false,
                'default'  => '0',
               'comment'  => 'Design',
           ]
       );

       $setup->getConnection()->addColumn(
           $setup->getTable('catalog_product_option'),
           'screen',
           [
               'type'     => Table::TYPE_TEXT,
               'unsigned' => true,
               'nullable' => false,
               'default'  => '0',
               'comment'  => 'Screen',
           ]
       );

       $setup->endSetup();
   }
}

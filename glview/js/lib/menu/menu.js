function createMenu(createCallback) {

    function is_touch_device() {
        try {
            document.createEvent("TouchEvent");
            return true;
        } catch (e) {
            return false;
        }
    }

    function button(urlOrJs, label, type, additional=null) {
        //alert(additional);
        var isActive = window.location.href.indexOf(urlOrJs) != -1 && urlOrJs != '/';

        var result = document.createElement("li");
        result.className = (isActive ? "dropdown-item" : "dropdown-item") + (type != null ? ' ' + type : '');
        
        if (type == 'call') {
            result.innerHTML = '<a href="javascript:void(0);" class="btn btn-secondary btn-gradient">' + label + '</a>';

            function execute() {
                eval(urlOrJs);
            }

            if (is_touch_device()) {
                result.addEventListener('touchstart', execute);
            }
            else {
                result.addEventListener('click', execute);
            }
        }
        else if (type == 'label' || type == 'headline') {
            result.innerHTML = '<p>' + label + '</p>';
        }
        else {
            result.innerHTML = '<a class="btn btn-secondary btn-gradient" href="' + urlOrJs + '" target="' + (type == 'extern' ? '_blank' : '_self') + '">' + label + '</a>';
        }

        return result;
    }

    function input(method, value, type) {

        var result = document.createElement("li");
        result.className = type;

        var iDom = document.createElement("input");
        iDom.className = "nav_input";
        iDom.value = value;
        iDom.onkeyup = iDom.onchange = function(x)
        {
            console.log('change1:', iDom.value);
            var methodWithValue = method.split("{value}").join(iDom.value);
            eval(methodWithValue);
        }
        result.appendChild(iDom);

        return result;
    }

    var ul = document.createElement("ul");
    ul.setAttribute("class", "dropdown-menu dropdown_menu_more hide");
    ul.setAttribute("aria-labelledby", "dropdownMenu2");

    var menu = {};
    menu.addButton = function (url, label, type) {
        ul.appendChild(button(url, label, type));
    }

    menu.addInput = function (method, value, type) {
        ul.appendChild(input(method, value, type));
    }

    createCallback(menu);

    var container = document.getElementById("dropdownMenuCls");
    //container.setAttribute("id", "navScroll");
    //container.setAttribute("className", "collapse navbar-collapse");
    
    container.appendChild(ul);

    /*var nav = document.createElement("div");
    nav.setAttribute("id", "nav");
    nav.appendChild(container);

    document.getElementById('containerNavigation').appendChild(nav);*/
}
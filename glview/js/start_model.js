if (console)
{
    function perfNow(x)
    {
        try
        {
            console.log('%c ' + x + ': ' + window.performance.now() + "ms ", 'background: #222; color: #bada55');
        }
        catch(e)
        {
        }
    }
    perfNow("head");
}


window.startComponent = function (frame, appName) {
    console.log("window.startApp")

    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    console.log("registering error event");
    window.addEventListener('error', function (evt) {
        console.error("Caught[via 'error' event]:  '" + evt.message + "' from " + evt.filename + ":" + evt.lineno);
        evt.preventDefault();
    });


    var config = {
        baseUrl: './',
        paths: {
            app: 'js/app',
            hr: 'js/hr',
            vendor: 'js/vendor',
            lib: 'js/lib',
            jquery: 'js/vendor/jquery-2.1.4.min',
            text: '/skin/frontend/helmade/default/js/lib/text',
            json: '/skin/frontend/helmade/default/js/lib/json'
        },
        urlArgs: "bust=" + "5",
        waitSeconds: 200
    };

    var appName = getParameterByName('app');
    document.title = appName;

    require.config(config);

    require(['jquery', 'json!https://cdn.helmade.com/configurator/color-config.txt', frame, 'app/' + appName + '/' + appName], function ($, colorConfig, Frame, App) {
        console.log("umbrella import finished: ", arguments);

        var frame = new Frame(true);
        var canvas = $(frame.renderer.domElement);
        canvas.css('position', 'absolute');
        canvas.css('top', '0px');
        canvas.css('left', '0px');

        function layout() {
            var width = $(window).width();
            var height = $(window).height();
            frame.layout(width, height);
        }

        $(window).resize(layout);

        $('.banner-img').append(canvas);

        var allColorNames = [];
        var allMatteColorNames = [];
        for (var name in colorConfig.colors) {
            if (hasOwnProperty.call(colorConfig.colors, name)) {
                allColorNames.push(name);

                var c = colorConfig.colors[name];
                if (c.type == null || c.type == 'gradient')
                    allMatteColorNames.push(name);
            }
        }

        function getRandomColor() {
            return allColorNames[Math.floor(Math.random() * allColorNames.length)];
        }

        function getRandomMatteColor() {
            return allMatteColorNames[Math.floor(Math.random() * allMatteColorNames.length)];
        }

        var driverNames = ["MODULES", "Otto", "Willi", "Max Mustermann", "747", "demodern", "helmade"];

        function getRandomDriverName() {
            return driverNames[Math.floor(Math.random() * driverNames.length)];
        }

        window.start2 = {
            app: null,

            colorType : "normal",
            lastColorString : "0xffffff",

            toggleMatteGlossy: function () {
                app.setGlossy(!app.getGlossy());
            },

            toggleType:function(type)
            {
                window.start2.colorType = type;
                window.start2.toggleColor(window.start2.lastColorString);
            },

            toggleColor: function (colorString) {
                window.start2.lastColorString = colorString;
                console.log("toggleColor: " + colorString);
                if (colorString.indexOf('#') == 0)
                    colorString = colorString.substring(1);
                console.log("toggleColorTrim: " + colorString);

                var hex = parseInt(colorString, 16);
                var colorConfig = {};
                colorConfig.gl = {};
                colorConfig.gl.diffuse = hex;
                colorConfig.hex = hex;
                colorConfig.type = window.start2.colorType;
                app.setColorConfig(0, colorConfig);
                app.setColorConfig(0, colorConfig);
                app.setColorConfig(0, colorConfig);
                app.setColorConfig(0, colorConfig);
            },

            randomGlossy: function () {
                app.setGlossy(true);

                app.setColor(0, getRandomColor());
                app.setColor(1, getRandomColor());
                app.setColor(2, getRandomColor());
                app.setColor(3, getRandomColor());
            },

            randomMatte: function () {
                app.setGlossy(false);

                app.setColor(0, getRandomMatteColor());
                app.setColor(1, getRandomMatteColor());
                app.setColor(2, getRandomMatteColor());
                app.setColor(3, getRandomMatteColor());
            },

            randomDriverName: function () {
                app.setDriverName(getRandomDriverName());
            },

            setInterest: function (name) {
                app.setInterest(name);
            },

            visor: function (name) {
                app.setVisor(name);
            },

            openWithPreview: function () {
                window.location.href = window.location.href + "&preview=true";
            }
        }

        var delegate = {
            interestChanged: function (interest) {
                console.log("delegate.interestChanged", interest);

                app.setInterest(interest);
            },
            setLogoColor:function(){}
        };
        var app = window.start2.app = new App(frame, colorConfig, delegate, getParameterByName('model'), getParameterByName('design'));
        app.debug = {
            showTextures: false, //code by madeone(Kaushik)
            showFPS: false,
            showFlicker: false,
            allowZoom: true,
            preview: getParameterByName('preview') == "true"
        };
        frame.start();

        THREE.debug = false;

        var config = getParameterByName('config');
        console.log("Config:" + config);

        switch (config) {
            case 'logo0':
                app.setColor(0, 'color_basic_weiss');
                app.setColor(1, 'color_basic_weiss');
                app.setColor(2, 'color_basic_weiss');
                app.setColor(3, 'color_basic_weiss');
                app.setGlossy(true);
                app.setDriverName(getRandomDriverName());
                app.showProducerLogo(false);
                break;
            case 'logo1':
                app.setColor(0, 'color_basic_weiss');
                app.setColor(1, 'color_basic_weiss');
                app.setColor(2, 'color_basic_weiss');
                app.setColor(3, 'color_basic_weiss');
                app.setGlossy(true);
                app.setDriverName(getRandomDriverName());
                app.showProducerLogo(false);
                break;
            case 'logo2':
                app.setColor(0, 'color_basic_schwarz');
                app.setColor(1, 'color_basic_schwarz');
                app.setColor(2, 'color_basic_schwarz');
                app.setColor(3, 'color_basic_schwarz');
                app.setGlossy(true);
                app.setDriverName(getRandomDriverName());
                app.showProducerLogo(true);
                break;
            case 'brightMatt':
                app.setColor(0, 'color_basic_weiss');
                app.setColor(1, 'color_bold_zitronengelb');
                app.setColor(2, 'color_bold_japanisches-gruen');
                app.setColor(3, 'color_basic_schwarz');
                app.setGlossy(false);
                app.setDriverName(getRandomDriverName());
                app.setVisor('visor_standard');
                break;
            case 'darkMatt':
                app.setColor(0, 'color_bold_lila');
                app.setColor(1, 'color_basic_anthrazit');
                app.setColor(2, 'color_gradient_eigelb_rot');
                app.setColor(3, 'color_basic_schwarz');
                app.setGlossy(false);
                app.setDriverName(getRandomDriverName());
                app.setVisor('visor_2');
                break;
            case 'holoGold':
                app.setColor(0, 'color_effect_holo-flake_silber');
                app.setColor(1, 'color_effect_holo-flake_schwarz');
                app.setColor(2, 'color_effect_holo-flake_gold');
                app.setColor(3, 'color_effect_holo-flake_silber');
                app.setColor(4, 'color_effect_holo-flake_silber');
                app.setGlossy(true);
                app.setDriverName(getRandomDriverName());
                app.setVisor('23001');
                break;
            case 'holoBlack':
                app.setColor(0, 'color_effect_metallic_silber');
                app.setColor(1, 'color_basic_schwarz');
                app.setColor(2, 'color_gradient_steingrau_tuerkis');
                app.setColor(3, 'color_basic_anthrazit');
                app.setGlossy(false);
                app.setDriverName(getRandomDriverName());
                app.setVisor('visor_4');
                break;
            case 'metallic':
                app.setColor(0, 'color_basic_pflaume');
                app.setColor(1, 'color_effect_metallic_rot-blau');
                app.setColor(2, 'color_effect_metallic_silber');
                app.setColor(3, 'color_effect_metallic_schwarz');
                app.setGlossy(true);
                app.setDriverName("Billi");
                app.setVisor('visor_5');
                break;
            case 'darkFlake':
                app.setColor(0, 'color_basic_anthrazit');
                app.setColor(1, 'color_effect_flake_blau');
                app.setColor(2, 'color_basic_schwarz');
                app.setColor(3, 'color_basic_marine');
                app.setGlossy(true);
                app.setDriverName("#RACING");
                app.setVisor('visor_6');
                break;
            case 'brightFlake':
                app.setColor(0, 'color_gradient_steingrau_tuerkis');
                app.setColor(1, 'color_gradient_steingrau_tuerkis');
                app.setColor(2, 'color_effect_flake_grasgruen');
                app.setColor(3, 'color_basic_schwarz');
                app.setGlossy(true);
                app.setDriverName("Quarks");
                app.setVisor('visor_7');
                break;
            case 'flatIridium':
                app.setColor(0, 'color_gradient_steingrau_tuerkis');
                app.setColor(1, 'color_gradient_steingrau_tuerkis');
                app.setColor(2, 'color_effect_flake_grasgruen');
                app.setColor(3, 'color_basic_schwarz');
                app.setGlossy(true);
                app.setDriverName("Quarks");
                app.setVisor('flat_iridium');
                break;
            case 'white':
                window.start2.toggleColor("#ffffff");
                app.setGlossy(false);
                app.setDriverName("");
                app.setVisor('visor_standard');
                break;
        }

        app.setVisorStickerColor("color_basic_weiss");
        layout();
    });

}
define([
    "vendor/three"
], function () {

    function HelmConstraint(object) {

        this.object = object;

        // "target" sets the location of focus, where the object orbits around
        this.target = new THREE.Vector3();

        // Limits to how far you can dolly in and out ( PerspectiveCamera only )
        this.minDistance = 0;
        this.maxDistance = Infinity;

        // Limits to how far you can zoom in and out ( OrthographicCamera only )
        this.minZoom = 0;
        this.maxZoom = Infinity;

        // How far you can orbit vertically, upper and lower limits.
        // Range is 0 to Math.PI radians.
        this.minPolarAngle = 0; // radians
        this.maxPolarAngle = Math.PI; // radians

        // How far you can orbit horizontally, upper and lower limits.
        // If set, must be a sub-interval of the interval [ - Math.PI, Math.PI ].
        this.minAzimuthAngle = -Infinity; // radians
        this.maxAzimuthAngle = Infinity; // radians

        // Set to true to enable damping (inertia)
        // If damping is enabled, you must call controls.update() in your animation loop
        this.enableDamping = false;
        this.dampingFactor = 0.25;

        ////////////
        // internals

        var scope = this;

        var EPS = 0.000001;

        // Current position in spherical coordinate system.
        var theta;
        var phi;

        // Pending changes
        var phiDelta = 0;
        var thetaDelta = 0;
        var scale = 1;
        var zoomChanged = false;

        // API

        this.getPolarAngle = function () {

            return phi;

        };

        this.getAzimuthalAngle = function () {

            return theta;

        };

        this.rotateLeft = function (angle) {

            thetaDelta -= angle;

        };

        this.rotateUp = function (angle) {

            phiDelta -= angle;

        };

        this.dollyIn = function (dollyScale) {

            if (scope.object instanceof THREE.PerspectiveCamera) {

                scale /= dollyScale;

            } else if (scope.object instanceof THREE.OrthographicCamera) {

                scope.object.zoom = Math.max(this.minZoom, Math.min(this.maxZoom, this.object.zoom * dollyScale));
                scope.object.updateProjectionMatrix();
                zoomChanged = true;

            } else {

                console.warn('WARNING: HelmControls.js encountered an unknown camera type - dolly/zoom disabled.');

            }

        };

        this.dollyOut = function (dollyScale) {

            if (scope.object instanceof THREE.PerspectiveCamera) {

                scale *= dollyScale;

            } else if (scope.object instanceof THREE.OrthographicCamera) {

                scope.object.zoom = Math.max(this.minZoom, Math.min(this.maxZoom, this.object.zoom / dollyScale));
                scope.object.updateProjectionMatrix();
                zoomChanged = true;

            } else {

                console.warn('WARNING: HelmControls.js encountered an unknown camera type - dolly/zoom disabled.');

            }

        };

        this.update = function () {

            var offset = new THREE.Vector3();

            // so camera.up is the orbit axis
            var quat = new THREE.Quaternion().setFromUnitVectors(object.up, new THREE.Vector3(0, 1, 0));
            var quatInverse = quat.clone().inverse();

            var lastPosition = new THREE.Vector3();
            var lastQuaternion = new THREE.Quaternion();

            return function () {

                var position = this.object.position;

                offset.copy(position).sub(this.target);

                // rotate offset to "y-axis-is-up" space
                offset.applyQuaternion(quat);

                // angle from z-axis around y-axis

                theta = Math.atan2(offset.x, offset.z);

                // angle from y-axis

                phi = Math.atan2(Math.sqrt(offset.x * offset.x + offset.z * offset.z), offset.y);

                theta += thetaDelta;
                phi += phiDelta;

                // restrict theta to be between desired limits
                theta = Math.max(this.minAzimuthAngle, Math.min(this.maxAzimuthAngle, theta));

                // restrict phi to be between desired limits
                phi = Math.max(this.minPolarAngle, Math.min(this.maxPolarAngle, phi));

                // restrict phi to be betwee EPS and PI-EPS
                phi = Math.max(EPS, Math.min(Math.PI - EPS, phi));

                var radius = offset.length() * scale;

                // restrict radius to be between desired limits
                radius = Math.max(this.minDistance, Math.min(this.maxDistance, radius));

                offset.x = radius * Math.sin(phi) * Math.sin(theta);
                offset.y = radius * Math.cos(phi);
                offset.z = radius * Math.sin(phi) * Math.cos(theta);

                // rotate offset back to "camera-up-vector-is-up" space
                offset.applyQuaternion(quatInverse);

                position.copy(this.target).add(offset);

                this.object.lookAt(this.target);

                if (this.enableDamping === true) {

                    thetaDelta *= ( 1 - this.dampingFactor );
                    phiDelta *= ( 1 - this.dampingFactor );

                } else {

                    thetaDelta = 0;
                    phiDelta = 0;

                }

                scale = 1;

                // update condition is:
                // min(camera displacement, camera rotation in radians)^2 > EPS
                // using small-angle approximation cos(x/2) = 1 - x^2 / 8

                if (zoomChanged ||
                    lastPosition.distanceToSquared(this.object.position) > EPS ||
                    8 * ( 1 - lastQuaternion.dot(this.object.quaternion) ) > EPS) {

                    lastPosition.copy(this.object.position);
                    lastQuaternion.copy(this.object.quaternion);
                    zoomChanged = false;

                    return true;

                }

                return false;

            };

        }();

    };


    // This set of controls performs orbiting, dollying (zooming). It maintains
    // the "up" direction as +Y, unlike the TrackballControls. Touch on tablet and phones is
    // supported.
    //
    //    Orbit - left mouse / touch: one finger move
    //    Zoom - mousewheel / touch: two finger spread or squish

    function HelmControls(object, domElement) {

        var constraint = new HelmConstraint(object);

        this.domElement = ( domElement !== undefined ) ? domElement : document;

        // API

        Object.defineProperty(this, 'constraint', {

            get: function () {

                return constraint;

            }

        });

        this.getPolarAngle = function () {

            return constraint.getPolarAngle();

        };

        this.getAzimuthalAngle = function () {

            return constraint.getAzimuthalAngle();

        };

        // Set to false to disable this control
        this.enabled = true;

        // center is old, deprecated; use "target" instead
        this.center = this.target;

        // This option actually enables dollying in and out; left as "zoom" for
        // backwards compatibility.
        // Set to false to disable zooming
        this.enableZoom = true;
        this.zoomSpeed = 1.0;

        // Set to false to disable rotating
        this.enableRotate = true;
        this.rotateSpeed = 1.0;

        // Mouse buttons
        this.mouseButtons = {ORBIT: THREE.MOUSE.LEFT};

        ////////////
        // internals

        var scope = this;

        var rotateStart = new THREE.Vector2();
        var rotateEnd = new THREE.Vector2();
        var rotateDelta = new THREE.Vector2();

        var dollyStart = new THREE.Vector2();
        var dollyEnd = new THREE.Vector2();
        var dollyDelta = new THREE.Vector2();

        var STATE = {NONE: -1, ROTATE: 0, DOLLY: 1, TOUCH_ROTATE: 3, TOUCH_DOLLY: 4};

        var state = STATE.NONE;

        // for reset

        this.target0 = this.target.clone();
        this.position0 = this.object.position.clone();
        this.zoom0 = this.object.zoom;

        // events

        var changeEvent = {type: 'change'};
        var startEvent = {type: 'start'};
        var endEvent = {type: 'end'};

        this.update = function () {

            var hasUpdate = false;
            if (constraint.update() === true) {
                this.dispatchEvent(changeEvent);
                hasUpdate = true;
            }

            if (this.targetMode) {

                var controls = this;

                controls.dampingFactor = 0.2; //damping for auto move

                var offset = new THREE.Vector3();
                var position = controls.object.position;

                offset.copy(position).sub(controls.target);

                var quat = new THREE.Quaternion().setFromUnitVectors(controls.object.up, new THREE.Vector3(0, 1, 0));
                // rotate offset to "y-axis-is-up" space
                offset.applyQuaternion(quat);

                // angle from z-axis around y-axis
                var theta = Math.atan2(offset.x, offset.z);

                // angle from y-axis
                var phi = Math.atan2(Math.sqrt(offset.x * offset.x + offset.z * offset.z), offset.y);

                var p = controls.targetTheta;
                var thetaD = 0.4;
                var pMin = p - thetaD / 2;
                var pMax = p + thetaD / 2;
                var pHalf = p - (Math.PI * 2 - thetaD) / 2;
                if (theta > pMin && theta < pMax) {
                    var radius = offset.length();
                    if (radius > controls.minDistance)
                        controls.constraint.dollyOut(0.991);

                    if (!controls.targetReached) {

                        controls.currentTargetIvId = setTimeout(function () {
                            controls.currentTargetIvId = -1;
                            controls.dampingFactor = 0.05; //damping for user interaction
                            controls.targetMode = false;
                        }, 500);
                    }

                    controls.targetReached = true;

                }
                else if (theta > pHalf && theta < pMin) {
                    controls.constraint.rotateLeft(-0.015);
                }
                else {
                    controls.constraint.rotateLeft(0.015);
                }

                var p = controls.targetPhi;
                var thetaD = 0.4;
                var pMin = p - thetaD / 2;
                var pMax = p + thetaD / 2;
                var pHalf = p - (Math.PI * 2 - thetaD) / 2;
                if (phi > pMin && phi < pMax) {
                }
                else if (phi > pHalf && phi < pMin) {
                    controls.constraint.rotateUp(-0.005);
                }
                else {
                    controls.constraint.rotateUp(0.005);
                }

            }

            return hasUpdate;
        };

        this.setTargetMode = function (targetTheta, targetPhi) {
            var controls = this;
            clearTimeout(controls.currentTargetIvId);
            controls.targetReached = false;
            controls.targetTheta = targetTheta;
            controls.targetPhi = targetPhi;
            controls.targetMode = true;
        }


        this.reset = function () {

            state = STATE.NONE;

            this.target.copy(this.target0);
            this.object.position.copy(this.position0);
            this.object.zoom = this.zoom0;

            this.object.updateProjectionMatrix();
            this.dispatchEvent(changeEvent);

            this.update();

        };

        function getZoomScale() {

            return Math.pow(0.95, scope.zoomSpeed);

        }

        function onMouseDown(event) {

            if (scope.enabled === false) return;

            event.preventDefault();

            if (event.button === scope.mouseButtons.ORBIT) {

                if (scope.enableRotate === false) return;

                state = STATE.ROTATE;

                rotateStart.set(event.clientX, event.clientY);

            }

            if (state !== STATE.NONE) {

                document.addEventListener('mousemove', onMouseMove, false);
                document.addEventListener('mouseup', onMouseUp, false);
                scope.dispatchEvent(startEvent);

            }

        }

        function onMouseMove(event) {

            if (scope.enabled === false) return;

            event.preventDefault();

            var element = scope.domElement === document ? scope.domElement.body : scope.domElement;

            if (state === STATE.ROTATE) {

                if (scope.enableRotate === false) return;

                rotateEnd.set(event.clientX, event.clientY);
                rotateDelta.subVectors(rotateEnd, rotateStart);

                // rotating across whole screen goes 360 degrees around
                constraint.rotateLeft(2 * Math.PI * rotateDelta.x / element.clientWidth * scope.rotateSpeed);

                // rotating up and down along whole screen attempts to go 360, but limited to 180
                constraint.rotateUp(2 * Math.PI * rotateDelta.y / element.clientHeight * scope.rotateSpeed);

                rotateStart.copy(rotateEnd);

            }

            if (state !== STATE.NONE) scope.update();

        }

        function onMouseUp(/* event */) {

            if (scope.enabled === false) return;

            document.removeEventListener('mousemove', onMouseMove, false);
            document.removeEventListener('mouseup', onMouseUp, false);
            scope.dispatchEvent(endEvent);
            state = STATE.NONE;

        }

        function onMouseWheel(event) {

            if (scope.enabled === false || scope.enableZoom === false || state !== STATE.NONE) return;

            var delta = 0;

            if (event.wheelDelta !== undefined) {

                // WebKit / Opera / Explorer 9

                delta = event.wheelDelta;

            } else if (event.detail !== undefined) {

                // Firefox

                delta = -event.detail;

            }

            if (delta > 0) {

                constraint.dollyOut(getZoomScale());

            } else if (delta < 0) {

                constraint.dollyIn(getZoomScale());

            }

            var hasUpdate = scope.update();

            if (hasUpdate) {
                event.preventDefault();
                event.stopPropagation();
            }

            scope.dispatchEvent(startEvent);
            scope.dispatchEvent(endEvent);

        }

        function touchstart(event) {

            if (scope.enabled === false) return;

            switch (event.touches.length) {

                case 1:	// one-fingered touch: rotate

                    if (scope.enableRotate === false) return;

                    state = STATE.TOUCH_ROTATE;

                    rotateStart.set(event.touches[0].pageX, event.touches[0].pageY);
                    break;

                case 2:	// two-fingered touch: dolly

                    if (scope.enableZoom === false) return;

                    state = STATE.TOUCH_DOLLY;

                    var dx = event.touches[0].pageX - event.touches[1].pageX;
                    var dy = event.touches[0].pageY - event.touches[1].pageY;
                    var distance = Math.sqrt(dx * dx + dy * dy);
                    dollyStart.set(0, distance);
                    break;

                default:

                    state = STATE.NONE;

            }

            if (state !== STATE.NONE) scope.dispatchEvent(startEvent);

        }

        function touchmove(event) {

            if (scope.enabled === false) return;

            var element = scope.domElement === document ? scope.domElement.body : scope.domElement;

            var hasChanges = false;
            switch (event.touches.length) {

                case 1: // one-fingered touch: rotate

                    if (scope.enableRotate === false) return;
                    if (state !== STATE.TOUCH_ROTATE) return;

                    rotateEnd.set(event.touches[0].pageX, event.touches[0].pageY);
                    rotateDelta.subVectors(rotateEnd, rotateStart);

                    // rotating across whole screen goes 360 degrees around
                    constraint.rotateLeft(2 * Math.PI * rotateDelta.x / element.clientWidth * scope.rotateSpeed);
                    // rotating up and down along whole screen attempts to go 360, but limited to 180
                    constraint.rotateUp(2 * Math.PI * rotateDelta.y / element.clientHeight * scope.rotateSpeed);

                    rotateStart.copy(rotateEnd);

                    hasChanges = scope.update();
                    break;

                case 2: // two-fingered touch: dolly

                    if (scope.enableZoom === false) return;
                    if (state !== STATE.TOUCH_DOLLY) return;

                    var dx = event.touches[0].pageX - event.touches[1].pageX;
                    var dy = event.touches[0].pageY - event.touches[1].pageY;
                    var distance = Math.sqrt(dx * dx + dy * dy);

                    dollyEnd.set(0, distance);
                    dollyDelta.subVectors(dollyEnd, dollyStart);

                    if (dollyDelta.y > 0) {

                        constraint.dollyOut(getZoomScale());

                    } else if (dollyDelta.y < 0) {

                        constraint.dollyIn(getZoomScale());

                    }

                    dollyStart.copy(dollyEnd);

                    hasChanges = scope.update();
                    break;

                default:

                    state = STATE.NONE;

            }

            if (hasChanges) {
                event.preventDefault();
                event.stopPropagation();
            }
        }

        function touchend(/* event */) {

            if (scope.enabled === false) return;

            scope.dispatchEvent(endEvent);
            state = STATE.NONE;

        }

        this.dispose = function () {
            this.domElement.removeEventListener('mousedown', onMouseDown, false);
            this.domElement.removeEventListener('mousewheel', onMouseWheel, false);
            this.domElement.removeEventListener('MozMousePixelScroll', onMouseWheel, false); // firefox

            this.domElement.removeEventListener('touchstart', touchstart, false);
            this.domElement.removeEventListener('touchend', touchend, false);
            this.domElement.removeEventListener('touchmove', touchmove, false);

            document.removeEventListener('mousemove', onMouseMove, false);
            document.removeEventListener('mouseup', onMouseUp, false);
        }

        this.domElement.addEventListener('mousedown', onMouseDown, false);

        this.domElement.addEventListener('touchstart', touchstart, false);
        this.domElement.addEventListener('touchend', touchend, false);
        this.domElement.addEventListener('touchmove', touchmove, false);

        this.enableZoom = function()
        {
            this.domElement.addEventListener('mousewheel', onMouseWheel, false);
            this.domElement.addEventListener('MozMousePixelScroll', onMouseWheel, false); // firefox
        }
        // force an update at start
        this.update();

    };

    HelmControls.prototype = Object.create(THREE.EventDispatcher.prototype);
    HelmControls.prototype.constructor = HelmControls;

    Object.defineProperties(HelmControls.prototype, {

        object: {

            get: function () {

                return this.constraint.object;

            }

        },

        target: {

            get: function () {

                return this.constraint.target;

            },

            set: function (value) {

                console.warn('HelmControls: target is now immutable. Use target.set() instead.');
                this.constraint.target.copy(value);

            }

        },

        minDistance: {

            get: function () {

                return this.constraint.minDistance;

            },

            set: function (value) {

                this.constraint.minDistance = value;

            }

        },

        maxDistance: {

            get: function () {

                return this.constraint.maxDistance;

            },

            set: function (value) {

                this.constraint.maxDistance = value;

            }

        },

        minZoom: {

            get: function () {

                return this.constraint.minZoom;

            },

            set: function (value) {

                this.constraint.minZoom = value;

            }

        },

        maxZoom: {

            get: function () {

                return this.constraint.maxZoom;

            },

            set: function (value) {

                this.constraint.maxZoom = value;

            }

        },

        minPolarAngle: {

            get: function () {

                return this.constraint.minPolarAngle;

            },

            set: function (value) {

                this.constraint.minPolarAngle = value;

            }

        },

        maxPolarAngle: {

            get: function () {

                return this.constraint.maxPolarAngle;

            },

            set: function (value) {

                this.constraint.maxPolarAngle = value;

            }

        },

        minAzimuthAngle: {

            get: function () {

                return this.constraint.minAzimuthAngle;

            },

            set: function (value) {

                this.constraint.minAzimuthAngle = value;

            }

        },

        maxAzimuthAngle: {

            get: function () {

                return this.constraint.maxAzimuthAngle;

            },

            set: function (value) {

                this.constraint.maxAzimuthAngle = value;

            }

        },

        enableDamping: {

            get: function () {

                return this.constraint.enableDamping;

            },

            set: function (value) {

                this.constraint.enableDamping = value;

            }

        },

        dampingFactor: {

            get: function () {

                return this.constraint.dampingFactor;

            },

            set: function (value) {

                this.constraint.dampingFactor = value;

            }

        },

    });

    return HelmControls;
});

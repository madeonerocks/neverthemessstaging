define([
    "hr/CDN",
    "hr/MaterialFactory",
    "vendor/three"
], function (CDN, MaterialFactory) {

    var factory = new MaterialFactory();
    factory.defaultMaterial = function () {
        return null
    };

    var loader = new THREE.TextureLoader();
    loader.setCrossOrigin('');

    factory.addMaterial('uv', function () {

        var uv = loader.load(CDN.path + "gl/mat/UV_Grid_Sm.jpg")
        uv.wrapS = THREE.RepeatWrapping;
        uv.wrapT = THREE.RepeatWrapping;
        uv.repeat = new THREE.Vector2(1, 1);

        var uvMaterial = new THREE.MeshLambertMaterial({
            color: 0xffffff,
            map: uv
        });

        return uvMaterial;
    });

    factory.addMaterial('uvSmall', function () {

        var uv = loader.load(CDN.path + "gl/mat/UV_Grid_Sm.jpg")
        uv.wrapS = THREE.RepeatWrapping;
        uv.wrapT = THREE.RepeatWrapping;
        uv.repeat = new THREE.Vector2(10, 10);

        var uvMaterial = new THREE.MeshLambertMaterial({
            color: 0xffffff,
            map: uv
        });

        return uvMaterial;
    });

    factory.addMaterial('wireframe', function () {

        var uvMaterial = new THREE.MeshPhongMaterial({
            color: 0x6666ff,
            specular: 0x0,
            shininess: 10,
            side: THREE.FrontSide,
            wireframe: true
        });

        return uvMaterial;
    });

    factory.addMaterial('white', function () {

        var uvMaterial = new THREE.MeshPhongMaterial({
            color: 0xffffff,
            specular: 0x0,
            shininess: 10,
            side: THREE.FrontSide,
        });

        return uvMaterial;
    });
    //factory.addMaterial('wireframe', function () {
    //
    //
    //
    //    return new THREE.MeshBasicMaterial( { color: 0xff0000} );
    //});

    return {
        get: function (name) {
            return factory.mat(name);
        }
    };
});

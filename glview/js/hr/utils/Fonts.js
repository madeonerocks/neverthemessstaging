define(['jquery'], function ($) {

    function loadFonts() {
        var cssLocation = "https://cdn.helmade.com/gl/fonts/fonts-declarations.css";
        var cssLink = $("<link rel='stylesheet' type='text/css' href='" + cssLocation + "'>");
        $("head").append(cssLink);
    }

    var fontSpy = function (fontName, conf) {
        var $html = $('html'),
            $body = $('body'),
            fontFamilyName = fontName;

        // Throw error if fontName is not a string or not is left as an empty string
        if (typeof fontFamilyName !== 'string' || fontFamilyName === '') {
            throw 'A valid fontName is required. fontName must be a string and must not be an empty string.';
        }

        var defaults = {
            font: fontFamilyName,
            fontClass: fontFamilyName.toLowerCase().replace(/\s/g, ''),
            success: function () {
            },
            failure: function () {
            },
            testFont: 'Courier New',
            testString: 'QW@HhsXJ',
            glyphs: '',
            delay: 50,
            timeOut: 1000,
            callback: $.noop
        };

        var config = $.extend(defaults, conf);

        var $tester = $('<span>' + config.testString + config.glyphs + '</span>')
            .css('position', 'absolute')
            .css('top', '-9999px')
            .css('left', '-9999px')
            .css('visibility', 'hidden')
            .css('fontFamily', config.testFont)
            .css('fontSize', '250px');

        $body.append($tester);

        var fallbackFontWidth = $tester.outerWidth();

        $tester.css('fontFamily', config.font + ',' + config.testFont);

        var failure = function () {
            $html.addClass("no-" + config.fontClass);
            if (config && config.failure) {
                config.failure();
            }
            config.callback(new Error('FontSpy timeout'));
            $tester.remove();
        };

        var success = function () {
            config.callback();
            $html.addClass(config.fontClass);
            if (config && config.success) {
                config.success();
            }
            $tester.remove();
        };

        var retry = function () {
            setTimeout(checkFont, config.delay);
            config.timeOut = config.timeOut - config.delay;
        };

        var checkFont = function () {
            var loadedFontWidth = $tester.outerWidth();

            if (fallbackFontWidth !== loadedFontWidth) {
                success();
            } else if (config.timeOut < 0) {
                failure();
            } else {
                retry();
            }
        };

        checkFont();
    };

    return {
        start: function () {
            loadFonts();
        },

        getFont: function (name, complete) {

            function doIt() {
                fontSpy(name, {
                    success: function () {
                        //console.log("SUCCESS: " + name);
                        complete();
                    }
                    ,
                    failure: function () {
                        console.warn("Font not loaded: " + name);
                        setTimeout(doIt, 1000);
                    }
                });
            }

            doIt();
        }
    }
});
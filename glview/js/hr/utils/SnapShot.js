define([], function () {

    function SnapShot(snapSize) {

        this.texture = new THREE.WebGLRenderTarget(snapSize, snapSize, {
            format: THREE.RGBAFormat,
        });

        this.takeSnapshot = function(renderer, camera, scene)
        {
            console.log("snapshot...")
            camera.aspect = 1;
            camera.updateProjectionMatrix();

            renderer.render(scene, camera, this.texture, true);

            var size = renderer.getSize();
            camera.aspect = size.width / size.height;
            camera.updateProjectionMatrix();

            var pixels = new Uint8Array(snapSize * snapSize * 4);
            renderer.readRenderTargetPixels(this.texture, 0, 0, snapSize, snapSize, pixels);

            var canvas = document.createElement('canvas');
            canvas.width = snapSize;
            canvas.height = snapSize;
            var canvasContext = canvas.getContext('2d');
            var imgdata = canvasContext.createImageData(snapSize, snapSize);
            var imgdatalen = imgdata.data.length;
            for (var i = 0; i < imgdatalen; i++) {
                imgdata.data[i] = pixels[i];
            }
            canvasContext.putImageData(imgdata, 0, 0);

            var aa = 2;

            var canvas2 = document.createElement('canvas');
            canvas2.width = snapSize / aa;
            canvas2.height = snapSize / aa;

            var canvasContext2 = canvas2.getContext('2d');
            canvasContext2.scale(1, -1);
            canvasContext2.translate(0, -snapSize / aa);
            canvasContext2.drawImage(canvas, 0, 0, snapSize / aa, snapSize / aa);

            var canvas3 = document.createElement('canvas');
            canvas3.width = snapSize / aa / aa;
            canvas3.height = snapSize / aa / aa;

            var canvasContext3 = canvas3.getContext('2d');
            canvasContext3.drawImage(canvas2, 0, 0, snapSize / aa / aa, snapSize / aa / aa);

            var image = canvas3.toDataURL("image/png").replace("image/png", "image/octet-stream");
            console.log("snapshot... set href")
            window.location.href = image;
        }

    }

    return SnapShot;
});
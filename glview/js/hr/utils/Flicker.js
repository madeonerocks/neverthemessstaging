define([
    "text!/glview/js/hr/utils/Flicker.vert",
    "text!/glview/js/hr/utils/Flicker.frag",
], function (vertexShader,
             fragmentShader) {

    function Flicker() {
    }

    var material;
    var scene;
    var camera;
    var initialized = false;
    var frame = 0;

    Flicker.render = function (renderer) {
        Flicker.initialize(renderer);

        var size = renderer.getSize();

        camera.top = 0;
        camera.bottom = size.height;
        camera.left = 0;
        camera.right = size.width;
        camera.updateProjectionMatrix();

        material.uniforms.color.value = [frame % 2, frame % 2, frame % 2, 0.5];
        renderer.render(scene, camera);

        frame++;
    }

    Flicker.initialize = function (renderer) {
        if (initialized)
            return;
        initialized = true;

        scene = new THREE.Scene();
        camera = new THREE.OrthographicCamera(0, 1, 0, 1, -10000, 10000);

        material = new THREE.ShaderMaterial({
            uniforms: {
                color: {type: "4f"},
            },
            vertexShader: vertexShader,
            fragmentShader: fragmentShader,
            side: THREE.DoubleSide,
            depthWrite: false
        });

        var quad = new THREE.Mesh(new THREE.PlaneBufferGeometry(1, 1), material);
        quad.position.x = 0.5;
        quad.position.y = 0.5;
        quad.scale.y = -1;

        var sss = new THREE.Object3D();
        sss.position.x = 110;
        sss.position.y = 10;
        sss.scale.x = 20;
        sss.scale.y = 20;
        sss.add(quad);

        scene.add(sss);
    }

    return Flicker;
});
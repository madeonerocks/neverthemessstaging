define([
    "text!/glview/shader/fragment_shader_screen.glsl",
    "text!/glview/shader/vertex_shader.glsl",
], function (fragmentShaderScreen,
             vertexShader) {

    function DebugScene() {

        var sceneScreen = new THREE.Scene();

        var cameraRTT = new THREE.OrthographicCamera(0, 1, 0, 1, -10000, 10000);

        this.layout = function (size) {
            cameraRTT.top = 0;
            cameraRTT.bottom = size.height;

            cameraRTT.left = 0;
            cameraRTT.right = size.width;
            cameraRTT.updateProjectionMatrix();
        }

        this.render = function (renderer) {
            renderer.render(sceneScreen, cameraRTT);
        }

        this.addTexture = function (texture, x, y, width, height) {
            var materialScreen2 = new THREE.ShaderMaterial({
                uniforms: {tDiffuse: {type: "t", value: texture}},
                vertexShader: vertexShader,
                fragmentShader: fragmentShaderScreen,
                side: THREE.DoubleSide,
                depthWrite: false
            });

            var quad = new THREE.Mesh(new THREE.PlaneBufferGeometry(1, 1), materialScreen2);
            quad.position.x = 0.5;
            quad.position.y = 0.5;
            quad.scale.y = -1;

            var sss = new THREE.Object3D();
            sss.position.x = x;
            sss.position.y = y;
            sss.scale.x = width;
            sss.scale.y = height;
            sss.add(quad);

            sceneScreen.add(sss);
        }
    }

    return DebugScene;
})
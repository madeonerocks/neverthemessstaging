define("Frame",['vendor/signals-1.0.0'], function (signals) {

    function FullScreenFrame() {

        var self = this;

        var renderer = this.renderer = new THREE.WebGLRenderer({antialias: true, alpha:true});
        renderer.setClearColor(0x111111);
        renderer.setPixelRatio(window.devicePixelRatio);

        var jr = $(renderer.domElement);
        jr.css("position", "absolute");
        jr.css("left", "0px");
        jr.css("top", "0px");
        document.body.appendChild(renderer.domElement);

        window.addEventListener("resize", function () {
            self.layout();
        });

        var onLayout = this.onLayout = new signals.Signal();
        var onFrame = this.onFrame = new signals.Signal();

        var layout = this.layout = function () {
            renderer.setSize(window.innerWidth, window.innerHeight);
            onLayout.dispatch();
        };

        this.start = function () {

            function startLoop()
            {
                layout();

                var frame = 0;

                function animate() {
                    requestAnimationFrame(animate);

                    if (frame >= 0)
                        onFrame.dispatch(frame);
                    frame++;
                }

                animate();
            }

            renderer.setSize(window.innerWidth, window.innerHeight);
            renderer.clear();

            setTimeout(startLoop, 100);
        };


    };

    return FullScreenFrame;
});
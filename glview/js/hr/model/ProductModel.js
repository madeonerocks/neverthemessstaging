define([
    'hr/CDN',
], function (CDN) {

    function ProductModel(basisModel, designLine) {
        var helmUrl = '';
        var designUrl = '';

        this.helmXRotation = -12 * Math.PI / 180;

        switch(basisModel)
        {
            case "arai_sk6":
                helmUrl = CDN.path + 'gl/helmets/arai_sk6/';
                designUrl = CDN.path + 'gl/helmets/arai_gp6s/';
                console.warn('special urls for model: ' + basisModel, helmUrl, designUrl);
                break;

            // case "arai_ck6":
            //     helmUrl = CDN.path + 'gl/helmets/arai_ck6/';
            //     designUrl = CDN.path + 'gl/helmets/arai_ck6/';
            //     console.warn('special urls for model: ' + basisModel, helmUrl, designUrl);
            //     break;
            //
            // case "arai_gp6s":
            //     helmUrl = CDN.path + 'gl/helmets/arai_gp6s/';
            //     designUrl = CDN.path + 'gl/helmets/arai_gp6s/';
            //     console.warn('special urls for model: ' + basisModel, helmUrl, designUrl);
            //     break;
            //
            // case "helmade_one":
            //     helmUrl = CDN.path + 'gl/helmets/helmade_one/';
            //     designUrl = CDN.path + 'gl/helmets/helmade_one/';
            //     console.warn('special urls for model: ' + basisModel, helmUrl, designUrl);
            //     break;
            //
            // case "bell_bullitt":
            //     helmUrl = CDN.path + 'gl/helmets/bell_bullitt/';
            //     designUrl = CDN.path + 'gl/helmets/bell_bullitt/';
            //     console.warn('special urls for model: ' + basisModel, helmUrl, designUrl);
            //     break;

            default:
                helmUrl = CDN.path + 'gl/helmets/' + basisModel + '/';
                designUrl = CDN.path + 'gl/helmets/' + basisModel + '/';
        }

        this.getObjUrl = function()
        {
            return helmUrl + 'model.obj';
        }

        this.getConfigUrl = function()
        {
            return helmUrl + 'model.txt';
        }

        this.getShadingUrl = function()
        {
            return helmUrl + 'shading.jpg';
        }

        this.getWhiteStickerUrl = function()
        {
            return helmUrl + 'visorsticker_white.png';
        }

        this.getBlackStickerUrl = function()
        {
            return helmUrl + 'visorsticker_black.png';
        }

        this.getInterestUrl = function()
        {
            return designUrl + designLine + '/interest.png';
        }

        this.getDesignUrl = function()
        {
            return designUrl + designLine + '/design.txt';
        }

        this.getPreviewUrl = function()
        {
            return designUrl + designLine + '/preview.png?date=' + new Date().getTime();
        }

        this.getMaskUrl = function(layerIndex)
        {
            return designUrl + designLine + '/mask' + layerIndex + '.png';
        }

        this.getOverlayUrl = function(layerIndex)
        {
            return designUrl + designLine + '/overlay.png';
        }

        this.getSideProjectionMatrix = function()
        {
            var inverseScale = this.designJson.signature.inverseScale;
            var rotZAngle = this.designJson.signature.rotZAngle;
            var upDownAngle = this.designJson.signature.upDownAngle;
            var frontBackAngle = this.designJson.signature.frontBackAngle;

            var projectionMatrixSide = new THREE.Matrix4();
            projectionMatrixSide.identity();
            projectionMatrixSide.multiply(new THREE.Matrix4().makeTranslation(0.5, 0.5, 0));
            projectionMatrixSide.multiply(new THREE.Matrix4().makeScale(1 * inverseScale, 4 * inverseScale, 1));
            projectionMatrixSide.multiply(new THREE.Matrix4().makeRotationZ(rotZAngle * Math.PI / 180 ));
            projectionMatrixSide.multiply(new THREE.Matrix4().makeRotationX(upDownAngle * Math.PI / 180));//up down
            projectionMatrixSide.multiply(new THREE.Matrix4().makeRotationY(3.141 / 2 + frontBackAngle * Math.PI / 180));//front back
            projectionMatrixSide.multiply(new THREE.Matrix4().makeRotationX(-this.helmXRotation));

            return projectionMatrixSide;
        }

        this.getLogoProjectionMatrix = function()
        {
            var inverseScale = this.designJson.logo.inverseScale;
            var upDownAngle =  this.designJson.logo.upDownAngle;

            var projectionMatrixBack = new THREE.Matrix4();
            projectionMatrixBack.identity();
            projectionMatrixBack.multiply(new THREE.Matrix4().makeTranslation(0.5, 0.5, 0));
            projectionMatrixBack.multiply(new THREE.Matrix4().makeScale(inverseScale, inverseScale * 2, 1));
            projectionMatrixBack.multiply(new THREE.Matrix4().makeRotationX(upDownAngle * Math.PI / 180));
            projectionMatrixBack.multiply(new THREE.Matrix4().makeRotationX(-this.helmXRotation));

            return projectionMatrixBack;
        }

        this.getIconMarker = function()
        {
            if (this.designJson.icon == null)
            {
                //no icon
                return 0;
            }
            else
            {
                if (this.designJson.icon.type == null)
                {
                    //original retrospective
                    return 1;
                }
                else
                {
                    //future icon types (e.g. 2 -> ferrari
                    return this.designJson.icon.type;
                }
            }
        }

        this.hasProducerLogo = function()
        {
            if (this.designJson == null)
                return false;

            return this.designJson.producer_logo != null;
        }

        this.hasCustomLogo = function()
        {
            return productData.custom_logo.length > 0;
        }

        this.getCustomLogo = function()
        {
            return productData.custom_logo;
        }

        this.getIconProjectionMatrix = function()
        {
            //original retrospective
            var inverseScale = 1.6;
            var rotZAngle = 9;
            var upDownAngle = -10;
            var frontBackAngle = 2;


            if (this.designJson.icon != null && this.designJson.icon.type > 1)
            {
                inverseScale = this.designJson.icon.icon1.inverseScale;
                rotZAngle = this.designJson.icon.icon1.rotZAngle;
                upDownAngle = this.designJson.icon.icon1.upDownAngle;
                frontBackAngle = this.designJson.icon.icon1.frontBackAngle;
            }

            var projectionMatrixSide = new THREE.Matrix4();
            projectionMatrixSide.identity();
            projectionMatrixSide.multiply(new THREE.Matrix4().makeTranslation(0.5, 0.5, 0));
            projectionMatrixSide.multiply(new THREE.Matrix4().makeScale(inverseScale, inverseScale, 1));
            projectionMatrixSide.multiply(new THREE.Matrix4().makeRotationZ(rotZAngle * Math.PI / 180 ));
            projectionMatrixSide.multiply(new THREE.Matrix4().makeRotationX(upDownAngle * Math.PI / 180));//up down
            projectionMatrixSide.multiply(new THREE.Matrix4().makeRotationY(3.141 / 2 + frontBackAngle * Math.PI / 180));//front back
            projectionMatrixSide.multiply(new THREE.Matrix4().makeRotationX(-this.helmXRotation));

            return projectionMatrixSide;
        }

        this.getIcon2ProjectionMatrix = function()
        {
            var inverseScale = 0;
            var rotZAngle = 0;
            var upDownAngle = 0;
            var frontBackAngle = 0;

            if (this.designJson.icon != null && this.designJson.icon.type > 1)
            {
                inverseScale = this.designJson.icon.icon2.inverseScale;
                rotZAngle = this.designJson.icon.icon2.rotZAngle;
                upDownAngle = this.designJson.icon.icon2.upDownAngle;
                frontBackAngle = this.designJson.icon.icon2.frontBackAngle;
            }

            var projectionMatrixSide = new THREE.Matrix4();
            projectionMatrixSide.identity();
            projectionMatrixSide.multiply(new THREE.Matrix4().makeTranslation(0.5, 0.5, 0));
            projectionMatrixSide.multiply(new THREE.Matrix4().makeScale(inverseScale, inverseScale, 1));
            projectionMatrixSide.multiply(new THREE.Matrix4().makeRotationZ(rotZAngle * Math.PI / 180 ));
            projectionMatrixSide.multiply(new THREE.Matrix4().makeRotationX(upDownAngle * Math.PI / 180));//up down
            projectionMatrixSide.multiply(new THREE.Matrix4().makeRotationY(3.141 / 2 + frontBackAngle * Math.PI / 180));//front back
            projectionMatrixSide.multiply(new THREE.Matrix4().makeRotationX(-this.helmXRotation));

            return projectionMatrixSide;
        }

        this.getProducerLogoProjectionMatrix = function()
        {
            var inverseScale = 0;
            var rotZAngle = 0;
            var upDownAngle = 0;
            var frontBackAngle = 90;

            if (this.designJson.producer_logo != null)
            {
                inverseScale = this.designJson.producer_logo.inverseScale;
                upDownAngle = this.designJson.producer_logo.upDownAngle;
            }

            if (upDownAngle > 90)
                rotZAngle = (upDownAngle > 90) ? 180 : 0;

            var producerLogoProjectionMatrix = new THREE.Matrix4();
            producerLogoProjectionMatrix.identity();
            producerLogoProjectionMatrix.multiply(new THREE.Matrix4().makeTranslation(0.5, 0.5, 0));
            producerLogoProjectionMatrix.multiply(new THREE.Matrix4().makeScale(inverseScale, inverseScale, 1));
            producerLogoProjectionMatrix.multiply(new THREE.Matrix4().makeRotationZ(rotZAngle * Math.PI / 180 ));
            producerLogoProjectionMatrix.multiply(new THREE.Matrix4().makeRotationX(upDownAngle * Math.PI / 180));//up down
            producerLogoProjectionMatrix.multiply(new THREE.Matrix4().makeRotationY(3.141 / 2 + frontBackAngle * Math.PI / 180));//front back
            producerLogoProjectionMatrix.multiply(new THREE.Matrix4().makeRotationX(-this.helmXRotation));

            // console.log(producerLogoProjectionMatrix.elements);

            return producerLogoProjectionMatrix;
        }
    }

    return ProductModel;
});
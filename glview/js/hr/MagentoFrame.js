define([
    "vendor/signals-1.0.0",
    "vendor/three"
], function (signals) {

    function FullScreenFrame(applyStyles) {

        var renderer = this.renderer = new THREE.WebGLRenderer({antialias: true, alpha: true});
        renderer.setClearColor(0xff1111, 0.5);
        renderer.setPixelRatio(window.devicePixelRatio);
        //renderer.setPixelRatio(1);

        var onLayout = this.onLayout = new signals.Signal();
        var onFrame = this.onFrame = new signals.Signal();
        var onStart = this.onStart = new signals.Signal();

        this.layout = function (width, height) {
            renderer.setSize(width, height, applyStyles);
            onLayout.dispatch();
        };

        this.start = function () {

            function startLoop() {
                var frame = 0;

                function animate() {
                    requestAnimationFrame(animate);

                    if (frame >= 0)
                        onFrame.dispatch(frame);
                    frame++;
                }

                animate();
            }

            renderer.clear();

            onStart.dispatch();
            setTimeout(startLoop, 100);
        };


    };

    return FullScreenFrame;
});
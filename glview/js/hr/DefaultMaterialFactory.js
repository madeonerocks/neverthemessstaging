define(["hr/CDN", "hr/MaterialFactory"], function (CDN, Super) {

    function DefaultMaterialFactory() {
        Super.call(this);

        var textureLoader = new THREE.TextureLoader();
        textureLoader.setCrossOrigin('');

        var cubeTextureLoader = new THREE.CubeTextureLoader();
        cubeTextureLoader.setCrossOrigin('');

        var loader = new THREE.TextureLoader();
        loader.setCrossOrigin('');

        var path = CDN.path + "gl/mat/pisa_blur/";
        var format = '.png';
        var urls = [
            path + 'px' + format, path + 'nx' + format,
            path + 'py' + format, path + 'ny' + format,
            path + 'pz' + format, path + 'nz' + format
        ];
        var reflectionCubeBlur = cubeTextureLoader.load(urls);
        reflectionCubeBlur.format = THREE.RGBFormat;

        var path = CDN.path + "gl/mat/pisa/";
        var format = '.png';
        var urls = [
            path + 'px' + format, path + 'nx' + format,
            path + 'py' + format, path + 'ny' + format,
            path + 'pz' + format, path + 'nz' + format
        ];
        var reflectionCube = cubeTextureLoader.load(urls);
        reflectionCube.format = THREE.RGBFormat;

        var envMap2 = loader.load(CDN.path + "gl/mat/mirror/ModelTest/" + "env/9.jpg");
        envMap2.mapping = THREE.EquirectangularReflectionMapping;

        var metalTexture = textureLoader.load(CDN.path + "gl/mat/10299-normal.jpg");
        metalTexture.wrapS = metalTexture.wrapT = THREE.RepeatWrapping;
        metalTexture.repeat.set(7, 7);

        this.addMaterial('shellMint1', function () {
            return new THREE.MeshPhongMaterial({
                color: 0xffffff,
                specular: 0x202020,
                shininess: 80,
            });
        });

        this.addMaterial('shellTexture', function () {
            return new THREE.MeshPhongMaterial({
                color: 0xffffff,
                specular: 0x202020,
                shininess: 80,
            });
        });

        this.addMaterial('shellTexture2', function () {
            return new THREE.MeshPhongMaterial({
                color: 0xffffff,
                specular: 0x202020,
                shininess: 80,
            });
        });

        this.addMaterial('shellRed1', function () {
            return new THREE.MeshPhongMaterial({
                color: 0xFA9BD0,
                specular: 0x060606,
                shininess: 80,
                //side: THREE.DoubleSide,
                envMap: reflectionCube,
                reflectivity: 0.1,
            });
        });

        this.addMaterial('metal', function () {
            return new THREE.MeshPhongMaterial({
                color: 0xcccccc,
                specular: 0xffffff,
                shininess: 1000,
                envMap: reflectionCube,
                reflectivity: 0.9,
                metal: true,
                //side: THREE.DoubleSide,
                normalMap: metalTexture,
                normalScale: new THREE.Vector2(0.1, 0.1),
            });
        });

        this.addMaterial('clothBlack', function () {
            return new THREE.MeshPhongMaterial({
                color: 0x222222,
                specular: 0x020202,
                shininess: 1,
                //side: THREE.DoubleSide
            });
        });

        this.addMaterial('plasticBlack', function () {
            return new THREE.MeshPhongMaterial({
                color: 0x151515,
                specular: 0x333333,
                shininess: 50
            });
        });

        this.addMaterial('plasticTransparent', function () {
            return new THREE.MeshPhongMaterial({
                color: 0xffffff,
                specular: 0xffffff,
                shininess: 250,
                transparent: true,
                opacity: 0.25,
                envMap: reflectionCube,
                reflectivity: 0.9,
                metal: false,
                //side: THREE.DoubleSide,
                refractionRatio: 0.2
            });
        });

        this.addMaterial('visorGreen', function () {
            return new THREE.MeshPhongMaterial({
                color: 0x7BeAb0,
                specular: 0x333366,
                shininess: 90,
                transparent: true,
                opacity: 0.9,
                envMap: reflectionCube,
                reflectivity: 0.7,
                metal: false,
                //side: THREE.DoubleSide
            });
        });

        this.addMaterial('visorYellow', function () {
            return new THREE.MeshPhongMaterial({
                color: 0x938855,
                specular: 0x666033,
                shininess: 90,
                transparent: true,
                opacity: 0.9,
                envMap: reflectionCube,
                reflectivity: 0.7,
                metal: false,
                //side: THREE.DoubleSide
            });
        });

    };

    DefaultMaterialFactory.prototype = Object.create(Super.prototype);
    DefaultMaterialFactory.prototype.constructor = Super;

    return DefaultMaterialFactory;
});
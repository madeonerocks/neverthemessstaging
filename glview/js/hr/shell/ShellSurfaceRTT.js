define([
    "hr/shell/AbstractShellRTT",
    "text!/glview/js/hr/shell/shader/surface.frag",
    "text!/glview/js/hr/shell/shader/surface.vert"
], function (AbstractShellRTT,
             fragmentShader,
             vertexShader) {

    function ShellSurfaceRTT(shellModel)
    {
        this.shellModel = shellModel;

        AbstractShellRTT.call(this);
    }

    ShellSurfaceRTT.prototype = Object.create(AbstractShellRTT.prototype);
    ShellSurfaceRTT.prototype.constructor = AbstractShellRTT;

    ShellSurfaceRTT.prototype.createMaterial = function()
    {
        this.material = new THREE.ShaderMaterial({
            uniforms: {
                tMask1: {type: "t", value: this.shellModel.layers[1].mask},
                tMask2: {type: "t", value: this.shellModel.layers[2].mask},
                tMask3: {type: "t", value: this.shellModel.layers[3].mask},
                tMask4: {type: "t", value: this.shellModel.layers[4].mask},

                color0: {type: "c"},
                color1: {type: "c"},
                color2: {type: "c"},
                color3: {type: "c"},
                color4: {type: "c"},
            },
            vertexShader: vertexShader,
            fragmentShader: fragmentShader,
            depthWrite: false
        });

        return this.material;
    }

    ShellSurfaceRTT.prototype.updateUniforms = function()
    {
        var uniforms = this.material.uniforms;
        var layers = this.shellModel.layers;
        
        uniforms.color0.value = layers[0].surface;
        uniforms.color1.value = layers[1].surface;
        uniforms.color2.value = layers[2].surface;
        uniforms.color3.value = layers[3].surface;
        uniforms.color4.value = layers[4].surface;
    }

    return ShellSurfaceRTT;
});
define([], function () {

    function AbstractShellRTT() {

        this.texture = new THREE.WebGLRenderTarget(1024, 1024, {
            minFilter: THREE.LinearMipMapLinearFilter,
            magFilter: THREE.LinearFilter,
            format: THREE.RGBAFormat,
        });

        this.camera = new THREE.OrthographicCamera(-0.5, 0.5, 0.5, -0.5, -10000, 10000);
        this.scene = new THREE.Scene();

        this.scene.add(new THREE.Mesh(new THREE.PlaneBufferGeometry(1, 1), this.createMaterial()));
    }

    AbstractShellRTT.prototype.render = function (renderer) {
        this.updateUniforms();
        renderer.render(this.scene, this.camera, this.texture, true);
    }

    AbstractShellRTT.prototype.createMaterial = function () {
        return null;
    }

    AbstractShellRTT.prototype.updateUniforms = function () {
    }

    return AbstractShellRTT;
});
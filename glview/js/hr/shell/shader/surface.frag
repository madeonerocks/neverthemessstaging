varying vec2 vUv;

uniform sampler2D tMask1;
uniform sampler2D tMask2;
uniform sampler2D tMask3;
uniform sampler2D tMask4;

uniform vec3 color0;
uniform vec3 color1;
uniform vec3 color2;
uniform vec3 color3;
uniform vec3 color4;

void main()
{
	float t1 = texture2D( tMask1, vUv ).r;
	float t2 = texture2D( tMask2, vUv ).r;
	float t3 = texture2D( tMask3, vUv ).r;
	float t4 = texture2D( tMask4, vUv ).r;

	vec3 rgb = color0;
	rgb = mix(rgb, color1, t1);
	rgb = mix(rgb, color2, t2);
	rgb = mix(rgb, color3, t3);
	rgb = mix(rgb, color4, t4);

	gl_FragColor = vec4(rgb, 1);
}
//creates shell texture
//possible optimization: dont render empty masks

varying vec2 vUv;

uniform sampler2D tMask1;
uniform sampler2D tMask2;
uniform sampler2D tMask3;
uniform sampler2D tMask4;

uniform sampler2D tOverlay;

uniform mat4 mGradient;

uniform float overlay0;
uniform float overlay1;
uniform float overlay2;
uniform float overlay3;
uniform float overlay4;

uniform vec3 colorA0;
uniform vec3 colorA1;
uniform vec3 colorA2;
uniform vec3 colorA3;
uniform vec3 colorA4;

uniform vec3 colorB0;
uniform vec3 colorB1;
uniform vec3 colorB2;
uniform vec3 colorB3;
uniform vec3 colorB4;

uniform float highlight0;
uniform float highlight1;
uniform float highlight2;
uniform float highlight3;
uniform float highlight4;

float cubicPulse( float c, float w, float x )
{
    x = abs(x - c);
    if( x>w ) return 0.0;
    x /= w;
    return 1.0 - x*x*(3.0-2.0*x);
}

float sine( float x )
{
    return sin(x * 3.14159);
}

float sine2( float x )
{
    return (1.0 - sin(x * 3.14159) * 0.1);
}

float ease(float x)
{
	float p = sine(x);
	return (sin((vUv.x - 0.0) * (vUv.y - 1.0) * 1000.0 + x * 10.0) * 0.1) * p;
}

float getLuminance(vec3 rgb)
{
   // Algorithm from Chapter 10 of Graphics Shaders.
   const vec3 W = vec3(0.2125, 0.7154, 0.0721);
   return dot(rgb, W);
}

void main() {
	//rotate gradient
	float gradient = clamp((mGradient * vec4(vUv, 0, 1)).x, 0.0, 1.0);

	float tOverlay = texture2D( tOverlay, vUv).r;

	//smooth curve to avoid hard corners
	gradient = exp(-4.0 * pow(gradient, 3.0));

	vec3 colorHighlight0 = vec3(1, 1, 1) * ease(highlight0);
	vec3 colorHighlight1 = vec3(1, 1, 1) * ease(highlight1);
	vec3 colorHighlight2 = vec3(1, 1, 1) * ease(highlight2);
	vec3 colorHighlight3 = vec3(1, 1, 1) * ease(highlight3);
	vec3 colorHighlight4 = vec3(1, 1, 1) * ease(highlight4);

	//create gradient
	vec3 color0 = mix(colorA0, colorB0, gradient) + colorHighlight0;
	vec3 color1 = mix(colorA1, colorB1, gradient) + colorHighlight1;
	vec3 color2 = mix(colorA2, colorB2, gradient) + colorHighlight2;
	vec3 color3 = mix(colorA3, colorB3, gradient) + colorHighlight3;
	vec3 color4 = mix(colorA4, colorB4, gradient) + colorHighlight4;

	//add overlays
	color0 = mix(color0, vec3(overlay0), tOverlay);
	color1 = mix(color1, vec3(overlay1), tOverlay);
	color2 = mix(color2, vec3(overlay2), tOverlay);
	color3 = mix(color3, vec3(overlay3), tOverlay);
	color4 = mix(color4, vec3(overlay4), tOverlay);

	//lookup masks
	float t1 = texture2D( tMask1, vUv).r;
	float t2 = texture2D( tMask2, vUv).r;
	float t3 = texture2D( tMask3, vUv).r;
	float t4 = texture2D( tMask4, vUv).r;

	//mix colors over maks
	vec3 rgb = color0;
	rgb = mix(rgb, color1, t1);
	rgb = mix(rgb, color2, t2);
	rgb = mix(rgb, color3, t3);
	rgb = mix(rgb, color4, t4);

	gl_FragColor = vec4(rgb, 1);

}
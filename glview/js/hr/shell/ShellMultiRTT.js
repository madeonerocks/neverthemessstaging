define([
    "hr/shell/ShellDiffuseRTT",
    "hr/shell/ShellSurfaceRTT",
], function (ShellDiffuseRTT,
             ShellSurfaceRTT) {

    function ShellMultiRTT(model) {

        this.diffuseRTT = new ShellDiffuseRTT(model);
        this.surfaceRTT = new ShellSurfaceRTT(model);

        this.render = function (renderer) {

            model.render();

            if (model.renderNeedsUpdate > 0)
            {
                model.renderNeedsUpdate--;

                //if (model.renderNeedsUpdate == 0)
                //    console.log("ShellMultiRTT now paused...");

                this.diffuseRTT.render(renderer);
                this.surfaceRTT.render(renderer);
            }
        }
    }

    return ShellMultiRTT;

});
define([
    "hr/shell/AbstractShellRTT",
    "text!/glview/js/hr/shell/shader/diffuse.frag",
    "text!/glview/js/hr/shell/shader/diffuse.vert"
], function (AbstractShellRTT,
             fragmentShader,
             vertexShader) {

    function ShellDiffuseRTT(shellModel)
    {
        this.shellModel = shellModel;

        AbstractShellRTT.call(this);
    }

    ShellDiffuseRTT.prototype = Object.create(AbstractShellRTT.prototype);
    ShellDiffuseRTT.prototype.constructor = AbstractShellRTT;

    ShellDiffuseRTT.prototype.createMaterial = function()
    {
        this.material = new THREE.ShaderMaterial({
            uniforms: {
                tOverlay: {type: "t", value: this.shellModel.overlay},
                tMask1: {type: "t", value: this.shellModel.layers[1].mask},
                tMask2: {type: "t", value: this.shellModel.layers[2].mask},
                tMask3: {type: "t", value: this.shellModel.layers[3].mask},
                tMask4: {type: "t", value: this.shellModel.layers[4].mask},

                mGradient: {type: "m4"},

                colorA0: {type: "c"},
                colorA1: {type: "c"},
                colorA2: {type: "c"},
                colorA3: {type: "c"},
                colorA4: {type: "c"},

                highlight0: {type: "f"},
                highlight1: {type: "f"},
                highlight2: {type: "f"},
                highlight3: {type: "f"},
                highlight4: {type: "f"},

                overlay0: {type: "f", value:0},
                overlay1: {type: "f", value:0},
                overlay2: {type: "f", value:0},
                overlay3: {type: "f", value:0},
                overlay4: {type: "f", value:0},

                colorB0: {type: "c"},
                colorB1: {type: "c"},
                colorB2: {type: "c"},
                colorB3: {type: "c"},
                colorB4: {type: "c"},
            },
            vertexShader: vertexShader,
            fragmentShader: fragmentShader,
            depthWrite: false
        });

        return this.material;
    }

    ShellDiffuseRTT.prototype.updateUniforms = function()
    {
        var uniforms = this.material.uniforms;
        var layers = this.shellModel.layers;

        uniforms.overlay0.value = layers[0].currentColorConfig.logo_color;
        uniforms.overlay1.value = layers[1].currentColorConfig.logo_color;
        uniforms.overlay2.value = layers[2].currentColorConfig.logo_color;
        uniforms.overlay3.value = layers[3].currentColorConfig.logo_color;
        uniforms.overlay4.value = layers[4].currentColorConfig.logo_color;

        uniforms.colorA0.value = layers[0].colorA;
        uniforms.colorA1.value = layers[1].colorA;
        uniforms.colorA2.value = layers[2].colorA;
        uniforms.colorA3.value = layers[3].colorA;
        uniforms.colorA4.value = layers[4].colorA;

        uniforms.colorB0.value = layers[0].colorB;
        uniforms.colorB1.value = layers[1].colorB;
        uniforms.colorB2.value = layers[2].colorB;
        uniforms.colorB3.value = layers[3].colorB;
        uniforms.colorB4.value = layers[4].colorB;

        uniforms.highlight0.value = layers[0].highlight;
        uniforms.highlight1.value = layers[1].highlight;
        uniforms.highlight2.value = layers[2].highlight;
        uniforms.highlight3.value = layers[3].highlight;
        uniforms.highlight4.value = layers[4].highlight;

        var mGradient = new THREE.Matrix4();

        try
        {
            var gradientDef = this.shellModel.productModel.designJson.gradient;

            var scale = gradientDef.scale;

            mGradient.multiply(new THREE.Matrix4().makeTranslation(0.5, 0.5, 0.5));
            mGradient.multiply(new THREE.Matrix4().makeRotationZ(gradientDef.angle * Math.PI / 180));
            mGradient.multiply(new THREE.Matrix4().makeScale(-scale, -scale, -scale));
            mGradient.multiply(new THREE.Matrix4().makeTranslation(-0.5, -0.5, -0.5));
            mGradient.multiply(new THREE.Matrix4().makeTranslation(gradientDef.x, gradientDef.y, gradientDef.z));


            if (this.texture.width == this.texture.height && this.shellModel.productModel.designJson.asymmetric)
            {
                console.warn("Update Rendertarget Size: " + this.texture.width);
                this.texture.setSize(this.texture.width * 2, this.texture.height);
            }

        }
        catch(e)
        {
        }

        uniforms.mGradient.value = mGradient;
    }

    return ShellDiffuseRTT;
});
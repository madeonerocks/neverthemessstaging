define([
    'hr/CDN',
], function (CDN) {

    function ShellModel(productModel) {

        var self = this;
        this.productModel = productModel;
        this.renderNeedsUpdate = 10;
        var textureLoader = new THREE.TextureLoader();
        textureLoader.setCrossOrigin("");

        this.pixelContext = {
            context: null,
            width: 0,
            height: 0
        };

        var imageLoader = new THREE.ImageLoader();
        imageLoader.setCrossOrigin("");
        imageLoader.load(productModel.getInterestUrl(), function (img) {
            var canvas = document.createElement('canvas');
            canvas.width = img.width;
            canvas.height = img.height;

            var context2d = canvas.getContext('2d');
            context2d.drawImage(img, 0, 0, img.width, img.height);

            self.pixelContext.context = context2d;
            self.pixelContext.width = img.width;
            self.pixelContext.height = img.height;
        });

        this.getInterest =function (uv, point) {
            console.log(uv, point.x, self.pixelContext.width, self.pixelContext.height);

            var px = Math.round(uv.x * this.pixelContext.width);

            if (productModel.designJson.asymmetric)
            {
                if (point.x > 0)
                    px = Math.round(uv.x * this.pixelContext.width * 0.5 + this.pixelContext.width * 0.5);
                else
                    px = Math.round(uv.x * this.pixelContext.width * 0.5);
            }


            var data = self.pixelContext.context.getImageData(
                px,
                Math.round((1 - uv.y) * this.pixelContext.height), 1, 1).data;

            var r = Math.round(data[0] / 255);
            var g = Math.round(data[1] / 255);
            var b = Math.round(data[2] / 255);
            return parseInt(r + '' + g + '' + b, 2);
        }

        function forceUpdate() {
            self.renderNeedsUpdate = 10;
        };

        function createLayer(url) {

            var layer = {
                mask: null,
                highlight: 0,
                colorA: new THREE.Color(0xffffff),
                colorB: new THREE.Color(0xff0000),
                surface: new THREE.Color(0, 0, 0),
                currentColorConfig:{logo_color:0},

                makeNormal: function () {
                    this.surface = new THREE.Color(0, 0, 0);
                    forceUpdate();
                    return this;
                },

                makeMetallic: function () {
                    this.surface = new THREE.Color(1, 0, 0);
                    forceUpdate();
                    return this;
                },

                makeFlake: function () {
                    this.surface = new THREE.Color(0, 1, 0);
                    forceUpdate();
                    return this;
                },

                makeHoloFlake: function () {
                    this.surface = new THREE.Color(0, 0, 1);
                    forceUpdate();
                    return this;
                },

                setColor: function (color) {
                    this.colorA = color;
                    this.colorB = color;
                    forceUpdate();
                    return this;
                },

                setGradient: function (colorA, colorB) {
                    this.colorA = colorA;
                    this.colorB = colorB;
                    forceUpdate();
                    return this;
                },

                setHighlight: function () {
                    this.highlight = 1;
                    forceUpdate();
                    return this;
                }
            };

            if (url != null) {
                layer.mask = textureLoader.load(url, function (texture) {
                    forceUpdate();
                });
            } else {
              var canvasBlack = document.createElement('canvas');
              canvasBlack.width = 64;
              canvasBlack.height = 64;

              layer.mask = canvasBlack;
            }

            return layer;
        }

        this.overlay = textureLoader.load(productModel.getOverlayUrl(), function (texture) {
            forceUpdate();
        });

        this.layers = [];

        var mask0 = null;
        var mask1 = productModel.getMaskUrl(1);
        var mask2 = productModel.getMaskUrl(2);
        var mask3 = productModel.getMaskUrl(3);
        var mask4 = productModel.getMaskUrl(4);

        this.layers.push(createLayer(mask0));
        this.layers.push(createLayer(mask1));
        this.layers.push(createLayer(mask2));
        this.layers.push(createLayer(mask3));
        this.layers.push(createLayer(mask4));

        this.update = function () {
            forceUpdate();
        }

        this.render = function () {
            this.layers.forEach(function (layer) {
                if (layer.highlight > 0) {
                    layer.highlight -= 0.05;
                    if (layer.highlight < 0)
                        layer.highlight = 0;

                    forceUpdate();
                }
            });

        }

    }

    return ShellModel;
});

define(['jquery'], function ($) {
    var path = "https://cdn.helmade.com/";

    //if (window.location.href.indexOf("localhost:4000") != -1)
    //    path = "http://localhost:8000/";
    //
    //if (window.location.href.indexOf("localhost:5000") != -1)
    //    path = "http://localhost:8000/";
    //
    if (window.location.href.indexOf("localhost:5555") != -1)
        path = "http://localhost:8000/";

    if (window.location.href.indexOf("helmade.alienware") != -1)
        path = "http://helmade-cdn.alienware/";

    if (window.location.href.indexOf("helmadestage.to.dmdr.io") != -1)
        path = window.location.protocol + "//helmadestagecdn.to.dmdr.io/";

    //if (window.location.href.indexOf("duese.local") != -1)
    //    path = "http://localhost:8000/";

    //path = "http://cdn1.helmade.net.global.prod.fastly.net/";

    //path = "https://cdn.helmade.com/";

    return {
        loadJson: function (url, complete) {
            console.log("Loading", url)
            $.ajax({
                dataType: "json",
                url: url,
                success: complete,
                error: function () {
                    console.error("Cannot load: " + url, arguments);
                }
            });
        },

        path: path
    }
});

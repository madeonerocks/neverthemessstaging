define([], function () {

    function MaterialFactory() {

        this.cache = {};
        this.factory = {};

        this.defaultMaterial = function () {
            return new THREE.MeshPhongMaterial({
                color: 0xff6666,
                specular: 0x020202,
                shininess: 10,
                side: THREE.DoubleSide,
                wireframe: true
            })
        }
    };

    MaterialFactory.prototype.setDefaultMaterial = function (method) {
        this.defaultMaterial = method;
    }

    MaterialFactory.prototype.addMaterial = function (name, method) {
        this.factory[name] = method;
    }

    MaterialFactory.prototype.mat = function (name) {
        var result = this.cache[name];

        if (result == null)
            result = this.cache[name] = this.createMat(name);

        return result;
    }

    MaterialFactory.prototype.createMat = function (name) {

        var method = this.factory[name];

        if (method == null) {
            method = this.defaultMaterial;
        }

        return method();
    }

    return MaterialFactory;
});
/*
 * Adds signature and helmade logo to diffuse map
 */

uniform mat4 pmSide;
uniform mat4 pmBack;
uniform mat4 pmIcon;
uniform mat4 pmIcon2;
uniform mat4 pmProducerLogo;

uniform vec3 signatureColor;
uniform vec3 logoColor;
uniform float bothSides;
uniform float iconMarker;
uniform float logoMarker;
uniform float signatureColorType;

vec2 addProjections(inout vec3 diffuseMap)
{
	vec2 projectionMask = vec2(0,0);

	//project driver name
	bool flipLabel = vWorldPosition.x > 0.0;
	vec3 dirSide = normalize(vWorldPosition * vec3((flipLabel ? 1.0 : -1.0), 1, 1) + vec3(0.0, 57.0, 23.0));
	dirSide = (pmSide * vec4(dirSide, 1.0)).xyz;
	dirSide.x = flipLabel ? 1.0 - dirSide.x : dirSide.x;
	vec4 projectMap = texture2D(tProject, dirSide.xy * vec2(0.5, 0.25) + vec2(0.5, flipLabel ? 0.0 : 0.25));

	float driverVisibility = (
			dirSide.x > 0.0 && dirSide.x < 1.0 &&
            dirSide.y > 0.0 && dirSide.y < 1.0 &&
            (!flipLabel || bothSides > 0.5)
		) ? 1.0 : 0.0;

	diffuseMap = mix(diffuseMap, signatureColor, driverVisibility * projectMap.a * (1.0 - signatureColorType));
	projectionMask.x = projectMap.a * driverVisibility;
	projectionMask.y = signatureColorType * driverVisibility;

    if (logoMarker > 1.5) //producer logo enabled
    {
    	//helmade logo
    	vec3 dirBack = normalize(vWorldPosition + vec3(0.0, 40.0, 0));
    	dirBack = (pmBack * vec4(dirBack, 1.0)).xyz;
    	vec4 logoMap = texture2D(tProject, dirBack.xy * vec2(0.25, 0.25) + vec2(0.5, 0.75));
    	float logoVisibility = (
    			dirBack.z > 0.0 &&
    			dirBack.x > 0.0 && dirBack.x < 1.0 &&
    			dirBack.y > 0.0 && dirBack.y < 1.0
    		) ? 1.0 : 0.0;

    	//could be applied in expression above, but breaks Iris Pro
        float logoAlpha2 = logoMap.a * logoVisibility;

    	diffuseMap = mix(diffuseMap, logoColor, logoAlpha2);
    	projectionMask.x = max(projectionMask.x, logoAlpha2);

    	//producer logo
        vec3 dirIcon2= normalize(vWorldPosition + vec3(0, 0, -10.0));
        dirIcon2 = (pmProducerLogo * vec4(dirIcon2, 1.0)).xyz;
        vec4 iconMap2 = texture2D(tProject, dirIcon2.xy * vec2(0.25, 0.5) + vec2(0.75, 0.75));

        float dirAdjust = (pmProducerLogo[0][0] > 0.0) ? 1.0 : -1.0;

        float iconAlpha2 = (
                vWorldPosition.z * dirAdjust > 0.0 &&
                dirIcon2.x > 0.0 && dirIcon2.x < 1.0 &&
                dirIcon2.y > 0.0 && dirIcon2.y < 1.0
            ) ? 1.0 : 0.0;

        float iconBlendAlpha2 = pow(iconMap2.a, 5.0);
        diffuseMap = mix(diffuseMap, iconMap2.rgb, iconBlendAlpha2 * iconAlpha2);
        projectionMask.x = max(projectionMask.x, iconBlendAlpha2 * iconAlpha2);
    }
    else if (logoMarker > 0.5)  //producer logo disabled
    {
    	//project helmade logo
        vec3 dirBack = normalize(vWorldPosition + vec3(0.0, 40.0, 0));
        dirBack = (pmBack * vec4(dirBack, 1.0)).xyz;
        vec4 logoMap = texture2D(tProject, dirBack.xy * vec2(0.25, 0.25) + vec2(0.5, 0.75));
        float logoVisibility = (
                dirBack.z > 0.0 &&
                dirBack.x > 0.0 && dirBack.x < 1.0 &&
                dirBack.y > 0.0 && dirBack.y < 1.0
            ) ? 1.0 : 0.0;

        //could be applied in expression above, but breaks Iris Pro
        float logoAlpha2 = logoMap.a * logoVisibility;

        diffuseMap = mix(diffuseMap, logoColor, logoAlpha2);
        projectionMask.x = max(projectionMask.x, logoAlpha2);
    }
    else // no producer logo
    {
    	//project helmade logo
        vec3 dirBack = normalize(vWorldPosition + vec3(0.0, 40.0, 0));
        dirBack = (pmBack * vec4(dirBack, 1.0)).xyz;
        vec4 logoMap = texture2D(tProject, dirBack.xy * vec2(0.5, 0.5) + vec2(0.5, 0.5));
        float logoVisibility = (
                dirBack.z > 0.0 &&
                dirBack.x > 0.0 && dirBack.x < 1.0 &&
                dirBack.y > 0.0 && dirBack.y < 1.0
            ) ? 1.0 : 0.0;

        //could be applied in expression above, but breaks Iris Pro
        float logoAlpha2 = logoMap.a * logoVisibility;

        diffuseMap = mix(diffuseMap, logoColor, logoAlpha2);
        projectionMask.x = max(projectionMask.x, logoAlpha2);
    }

	//icons

	if (iconMarker > 1.5)
	{
		//ferrari 2 icons rgb
		vec3 dirIcon1 = normalize(vWorldPosition + vec3(0, 0, -10.0));
		dirIcon1 = (pmIcon * vec4(dirIcon1, 1.0)).xyz;
		vec4 iconMap1 = texture2D(tProject, dirIcon1.xy * vec2(0.5, 1.0));

		float iconAlpha1 = (
				vWorldPosition.z > 0.0 &&
				dirIcon1.x > 0.0 && dirIcon1.x < 1.0 &&
				dirIcon1.y > 0.0 && dirIcon1.y < 1.0
			) ? 1.0 : 0.0;

		float iconBlendAlpha1 = pow(iconMap1.a, 5.0);
		diffuseMap = mix(diffuseMap, iconMap1.rgb, iconBlendAlpha1 * iconAlpha1);
		projectionMask.x = max(projectionMask.x, iconBlendAlpha1 * iconAlpha1);

		vec3 dirIcon2= normalize(vWorldPosition + vec3(0, 0, -10.0));
		dirIcon2 = (pmIcon2 * vec4(dirIcon2, 1.0)).xyz;
		vec4 iconMap2 = texture2D(tProject, dirIcon2.xy * vec2(0.5, 1.0));

		float iconAlpha2 = (
				vWorldPosition.z < 0.0 &&
				dirIcon2.x > 0.0 && dirIcon2.x < 1.0 &&
				dirIcon2.y > 0.0 && dirIcon2.y < 1.0
			) ? 1.0 : 0.0;

		float iconBlendAlpha2 = pow(iconMap2.a, 5.0);
		diffuseMap = mix(diffuseMap, iconMap2.rgb, iconBlendAlpha2 * iconAlpha2);
		projectionMask.x = max(projectionMask.x, iconBlendAlpha2 * iconAlpha2);
	}
	else if (iconMarker > 0.5)
	{
		//monocrome icon
		bool flipIcon = vWorldPosition.x > 0.0;
		vec3 dirIcon = normalize(vWorldPosition * vec3((flipIcon ? 1.0 : -1.0), 1, 1) + vec3(0, 0, -10.0));
		dirIcon = (pmIcon * vec4(dirIcon, 1.0)).xyz;
		dirIcon.x = flipIcon ? 1.0 - dirIcon.x : dirIcon.x;
		vec4 iconMap = texture2D(tProject, dirIcon.xy * vec2(0.5, 1.0));

		float iconAlpha = (
				dirIcon.x > 0.0 && dirIcon.x < 1.0 &&
				dirIcon.y > 0.0 && dirIcon.y < 1.0
			) ? 1.0 : 0.0;

		float a1 = iconAlpha * iconMap.r;
		float a2 = iconAlpha * iconMap.b;
		diffuseMap = mix(diffuseMap, vec3(1,1,1), a1);
		diffuseMap = mix(diffuseMap, vec3(0,0,0), a2);
		projectionMask.x = max(projectionMask.x, a1);
		projectionMask.x = max(projectionMask.x, a2);
	}

	return projectionMask;
}
#define RECIPROCAL_PI2 0.15915494
#define PI 3.141592653

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

float saturate(float a)
{
	return clamp( a, 0.0, 1.0 );
}

vec3 inverseTransformDirection( in vec3 normal, in mat4 matrix )
{
	return normalize( ( vec4( normal, 0.0 ) * matrix ).xyz );
}

float getLuminance(vec3 rgb)
{
   // Algorithm from Chapter 10 of Graphics Shaders.
   const vec3 W = vec3(0.2125, 0.7154, 0.0721);
   return dot(rgb, W);
}

vec3 perturbNormal2Arb( sampler2D map, vec3 eye_pos, vec3 surf_norm, vec2 normalScale, float scale)
{
	vec3 q0 = dFdx( eye_pos.xyz );
	vec3 q1 = dFdy( eye_pos.xyz );
	vec2 st0 = dFdx( vUv.st );
	vec2 st1 = dFdy( vUv.st );

	vec3 S = normalize( q0 * st1.t - q1 * st0.t );
	vec3 T = normalize( -q0 * st1.s + q1 * st0.s );

	vec3 N = normalize( surf_norm );

	vec3 mapN = texture2D( map, vUv * scale ).xyz * 2.0 - 1.0;
	mapN.xy = normalScale * mapN.xy;
	mat3 tsn = mat3( S, T, N );
	return normalize( tsn * mapN );
}
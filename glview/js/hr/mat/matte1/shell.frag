uniform sampler2D tAmbient;
uniform sampler2D tDiffuse;
uniform sampler2D tSurface;

uniform sampler2D tRandomNormal;
uniform sampler2D tNormal2;
uniform sampler2D tNormal3;
uniform sampler2D tProject;
uniform sampler2D tEnv;

uniform float asymmetric;

varying vec2 vUv;
varying vec3 vViewPosition;
varying vec3 vNormal;
varying vec3 vWorldPosition;

#define include_tools
#define include_projections

/*
 * Main fagment for metallic without clear coat
 */
vec3 renderMetallic(
	vec3 normal,
	vec3 diffuseMap,
	vec3 reflectVec
) {
	float luminance =  getLuminance(diffuseMap);

	vec3 lightDir1 = normalize(vec3(0, 0.25, 1.0));
	vec3 normalMetallic = perturbNormal2Arb(tRandomNormal, -vViewPosition, normal, vec2(0.05, 0.05) , 20.0);

	float lambertMetallic = ( dot( normalMetallic, lightDir1 ) );
	float cosineTermMetallic = (sin(lambertMetallic * 12.6));

	float lambertNorm = saturate( dot( normal, lightDir1 ) );
	vec3 worldNormalPhong = inverseTransformDirection( normalMetallic, viewMatrix );
	float phong = pow(dot(reflectVec, worldNormalPhong), 20.0);

	//increase luminance in dark areas
	luminance = pow(luminance,0.65);

	float gamma = 1.0;
	float invGamma = 1.0 / gamma;

	vec3 result = vec3(0,0,0);
	result += diffuseMap * 0.35 * gamma;
	result += diffuseMap * 0.4 * (lambertNorm);

	//bright ring without darkening
	result += (diffuseMap + vec3(0.25,0.25,0.25) * ( 1.0 - luminance * 1.0 ) ) * saturate(cosineTermMetallic) * ( 0.7 - luminance * 0.69 ) * 0.4 * invGamma;
	//bright ring with darkening
	result += (diffuseMap + vec3(0.25,0.25,0.25)) * cosineTermMetallic * ( 0.7 - luminance * 0.69 ) * 0.2 * invGamma;
	//bright dot in center
	result += (diffuseMap + vec3(0.25,0.25,0.25)) * phong * ( 0.5 - luminance * 0.2 ) * invGamma * 0.8;

	return result;
}

void main() {
	vec3 viewDir = normalize( vViewPosition );

	vec3 ambientMapLeft = texture2D( tAmbient, vUv * vec2(0.5, 1.0) + vec2(0.0, 0.0)).rgb;
	vec3 ambientMapRight = texture2D( tAmbient, vUv * vec2(0.5, 1.0) + vec2(0.5, 0.0)).rgb;

	float mixLeftRight = clamp(vWorldPosition.x * 0.4, 0.0, 1.0);
	vec3 ambientMap = mix(ambientMapLeft, ambientMapRight, mixLeftRight);

	vec3 diffuseMap = vec3(0,0,0);
	vec4 surfaceMap = vec4(0,0,0,0);

	if (asymmetric > 0.5)
	{
		vec3 diffuseMapLeft = texture2D( tDiffuse, vUv * vec2(0.5, 1.0) + vec2(0.0, 0.0)).rgb;
		vec3 diffuseMapRight = texture2D( tDiffuse, vUv * vec2(0.5, 1.0) + vec2(0.5, 0.0)).rgb;
		diffuseMap = mix(diffuseMapLeft, diffuseMapRight, mixLeftRight);

		vec4 surfaceMapLeft = texture2D( tSurface, vUv * vec2(0.5, 1.0) + vec2(0.0, 0.0));
		vec4 surfaceMapRight = texture2D( tSurface, vUv * vec2(0.5, 1.0) + vec2(0.5, 0.0));
		surfaceMap = mix(surfaceMapLeft, surfaceMapRight, mixLeftRight);
	}
	else
	{
		diffuseMap = texture2D( tDiffuse, vUv).rgb;
		surfaceMap = texture2D( tSurface, vUv);
	}

	float luminance =  getLuminance (diffuseMap);

	vec3 specular = vec3(1, 1, 1);
	float shininess = 4.0;
	float specularStrength = 1.0;

	vec3 normal = normalize( vNormal );
	vec3 normal2 = normal;

	//plastic disturb
	vec3 normal3 = perturbNormal2Arb(tNormal2, -vViewPosition, normal, vec2(-0.1, -0.1) , 15.0);

	//soft noise
	vec3 normal4 = normal3;

	//noise env
	vec3 normal5 = perturbNormal2Arb(tNormal2, -vViewPosition, normal, vec2(-0.1, -0.1) , 15.0);
	normal5 = perturbNormal2Arb(tNormal3, -vViewPosition, normal5, vec2(0.015, 0.015) , 5.0);

	vec3 totalDiffuseLight = vec3( 0.0 );
	vec3 totalSpecularLight = vec3( 0.0 );

	vec3 lightColor = vec3(1.0, 1.0, 1.0);
	vec3 lightDir1 = normalize(vec3(0.5, 0.2, 0.5));
	vec3 lightDir2 = normalize(vec3(-0.5, 0.2, 0.5));
	vec3 lightDirRing = normalize(vec3(0, 0.25, 1.0));

	// diffuse

	float lambert1 = saturate( dot( normal4, lightDir1 ) );
	float lambert2 = saturate( dot( normal4, lightDir2 ) );
	float lambertRing = saturate( dot( normal4, lightDirRing ) );
	float cosineTerm1 = saturate(sin(lambert1 * 12.6));
	float cosineTerm2 = saturate(sin(lambert2 * 24.0));
	float cosineTermRing = saturate(sin(lambertRing * 12.6));

	vec3 cameraToVertex = normalize( vWorldPosition - cameraPosition );
	vec3 worldNormal = inverseTransformDirection( normal5, viewMatrix );
	vec3 reflectVec = reflect( cameraToVertex, worldNormal );

	vec2 sampleUV = vec2(
		atan(reflectVec.z,reflectVec.x ) * RECIPROCAL_PI2 + 0.5,
		saturate(reflectVec.y * 0.5 + 0.5 )
	);
	vec4 envColor = texture2D( tEnv, sampleUV );

	vec2 projectionMask = addProjections(diffuseMap);

    //render normal
	vec3 result = vec3(0, 0, 0);
	result += envColor.xyz * 0.1 * lambertRing;
	result += envColor.xyz * 0.4;
	result += diffuseMap * 0.6;
	result += diffuseMap * lambert1 * 0.3;
	result += diffuseMap * lambert2 * 0.3;
	result += (vec3(0.4, 0.4, 0.4)) * cosineTermRing * ( 0.62 - luminance * 0.52 ) * 0.12;

	float metallicMask = surfaceMap.r * (1.0 - projectionMask.x);
	if (metallicMask > 0.1)
	{
		vec3 metallicResult = renderMetallic(normal, diffuseMap, reflectVec);
    	result += envColor.xyz * 0.1 * lambertRing;
	    result += envColor.xyz * 0.4;
		result = mix(result, metallicResult, metallicMask);
	}

	if (projectionMask.y > 0.5)
	{
		vec3 org = result;

		vec3 lightDir1 = normalize(vec3(0, 0.25, 1.0));
		vec3 normalMetallic = perturbNormal2Arb(tRandomNormal, -vViewPosition, normal, vec2(0.1, 0.1) , 20.0);
		float lambertNorm = saturate( dot( normalMetallic, lightDir1 ) );
		vec3 worldNormalPhong = inverseTransformDirection( normalMetallic, viewMatrix );
		float phong = pow(dot(reflectVec, worldNormalPhong), 20.0);

		vec3 result2 = vec3(0,0,0);
		result2 += envColor.xyz * lambertNorm * 0.2;
		result2 += signatureColor * 0.4;
		result2 += signatureColor * 0.3 * (lambertNorm);
		result2 += (signatureColor + vec3(0.25,0.25,0.25)) * phong * 0.5;

		result = mix(result2, org, 1.0 - projectionMask.x);
	}

	//hemi
	float lambertHemi1 = dot( normal3, vec3(0.4, 0.4, -0.4));
	float lambertHemi2 = dot( normal3, vec3(-0.1, -0.8, -0.1));
	float hemi1 = clamp(lambertHemi1, 0.0, 0.3);
	float hemi2 = clamp(lambertHemi2, 0.0, 0.3);
	result += result * vec3(0.8,0.9,1.2) * hemi1 * 0.3;
	result += vec3(0.0,0.05,0.1) * hemi1 * 0.3;
	result -= result * vec3(0.8,1.0,1.2) * hemi2 * 0.3;
	result -= vec3(0.1,0.1,0.3) * hemi2 * 0.3;

	float occlusion = (0.3 + ambientMap.r * 0.7);
	gl_FragColor = vec4(result * occlusion, 1);

}
uniform sampler2D tAmbient;
uniform sampler2D tDiffuse;
uniform sampler2D tSurface;

uniform sampler2D tPlasticNormal;
uniform sampler2D tRandomNormal;
uniform sampler2D tFlakeNormal;

uniform sampler2D tProject;
uniform sampler2D tEnv;

uniform float asymmetric;

varying vec2 vUv;
varying vec3 vViewPosition;
varying vec3 vNormal;
varying vec3 vWorldPosition;

#define include_tools

/*
 * Main fagment for normal varnish
 */
vec3 renderNormal(
	vec3 normal,
	vec3 diffuseMap,
	vec3 clearVarnish
) {
	float luminance =  getLuminance(diffuseMap);

	//soft noise
	vec3 normalSoftNoise = perturbNormal2Arb(tRandomNormal, -vViewPosition, normal, vec2(-0.05, -0.05) , 20.0);

	//diffuse and specular

	vec3 lightDir1 = normalize(vec3(0, 0.25, 1.0));
	float lambert1 = saturate( dot( normalSoftNoise, lightDir1 ) );
	float cosineTerm1 = saturate(sin(lambert1 * 12.6));

	//finally mix all colors
	vec3 result = vec3(0,0,0);
	result += clearVarnish.xyz * lambert1 * 0.1;
	result += clearVarnish.xyz * 0.1;
	result += diffuseMap * 0.7; //0.6
	result += diffuseMap * lambert1 * 0.30; //0.22
	result += (vec3(0.4, 0.4, 0.4)) * cosineTerm1 * ( 0.62 - luminance * 0.52 ) * 0.3;

	return result;
}

/*
 * Main fagment for metallic varnish
 */
vec3 renderMetallic(
	vec3 normal,
	vec3 diffuseMap,
	vec3 reflectVecPlastic,
	vec3 clearVarnish
) {
	float luminance =  getLuminance(diffuseMap);

	vec3 lightDir1 = normalize(vec3(0, 0.25, 1.0));
	vec3 normalMetallic = perturbNormal2Arb(tRandomNormal, -vViewPosition, normal, vec2(0.1, 0.1) , 20.0);

	float lambertMetallic = ( dot( normalMetallic, lightDir1 ) );
	float cosineTermMetallic = (sin(lambertMetallic * 12.6));

	float lambertNorm = saturate( dot( normal, lightDir1 ) );
	vec3 worldNormalPhong = inverseTransformDirection( normalMetallic, viewMatrix );
	float phong = pow(dot(reflectVecPlastic, worldNormalPhong), 20.0);

	//increase luminance in dark areas
	luminance = pow(luminance,0.65);

	float gamma = 1.0;
	float invGamma = 1.0 / gamma;

	vec3 result = vec3(0,0,0);
	result += clearVarnish.xyz * lambertNorm * 0.2;
	result += diffuseMap * 0.35 * gamma;
	result += diffuseMap * 0.4 * (lambertNorm);

	//bright ring without darkening
	result += (diffuseMap + vec3(0.25,0.25,0.25) * ( 1.0 - luminance * 1.0 ) ) * saturate(cosineTermMetallic) * ( 0.7 - luminance * 0.69 ) * 0.5 * invGamma;
	//bright ring with darkening
	result += (diffuseMap + vec3(0.25,0.25,0.25)) * cosineTermMetallic * ( 0.7 - luminance * 0.69 ) * 0.2 * invGamma;
	//bright dot in center
	result += (diffuseMap + vec3(0.25,0.25,0.25)) * phong * ( 0.5 - luminance * 0.2 ) * invGamma;

	return result;
}

/*
 * Main fagment for flakes
 */
vec3 renderFlake(
	vec3 normal,
	vec3 diffuseMap,
	vec3 reflectVecPlastic,
	vec3 clearVarnish
) {
	//lambert and specular
	vec3 lightDir1 = normalize(vec3(0, 0.25, 1.0));
	vec3 normalMetallic = perturbNormal2Arb(tRandomNormal, -vViewPosition, normal, vec2(0.0, 0.0) , 20.0);
	vec3 normalFlake = perturbNormal2Arb(tFlakeNormal, -vViewPosition, normal, vec2(-0.5, -0.5) , 14.0);

	float lambertMetallic = ( dot( normalMetallic, lightDir1 ) );
	float lambertFlake = ( dot( normalFlake, lightDir1 ) );
	float cosineTermMetallic = (sin(lambertMetallic * 12.6));

	float lambertNorm = saturate( dot( normal, lightDir1 ) );
	vec3 worldNormalPhong = inverseTransformDirection( normalMetallic, viewMatrix );
	float phong = pow(dot(reflectVecPlastic, worldNormalPhong), 20.0);

	vec3 flake1 = texture2D( tFlakeNormal, vUv * 14.0 + vec2(0.2, 0.2)).xyz;
	vec3 flake2 = texture2D( tFlakeNormal, vUv * 15.0 + vec2(0.1, 0.3) ).xyz;
	vec3 flake3 = texture2D( tFlakeNormal, vUv * 16.0 + vec2(0.3, 0.1) ).xyz;

	vec3 flakeDiffuse = diffuseMap.rgb -
		diffuseMap.rgb * pow(flake1.g, 3.0) * 1.5 +
		diffuseMap.rgb * pow(flake2.r, 4.0) * 1.5;

	vec3 grey = vec3(0.5, 0.5, 0.5);
	vec3 flakeSpecularBase =
		grey * 0.2 -
		grey * pow(flake1.g, 3.0) * 0.0 +
		grey * pow(flake3.r, 3.0) * 4.0;
		grey * pow(flake2.r, 2.0) * 4.0;

	float luminance =  getLuminance(flakeDiffuse);

	vec3 holoColor = vec3(0,0,0);
//	holoColor += hsv2rgb(vec3(fract(flake1.r * 2.5 + lambertNorm * 6.0), 1, 1));
//	holoColor -= hsv2rgb(vec3(fract(lambertNorm * 500.0), 1, 1)) * 0.4;

	vec3 flakeSpecular = (diffuseMap.rgb * 1.5 + vec3(0.3, 0.3, 0.3) + holoColor * 0.4) *
		pow(clamp(flakeSpecularBase.r, 0.0, 1.0), 4.5) * 3.0;


	vec3 result = vec3(0,0,0);

	result += clearVarnish.xyz * lambertNorm * 0.1;
	result += diffuseMap * 0.5;
	result += diffuseMap * 0.2 * (lambertFlake);
	result += flakeDiffuse * (lambertFlake) * 0.1;
	result += lambertFlake * (flakeDiffuse ) * ((cosineTermMetallic * 0.5 + 0.5) + 1.0) * 0.5;
	result += (flakeSpecular) * ((clearVarnish.x * 0.5 + cosineTermMetallic * 0.5 + 0.5)) * 1.2;
	result += (lambertFlake + clearVarnish.x * 0.5) * (flakeSpecular ) * phong * 0.6;

	return clamp(result, 0.0, 1.0);
}

/*
 * Main fagment for holo flakes
 */
vec3 renderHoloFlake(
	vec3 normal,
	vec3 diffuseMap,
	vec3 reflectVecPlastic,
	vec3 clearVarnish
) {
	//lambert and specular
	vec3 lightDir1 = normalize(vec3(0, 0.25, 1.0));
	vec3 normalMetallic = perturbNormal2Arb(tRandomNormal, -vViewPosition, normal, vec2(0.0, 0.0) , 20.0);
	vec3 normalFlake = perturbNormal2Arb(tFlakeNormal, -vViewPosition, normal, vec2(-0.1, -0.1) , 14.0);

	float lambertMetallic = ( dot( normalMetallic, lightDir1 ) );
	float lambertFlake = ( dot( normalFlake, lightDir1 ) );
	float cosineTermMetallic = (sin(lambertMetallic * 12.6));

	float lambertNorm = saturate( dot( normal, lightDir1 ) );
	vec3 worldNormalPhong = inverseTransformDirection( normalMetallic, viewMatrix );
	float phong = pow(dot(reflectVecPlastic, worldNormalPhong), 20.0);

	vec3 flake1 = texture2D( tFlakeNormal, vUv * 14.0 + vec2(0.2, 0.2)).xyz;
	vec3 flake2 = texture2D( tFlakeNormal, vUv * 15.0 + vec2(0.1, 0.3) ).xyz;
	vec3 flake3 = texture2D( tFlakeNormal, vUv * 16.0 + vec2(0.3, 0.1) ).xyz;

	vec3 flakeDiffuse = diffuseMap.rgb;

	vec3 grey = vec3(0.5, 0.5, 0.5);
	vec3 flakeSpecularBase =
		grey * 0.2 +
		grey * pow(flake3.r, 3.0) * 4.0;

	vec3 holoColor = vec3(0,0,0);
	holoColor += hsv2rgb(vec3(fract(flake1.r * 2.5 + lambertNorm * 6.0), 1, 1));

	vec3 flakeSpecular = (holoColor) *
		pow(clamp(flakeSpecularBase.r, 0.0, 1.0), 4.0) * 3.0;

	float luminance =  getLuminance(flakeDiffuse);

	vec3 result = vec3(0,0,0);

	cosineTermMetallic += clearVarnish.r * 2.0;

	result += clearVarnish.xyz * lambertNorm * 0.05;
	result += diffuseMap * 0.4 * (lambertFlake);
	result += flakeDiffuse * (lambertFlake) * 0.2;
	result += lambertFlake * (flakeDiffuse ) * ((cosineTermMetallic * 0.5 + 0.5) + 1.0) * 0.5;

	float rainbowDamp = 0.3 + luminance * 0.7;

	result += (flakeSpecular) * ((cosineTermMetallic * 0.5 + 0.5)) * 1.0 * rainbowDamp;
	result += lambertFlake * (flakeSpecular ) * phong * 0.6 * rainbowDamp;

	return clamp(result, 0.0, 1.0);
}

#define include_projections

void main() {
	vec3 ambientMapLeft = texture2D( tAmbient, vUv * vec2(0.5, 1.0) + vec2(0.0, 0.0)).rgb;
	vec3 ambientMapRight = texture2D( tAmbient, vUv * vec2(0.5, 1.0) + vec2(0.5, 0.0)).rgb;

	float mixLeftRight = clamp(vWorldPosition.x * 0.4, 0.0, 1.0);
	vec3 ambientMap = mix(ambientMapLeft, ambientMapRight, mixLeftRight);

	vec3 diffuseMap = vec3(0,0,0);
	vec4 surfaceMap = vec4(0,0,0,0);
	if (asymmetric > 0.5)
	{
		vec3 diffuseMapLeft = texture2D( tDiffuse, vUv * vec2(0.5, 1.0) + vec2(0.0, 0.0)).rgb;
		vec3 diffuseMapRight = texture2D( tDiffuse, vUv * vec2(0.5, 1.0) + vec2(0.5, 0.0)).rgb;
		diffuseMap = mix(diffuseMapLeft, diffuseMapRight, mixLeftRight);

		vec4 surfaceMapLeft = texture2D( tSurface, vUv * vec2(0.5, 1.0) + vec2(0.0, 0.0));
		vec4 surfaceMapRight = texture2D( tSurface, vUv * vec2(0.5, 1.0) + vec2(0.5, 0.0));
		surfaceMap = mix(surfaceMapLeft, surfaceMapRight, mixLeftRight);
	}
	else
	{
		diffuseMap = texture2D( tDiffuse, vUv).rgb;
		surfaceMap = texture2D( tSurface, vUv);
	}


	vec3 normal = normalize( vNormal );
	vec3 cameraToVertex = normalize( vWorldPosition - cameraPosition );

	//compute varnisch environmap
	vec3 normalPlasic = perturbNormal2Arb(tPlasticNormal, -vViewPosition, normal, vec2(0.005, 0.005) , 30.0);
	vec3 worldNormalPlastic = inverseTransformDirection( normalPlasic, viewMatrix );
	vec3 reflectVecPlastic = reflect( cameraToVertex, worldNormalPlastic );
	vec2 environmentUv = vec2(
		atan(reflectVecPlastic.z,reflectVecPlastic.x) * RECIPROCAL_PI2 + 0.5,
		reflectVecPlastic.y * 0.5 + 0.5
	);
	vec3 clearVarnish = texture2D( tEnv, environmentUv ).xyz;

	//projectionMask ensures no effects like metallic are drawn over projections
	vec2 projectionMask = addProjections(diffuseMap);

	vec3 result = renderNormal( normal, diffuseMap, clearVarnish);

	float metallicMask = surfaceMap.r * (1.0 - projectionMask.x);
	if (metallicMask > 0.1)
	{
		vec3 metallicResult = renderMetallic(normal, diffuseMap, reflectVecPlastic, clearVarnish);
		result = mix(result, metallicResult, metallicMask);
	}

	float flakeMask = surfaceMap.g * (1.0 - projectionMask.x);
	if (flakeMask > 0.1)
	{
		vec3 flakeResult = renderFlake(normal, diffuseMap, reflectVecPlastic, clearVarnish);
		result = mix(result, flakeResult, flakeMask);
	}

	float holoFlakeMask = surfaceMap.b * (1.0 - projectionMask.x);
	if (holoFlakeMask > 0.1)
	{
		vec3 holoFlakeResult = renderHoloFlake(normal, diffuseMap, reflectVecPlastic, clearVarnish);
		result = mix(result, holoFlakeResult, holoFlakeMask);
	}

	if (projectionMask.y > 0.5)
	{
		vec3 org = result;

		vec3 lightDir1 = normalize(vec3(0, 0.25, 1.0));
		vec3 normalMetallic = perturbNormal2Arb(tRandomNormal, -vViewPosition, normal, vec2(0.1, 0.1) , 20.0);
		float lambertNorm = saturate( dot( normalMetallic, lightDir1 ) );
		vec3 worldNormalPhong = inverseTransformDirection( normalMetallic, viewMatrix );
		float phong = pow(dot(reflectVecPlastic, worldNormalPhong), 20.0);

		vec3 result2 = vec3(0,0,0);
		result2 += clearVarnish.xyz * lambertNorm * 0.2;
		result2 += signatureColor * 0.4;
		result2 += signatureColor * 0.3 * (lambertNorm);
		result2 += (signatureColor + vec3(0.25,0.25,0.25)) * phong * 0.5;

		result = mix(result2, org, 1.0 - projectionMask.x);
	}

	//cheap hemi light
	float lambertHemi1 = dot( normalPlasic, vec3(0.4, 0.4, -0.4));
	float lambertHemi2 = dot( normalPlasic, vec3(-0.1, -0.8, -0.1));
	float hemi1 = clamp(lambertHemi1, 0.0, 0.3);
	float hemi2 = clamp(lambertHemi2, 0.0, 0.3);
	result += result * vec3(0.8,0.9,1.2) * hemi1 * 0.3;
	result += vec3(0.0,0.05,0.1) * hemi1 * 0.3;
	result -= result * vec3(0.8,1.0,1.2) * hemi2 * 0.3;
	result -= vec3(0.1,0.1,0.3) * hemi2 * 0.3;

	float occlusion = (0.3 + ambientMap.r * 0.7);
	gl_FragColor = vec4(result * occlusion, 1);

}
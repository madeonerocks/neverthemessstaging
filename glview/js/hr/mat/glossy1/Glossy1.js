define([
    'hr/CDN',
    'text!hr/mat/glossy1/shell.vert',
    'text!hr/mat/glossy1/shell.frag',
    'text!hr/mat/shaderparts/projections.frag',
    'text!hr/mat/shaderparts/tools.frag'
], function (CDN,
             vert,
             frag,
             projectionsPart,
             toolsPart) {

    function Glossy1(productModel, debug, shellMultiRTT, projectionTexture) {
        var loader = new THREE.TextureLoader();
        loader.setCrossOrigin('');

        var envMap = loader.load(CDN.path + "gl/mat/mirror/ModelTest/" + "env/9.jpg");
        envMap.mapping = THREE.EquirectangularReflectionMapping;

        var ambient = loader.load(productModel.getShadingUrl());

        var preview = null;
        if (debug.preview)
            preview = loader.load(productModel.getPreviewUrl());

        var tPlasticNormal = loader.load(CDN.path + "gl/mat/7657-normal.jpg");
        tPlasticNormal.wrapS = THREE.RepeatWrapping;
        tPlasticNormal.wrapT = THREE.RepeatWrapping;

        var tRandomNormal = loader.load(CDN.path + "gl/mat2/randomnormal.png");
        tRandomNormal.wrapS = THREE.RepeatWrapping;
        tRandomNormal.wrapT = THREE.RepeatWrapping;

        var tFlakeNormal = loader.load(CDN.path + "gl/mat2/flake.png");
        //tFlakeNormal.magFilter = THREE.LinearFilter;
        //tFlakeNormal.minFilter = THREE.LinearFilter;

        tFlakeNormal.wrapS = THREE.RepeatWrapping;
        tFlakeNormal.wrapT = THREE.RepeatWrapping;

        var projectionMatrixSide = new THREE.Matrix4();
        projectionMatrixSide.identity();

        var projectionMatrixBack = new THREE.Matrix4();
        projectionMatrixBack.identity();

        var projectionMatrixIcon = new THREE.Matrix4();
        projectionMatrixIcon.identity();

        this.material = new THREE.ShaderMaterial({
            uniforms: {
                pmSide: {type: "m4", value: projectionMatrixSide},
                pmBack: {type: "m4", value: projectionMatrixBack},
                pmIcon: {type: "m4", value: projectionMatrixIcon},
                pmIcon2: {type: "m4", value: projectionMatrixIcon},
                pmProducerLogo: {type: "m4", value: projectionMatrixIcon},
                tProject: {type: "t", value: projectionTexture},
                tDiffuse: {type: "t", value: shellMultiRTT.diffuseRTT.texture},
                tAmbient: {type: "t", value: ambient},
                tSurface: {type: "t", value: shellMultiRTT.surfaceRTT.texture},
                tPlasticNormal: {type: "t", value: tPlasticNormal},
                tRandomNormal: {type: "t", value: tRandomNormal},
                tFlakeNormal: {type: "t", value: tFlakeNormal},
                tEnv: {type: "t", value: envMap},
                logoColor: {type: "c", value: new THREE.Color(0xff0000)},
                signatureColor: {type: "c", value: new THREE.Color(0xff0000)},
                bothSides: {type: "f", value: 1},
                logoMarker: {type: "f", value: 0},
                iconMarker: {type: "f", value: 0},
                asymmetric: {type: "f", value: 0},
                signatureColorType: {type: "f", value: 0},
            },
            vertexShader: vert,
            fragmentShader: frag
                .replace('#define include_projections', projectionsPart)
                .replace('#define include_tools', toolsPart),
            derivatives: true,
        });

        if (preview != null)
        {
            var diffuse = this.material.uniforms.tDiffuse;
            diffuse.value = preview;

            function update()
            {
                //console.log("load");

                var newImage = loader.load(productModel.getPreviewUrl(), function(){
                    //console.log("onLoad");
                    diffuse.value.dispose();
                    diffuse.value = newImage;
                    setTimeout(update, 500);
                });
            }

            update();

        }

        this.handleModelLoaded = function()
        {
            this.material.uniforms.pmSide.value = productModel.getSideProjectionMatrix();
            this.material.uniforms.pmBack.value = productModel.getLogoProjectionMatrix();
            this.material.uniforms.pmIcon.value = productModel.getIconProjectionMatrix();
            this.material.uniforms.pmIcon2.value = productModel.getIcon2ProjectionMatrix();
            this.material.uniforms.pmProducerLogo.value = productModel.getProducerLogoProjectionMatrix();

            this.material.uniforms.iconMarker.value = productModel.getIconMarker();
            this.material.uniforms.logoMarker.value = 0; // 0 = old, 1 = has producer logo, 2 = use producer logo
            this.material.uniforms.bothSides.value = productModel.designJson.signature.bothSides;
            this.material.uniforms.asymmetric.value = productModel.designJson.asymmetric ? 1 : 0;
        }

        this.showProducerLogo = function(enabled)
        {
            if (!productModel.hasProducerLogo())
            {
                this.material.uniforms.logoMarker.value = 0;
            }
            else
            {
                this.material.uniforms.logoMarker.value = enabled ? 2 : 1;
            }
        }

        this.setLogoColor = function(color)
        {
            this.material.uniforms.logoColor.value = new THREE.Color(color);
        }

        this.setSignatureColor = function(colorConfig)
        {
            this.material.uniforms.signatureColorType.value = colorConfig.type == "metallic" ? 1 : 0;

            var color = colorConfig.hex;
            if (colorConfig.type == "metallic")
                color = colorConfig.gl.diffuse;

            this.material.uniforms.signatureColor.value = new THREE.Color(color);
        }
    }

    return Glossy1;
});
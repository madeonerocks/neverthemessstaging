define([
    'hr/CDN',
    'hr/utils/Fonts'
], function (CDN, Fonts) {

    function Projections(productModel) {

        var numberFont = "ale_proregular";
        var self = this;
        var logoImage = null;
        var producerImage = null;
        var iconImage = null;
        var imageUv = null;

        var loader = new THREE.ImageLoader();
        loader.setCrossOrigin('');

        var projectionCanvas = document.createElement("canvas");
        projectionCanvas.setAttribute('width', '1024');
        projectionCanvas.setAttribute('height', '512');
        var ctx = projectionCanvas.getContext("2d");

        this.texture = new THREE.Texture(projectionCanvas);
        this.texture.premultiplyAlpha = true;

        this.driverName = "";
        this.signatureFont = "Arial";
        this.number = "";

        this.renderCanvas = function () {
            ctx.clearRect(0, 0, 1024, 512);

            // ctx.fillStyle = "rgba(0, 0, 0, 0.2)";
            // ctx.fillRect(0, 0, 1024, 512);

            if (imageUv != null) {
                // ctx.drawImage(imageUv, 0, 0, 512, 512);
                // ctx.drawImage(imageUv, 512, 0, 512, 256);
                // ctx.drawImage(imageUv, 512, 256, 512, 128);
                // ctx.drawImage(imageUv, 512, 256 + 128, 512, 128);
            }

            try {
                if (logoImage == null && productModel.designJson != null) {

                    var url = 'gl/mat3/logo.png';
                    if (productModel.designJson.logo != null)
                    {
                        if (productModel.designJson.logo.location != null)
                        {
                            url = productModel.designJson.logo.location;
                        }
                    }
                    loader.load(CDN.path + url, function (result) {
                        logoImage = result;
                        self.renderCanvas();
                    });
                }
            }
            catch (e) {
                //not loaded yet or not defined
            }

            if (productModel.designJson != null
                && productModel.designJson.producer_logo != null
                && logoImage != null
                && producerImage != null
                )
            {
                if (producerImage != null)
                    ctx.drawImage(producerImage, 512 + 256 + 4, 0 + 4, 256 - 8, 128 - 8);

                if (logoImage != null)
                    ctx.drawImage(logoImage, 512, 0, 256, 128);
            }
            else
            {
                if (logoImage != null)
                    ctx.drawImage(logoImage, 512, 0);
            }


            //render icon
            try {
                //icon type 1 (retrospective
                if (productModel.designJson.icon != null && (productModel.designJson.icon.type == null || productModel.designJson.icon.type == 1)) {
                    ctx.fillStyle = "rgba(255, 0, 0, 1.0)";
                    ctx.beginPath();
                    ctx.arc(256, 256, 254, 0, 2 * Math.PI, false);
                    ctx.fill();

                    ctx.fillStyle = "#0000ff";
                    ctx.font = "350px " + numberFont;
                    var numberSize = ctx.measureText(this.number);
                    ctx.fillText(this.number, 256 - numberSize.width / 2 + 10, 256 + 115);
                }
            }
            catch (e) {
                //not loaded yet
            }

            //render icon
            try {
                if (productModel.designJson.icon.type > 1) {

                    if (iconImage == null)
                    {
                        loader.load(CDN.path + productModel.designJson.icon.png, function (result) {
                            iconImage = result;
                            self.renderCanvas();
                        });
                    }

                    ctx.drawImage(iconImage, 0, 0);
                }
            }
            catch (e) {
                //not loaded yet or not defined
            }

            //render producer logo
            try {
                if (productModel.designJson.producer_logo != null) {

                    if (producerImage == null)
                    {
                        loader.load(CDN.path + productModel.designJson.producer_logo.png, function (result) {
                            producerImage = result;
                            self.renderCanvas();
                        });
                    }

                    // ctx.drawImage(iconImage, 0, 0);
                }
            }
            catch (e) {
                //not loaded yet or not defined
            }

            var fontSize = 55;

            //individual font corrections
            switch (this.signatureFont) {
                case "claytonregular":
                    fontSize = 65;
                    break;
                case "microgrammadbolextregular":
                    fontSize = 45;
                    break;
            }

            ctx.font = fontSize + "px " + this.signatureFont;
            ctx.fillStyle = "#ffffff";

            //shrink text if bigger than max width
            var text = this.driverName;
            var size = ctx.measureText(this.driverName);
            var count = 0;
            while (size.width > 500 && count < 40) {
                text = this.driverName.substring(0, text.length - 1);
                size = ctx.measureText(text);
                count++;
            }

            //get alignment
            var alignment = "center";
            try {
                if (productModel.designJson.signature.align == "left") {
                    alignment = "left";
                }
                if (productModel.designJson.signature.align == "right") {
                    alignment = "right";
                }
            }
            catch (e) {
                //not loaded yet
            }

            switch (alignment) {
                case "center":
                    ctx.fillText(text, 512 + (512 - size.width) / 2, 3 * 128 - 23);
                    ctx.fillText(text, 512 + (512 - size.width) / 2, 4 * 128 - 23);
                    break;
                case "left":
                    ctx.fillText(text, 512 + 10, 3 * 128 - 23);
                    ctx.fillText(text, 1024 - size.width - 10, 4 * 128 - 23);
                    break;
                case "right":
                    ctx.fillText(text, 512 + 10, 4 * 128 - 23);
                    ctx.fillText(text, 1024 - size.width - 10, 3 * 128 - 23);
                    break;
            }

            this.texture.needsUpdate = true;
        }


        this.setDriverName = function (name) {

            this.driverName = name;
            this.renderCanvas();


            // egg here
            try {
                var self = this;
                var currentIndex = 0;
                self.currentLoop = "";
                function loop(localloop, a) {
                    if (localloop == self.currentLoop) {
                        // lowlevel redraw
                        self.driverName = a[currentIndex];
                        self.renderCanvas();
                        currentIndex = (currentIndex + 1) % a.length;
                        setTimeout(function () {
                            loop(localloop, a);
                        }, currentIndex == 0 ? 3000 : 1000);
                    }
                }

                var x = {};
                x["3105999"] = "RGVkaWNhdGVkIHRvLW15IGxvdmUtYWRvcmVlISEh";
                x["314159x"] = "Y3JlZGl0cyA+Pj4tYWxleC1hbmR5LWJvYmJ5LWNocmlzdG9waGVyLWZsby1oZWlrZS1rYXJpbmEta2FzY2hhLWtqZWwtbWFyaWFuYS1uaWNvLXNvbmphLXRvYmktPDw8IFRIWCAhISEi";
                if (x[name] != null) {
                    self.currentLoop = name;
                    loop(name, atob(x[name]).split("-"));
                }
            }
            catch (e) {
                //ignore, just to be on the safe side
            }
        };

        this.setSignatureFont = function (name) {

            this.signatureFont = name;

            Fonts.getFont(name, function () {
                self.renderCanvas();
            });
        }

        this.setNumber = function (number) {
            this.number = number;
            self.renderCanvas();
        }

        Fonts.getFont(numberFont, function () {
            console.log("Font Canvas Ready...");
            self.renderCanvas();
        });

        this.renderCanvas();

        loader.load(CDN.path + 'gl/mat/UV_Grid_Sm.jpg', function (result) {
           imageUv = result;
           self.renderCanvas();
        });
    }

    return Projections;
});
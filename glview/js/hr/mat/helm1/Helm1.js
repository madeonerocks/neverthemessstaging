define([
    'hr/CDN',
    "hr/loader/OBJLoader",
    "hr/mat/helm1/VisorConfigurator",
    'text!hr/mat/helm1/visor.vert',
    'text!hr/mat/helm1/visor.frag',
    'text!hr/mat/helm1/visor2.frag'
], function (CDN,
             OBJLoader,
             VisorConfigurator,
             visorVert,
             visorFrag,
             visor2Frag) {


    function Helm1(productModel, shellMesh, onLoad) {

        var TextureLoader2 = function (manager) {
            this.manager = ( manager !== undefined ) ? manager : THREE.DefaultLoadingManager;
        };

        TextureLoader2.prototype = {

            constructor: THREE.TextureLoader,

            load: function (url, onLoad, onProgress, onError) {

                var defaultImageCanvas = document.createElement("canvas");
                defaultImageCanvas.setAttribute('width', '2');
                defaultImageCanvas.setAttribute('height', '2');

                var texture = new THREE.Texture(defaultImageCanvas);
                texture.needsUpdate = true;

                var loader = new THREE.ImageLoader(this.manager);
                loader.setCrossOrigin(this.crossOrigin);
                loader.load(url, function (image) {

                    texture.image = image;
                    texture.needsUpdate = true;

                    if (onLoad !== undefined) {

                        onLoad(texture);

                    }

                }, onProgress, onError);

                return texture;
            },

            setCrossOrigin: function (value) {
                this.crossOrigin = value;
            }
        };

        this.vc = new VisorConfigurator();

        this.visor1Visible = true;
        this.visor2Visible = false;
        this.visor1 = [];
        this.visor2 = [];

        this.suncap1Visible = false;
        this.suncap2Visible = false;
        this.suncap3Visible = false;
        this.suncap1 = [];
        this.suncap2 = [];
        this.suncap3 = [];

        this.setVisorVisible = function (id) {
            console.log("setVisorVisible", id);
            switch (id) {
                case 0:
                    this.setVisor1Visible(false);
                    this.setVisor2Visible(false);
                    break;

                case 1:
                    this.setVisor1Visible(true);
                    this.setVisor2Visible(false);
                    break;

                case 2:
                    this.setVisor1Visible(false);
                    this.setVisor2Visible(true);
                    break;
            }
        }

        this.setVisor1Visible = function (visible) {
            if (self.vc.isActive()) return;

            this.visor1Visible = visible;
            for (var i = 0; i < this.visor1.length; i++) {
                this.visor1[i].visible = visible;
            }
        }

        this.setVisor2Visible = function (visible) {
            if (self.vc.isActive()) return;

            this.visor2Visible = visible;
            for (var i = 0; i < this.visor2.length; i++) {
                this.visor2[i].visible = visible;
            }
        }

        this.setSuncapVisible = function (id) {
            console.log("setSuncapVisible", id);
            switch (id) {
                case 0:
                    this.setSuncap1Visible(false);
                    this.setSuncap2Visible(false);
                    this.setSuncap3Visible(false);
                    break;

                case 1:
                    this.setSuncap1Visible(true);
                    this.setSuncap2Visible(false);
                    this.setSuncap3Visible(false);
                    break;

                case 2:
                    this.setSuncap1Visible(false);
                    this.setSuncap2Visible(true);
                    this.setSuncap3Visible(false);
                    break;

                case 3:
                    this.setSuncap1Visible(false);
                    this.setSuncap2Visible(false);
                    this.setSuncap3Visible(true);
                    break;
            }
        }

        this.setSuncap1Visible = function (visible) {
            if (self.vc.isActive()) return;

            this.suncap1Visible = visible;
            for (var i = 0; i < this.suncap1.length; i++) {
                this.suncap1[i].visible = visible;
            }
        }

        this.setSuncap2Visible = function (visible) {
            if (self.vc.isActive()) return;

            this.suncap2Visible = visible;
            for (var i = 0; i < this.suncap2.length; i++) {
                this.suncap2[i].visible = visible;
            }
        }

        this.setSuncap3Visible = function (visible) {
            if (self.vc.isActive()) return;

            this.suncap3Visible = visible;
            for (var i = 0; i < this.suncap3.length; i++) {
                this.suncap3[i].visible = visible;
            }
        }

        var loader = new TextureLoader2();
        loader.setCrossOrigin('');

        var envMap2 = loader.load(CDN.path + "gl/mat/mirror/ModelTest/" + "env/9.jpg");
        envMap2.mapping = THREE.EquirectangularReflectionMapping;

        var leather = loader.load(CDN.path + "gl/mat2/filterforge/3413-diffuse.jpg")
        leather.wrapS = THREE.RepeatWrapping;
        leather.wrapT = THREE.RepeatWrapping;
        leather.repeat = new THREE.Vector2(10, 10);

        var leatherNormal = loader.load(CDN.path + "gl/mat2/filterforge/3413-normal.jpg");
        leatherNormal.wrapS = THREE.RepeatWrapping;
        leatherNormal.wrapT = THREE.RepeatWrapping;

        var leatherSpec = loader.load(CDN.path + "gl/mat2/filterforge/3413-specstrength.jpg");
        leatherSpec.wrapS = THREE.RepeatWrapping;
        leatherSpec.wrapT = THREE.RepeatWrapping;
		
        var matTest = new THREE.MeshPhongMaterial({
            color: 0xff0000,
            specular: 0xffffff,
            shininess: 50,
        });

        var matLeather = new THREE.MeshPhongMaterial({
            color: 0x666666,
            specular: 0xffffff,
            shininess: 50,
            map: leather,
            normalMap: leatherNormal,
            normalScale: new THREE.Vector2(0.5, 0.5),
            specularMap: leatherSpec
        });

        var matLeatherBrown = new THREE.MeshPhongMaterial({
            color: 0x7d3d31,
            specular: 0xffffff,
            shininess: 50,
            map: leather,
            normalMap: leatherNormal,
            normalScale: new THREE.Vector2(0.5, 0.5),
            specularMap: leatherSpec
        });

        var rubberNormal = loader.load(CDN.path + "gl/mat/maps/fabric/7415-normal.jpg")
        rubberNormal.repeat = new THREE.Vector2(20, 10);
        rubberNormal.wrapS = THREE.RepeatWrapping;
        rubberNormal.wrapT = THREE.RepeatWrapping;

        var rubberRandomNormal = loader.load(CDN.path + "gl/mat2/randomnormal.png")
        rubberRandomNormal.repeat = new THREE.Vector2(10, 10);
        rubberRandomNormal.wrapS = THREE.RepeatWrapping;
        rubberRandomNormal.wrapT = THREE.RepeatWrapping;

        var rubberRandomNormal2 = loader.load(CDN.path + "gl/mat2/randomnormal.png")
        rubberRandomNormal2.repeat = new THREE.Vector2(15, 15);
        rubberRandomNormal2.wrapS = THREE.RepeatWrapping;
        rubberRandomNormal2.wrapT = THREE.RepeatWrapping;

        var leatherRandomNormal = loader.load(CDN.path + "gl/mat2/11081-normal.jpg")
        leatherRandomNormal.repeat = new THREE.Vector2(13, 13);
        leatherRandomNormal.wrapS = THREE.RepeatWrapping;
        leatherRandomNormal.wrapT = THREE.RepeatWrapping;

        var leatherRandomAO = loader.load(CDN.path + "gl/mat2/11081-ambientocclusion.jpg")
        leatherRandomAO.repeat = new THREE.Vector2(13, 13);
        leatherRandomAO.wrapS = THREE.RepeatWrapping;
        leatherRandomAO.wrapT = THREE.RepeatWrapping;

        var leatherRandomNormal2 = loader.load(CDN.path + "gl/mat2/11081-normal.jpg")
        leatherRandomNormal2.repeat = new THREE.Vector2(8, 8);
        leatherRandomNormal2.wrapS = THREE.RepeatWrapping;
        leatherRandomNormal2.wrapT = THREE.RepeatWrapping;

        var leatherRandomAO2 = loader.load(CDN.path + "gl/mat2/11081-ambientocclusion.jpg")
        leatherRandomAO2.repeat = new THREE.Vector2(8, 8);
        leatherRandomAO2.wrapS = THREE.RepeatWrapping;
        leatherRandomAO2.wrapT = THREE.RepeatWrapping;

        var matRubber = new THREE.MeshPhongMaterial({
            color: 0x202020,
            specular: 0x555555,
            shininess: 50,
            normalMap: rubberNormal,
            normalScale: new THREE.Vector2(0.04, 0.04),
        });

        var rubberNoiseBlack = new THREE.MeshPhongMaterial({
            color: 0x202020,
            specular: 0x777777,
            shininess: 30,
            normalMap: rubberRandomNormal2,
            normalScale: new THREE.Vector2(0.04, 0.04),
        });

        var matRubberBrown = new THREE.MeshPhongMaterial({
            color: 0x965a37,
            specular: 0x666666,
            shininess: 40,
            normalMap: rubberRandomNormal,
            normalScale: new THREE.Vector2(0.02, 0.02),
        });

        //one leather innen
        var matRubberBrownOne = new THREE.MeshPhongMaterial({
            color: 0x9d6237,
            specular: 0x555555,
            shininess: 20,
            normalMap: leatherRandomNormal,
            specularMap: leatherRandomAO,
            normalScale: new THREE.Vector2(0.5, 0.5),
        });

        var leather_black_bullit = new THREE.MeshPhongMaterial({
            color: 0x111111,
            specular: 0x888888,
            shininess: 20,
            normalMap: leatherRandomNormal,
            specularMap: leatherRandomAO,
            normalScale: new THREE.Vector2(0.5, 0.5),
        });

        var leather_black_sk6 = new THREE.MeshPhongMaterial({
            color: 0x111111,
            specular: 0x888888,
            shininess: 20,
            normalMap: leatherRandomNormal2,
            specularMap: leatherRandomAO2,
            normalScale: new THREE.Vector2(0.5, 0.5),
        });

        var fabric = loader.load(CDN.path + "gl/mat/maps/fabric/7415-diffuse.jpg")
        fabric.wrapS = THREE.RepeatWrapping;
        fabric.wrapT = THREE.RepeatWrapping;
        fabric.repeat = new THREE.Vector2(300, 300);

        var fabricNormal = loader.load(CDN.path + "gl/mat/maps/fabric/7415-normal.jpg")
        fabricNormal.wrapS = THREE.RepeatWrapping;
        fabricNormal.wrapT = THREE.RepeatWrapping;

        var matCloth = new THREE.MeshPhongMaterial({
            color: 0x555599,
            specular: 0x999999,
            shininess: 2,
            map: fabric,
            normalMap: fabricNormal,
            normalScale: new THREE.Vector2(0.2, 0.2),
        });

        var matClothBrown = new THREE.MeshPhongMaterial({
            color: 0x7d3d31,
            specular: 0x999999,
            shininess: 2,
            map: fabric,
            normalMap: fabricNormal,
            normalScale: new THREE.Vector2(0.2, 0.2),
        });

        var matClothBrownOneNormal = loader.load(CDN.path + "gl/mat2/randomnormal.png");
        matClothBrownOneNormal.wrapS = THREE.RepeatWrapping;
        matClothBrownOneNormal.wrapT = THREE.RepeatWrapping;
        matClothBrownOneNormal.repeat = new THREE.Vector2(15, 15);

        var matClothBrownOne = new THREE.MeshPhongMaterial({
            color: 0xcc7d43,
            specular: 0x333333,
            shininess: 1,
            normalMap: matClothBrownOneNormal,
            normalScale: new THREE.Vector2(0.5, 0.5),
        });

        var cloth_black = new THREE.MeshPhongMaterial({
            color: 0x111111,
            specular: 0x333333,
            shininess: 1,
            normalMap: matClothBrownOneNormal,
            normalScale: new THREE.Vector2(0.5, 0.5),
        });

        var cloth_black_gp6s = new THREE.MeshPhongMaterial({
            color: 0x262626,
            specular: 0xffffff,
            shininess: 3,
            normalMap: matClothBrownOneNormal,
            normalScale: new THREE.Vector2(0.2, 0.2),
        });

        var cloth_black_gp6s_blue = new THREE.MeshPhongMaterial({
            color: 0x222228,
            specular: 0x9999ff,
            shininess: 3,
            normalMap: matClothBrownOneNormal,
            normalScale: new THREE.Vector2(0.2, 0.2),
        });

        var cloth_blue_sk6 = new THREE.MeshPhongMaterial({
            color: 0x222244,
            specular: 0xfffffff,
            shininess: 3,
            normalMap: matClothBrownOneNormal,
            normalScale: new THREE.Vector2(0.2, 0.2),
        });

        var fabricGrid = loader.load(CDN.path + "gl/mat/maps/fabric/7415-diffuse.jpg")
        fabricGrid.wrapS = THREE.RepeatWrapping;
        fabricGrid.wrapT = THREE.RepeatWrapping;
        fabricGrid.repeat = new THREE.Vector2(200, 200);

        var fabricNormal = loader.load(CDN.path + "gl/mat/maps/fabric/7415-normal.jpg")
        fabricNormal.wrapS = THREE.RepeatWrapping;
        fabricNormal.wrapT = THREE.RepeatWrapping;

        var matClothGrid = new THREE.MeshPhongMaterial({
            color: 0x6666ff,
            specular: 0x999999,
            shininess: 2,
            map: fabricGrid,
            normalMap: fabricNormal,
            normalScale: new THREE.Vector2(1.0, 1.0),
        });

        var clothGridNormal = loader.load(CDN.path + "gl/mat2/wire-normal.jpg");
        clothGridNormal.wrapS = clothGridNormal.wrapT = THREE.MirroredRepeatWrapping;
        var clothGridDiffuse = loader.load(CDN.path + "gl/mat2/wire-diffuse.jpg");
        clothGridDiffuse.wrapS = clothGridDiffuse.wrapT = THREE.MirroredRepeatWrapping;
        clothGridDiffuse.repeat.set(4, 4);

        var matClothBrownGrid = new THREE.MeshPhongMaterial({
            color: 0x7d3d31,
            specular: 0x999999,
            shininess: 2,
            map: clothGridDiffuse,
            normalMap: clothGridNormal,
            normalScale: new THREE.Vector2(1.0, 1.0),
        });

        var metalTexture1 = loader.load(CDN.path + "gl/mat/10299-normal.jpg");
        metalTexture1.wrapS = metalTexture1.wrapT = THREE.RepeatWrapping;
        metalTexture1.repeat.set(50, 50);

        var matSilver1 = new THREE.MeshPhongMaterial({
            color: 0xffffff,
            specular: 0xffffff,
            shininess: 90,
            envMap: envMap2,
            reflectivity: 0.7,
            normalMap: metalTexture1,
            normalScale: new THREE.Vector2(0.02, 0.02),
            metal: true,
        });

        //var matCopperDark = new THREE.MeshPhongMaterial({
        //    color: 0x80664d,
        //    specular: 0x80664d,
        //    shininess: 90,
        //    envMap: envMap2,
        //    reflectivity: 0.4,
        //    metal: true,
        //    map:fabric
        //});

        var rings = loader.load(CDN.path + "gl/mat2/radial_normal.jpg");
        metalTexture1.wrapS = rings.wrapT = THREE.RepeatWrapping;
        rings.repeat.set(1, 1);

        var matCopperDark = new THREE.MeshPhongMaterial({
            color: 0x563f30,
            specular: 0x563f30,
            shininess: 35,
            metal: true,
            normalMap: rings,
            normalScale: new THREE.Vector2(0.3, 0.3),
        });

        var matGold1 = new THREE.MeshPhongMaterial({
            color: 0xfbb700,
            specular: 0xffffff,
            shininess: 90,
            envMap: envMap2,
            reflectivity: 0.7,
            normalMap: metalTexture1,
            normalScale: new THREE.Vector2(0.05, 0.05),
            metal: true,
        });

        var plasticTexture = loader.load(CDN.path + "gl/mat/5984-normal.jpg");
        plasticTexture.wrapS = plasticTexture.wrapT = THREE.RepeatWrapping;
        plasticTexture.repeat.set(40, 40);

        var plasticBlack1 = new THREE.MeshPhongMaterial({
            color: 0x222222,
            specular: 0x666666,
            shininess: 30,
            normalMap: plasticTexture,
            normalScale: new THREE.Vector2(0.2, 0.2),
        });

        var plasticBlackGlossy = new THREE.MeshPhongMaterial({
            color: 0x666666,
            specular: 0xffffff,
            shininess: 30,
            envMap: envMap2,
            reflectivity: 0.9,
        });

        var wireNormal = loader.load(CDN.path + "gl/mat2/wire-normal.jpg");
        wireNormal.wrapS = wireNormal.wrapT = THREE.MirroredRepeatWrapping;
        wireNormal.repeat.set(2, 2);

        var wireDiffuse = loader.load(CDN.path + "gl/mat2/wire-diffuse.jpg");
        wireDiffuse.wrapS = wireDiffuse.wrapT = THREE.MirroredRepeatWrapping;
        wireDiffuse.repeat.set(2, 2);

        var wire = new THREE.MeshPhongMaterial({
            color: 0x333333,
            specular: 0xaaaaaa,
            shininess: 30,
            map: wireDiffuse,
            specularMap: wireDiffuse,
            normalMap: wireNormal,
            normalScale: new THREE.Vector2(1, 1),
            metal: true,
        });

        var wireFabric = new THREE.MeshPhongMaterial({
            color: 0x161616,
            specular: 0x888888,
            shininess: 6,
            map: wireDiffuse,
            specularMap: wireDiffuse,
            normalMap: wireNormal,
            normalScale: new THREE.Vector2(1, 1),
        });

        var wire2Normal = loader.load(CDN.path + "gl/mat2/wire-normal.jpg");
        wire2Normal.wrapS = wire2Normal.wrapT = THREE.MirroredRepeatWrapping;
        var wire2Diffuse = loader.load(CDN.path + "gl/mat2/wire-diffuse.jpg");
        wire2Diffuse.wrapS = wire2Diffuse.wrapT = THREE.MirroredRepeatWrapping;
        wire2Diffuse.repeat.set(20, 20);

        var wire2 = new THREE.MeshPhongMaterial({
            color: 0x333333,
            specular: 0xaaaaaa,
            shininess: 30,
            map: wire2Diffuse,
            specularMap: wire2Diffuse,
            normalMap: wire2Normal,
            normalScale: new THREE.Vector2(1, 1),
            metal: true,
        });

        var wireSK6Normal = loader.load(CDN.path + "gl/mat2/wire-normal.jpg");
        wireSK6Normal.wrapS = wireSK6Normal.wrapT = THREE.MirroredRepeatWrapping;
        var wireSK6Diffuse = loader.load(CDN.path + "gl/mat2/wire-diffuse.jpg");
        wireSK6Diffuse.wrapS = wireSK6Diffuse.wrapT = THREE.MirroredRepeatWrapping;
        wireSK6Diffuse.repeat.set(0.8, 0.8);

        var grid_sk6 = new THREE.MeshPhongMaterial({
            color: 0x333333,
            specular: 0xaaaaaa,
            shininess: 30,
            map: wireSK6Diffuse,
            specularMap: wireSK6Diffuse,
            normalMap: wireSK6Normal,
            normalScale: new THREE.Vector2(1, 1),
            metal: true,
        });

        var tNormal2 = loader.load(CDN.path + "gl/mat/7657-normal.jpg");
        tNormal2.wrapS = THREE.RepeatWrapping;
        tNormal2.wrapT = THREE.RepeatWrapping;

        var tNormal3 = loader.load(CDN.path + "gl/mat/5984-normal.jpg");
        tNormal3.wrapS = THREE.RepeatWrapping;
        tNormal3.wrapT = THREE.RepeatWrapping;

        var tNormal4 = loader.load(CDN.path + "gl/mat2/randomnormal.png");
        tNormal4.wrapS = THREE.RepeatWrapping;
        tNormal4.wrapT = THREE.RepeatWrapping;

        var tGradient = loader.load(CDN.path + "gl/mat2/visor.png");
        tGradient.wrapS = THREE.MirroredRepeatWrapping;
        tGradient.wrapT = THREE.MirroredRepeatWrapping;

        var defaultImageCanvas = document.createElement("canvas");
        defaultImageCanvas.setAttribute('width', '2');
        defaultImageCanvas.setAttribute('height', '2');
        var defaultTexture = new THREE.Texture(defaultImageCanvas);
        defaultTexture.premultiplyAlpha = true;
        defaultTexture.needsUpdate = true;

        var tUv = defaultTexture;
        tUv.wrapS = THREE.RepeatWrapping;
        tUv.wrapT = THREE.RepeatWrapping;

        //used for plastic elements
        var visorMat = new THREE.ShaderMaterial({
            uniforms: {
                tNormal2: {type: "t", value: tNormal2},
                tNormal3: {type: "t", value: tNormal3},
                tNormal4: {type: "t", value: tNormal4},
                tGradient: {type: "t", value: tGradient},
                tEnv: {type: "t", value: envMap2},
                alpha: {type: "f", value: 0.4},
            },
            vertexShader: visorVert,
            fragmentShader: visorFrag,
            derivatives: true,
            transparent: true,
            side: THREE.FrontSide
        });

        this.visor2Mat = new THREE.ShaderMaterial({
            uniforms: {
                tUv: {type: "t", value: tUv},
                tNormal2: {type: "t", value: tNormal2},
                tNormal3: {type: "t", value: tNormal3},
                tNormal4: {type: "t", value: tNormal4},
                tGradient: {type: "t", value: tGradient},
                tEnv: {type: "t", value: envMap2},

                alpha: {type: "f", value: 0.4},
                hasSticker: {type: "f", value: 0.0},
                colorOffset: {type: "f", value: 1.0},
                visorColor: {type: "c", value: new THREE.Color("#000000")},
                visorReflectivity: {type: "f", value: 0.3},
                visorAlpha: {type: "f", value: 1}
            },
            vertexShader: visorVert,
            fragmentShader: visor2Frag,
            derivatives: true,
            transparent: true,
            side: THREE.FrontSide
        });

        this.visorInsideMat = new THREE.ShaderMaterial({
            uniforms: {
                tUv: {type: "t", value: tUv},
                tNormal2: {type: "t", value: tNormal2},
                tNormal3: {type: "t", value: tNormal3},
                tNormal4: {type: "t", value: tNormal4},
                tGradient: {type: "t", value: tGradient},
                tEnv: {type: "t", value: envMap2},

                alpha: {type: "f", value: 0.4},
                hasSticker: {type: "f", value: 0.0},
                colorOffset: {type: "f", value: -1.0},
                visorColor: {type: "c", value: new THREE.Color("#000000")},
                visorReflectivity: {type: "f", value: 0.3},
                visorAlpha: {type: "f", value: 1}
            },
            vertexShader: visorVert,
            fragmentShader: visor2Frag,
            derivatives: true,
            transparent: true,
            side: THREE.BackSide,
        });

        this.suncapMat = new THREE.MeshPhongMaterial({
            color: 0x666666,
            specular: 0xffffff,
            shininess: 30,
            envMap: envMap2,
            reflectivity: 0.9,
        });


        function createStandardMaterialFromJSON(json) {
            var options = {
                color: 0xffffff,
                specular: 0xffffff,
                shininess: 30
            };

            if (json.color)
                options.color = parseInt(json.color, 16);

            if (json.specular)
                options.specular = parseInt(json.specular, 16);

            if (json.shininess)
                options.shininess = json.shininess;

            var repeat = new THREE.Vector2(1, 1);
            if (json.repeat)
                repeat = new THREE.Vector2(json.repeat, json.repeat)

            if (json.map) {
                var map = loader.load(CDN.path + json.map);
                map.wrapT = map.wrapS = THREE.RepeatWrapping;
                map.repeat = repeat;
                options.map = map;
            }

            if (json.normalMap) {
                var map = loader.load(CDN.path + json.normalMap);
                map.wrapT = map.wrapS = THREE.RepeatWrapping;
                map.repeat = repeat;
                options.normalMap = map;

                if (json.normalScale) {
                    options.normalScale = new THREE.Vector2(json.normalScale, json.normalScale);
                }
            }

            if (json.specularMap) {
                var map = loader.load(CDN.path + json.specularMap);
                map.wrapT = map.wrapS = THREE.RepeatWrapping;
                map.repeat = repeat;
                options.specularMap = map;
            }

            if (json.reflectivity) {
                options.envMap = envMap2;
                options.reflectivity = json.reflectivity;
            }

            if (json.opacity) {
                options.side = THREE.DoubleSide;
                options.transparent = true;
                options.opacity = json.opacity;
                options.depthWrite = false;
            }

            var result = new THREE.MeshPhongMaterial(options);

            if (json.opacity) {
                result.renderOrder = 1000;
            }

            return result;
        }

        var self = this;

        function parseHelm(mesh) {

            //mesh.position.z = 150;
            //mesh.position.y = -150;

            var json = productModel.json;

            mesh.scale.set(json.scale, json.scale, json.scale);
            mesh.position.x = json.position.x;
            mesh.position.y = json.position.y;
            mesh.position.z = json.position.z;
            mesh.rotation.y = json.rotationY * Math.PI / 180;

            mesh.traverse(function (child) {

                if (child.geometry == null)
                    return;

                // child.visible = false;

                var materialName = json.materials[child.name];
                var type = typeof materialName;
                if (type == "object") {
                    child.material = createStandardMaterialFromJSON(materialName);
                    if (child.material.renderOrder)
                    {
                        child.renderOrder = child.material.renderOrder;
                    }
                    return;
                }

                if (materialName == null && child.name != "")
                    console.log("Default: ", child.name);

                if (materialName != null) {

                    switch (materialName) {

                        case "invisible":
                            child.visible = false;
                            break;

                        case "shell":
                            var currentMat = shellMesh.material;
                            shellMesh = child;
                            shellMesh.material = currentMat;
                            break;

                        case 'visor':

                            child.material = self.visor2Mat;
                            self.visor1.push(child);

                            // copy visor mesh and apply inside visor material
                            var visorInside = child.clone();
                            // visorInside.name += "_inside";
                            visorInside.material = self.visorInsideMat;
                            mesh.add(visorInside);

                            self.visor1.push(visorInside);

                            break;

                        case 'visor_silver':
                            child.material = matSilver1;
                            self.visor1.push(child);
                            break;

                        case 'visor2':
                            child.material = self.visor2Mat;
                            self.visor2.push(child);

                            // copy visor mesh and apply inside visor material
                            var visorInside = child.clone();
                            // visorInside.name += "_inside";
                            visorInside.material = self.visorInsideMat;
                            mesh.add(visorInside);

                            self.visor2.push(visorInside);
                            break;

                        case 'visor2_silver':
                            child.material = matSilver1;
                            self.visor2.push(child);
                            break;

                        case 'suncap':
                            child.material = plasticBlackGlossy;
                            self.suncap1.push(child);
                            break;

                        case 'suncap_silver':
                            child.material = matSilver1; // matSilver1;
                            self.suncap1.push(child);
                            break;

                        case 'suncap2':
                            child.material = plasticBlackGlossy;
                            self.suncap2.push(child);
                            break;

                        case 'suncap2_silver':
                            child.material = matSilver1;
                            self.suncap2.push(child);
                            break;

                        case 'suncap3':
                            child.material = self.suncapMat;
                            self.suncap3.push(child);
                            break;

                        case 'suncap3_silver':
                            child.material = matSilver1;
                            self.suncap3.push(child);
                            break;

                        case 'silver':
                            child.material = matSilver1;
                            break;

                        case 'plastic_transparent':
                            child.material = visorMat;
                            break;

                        case 'gold':
                            child.material = matGold1;
                            break;

                        case 'copper_dark':
                            child.material = matCopperDark;
                            break;

                        case 'plastic_black':
                            child.material = plasticBlack1;
                            break;

                        case 'wire':
                            child.material = wire;
                            break;

                        case 'wireFabric':
                            child.material = wireFabric;
                            break;

                        case 'plastic_black_glossy':
                            child.material = plasticBlackGlossy;
                            break;

                        case 'rubber_brown':
                            child.material = matRubberBrown;
                            break;

                        case 'leather_brown_one':
                            child.material = matRubberBrownOne;
                            break;

                        case 'leather_black_bullit':
                            child.material = leather_black_bullit;
                            break;

                        case 'leather_black_sk6':
                            child.material = leather_black_sk6;
                            break;

                        case 'cloth_brown':
                            child.material = matClothBrown;
                            break;

                        case 'cloth_brown_one':
                            child.material = matClothBrownOne;
                            break;

                        case 'cloth_black':
                            child.material = cloth_black;
                            break;

                        case 'cloth_black_gp6s':
                            child.material = cloth_black_gp6s;
                            break;

                        case 'cloth_black_gp6s_blue':
                            child.material = cloth_black_gp6s_blue;
                            break;

                        case 'cloth_blue_sk6':
                            child.material = cloth_blue_sk6;
                            break;

                        case 'cloth_brown_grid':
                            child.material = matClothBrownGrid;
                            break;

                        case 'leather_black':
                            child.material = matLeather;
                            break;

                        case 'rubber':
                            child.material = matRubber;
                            break;

                        case 'rubberNoiseBlack':
                            child.material = rubberNoiseBlack;
                            break;

                        case 'rubber_rough':
                            child.material = matRubber;
                            break;

                        case 'grid':
                            child.material = wire2;
                            break;

                        case 'grid_sk6':
                            child.material = grid_sk6;
                            break;

                        case 'cloth':
                            child.material = matCloth;
                            break;

                        case 'cloth_grid':
                            child.material = matClothGrid;
                            break;
                    }
                }
            });

            self.setVisor1Visible(self.visor1Visible);
            self.setVisor2Visible(self.visor2Visible);

            self.setSuncap1Visible(self.suncap1Visible);
            self.setSuncap2Visible(self.suncap2Visible);
            self.setSuncap3Visible(self.suncap3Visible);

            self.mesh = mesh;
            self.isLoaded = true;

            self.vc.setMesh(mesh);

            onLoad(mesh, shellMesh);
        }

		var slider = document.getElementById("myRange");
		var output = document.getElementById("demo");
		
		output.innerHTML = slider.value; // Display the default slider value

		// Update the current slider value (each time you drag the slider handle)
		slider.oninput = function() {
			
		  output.innerHTML = this.value;
		  var sliderValue = this.value;
		  self.mesh.traverse(function (child) {
				
				//alert(child.name);
				if (child instanceof THREE.Mesh) {
					if(sliderValue >= 30 && sliderValue <= 35)
					{
						if(child.name == 'insert_35er') {
							child.visible = true;
						}
						else 
					{
							child.visible = false;
					}
					}
					else if(sliderValue >= 36 && sliderValue <= 40)
					{
						if(child.name == 'insert_40er') {
							child.visible = true;
						}else 
						{
								child.visible = false;
						}
						
					}
					else if(sliderValue >= 41 && sliderValue <= 45)
					{
						if(child.name == 'insert_45er') {
							
							child.visible = true;
						}
						else 
						{
								child.visible = false;
						}
					}
					else if(sliderValue >= 46 && sliderValue <= 50)
					{
						if(child.name == 'insert_50er') {
							child.visible = true;
						}
						else 
						{
								child.visible = false;
						}
					}
					else if(sliderValue >= 51 && sliderValue <= 55)
					{
						if(child.name == 'insert_55er') {
							child.visible = true;
						}else 
						{
								child.visible = false;
						}
					}
					else if(sliderValue >= 56 && sliderValue <= 60)
					{
						if(child.name == 'insert_60er') {
							child.visible = true;
						}
						else 
						{
								child.visible = false;
						}
					}
					else if(sliderValue >= 61 && sliderValue <= 70)
					{
						if(child.name == 'insert_70er') {
							child.visible = true;
						}
						else 
						{
								child.visible = false;
						}
						
					}
					else if(sliderValue >= 71 && sliderValue <= 80)
					{
						if(child.name == 'insert_80er') {
							child.visible = true;
						}
						else 
						{
								child.visible = false;
						}
						
					}
					else if(sliderValue >= 81 && sliderValue <= 90)
					{
						if(child.name == 'insert_90er') {
							child.visible = true;
						}
						else 
						{
								child.visible = false;
						}
						
					}
					else if(sliderValue >= 91 && sliderValue <= 100)
					{
						if(child.name == 'insert_100er') {
							child.visible = true;
						}
						else 
						{
								child.visible = false;
						}
						
					}
					else if(sliderValue >= 101 && sliderValue <= 110)
					{
						if(child.name == 'insert_110er') {
							child.visible = true;
						}
						else 
						{
								child.visible = false;
						}
						
					}
					else if(sliderValue >= 111 && sliderValue <= 120)
					{
						if(child.name == 'insert_120er') {
							child.visible = true;
						}
						else 
						{
								child.visible = false;
						}
						
					}
					else 
					{
							child.visible = false;
					}
					
				}
				
            });
		}
		
        this.toggleVisibility = function (name) {
            self.mesh.traverse(function (child) {

                if (child.geometry == null)
                    return;

                var materialName = productModel.json.materials[child.name];

                if (materialName == name) {
                    child.visible = !child.visible;
                }
            });
        }

        this.setVisorStickerColor = function (sku) {
            // console.log("setVisorStickerColor: " + sku);

            //this produces gpu memory leak, as previous textures are not disposed

            if (sku == "color_basic_weiss") {
                self.visor2Mat.uniforms.tUv.value = loader.load(productModel.getWhiteStickerUrl());
            }
            else {
                self.visor2Mat.uniforms.tUv.value = loader.load(productModel.getBlackStickerUrl());
            }
        }

        function startLoading() {
            //loading starts here
            CDN.loadJson(CDN.path + "/gl/helmets/visor_materials.txt", function (materials) {
                CDN.loadJson(productModel.getDesignUrl(), function (designResult) {

                    productModel.designJson = designResult;

                    CDN.loadJson(productModel.getConfigUrl(), function (modelResult) {

                        productModel.json = modelResult;

                        self.vc.initVisorConfig(modelResult.visor, materials, loader);

                        self.visor2Mat.uniforms.hasSticker.value = modelResult.hasSticker;

                        var objLoader = new OBJLoader();
                        objLoader.load(productModel.getObjUrl(), parseHelm);
                    });
                });
            });
        }

        setTimeout(function () {
            startLoading();
        }, 1000);

        this.setVisor = function (sku) {
            console.log("setVisor", sku)
            this.visorSKU = sku;
            this.updateVisor();
        }


        this.updateVisor = function () {

            if (!this.isLoaded) {
                console.warn("ignore updateVisor, not loaded");
                return;
            }

            console.log("updateVisor", this.visorSKU);

            if (this.vc.isActive()) {
                this.vc.updateVisor(this.visorSKU);
            }
            else {
                this.oldVisorUpdate();

            }
        }

        this.oldVisorUpdate = function () {

            console.warn("using old visor update...");

            var helm = this;
            var sku = this.visorSKU;

            switch (sku) {

                //clear
                case '19001':
                case '19008':
                    helm.setVisorVisible(1);
                    helm.setSuncapVisible(0);
                    helm.visor2Mat.uniforms.colorOffset.value = -1;
                    break;

                //light
                case '19002':
                case '19009':
                    helm.setVisorVisible(1);
                    helm.setSuncapVisible(0);
                    helm.visor2Mat.uniforms.colorOffset.value = -2;
                    break;

                //dark
                case '19003':
                case '19010':
                    helm.setVisorVisible(1);
                    helm.setSuncapVisible(0);
                    helm.visor2Mat.uniforms.colorOffset.value = -3;
                    break;

                //silver
                case '19007':
                case '19014':
                    helm.setVisorVisible(1);
                    helm.setSuncapVisible(0);
                    helm.visor2Mat.uniforms.colorOffset.value = -4;
                    break;

                case '19076':
                    helm.setVisorVisible(2);
                    helm.setSuncapVisible(0);
                    helm.visor2Mat.uniforms.colorOffset.value = -4;
                    break;

                //red
                case '19004':
                case '19011':
                    helm.setVisorVisible(1);
                    helm.setSuncapVisible(0);
                    helm.visor2Mat.uniforms.colorOffset.value = 1.0;
                    break;

                //green
                case '19006':
                case '19013':
                    helm.setVisorVisible(1);
                    helm.setSuncapVisible(0);
                    helm.visor2Mat.uniforms.colorOffset.value = 0.5;
                    break;

                //blue
                case '19005':
                case '19012':
                    helm.setVisorVisible(1);
                    helm.setSuncapVisible(0);
                    helm.visor2Mat.uniforms.colorOffset.value = 0.2;

                    break;

                // bullett visors
                case '19025':
                    helm.setVisorVisible(1);
                    helm.setSuncapVisible(0);
                    helm.visor2Mat.uniforms.colorOffset.value = -5;
                    break;

                case '19024':
                    helm.setVisorVisible(2);
                    helm.setSuncapVisible(0);
                    helm.visor2Mat.uniforms.colorOffset.value = -3;
                    break;

                case '19026':
                    helm.setVisorVisible(1);
                    helm.setSuncapVisible(0);
                    helm.visor2Mat.uniforms.colorOffset.value = -3;
                    break;

                case '19027':
                    helm.visor2Mat.uniforms.colorOffset.value = 1.0;

                    var tGradient = loader.load(CDN.path + "gl/mat2/visor_iridium.png");
                    tGradient.wrapS = THREE.MirroredRepeatWrapping;
                    tGradient.wrapT = THREE.MirroredRepeatWrapping;
                    helm.visor2Mat.uniforms.tGradient.value = tGradient;

                    helm.setVisorVisible(1);
                    helm.setSuncapVisible(0);

                    break;

                case '19037':
                    helm.visor2Mat.uniforms.colorOffset.value = -5;

                    helm.setVisorVisible(2);
                    helm.setSuncapVisible(0);

                    break;

                case '19039':
                    helm.visor2Mat.uniforms.colorOffset.value = -1;

                    helm.setVisorVisible(1);
                    helm.setSuncapVisible(0);

                    break;

                case '19040':
                    helm.visor2Mat.uniforms.colorOffset.value = -1;

                    helm.setVisorVisible(2);
                    helm.setSuncapVisible(0);

                    break;

                case '19038':
                    helm.visor2Mat.uniforms.colorOffset.value = 1.0;

                    var tGradient = loader.load(CDN.path + "gl/mat2/visor_iridium.png");
                    tGradient.wrapS = THREE.MirroredRepeatWrapping;
                    tGradient.wrapT = THREE.MirroredRepeatWrapping;
                    helm.visor2Mat.uniforms.tGradient.value = tGradient;

                    helm.setVisorVisible(2);
                    helm.setSuncapVisible(0);

                    break;

                case '23000':
                    helm.setVisorVisible(0);
                    helm.setSuncapVisible(0);

                    helm.visor2Mat.uniforms.colorOffset.value = -1;
                    helm.visorInsideMat.uniforms.colorOffset.value = -1;
                    break;

                case '23001':
                    helm.setVisorVisible(0);
                    helm.setSuncapVisible(2);

                    helm.visor2Mat.uniforms.colorOffset.value = -1;
                    helm.visorInsideMat.uniforms.colorOffset.value = -1;
                    break;

                //cap clear
                case '23002':
                    helm.setVisorVisible(1);
                    helm.setSuncapVisible(1);

                    helm.visor2Mat.uniforms.colorOffset.value = -1;
                    helm.visorInsideMat.uniforms.colorOffset.value = -1;
                    break;

                //cap light
                case '23003':
                    helm.setVisorVisible(1);
                    helm.setSuncapVisible(1);

                    helm.visor2Mat.uniforms.colorOffset.value = -2;
                    helm.visorInsideMat.uniforms.colorOffset.value = -2;
                    break;

                //bubble clear
                case '23004':
                    helm.setVisorVisible(2);
                    helm.setSuncapVisible(0);

                    helm.visor2Mat.uniforms.colorOffset.value = -1;
                    helm.visorInsideMat.uniforms.colorOffset.value = -1;
                    break;

                //bubble blue
                case '23005':
                    helm.setVisorVisible(2);
                    helm.setSuncapVisible(0);

                    helm.visor2Mat.uniforms.colorOffset.value = -7;
                    helm.visor2Mat.uniforms.visorColor.value = new THREE.Color("#00BBEE");
                    helm.visor2Mat.uniforms.visorReflectivity.value = 0.5;
                    helm.visor2Mat.uniforms.visorAlpha.value = 0.6;

                    helm.visorInsideMat.uniforms.colorOffset.value = -7;
                    helm.visorInsideMat.uniforms.visorColor.value = new THREE.Color("#00BBEE");
                    helm.visorInsideMat.uniforms.visorReflectivity.value = 0.5;
                    helm.visorInsideMat.uniforms.visorAlpha.value = 0.6;
                    break;

                //bubble red
                case '23006':
                    helm.setVisorVisible(2);
                    helm.setSuncapVisible(0);

                    helm.visor2Mat.uniforms.colorOffset.value = -7;
                    helm.visor2Mat.uniforms.visorColor.value = new THREE.Color("#EE0000");
                    helm.visor2Mat.uniforms.visorReflectivity.value = 0.5;
                    helm.visor2Mat.uniforms.visorAlpha.value = 0.6;

                    helm.visorInsideMat.uniforms.colorOffset.value = -7;
                    helm.visorInsideMat.uniforms.visorColor.value = new THREE.Color("#EE0000");
                    helm.visorInsideMat.uniforms.visorReflectivity.value = 0.5;
                    helm.visorInsideMat.uniforms.visorAlpha.value = 0.6;
                    break;

                //bubble yellow
                case '23007':
                    helm.setVisorVisible(2);
                    helm.setSuncapVisible(0);

                    helm.visor2Mat.uniforms.colorOffset.value = -7;
                    helm.visor2Mat.uniforms.visorColor.value = new THREE.Color("#EEDD00");
                    helm.visor2Mat.uniforms.visorReflectivity.value = 0.6;
                    helm.visor2Mat.uniforms.visorAlpha.value = 0.6;

                    helm.visorInsideMat.uniforms.colorOffset.value = -7;
                    helm.visorInsideMat.uniforms.visorColor.value = new THREE.Color("#EEDD00");
                    helm.visorInsideMat.uniforms.visorReflectivity.value = 0.6;
                    helm.visorInsideMat.uniforms.visorAlpha.value = 0.6;
                    break;

                //bubble orange
                case '23008':
                    helm.setVisorVisible(2);
                    helm.setSuncapVisible(0);

                    helm.visor2Mat.uniforms.colorOffset.value = -7;
                    helm.visor2Mat.uniforms.visorColor.value = new THREE.Color("#EE6600");
                    helm.visor2Mat.uniforms.visorReflectivity.value = 0.5;
                    helm.visor2Mat.uniforms.visorAlpha.value = 0.6;

                    helm.visorInsideMat.uniforms.colorOffset.value = -7;
                    helm.visorInsideMat.uniforms.visorColor.value = new THREE.Color("#EE6600");
                    helm.visorInsideMat.uniforms.visorReflectivity.value = 0.5;
                    helm.visorInsideMat.uniforms.visorAlpha.value = 0.6;
                    break;

                //bubble gradient blue
                case '23009':
                    helm.setVisorVisible(2);
                    helm.setSuncapVisible(0);

                    var tGradient = loader.load(CDN.path + "gl/mat2/visor_gradient_alpha.png");
                    tGradient.wrapS = THREE.MirroredRepeatWrapping;
                    tGradient.wrapT = THREE.MirroredRepeatWrapping;

                    helm.visor2Mat.uniforms.tGradient.value = tGradient;
                    helm.visor2Mat.uniforms.colorOffset.value = -6;
                    helm.visor2Mat.uniforms.visorColor.value = new THREE.Color("#0077EE");
                    helm.visor2Mat.uniforms.visorReflectivity.value = 0.3;
                    helm.visor2Mat.uniforms.visorAlpha.value = 0.7;

                    helm.visorInsideMat.uniforms.tGradient.value = tGradient;
                    helm.visorInsideMat.uniforms.colorOffset.value = -6;
                    helm.visorInsideMat.uniforms.visorColor.value = new THREE.Color("#0077EE");
                    helm.visorInsideMat.uniforms.visorReflectivity.value = 0.3;
                    helm.visorInsideMat.uniforms.visorAlpha.value = 0.7;
                    break;

                //bubble gradient green
                case '23010':
                    helm.setVisorVisible(2);
                    helm.setSuncapVisible(0);

                    var tGradient = loader.load(CDN.path + "gl/mat2/visor_gradient_alpha.png");
                    tGradient.wrapS = THREE.MirroredRepeatWrapping;
                    tGradient.wrapT = THREE.MirroredRepeatWrapping;

                    helm.visor2Mat.uniforms.tGradient.value = tGradient;
                    helm.visor2Mat.uniforms.colorOffset.value = -6;
                    helm.visor2Mat.uniforms.visorColor.value = new THREE.Color("#339900");
                    helm.visor2Mat.uniforms.visorReflectivity.value = 0.3;
                    helm.visor2Mat.uniforms.visorAlpha.value = 0.7;

                    helm.visorInsideMat.uniforms.tGradient.value = tGradient;
                    helm.visorInsideMat.uniforms.colorOffset.value = -6;
                    helm.visorInsideMat.uniforms.visorColor.value = new THREE.Color("#339900");
                    helm.visorInsideMat.uniforms.visorReflectivity.value = 0.3;
                    helm.visorInsideMat.uniforms.visorAlpha.value = 0.7;
                    break;

                //bubble gradient red
                case '23011':
                    helm.setVisorVisible(2);
                    helm.setSuncapVisible(0);

                    var tGradient = loader.load(CDN.path + "gl/mat2/visor_gradient_alpha.png");
                    tGradient.wrapS = THREE.MirroredRepeatWrapping;
                    tGradient.wrapT = THREE.MirroredRepeatWrapping;

                    helm.visor2Mat.uniforms.tGradient.value = tGradient;
                    helm.visor2Mat.uniforms.colorOffset.value = -6;
                    helm.visor2Mat.uniforms.visorColor.value = new THREE.Color("#EE0000");
                    helm.visor2Mat.uniforms.visorReflectivity.value = 0.3;
                    helm.visor2Mat.uniforms.visorAlpha.value = 0.7;

                    helm.visorInsideMat.uniforms.tGradient.value = tGradient;
                    helm.visorInsideMat.uniforms.colorOffset.value = -6;
                    helm.visorInsideMat.uniforms.visorColor.value = new THREE.Color("#EE0000");
                    helm.visorInsideMat.uniforms.visorReflectivity.value = 0.3;
                    helm.visorInsideMat.uniforms.visorAlpha.value = 0.7;
                    break;

                //bubble gradient red
                case '23012':
                    helm.setVisorVisible(2);
                    helm.setSuncapVisible(0);

                    var tGradient = loader.load(CDN.path + "gl/mat2/visor_gradient_alpha.png");
                    tGradient.wrapS = THREE.MirroredRepeatWrapping;
                    tGradient.wrapT = THREE.MirroredRepeatWrapping;

                    helm.visor2Mat.uniforms.tGradient.value = tGradient;
                    helm.visor2Mat.uniforms.colorOffset.value = -6;
                    helm.visor2Mat.uniforms.visorReflectivity.value = 0.3;
                    helm.visor2Mat.uniforms.visorAlpha.value = 0.7;
                    helm.visor2Mat.uniforms.visorColor.value = new THREE.Color("#FF00DD");

                    helm.visorInsideMat.uniforms.tGradient.value = tGradient;
                    helm.visorInsideMat.uniforms.colorOffset.value = -6;
                    helm.visorInsideMat.uniforms.visorColor.value = new THREE.Color("#FF00DD");
                    helm.visorInsideMat.uniforms.visorReflectivity.value = 0.3;
                    helm.visorInsideMat.uniforms.visorAlpha.value = 0.7;
                    break;

                //bubble gradient red
                case '23013':
                    helm.setVisorVisible(2);
                    helm.setSuncapVisible(0);

                    var tGradient = loader.load(CDN.path + "gl/mat2/visor_gradient_alpha.png");
                    tGradient.wrapS = THREE.MirroredRepeatWrapping;
                    tGradient.wrapT = THREE.MirroredRepeatWrapping;

                    helm.visor2Mat.uniforms.tGradient.value = tGradient;
                    helm.visor2Mat.uniforms.colorOffset.value = -6;
                    helm.visor2Mat.uniforms.visorColor.value = new THREE.Color("#884400");
                    helm.visor2Mat.uniforms.visorReflectivity.value = 0.3;
                    helm.visor2Mat.uniforms.visorAlpha.value = 0.7;

                    helm.visorInsideMat.uniforms.tGradient.value = tGradient;
                    helm.visorInsideMat.uniforms.colorOffset.value = -6;
                    helm.visorInsideMat.uniforms.visorColor.value = new THREE.Color("#884400");
                    helm.visorInsideMat.uniforms.visorReflectivity.value = 0.3;
                    helm.visorInsideMat.uniforms.visorAlpha.value = 0.7;
                    break;

                //bubble gradient red
                case '23014':
                    helm.setVisorVisible(2);
                    helm.setSuncapVisible(0);

                    var tGradient = loader.load(CDN.path + "gl/mat2/visor_gradient_alpha.png");
                    tGradient.wrapS = THREE.MirroredRepeatWrapping;
                    tGradient.wrapT = THREE.MirroredRepeatWrapping;

                    helm.visor2Mat.uniforms.tGradient.value = tGradient;
                    helm.visor2Mat.uniforms.colorOffset.value = -6;
                    helm.visor2Mat.uniforms.visorColor.value = new THREE.Color("#FF6600");
                    helm.visor2Mat.uniforms.visorReflectivity.value = 0.3;
                    helm.visor2Mat.uniforms.visorAlpha.value = 0.7;

                    helm.visorInsideMat.uniforms.tGradient.value = tGradient;
                    helm.visorInsideMat.uniforms.colorOffset.value = -6;
                    helm.visorInsideMat.uniforms.visorColor.value = new THREE.Color("#FF6600");
                    helm.visorInsideMat.uniforms.visorReflectivity.value = 0.3;
                    helm.visorInsideMat.uniforms.visorAlpha.value = 0.7;
                    break;

                //bubble gradient coffee
                case '23015':
                    helm.setVisorVisible(2);
                    helm.setSuncapVisible(0);

                    var tGradient = loader.load(CDN.path + "gl/mat2/visor_gradient_alpha.png");
                    tGradient.wrapS = THREE.MirroredRepeatWrapping;
                    tGradient.wrapT = THREE.MirroredRepeatWrapping;

                    helm.visor2Mat.uniforms.tGradient.value = tGradient;
                    helm.visor2Mat.uniforms.colorOffset.value = -6;
                    helm.visor2Mat.uniforms.visorColor.value = new THREE.Color("#330000");
                    helm.visor2Mat.uniforms.visorReflectivity.value = 0.3;
                    helm.visor2Mat.uniforms.visorAlpha.value = 0.9;

                    helm.visorInsideMat.uniforms.tGradient.value = tGradient;
                    helm.visorInsideMat.uniforms.colorOffset.value = -6;
                    helm.visorInsideMat.uniforms.visorColor.value = new THREE.Color("#330000");
                    helm.visorInsideMat.uniforms.visorReflectivity.value = 0.3;
                    helm.visorInsideMat.uniforms.visorAlpha.value = 0.9;
                    break;

                //bubble gradient dark
                case '23016':
                    helm.setVisorVisible(2);
                    helm.setSuncapVisible(0);

                    var tGradient = loader.load(CDN.path + "gl/mat2/visor_gradient_alpha.png");
                    tGradient.wrapS = THREE.MirroredRepeatWrapping;
                    tGradient.wrapT = THREE.MirroredRepeatWrapping;

                    helm.visor2Mat.uniforms.tGradient.value = tGradient;
                    helm.visor2Mat.uniforms.colorOffset.value = -6;
                    helm.visor2Mat.uniforms.visorColor.value = new THREE.Color("#000000");
                    helm.visor2Mat.uniforms.visorReflectivity.value = 0.3;
                    helm.visor2Mat.uniforms.visorAlpha.value = 0.8;

                    helm.visorInsideMat.uniforms.tGradient.value = tGradient;
                    helm.visorInsideMat.uniforms.colorOffset.value = -6;
                    helm.visorInsideMat.uniforms.visorColor.value = new THREE.Color("#000000");
                    helm.visorInsideMat.uniforms.visorReflectivity.value = 0.3;
                    helm.visorInsideMat.uniforms.visorAlpha.value = 0.8;
                    break;

                //bubble mirrored silver
                case '23017':
                    helm.setVisorVisible(2);
                    helm.setSuncapVisible(0);

                    var tGradient = loader.load(CDN.path + "gl/mat2/visor_gradient_alpha.png");
                    tGradient.wrapS = THREE.MirroredRepeatWrapping;
                    tGradient.wrapT = THREE.MirroredRepeatWrapping;

                    helm.visor2Mat.uniforms.tGradient.value = tGradient;
                    helm.visor2Mat.uniforms.colorOffset.value = -7;
                    helm.visor2Mat.uniforms.visorColor.value = new THREE.Color("#000000");
                    helm.visor2Mat.uniforms.visorReflectivity.value = 0.8;
                    helm.visor2Mat.uniforms.visorAlpha.value = 0.8;

                    helm.visorInsideMat.uniforms.tGradient.value = tGradient;
                    helm.visorInsideMat.uniforms.colorOffset.value = -7;
                    helm.visorInsideMat.uniforms.visorColor.value = new THREE.Color("#000000");
                    helm.visorInsideMat.uniforms.visorReflectivity.value = 0.8;
                    helm.visorInsideMat.uniforms.visorAlpha.value = 0.8;
                    break;

                //bubble mirrored silver blue
                case '23018':
                    helm.setVisorVisible(2);
                    helm.setSuncapVisible(0);

                    var tGradient = loader.load(CDN.path + "gl/mat2/visor_gradient_color.png");
                    tGradient.wrapS = THREE.MirroredRepeatWrapping;
                    tGradient.wrapT = THREE.MirroredRepeatWrapping;

                    helm.visor2Mat.uniforms.tGradient.value = tGradient;
                    helm.visor2Mat.uniforms.colorOffset.value = -6;
                    helm.visor2Mat.uniforms.visorColor.value = new THREE.Color("#002277");
                    helm.visor2Mat.uniforms.visorReflectivity.value = 0.6;
                    helm.visor2Mat.uniforms.visorAlpha.value = 0.8;

                    helm.visorInsideMat.uniforms.tGradient.value = tGradient;
                    helm.visorInsideMat.uniforms.colorOffset.value = -6;
                    helm.visorInsideMat.uniforms.visorColor.value = new THREE.Color("#002277");
                    helm.visorInsideMat.uniforms.visorReflectivity.value = 0.6;
                    helm.visorInsideMat.uniforms.visorAlpha.value = 0.8;
                    break;

                //mx black
                case '23019':
                    helm.setVisorVisible(0);
                    helm.setSuncapVisible(3);
                    helm.suncapMat.transparent = false;
                    helm.suncapMat.opacity = 1;
                    helm.suncapMat.color.setHex(0x666666);
                    helm.suncapMat.specular.setHex(0x666666);
                    helm.suncapMat.emissive.setHex(0x000000);
                    helm.suncapMat.reflectivity = 0.9;
                    helm.suncapMat.shininess = 30;
                    break;

                //mx white
                case '23020':
                    helm.setVisorVisible(0);
                    helm.setSuncapVisible(3);
                    helm.suncapMat.transparent = false;
                    helm.suncapMat.opacity = 1;
                    helm.suncapMat.color.setHex(0xccd3cc);
                    helm.suncapMat.specular.setHex(0x222222);
                    helm.suncapMat.emissive.setHex(0x444444);
                    helm.suncapMat.reflectivity = 0.025;
                    helm.suncapMat.shininess = 90;
                    break;

                //mx transparent black
                case '23021':
                    helm.setVisorVisible(0);
                    helm.setSuncapVisible(3);
                    helm.suncapMat.transparent = true;
                    helm.suncapMat.opacity = 0.8;
                    helm.suncapMat.color.setHex(0x666666);
                    helm.suncapMat.specular.setHex(0x666666);
                    helm.suncapMat.emissive.setHex(0x000000);
                    helm.suncapMat.reflectivity = 0.9;
                    helm.suncapMat.shininess = 30;
                    break;

                default:
                    console.error("no config for visor: " + sku);
                    helm.visor2Mat.uniforms.colorOffset.value = 1.0;
            }
        }
    }

    return Helm1;
});

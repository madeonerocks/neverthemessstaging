define([
    'hr/CDN',
], function (CDN) {


    function VisorConfigurator() {
        console.log("VisorConfigurator");
    }

    VisorConfigurator.prototype.initVisorConfig = function (visorData, materialData, loader) {
        console.log("initVisorConfig", visorData, materialData);
        this.visorData = visorData;
        this.materialData = materialData;
        this.loader = loader;


        this.gradients = [];

        var texture = this.gradients["alpha"] = loader.load(CDN.path + "gl/mat2/visor_gradient_alpha.png");
        texture.wrapS = THREE.MirroredRepeatWrapping;
        texture.wrapT = THREE.MirroredRepeatWrapping;

        var texture = this.gradients["color"] = loader.load(CDN.path + "gl/mat2/visor_gradient_color.png");
        texture.wrapS = THREE.MirroredRepeatWrapping;
        texture.wrapT = THREE.MirroredRepeatWrapping;

        var texture = this.gradients["iridium"] = loader.load(CDN.path + "gl/mat2/visor_iridium.png");
        texture.wrapS = THREE.MirroredRepeatWrapping;
        texture.wrapT = THREE.MirroredRepeatWrapping;

        var texture = this.gradients["rgb"] = loader.load(CDN.path + "gl/mat2/visor.png");
        texture.wrapS = THREE.MirroredRepeatWrapping;
        texture.wrapT = THREE.MirroredRepeatWrapping;
    }

    VisorConfigurator.prototype.setMesh = function (mesh) {
        console.log("setMesh", mesh);
        this.mesh = mesh;
    }

    VisorConfigurator.prototype.isActive = function () {
        var result = this.visorData != null;
        console.log("isActive", result);
        return result;
    }

    VisorConfigurator.prototype.updateVisor = function (sku) {
        console.log("updateVisor", sku, this.mesh, this.visorData, this.materialData);

        var selectedStyle = this.visorData.sku[sku];
        if (selectedStyle == null)
            selectedStyle = {};

        //disable all but selected style elements
        var elementsToDisable = [];
        var extrasToEnable = [];
        var visorToEnable = null;
        var capToEnable = null;

        for (var styleName in this.visorData.styles) {

            var style = this.visorData.styles[styleName];

            var enable = styleName == selectedStyle.style;

            if (enable) {
                if (style.visor)
                    visorToEnable = style.visor;

                if (style.cap)
                    capToEnable = style.cap;

                if (style.extras)
                    extrasToEnable = extrasToEnable.concat(style.extras);
            }
            else {
                if (style.visor)
                    elementsToDisable.push(style.visor);

                if (style.cap)
                    elementsToDisable.push(style.cap);

                if (style.extras)
                    elementsToDisable = elementsToDisable.concat(style.extras);
            }

        }

        var self = this;

        this.mesh.traverse(function (element) {

            var elementShouldBeDisabled = elementsToDisable.indexOf(element.name) != -1;
            if (elementShouldBeDisabled) {
                element.visible = false;
            }

            var extraShouldBeEnabled = extrasToEnable.indexOf(element.name) != -1;
            if (extraShouldBeEnabled) {
                element.visible = true;
            }

            if (element.name == capToEnable) {
                element.visible = true;

                var material = self.materialData.cap_materials[selectedStyle.cap_color];

                if (material == null) {
                    console.warn("no material with name", selectedStyle.cap_color, "in", self.materialData.cap_materials);
                    material = {
                        opacity: 1,
                        color: "#ff0000",
                        specular: "#ff0000",
                        emissive: "#ff0000",
                        reflectivity: 0,
                        shininess: 0
                    }

                }

                var mat = element.material;
                mat.transparent = true;
                mat.opacity = material.opacity;
                mat.color.set(material.color);
                mat.specular.set(material.specular);
                mat.emissive.set(material.emissive);
                mat.reflectivity = material.reflectivity;
                mat.shininess = material.shininess;
            }

            if (element.name == visorToEnable) {
                var material = self.materialData.visor_materials[selectedStyle.visor_color];

                if (material == null) {
                    console.warn("no material with name", selectedStyle.visor_color, "in", self.materialData.visor_materials);
                    material = {};
                    material.colorOffset = -1;
                }

                var mat = element.material;

                //colorOffset
                mat.uniforms.colorOffset.value = material.colorOffset;

                //gradient
                if (material.gradient) {

                    if (material.gradient.indexOf("cdn:") == 0) {
                        var url = CDN.path + material.gradient.substring(4);
                        var texture = self.loader.load(url);
                        texture.wrapS = THREE.MirroredRepeatWrapping;
                        texture.wrapT = THREE.MirroredRepeatWrapping;
                        mat.uniforms.tGradient.value = texture;
                    }
                    else {
                        var texture = self.gradients[material.gradient];
                        if (texture == null) {
                            console.warn("gradient", material.gradient, "not configured");
                            texture = self.gradients["color"];
                        }
                        mat.uniforms.tGradient.value = texture;
                    }
                }
                else {
                    mat.uniforms.tGradient.value = null;
                }

                mat.uniforms.visorColor.value = new THREE.Color(material.color);
                mat.uniforms.visorReflectivity.value = material.reflectivity;
                mat.uniforms.visorAlpha.value = material.alpha;

                element.visible = true;
            }
        });
    }

    return VisorConfigurator;
});

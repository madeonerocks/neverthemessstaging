#define RECIPROCAL_PI2 0.15915494

uniform sampler2D tNormal2;
uniform sampler2D tNormal3;
uniform sampler2D tNormal4;
uniform sampler2D tEnv;
uniform sampler2D tGradient;
uniform sampler2D tUv;
uniform float alpha;

uniform float colorOffset;
uniform float hasSticker;

uniform vec3 visorColor;
uniform float visorAlpha;
uniform float visorReflectivity;

varying vec2 vUv;
varying vec3 vViewPosition;
varying vec3 vNormal;
varying vec3 vWorldPosition;

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

float saturate(float a)
{
	return clamp( a, 0.0, 1.0 );
}

vec3 inverseTransformDirection( in vec3 normal, in mat4 matrix )
{
	return normalize( ( vec4( normal, 0.0 ) * matrix ).xyz );
}

vec3 perturbNormal2Arb( sampler2D map, vec3 eye_pos, vec3 surf_norm, vec2 normalScale, float scale) {

	vec3 q0 = dFdx( eye_pos.xyz );
	vec3 q1 = dFdy( eye_pos.xyz );
	vec2 st0 = dFdx( vUv.st );
	vec2 st1 = dFdy( vUv.st );

	vec3 S = normalize( q0 * st1.t - q1 * st0.t );
	vec3 T = normalize( -q0 * st1.s + q1 * st0.s );
	vec3 N = normalize( surf_norm );

	vec3 mapN = texture2D( map, vUv * scale ).xyz * 2.0 - 1.0;
	mapN.xy = normalScale * mapN.xy;
	mat3 tsn = mat3( S, T, N );
	return normalize( tsn * mapN );

}

void main() {

	vec3 viewDir = normalize( vViewPosition );

	vec3 result = vec3(0, 0, 0);

	vec3 specular = vec3(1, 1, 1);
	float shininess = 4.0;
	float specularStrength = 1.0;

	vec3 normal = normalize( vNormal );
	vec3 normal3 = perturbNormal2Arb(tNormal2, -vViewPosition, normal, vec2(0.001, 0.001) , 30.0);
	normal3 = perturbNormal2Arb(tNormal3, -vViewPosition, normal3, vec2(0.00, 0.00) , 40.0);

	vec3 totalDiffuseLight = vec3( 0.0 );
	vec3 totalSpecularLight = vec3( 0.0 );

	vec3 lightColor = vec3(1.0, 1.0, 1.0);
	vec3 lightDir1 = normalize(vec3(0, 0.25, 1.0));
	vec3 lightDir2 = normalize(vec3(0, 0.25, 1.0));

	// diffuse

	float lambert1 = saturate( dot( normal, lightDir1 ) );
	float lambert2 = saturate( dot( normal, lightDir2 ) );
	float cosineTerm1 = saturate(sin(lambert1 * 12.6));
	float cosineTerm2 = saturate(sin(lambert1 * 17.0));

	vec3 cameraToVertex = normalize( vWorldPosition - cameraPosition );
	vec3 worldNormalPlastic = inverseTransformDirection( normal3, viewMatrix );
	vec3 reflectVec = reflect( cameraToVertex, worldNormalPlastic );

	vec2 sampleUV;
	sampleUV.y = saturate(reflectVec.y * 0.5 + 0.5 );
	sampleUV.x = atan(reflectVec.z,reflectVec.x ) * RECIPROCAL_PI2 + 0.5;
	vec3 envColor = texture2D( tEnv, sampleUV ).xyz;
	envColor += vec3(1,1,1) * (cos((sampleUV.y - 0.5) * 15.0) * 0.5 + 0.5) * 0.2;
	envColor += vec3(1,1,1) * cosineTerm1 * 0.15;
	envColor += vec3(1,1,1) * cosineTerm2 * 0.05;

	vec3 worldNormal = inverseTransformDirection( normal, viewMatrix );
	vec3 reflectVecNormal = reflect( cameraToVertex, worldNormal );

	float front = pow(saturate(dot( normal, vec3(0,0.3,1))), 5.0);

	vec3 worldNormalNoise = perturbNormal2Arb(tNormal4, -vViewPosition, normal, vec2(0.01, 0.01) , 20.0);

	float hue = 1.0 - (-dot(normalize(reflectVecNormal * vec3(1,-1.5,1)), normalize(worldNormalNoise * vec3(-1,-1.5,-1))));
	vec3 rgb = texture2D(tGradient, vec2(hue * colorOffset + (1.0 - colorOffset), 0.5)).xyz; //red

  float cornerIntensity = 1.0;

  if (colorOffset < -6.9)
	{
		result += visorColor;
		result += envColor * visorReflectivity;
		gl_FragColor = vec4(result, visorAlpha);
	}
  else if (colorOffset < -5.9)
  {
    cornerIntensity = 0.25;

    gl_FragColor = vec4(envColor, 0.0) * visorReflectivity + vec4(texture2D(tGradient, vUv).rgb * visorColor, texture2D(tGradient, vUv).a * visorAlpha);
  }
	else if (colorOffset < -4.9)
	{
		result += vec3(0.8, 0.7, 0.0);
		result += envColor * 0.3;
		gl_FragColor = vec4(result, 0.6);
	}
	else if (colorOffset < -3.9)
	{
		result += vec3(0, 0.005, 0.015);
		result += envColor * 0.8;
		gl_FragColor = vec4(result, 0.8);
	}
	else if (colorOffset < -2.9)
	{
		result += vec3(0, 0.005, 0.01);
		result += envColor * 0.2;
		gl_FragColor = vec4(result, 0.7);
	}
	else if (colorOffset < -1.9)
	{
		result += vec3(0, 0.005, 0.01);
		result += envColor * 0.4;
		gl_FragColor = vec4(result, 0.4);
	}
	else if (colorOffset < -0.9)
	{
		result += envColor * 0.9;
		gl_FragColor = vec4(result, 0.15);
	}
	else
	{
		float envIntense = pow(length(envColor), 0.5);
		result += rgb * envIntense * 0.9;
		result += rgb * 0.05;
		result += rgb * front * 0.05;
		result += rgb * saturate(cosineTerm1) * 0.05;
		result *= (0.7 + envIntense * 0.3);
		gl_FragColor = vec4(result, 0.95);
	}

	vec3 cornerColor = vec3(0.1,0.1,0.1);
	cornerColor += envColor * 0.5;

	float cornerColorMix1 = worldNormal.y - 0.3;
	float cornerColorMix2 = worldNormal.z - 0.3;
	float cornerColorMix3 = -worldNormal.y;
	float cornerColorMix = 0.0;
	cornerColorMix += clamp(cornerColorMix1, 0.0, 1.0);
	cornerColorMix += clamp(cornerColorMix2, 0.0, 1.0);
	cornerColorMix += clamp(cornerColorMix3, 0.0, 1.0);

	gl_FragColor = mix(gl_FragColor, vec4(cornerColor, 1), cornerColorMix * cornerIntensity);


	if (hasSticker > 0.5)
	{
		vec4 stickerMap = texture2D(tUv, vUv);
		if (stickerMap.a > 0.1)
		{
			vec3 sticker = vec3(0,0,0);

			vec3 sickerNormal = perturbNormal2Arb(tNormal3, -vViewPosition, normal, vec2(-0.01, -0.01), 12.0);
			float lambertSticker = saturate( dot( sickerNormal, lightDir1 ) );
			float cosineTermSticker = saturate(sin(lambertSticker * 12.6));

			vec3 worldNormalSticker = inverseTransformDirection( sickerNormal, viewMatrix );
			vec3 reflectVecSticker = reflect( cameraToVertex, worldNormalSticker );

			vec2 stickerEnvUV;
			stickerEnvUV.y = saturate(reflectVecSticker.y * 0.5 + 0.5 );
			stickerEnvUV.x = atan(reflectVecSticker.z,reflectVecSticker.x ) * RECIPROCAL_PI2 + 0.5;
			vec3 envColorSticker = texture2D( tEnv, stickerEnvUV ).xyz;

			sticker += envColorSticker * 0.3;
			sticker += stickerMap.rgb * 0.5;
			sticker += stickerMap.rgb * lambertSticker * 0.4;
			sticker += vec3(1,1,1) * cosineTermSticker * 0.1;

            //hemi
            float lambertHemi1 = dot( sickerNormal, vec3(0.4, 0.4, -0.4));
            float lambertHemi2 = dot( sickerNormal, vec3(-0.1, -0.8, -0.1));
            float hemi1 = clamp(lambertHemi1, 0.0, 0.5);
            float hemi2 = clamp(lambertHemi2, 0.0, 0.5);

            sticker += result * vec3(0.8,0.9,1.2) * hemi1 * 0.3;
            sticker += vec3(0.0,0.05,0.1) * hemi1 * 0.3;

            sticker -= result * vec3(0.8,1.0,1.2) * hemi2 * 0.3;
            sticker -= vec3(0.1,0.1,0.3) * hemi2 * 0.3;

			gl_FragColor = mix(gl_FragColor, vec4(sticker, stickerMap.a), pow(stickerMap.a, 2.0));
		}
	}
}

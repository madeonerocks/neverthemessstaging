window.startApp = function (frame) {
    console.log("window.startApp")

    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    console.log("registering error event");
    window.addEventListener('error', function (evt) {
        console.error("Caught[via 'error' event]:  '" + evt.message + "' from " + evt.filename + ":" + evt.lineno);
        evt.preventDefault();
    });

    var config = {
        baseUrl: './',
        shim: {
            featherlight: {
                deps: [
                    'jquery'
                ]
            }
        },
        paths: {
            app: 'js/app',
            hr: 'js/hr',
            vendor: 'js/vendor',
            lib: 'js/lib',
            jquery: 'js/vendor/jquery-2.1.4.min',
            featherlight: 'js/lib/featherlight/featherlight',
            text: '/skin/frontend/helmade/default/js/lib/text',
            json: '/skin/frontend/helmade/default/js/lib/json'
        },
        urlArgs: "bust=" + "3",
        waitSeconds: 200
    };

    var appName = getParameterByName('app');
    document.title = appName;

    require.config(config);

    require(['jquery', 'vendor/three', 'vendor/dat.gui'], function () {
        require([frame], function () {
            require(['app/' + appName + '/' + appName], function () {
                console.log("window.startApp finished");
            });
        });

    });
}
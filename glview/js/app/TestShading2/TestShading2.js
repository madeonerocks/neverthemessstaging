define([
        "hr/CDN",
        "Frame",
        "hr/utils/browser",
        "hr/DefaultMaterialFactory",
        "hr/loader/OBJLoader",
        "text!app/TestShading2/ibl.vert",
        "text!app/TestShading2/ibl_shell.frag",
        "text!app/TestShading2/ibl_visor.frag",
    ],
    function (CDN,
              Frame,
              browser,
              MaterialFactory,
              OBJLoader,
              ibl_vert,
              ibl_shell,
              ibl_visor) {

        var frame = new Frame();

        var renderer = frame.renderer;
        renderer.setClearColor(0xffffff, 1);
        renderer.autoClear = false;

        var scene = new THREE.Scene();

        var camera = new THREE.PerspectiveCamera(45, 1, 1, 10000);
        camera.position.x = 200;
        camera.position.z = -500;

        var hemiLight = new THREE.HemisphereLight(0xffffff, 0x000000, 0.4);
        hemiLight.position.set(0.3, 0.3, 1);
        scene.add(hemiLight);

        directionalLight = new THREE.DirectionalLight(0xffffff);
        directionalLight.intensity = 0.2;
        directionalLight.position.set(-0.5, 0.5, 0.2);
        scene.add(directionalLight);

        controls = new THREE.OrbitControls(camera, renderer.domElement);
        controls.enableDamping = true;
        controls.dampingFactor = 0.05;
        controls.enableZoom = true;
        controls.rotateSpeed = 0.1;
        controls.zoomSpeed = 0.2;

        var loader = new THREE.TextureLoader();
        loader.setCrossOrigin('');

        var mapUV = loader.load(CDN.path + "gl/mat/UV_Grid_Sm.jpg");
        mapUV.wrapS = THREE.RepeatWrapping;
        mapUV.wrapT = THREE.RepeatWrapping;
        mapUV.repeat = new THREE.Vector2(20, 20);

        var shellNormal = loader.load(CDN.path + "gl/mat/7657-normal.jpg");
        shellNormal.wrapS = THREE.RepeatWrapping;
        shellNormal.wrapT = THREE.RepeatWrapping;
        shellNormal.repeat = new THREE.Vector2(1, 1);

        var leather = loader.load(CDN.path + "gl/mat/maps/leather/4538-diffuse.jpg")
        var leatherNormal = loader.load(CDN.path + "gl/mat/bake_test_01/LederVRayBumpNormalsMap.png");

        var matLeather = new THREE.MeshPhongMaterial({
            color: 0x666666,
            specular: 0x444444,
            shininess: 50,
            map: leather,
            normalMap: leatherNormal,
            normalScale: new THREE.Vector2(5, 5),
            lightMap: loader.load(CDN.path + "gl/mat/bake_test_01/LederVRayLightingMap.png")
        });

        var tDiffuse = loader.load(CDN.path + "gl/mat/mirror/ModelTest/" + "diffuse.png");
        var tSpecular = loader.load(CDN.path + "gl/mat/mirror/ModelTest/" + "helmade-final-s.jpg");
        var tNormal = loader.load(CDN.path + "gl/mat/mirror/ModelTest/" + "helmade-final-n.jpg");
        var tEnv = loader.load(CDN.path + "gl/mat/mirror/ModelTest/" + "env/9.jpg");

        var tNormal2 = loader.load(CDN.path + "gl/mat/7657-normal.jpg");
        var tNormal3 = loader.load(CDN.path + "gl/mat/5984-normal.jpg");
        tNormal2.wrapS = THREE.RepeatWrapping;
        tNormal2.wrapT = THREE.RepeatWrapping;
        tNormal3.wrapS = THREE.RepeatWrapping;
        tNormal3.wrapT = THREE.RepeatWrapping;


        var shellMat = new THREE.ShaderMaterial({
            uniforms: {
                tDiffuse: {type: "t", value: loader.load(CDN.path + "gl/mat/helmade-final.jpg")},
                tAmbient: {type: "t", value: tDiffuse},
                tSpecular: {type: "t", value: tSpecular},
                tNormal: {type: "t", value: tNormal},
                tNormal2: {type: "t", value: tNormal2},
                tNormal3: {type: "t", value: tNormal3},
                tEnv: {type: "t", value: tEnv},
            },
            vertexShader: ibl_vert,
            fragmentShader: ibl_shell,
            derivatives: true,
            //passLights: true
        });

        var visorMat = new THREE.ShaderMaterial({
            uniforms: {
                tDiffuse: {type: "t", value: loader.load(CDN.path + "gl/mat/helmade-final.jpg")},
                tAmbient: {type: "t", value: tDiffuse},
                tSpecular: {type: "t", value: tSpecular},
                tNormal: {type: "t", value: tNormal},
                tNormal2: {type: "t", value: tNormal2},
                tNormal3: {type: "t", value: tNormal3},
                tEnv: {type: "t", value: tEnv},
                alpha: {type: "f", value: 0.4},
            },
            vertexShader: ibl_vert,
            fragmentShader: ibl_visor,
            derivatives: true,
            //passLights: true,
            transparent: true,
        });

        var matRubber = new THREE.MeshPhongMaterial({
            color: 0x151515,
            specular: 0x333333,
            shininess: 50
        });

        var fabric = loader.load(CDN.path + "gl/mat/maps/fabric/7415-diffuse.jpg")
        fabric.wrapS = THREE.RepeatWrapping;
        fabric.wrapT = THREE.RepeatWrapping;
        fabric.repeat = new THREE.Vector2(50, 50);

        var fabricNormal = loader.load(CDN.path + "gl/mat/maps/fabric/7415-normal.jpg")
        fabricNormal.wrapS = THREE.RepeatWrapping;
        fabricNormal.wrapT = THREE.RepeatWrapping;

        var matCloth = new THREE.MeshPhongMaterial({
            color: 0x444488,
            specular: 0x999999,
            shininess: 4,
            map: fabric,
            normalMap: fabricNormal,
            normalScale: new THREE.Vector2(0.5, 0.5),
        });

        var envMap2 = loader.load(CDN.path + "gl/mat/mirror/ModelTest/" + "env/9.jpg");
        envMap2.mapping = THREE.EquirectangularReflectionMapping;

        var metalTexture1 = loader.load(CDN.path + "gl/mat/10299-normal.jpg");
        metalTexture1.wrapS = metalTexture1.wrapT = THREE.RepeatWrapping;
        metalTexture1.repeat.set(10, 10);

        var metalTexture2 = loader.load(CDN.path + "gl/mat/10299-normal.jpg");
        metalTexture2.wrapS = metalTexture2.wrapT = THREE.RepeatWrapping;
        metalTexture2.repeat.set(1, 1);


        var matSilver1 = new THREE.MeshPhongMaterial({
            color: 0xffffff,
            specular: 0xffffff,
            shininess: 90,
            envMap: envMap2,
            reflectivity: 0.7,
            normalMap: metalTexture1,
            normalScale: new THREE.Vector2(0.1, 0.1),
            metal: true,
        });

        var matGold1 = new THREE.MeshPhongMaterial({
            color: 0xfbb700,
            specular: 0xffffff,
            shininess: 90,
            envMap: envMap2,
            reflectivity: 0.7,
            normalMap: metalTexture1,
            normalScale: new THREE.Vector2(0.1, 0.1),
            metal: true,
        });

        var matSilver2 = new THREE.MeshPhongMaterial({
            color: 0xffffff,
            specular: 0xffffff,
            shininess: 90,
            envMap: envMap2,
            reflectivity: 0.7,
            normalMap: metalTexture2,
            normalScale: new THREE.Vector2(0.1, 0.1),
            metal: true,
        });

        var plasticTexture = loader.load(CDN.path + "gl/mat/5984-normal.jpg");
        plasticTexture.wrapS = plasticTexture.wrapT = THREE.RepeatWrapping;
        plasticTexture.repeat.set(10, 10);

        var plasticBlack1 = new THREE.MeshPhongMaterial({
            color: 0x333333,
            specular: 0x666666,
            shininess: 30,
            normalMap: plasticTexture,
            normalScale: new THREE.Vector2(0.2, 0.2),
        });

        function load() {
            var manager = new THREE.LoadingManager();
            manager.onProgress = function (item, loaded, total) {

                console.log(item, loaded, total);

            };

            var onProgress = function (xhr) {
                if (xhr.lengthComputable) {
                    var percentComplete = xhr.loaded / xhr.total * 100;
                    console.log(Math.round(percentComplete, 2) + '% downloaded');
                }
            };

            var onError = function (xhr) {
            };


            // model

            var url = CDN.path + 'gl/mat/Helm_05.obj';
            var loader = new OBJLoader(manager);
            loader.load(url, function (object) {

                //console.log("Loaded...", object)
                object.position.z = 150;
                object.position.y = -150;

                object.traverse(function (x) {
                    switch (x.name) {

                        case 'zylon_sticker':
                            x.visible = false;
                            break;

                        case 'visor':
                            x.material = visorMat;
                            break;

                        case 'Helmet_000':
                            x.material = shellMat;
                            break;

                        case 'screw_visor_r':
                        case 'screw_visor_r001':
                        case 'Object001':
                        case 'Object002':
                        case 'pin_r':
                        case 'pin_l':

                            x.material = matSilver1;
                            break;

                        case 'screw_visor_r_RING':
                        case 'screw_visor_l_RING001':
                            x.material = matSilver2;
                            break;

                        case 'top_peds':
                        case 'Plane004':
                        case 'Plane019':
                            x.material = visorMat;
                            break;

                        case 'visor_clip':
                        case 'visor_clipper_Screw':
                            x.material = matGold1;
                            break;

                        //rect stufff
                        case 'visor_fix_l':
                        case 'visor_fix_r':

                        case 'visor_clipper':
                        case 'top1':
                        case 'top2':
                        case 'top3':
                        case 'stopper_r':
                        case 'stopper_r002':
                            x.material = plasticBlack1;
                            break;

                        case 'Plane005':
                            x.material = matLeather;
                            break;

                        case 'Shape001':
                        case 'Shape002':
                        case 'Plane006':
                            x.material = matRubber;
                            break;
                        case 'New_Helm_I001':
                            x.material = matCloth;
                            break;
                        default:
                            console.log("Default: ", x);


                    }
                });

                scene.add(object);

            }, onProgress, onError);
        }

        load();

        var parameters = {
            minFilter: THREE.LinearFilter,
            magFilter: THREE.LinearFilter,
            format: THREE.RGBFormat,
            stencilBuffer: false
        };
        var renderTarget = new THREE.WebGLRenderTarget(64, 64, parameters);
        var composer = new THREE.EffectComposer(frame.renderer, renderTarget);
        composer.addPass(new THREE.RenderPass(scene, camera));

        var bloom = new THREE.BloomPass(1.8, 12, 15, 512);
        composer.addPass(bloom);

        var bokehPass = new THREE.BokehPass(scene, camera, {
            focus: 0.9,
            aperture: 3.0,
            maxblur: 1.0,

            width: 200,
            height: 200
        });
        //composer.addPass( bokehPass );

        var fxaa = new THREE.ShaderPass(THREE.FXAAShader);
        composer.addPass(fxaa);

        var copy = new THREE.ShaderPass(THREE.CopyShader);
        //composer.addPass( copy );

        composer.passes[composer.passes.length - 1].renderToScreen = true;

        frame.onLayout.add(function () {
            var size = frame.renderer.getSize();
            camera.aspect = size.width / size.height;
            camera.updateProjectionMatrix();

            fxaa.uniforms.resolution.value = new THREE.Vector2(1 / size.width / renderer.getPixelRatio(), 1 / size.height / renderer.getPixelRatio());

            composer.reset();
        });


        frame.onFrame.add(function (d) {
            controls.update();

            hemiLight.position.set(camera.position.x, camera.position.y, camera.position.z);
            directionalLight.position.set(camera.position.x, camera.position.y, camera.position.z);

            //frame.renderer.render(scene, camera);

            //frame.renderer.clear();
            composer.render(0.1);
        });

        frame.start();
    }
)
;
varying vec2 vUv;
varying vec3 vViewPosition;
varying vec3 vNormal;
varying vec3 vWorldPosition;

void main() {
	vUv = uv;
	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );

	vec3 transformed = vec3( position );
	vec4 mvPosition = modelViewMatrix * vec4( transformed, 1.0 );
	vViewPosition = - mvPosition.xyz;

	vec3 objectNormal = vec3( normal );
	vec3 transformedNormal = normalMatrix * objectNormal;
	vNormal = normalize( transformedNormal );

	vec4 worldPosition = modelMatrix * vec4( transformed, 1.0 );
	vWorldPosition = worldPosition.xyz;
}
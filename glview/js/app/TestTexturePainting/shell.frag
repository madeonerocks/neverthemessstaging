varying vec2 vUv;

uniform sampler2D tDiffuse1;
uniform sampler2D tDiffuse2;
uniform sampler2D tDiffuse3;

uniform vec3 c0;
uniform vec3 c1;
uniform vec3 c2;
uniform vec3 c3;

uniform float time;

void main() {

	float t1 = texture2D( tDiffuse1, vUv ).r;
	float t2 = texture2D( tDiffuse2, vUv ).r;
	float t3 = texture2D( tDiffuse3, vUv ).r;

	vec3 rgb = c0;
	rgb = mix(rgb, c1, t1);
	rgb = mix(rgb, c2, t2);
	rgb = mix(rgb, c3, t3);

	gl_FragColor = vec4(rgb, 1);

}
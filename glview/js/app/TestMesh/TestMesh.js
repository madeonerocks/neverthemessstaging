define([
        "Frame",
        "hr/loader/OBJLoader",
        "hr/CDN"
    ],
    function (Frame,
              OBJLoader,
              CDN) {

        var mat = new THREE.MeshPhongMaterial({
            color: 0x336699,
            specular: 0xffffff,
            shininess: 1,
            wireframe: true
        });

        var frame = new Frame();

        var renderer = frame.renderer;
        renderer.setClearColor(0, 0);

        var scene = new THREE.Scene();

        var camera = new THREE.PerspectiveCamera(45, 1, 1, 10000);
        camera.position.x = 500;
        camera.position.z = -200;

        ambientLight = new THREE.AmbientLight(0x888888);
        scene.add(ambientLight);

        directionalLight = new THREE.DirectionalLight(0x777777);
        directionalLight.position.set(1, -0.5, -1);
        scene.add(directionalLight);

        controls = new THREE.OrbitControls(camera, renderer.domElement);
        controls.enableDamping = true;
        controls.dampingFactor = 0.05;
        controls.enableZoom = true;
        controls.rotateSpeed = 0.1;
        controls.zoomSpeed = 0.2;

        console.log("Loading Model...");
        loader = new OBJLoader();
        loader.load(CDN.path + "gl/mat/mirror/ModelTest/Helm_05.obj", function (s) {

            var m = s;
            m.position.z = 150;
            m.position.y = -150;
            console.log("Model Loaded!");

            m.traverse(function (x) {

                if (x.geometry != null) {
                    x.material = mat;
                }
            });
            scene.add(m);
        });

        frame.onLayout.add(function () {
            console.log("Layouting...");
            var size = frame.renderer.getSize();
            camera.aspect = size.width / size.height;
            camera.updateProjectionMatrix();
        });

        var f = 0;
        frame.onFrame.add(function () {
            if (f == 0)
                console.log("Rendering first frame...");

            controls.update();
            directionalLight.position.set(camera.position.x, camera.position.y, camera.position.z);
            frame.renderer.render(scene, camera);

            f++;
        });

        frame.start();
    }
)
;
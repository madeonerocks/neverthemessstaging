varying vec2 vUv;
uniform float time;

void main() {

	float r = -time * 0.00;
	float k1 = vUv.y * sin(r) + vUv.x * cos(r);
	float n = 1.5;
	float k2 = vUv.y * sin(r + n) + vUv.x * cos(r + n);
	float v1 = pow(sin(k1 * 150.0 + time * 0.3), 500.0);
	float v2 = pow(sin(k2 * 150.0 + time * 0.3), 500.0);

	float c = (v1 + v2) * 10.0 + 0.3;
	float t = 0.0;

	c += t;
	gl_FragColor = vec4(
		c * 0.5,
		c * 0.8,
		c,
		1);

}
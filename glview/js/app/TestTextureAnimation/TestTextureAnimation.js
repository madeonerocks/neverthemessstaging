define([
        "hr/CDN",
        "Frame",
        "hr/DefaultMaterialFactory",
        "hr/loader/OBJLoader",
        "text!app/TestTextureAnimation/default.vert",
        "text!app/TestTextureAnimation/diffuse.frag",
        "text!app/TestTextureAnimation/shell.frag",
    ],
    function (CDN,
              Frame,
              MaterialFactory,
              ObjectLoader,
              defaultVertex,
              diffuseFrag,
              shellFrag) {

        var matFactory = new MaterialFactory();
        var frame = new Frame();

        var renderer = frame.renderer;
        renderer.autoClear = false;
        renderer.setClearColor(0, 0);
        
        var materialShell = new THREE.ShaderMaterial({
            uniforms: {time: {type: "f", value: 0.1}},
            vertexShader: defaultVertex,
            fragmentShader: shellFrag,
            side: THREE.DoubleSide,
            depthWrite: false
        });

        var rtTexture = new THREE.WebGLRenderTarget(2048, 2048, {
            minFilter: THREE.LinearFilter,
            magFilter: THREE.LinearFilter,
            format: THREE.RGBFormat,
        });
        rtTexture.texture.wrapS = rtTexture.texture.wrapT = THREE.RepeatWrapping;

        var textureScreen = new THREE.Scene();

        var plane2 = new THREE.PlaneBufferGeometry(1, 1);
        var quad2 = new THREE.Mesh(plane2, materialShell);

        textureScreen.add(quad2);

        var cameraTextureRTT = new THREE.OrthographicCamera(-0.5, 0.5, 0.5, -0.5, -10000, 10000);

        var sceneScreen = new THREE.Scene();

        var materialScreen2 = new THREE.ShaderMaterial({
            uniforms: {tDiffuse: {type: "t", value: rtTexture}},
            vertexShader: defaultVertex,
            fragmentShader: diffuseFrag,
            side: THREE.DoubleSide,
            depthWrite: false
        });


        var plane = new THREE.PlaneBufferGeometry(1, 1);
        quad = new THREE.Mesh(plane, materialScreen2);
        quad.position.x = 0.5;
        quad.position.y = 0.5;

        var sss = new THREE.Object3D();
        sss.add(quad);
        sceneScreen.add(sss);

        cameraRTT = new THREE.OrthographicCamera(0, 1, 0, 1, -10000, 10000);

        matFactory.mat('shellTexture').map = rtTexture;
        //matFactory.mat('shellTexture').map = metalTexture;

        var scene = new THREE.Scene();

        var camera = new THREE.PerspectiveCamera(45, 1, 1, 10000);
        camera.position.x = 200;
        camera.position.z = -500;

        var hemiLight = new THREE.HemisphereLight(0xd2dbff, 0x9d8787, 1.0);
        hemiLight.position.set(0, 1, 0.1);
        scene.add(hemiLight);

        directionalLight = new THREE.DirectionalLight(0x777777);
        directionalLight.intensity = 0.2;
        directionalLight.position.set(-1, 1, 0.3);
        scene.add(directionalLight);

        controls = new THREE.OrbitControls(camera, renderer.domElement);
        controls.enableDamping = true;
        controls.dampingFactor = 0.05;
        controls.enableZoom = true;
        controls.rotateSpeed = 0.1;
        controls.zoomSpeed = 0.2;

        Object.defineProperty(THREE.Color.prototype, "datColor", {
            get: function datColor() {
                return this.getStyle()
            },
            set: function datColor(value) {
                this.set(value);
            }
        });

        loader = new ObjectLoader();
        loader.load(CDN.path + "gl/mat/mirror/ModelTest/Helm_05.obj", function (s) {

            var m = s;
            m.position.z = 150;
            m.position.y = -150;
            console.log("m: " + m);

            m.traverse(function (x) {

                if (x.geometry != null)
                    switch (x.name) {
                        case 'Helmet_000':
                            x.material = matFactory.mat('shellTexture');
                            break;
                    }
            });

            scene.add(m);
        });

        frame.onLayout.add(function () {
            var size = frame.renderer.getSize();

            camera.aspect = size.width / size.height;
            camera.updateProjectionMatrix();

            sss.scale.x = 400;
            sss.scale.y = 400;

            cameraRTT.top = 0;
            cameraRTT.bottom = size.height;

            cameraRTT.left = 0;
            cameraRTT.right = size.width;
            cameraRTT.updateProjectionMatrix();
        });

        var f = 0;
        frame.onFrame.add(function () {
            controls.update();

            f++;

            materialShell.uniforms.time.value = f * 0.1;

            hemiLight.position.set(camera.position.x, camera.position.y, camera.position.z);
            directionalLight.position.set(camera.position.x, camera.position.y, camera.position.z);

            frame.renderer.clear();
            frame.renderer.render(textureScreen, cameraTextureRTT, rtTexture, true);
            frame.renderer.render(scene, camera);
        });

        frame.start();
    }
)
;
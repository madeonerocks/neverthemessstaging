define([
    "jquery",
    "hr/utils/Fonts",
    "hr/CDN",
    "hr/utils/DebugScene",
    "hr/shell/ShellModel",
    "hr/shell/ShellMultiRTT",
    "hr/mat/glossy1/Glossy1",
    "hr/mat/matte1/Matte1",
    "hr/mat/helm1/Helm1",
    "hr/mat/projections/Projections1",
    "hr/utils/HelmControls",
    "hr/model/ProductModel",
    "hr/utils/SnapShot",
    "hr/utils/ThumbnailCreator",
    "hr/utils/Flicker",
    "hr/utils/DebugMaterials",
    "vendor/three"
], function ($,
             Fonts,
             CDN,
             DebugScene,
             ShellModel,
             ShellMultiRTT,
             Glossy1,
             Matte1,
             Helm1,
             Projections1,
             HelmControls,
             ProductModel,
             SnapShot,
             ThumbnailCreator,
             Ficker,
             DebugMaterials) {

    "use strict";

    function GLComponent(frame, config, delegate, baseModel, designLine) {

        Fonts.start();

        var loader = new THREE.TextureLoader();
        loader.setCrossOrigin('');

        this.debug = {};

        var loaded = false;
        var started = false;
        var startWithAnim = typeof(TweenMax) != "undefined";
        var controlsRunning = false;
        var autoRotateRunning = false;
        var autoRotateAngleAdd = 0;
        var startX = -344.8620530110187;
        var startY = -220.65847908708452;

        var productModel = new ProductModel(baseModel, designLine);
        productModel.config = config;

        var debugScene;

        var shellMultiRTT;
        var shellMesh;

        var scene;
        var camera;
        var controls;

        var model;

        var hemiLight;
        var directionalLight;

        var glossyMaterial;
        var matteMaterial;
        var helm;

        var projections;

        var snapShot;
        var thumbnailCreator;

        var debugDiv = document.createElement('div');
        debugDiv.style['white-space'] = 'pre';
        debugDiv.style['top'] = '10px';
        debugDiv.style['left'] = '10px';
        debugDiv.style['position'] = 'absolute';
        debugDiv.style['z-index'] = '100';

        //interaction code starts here

        var onClickPosition = new THREE.Vector2();
        var raycaster = new THREE.Raycaster();

        function getMouseInterest(e) {
            var getMousePosition = function (dom, x, y) {

                var rect = dom.getBoundingClientRect();
                return [( x - rect.left ) / rect.width, ( y - rect.top ) / rect.height];
            };

            var getIntersects = function (point, object) {
                var mouse = new THREE.Vector2();
                mouse.set(( point.x * 2 ) - 1, -( point.y * 2 ) + 1);
                raycaster.setFromCamera(mouse, camera);
                return raycaster.intersectObject(object, true);
            };


            var mousePositionArray = getMousePosition(frame.renderer.domElement, e.clientX, e.clientY);
            onClickPosition.fromArray(mousePositionArray);
            var intersects = getIntersects(onClickPosition, helm.mesh);

            return intersects;

        }

        var intersects = null;
        var downE = null;

        function onMouseDown(e) {
            downE = e;
            intersects = getMouseInterest(e);
        }

        function onMouseMove(e) {
            if (downE == null)
                return;

            var abortClick = false;
            var radiusOut = 3;

            if (Math.abs(e.clientX - downE.clientX) > radiusOut)
                abortClick = true;

            if (Math.abs(e.clientY - downE.clientY) > radiusOut)
                abortClick = true;


            if (abortClick) {
                downE = null;
                intersects = null;
            }
        }

        var self = this;

        function onMouseUp(e) {

            if (intersects == null || intersects.length < 1)
                return;

            var intersectMesh = intersects[0].object;

            if (self.debug.highlightMeshClick) {
                var highlight = new THREE.MeshLambertMaterial({
                    color: 0xff0000
                });

                if (intersectMesh.orgMat != highlight)
                    intersectMesh.orgMat = intersectMesh.material;

                intersectMesh.material = highlight;
                setTimeout(function () {
                    intersectMesh.material = intersectMesh.orgMat;
                }, 250);

                console.log(intersectMesh);
            }


            if (intersectMesh == shellMesh) {
                var uv = intersects[0].uv;
                var point = intersects[0].point;
                var interest = model.getInterest(uv, point);
                console.log("interest: " + interest);

                switch (interest) {
                    case 0:
                        delegate.interestChanged("color0");
                        break;
                    case 1:
                        delegate.interestChanged("color1");
                        break;
                    case 2:
                        delegate.interestChanged("color2");
                        break;
                    case 3:
                        delegate.interestChanged("color3");
                        break;
                    case 4:
                        delegate.interestChanged("color4");
                        break;
                    case 6:
                        delegate.interestChanged("signature");
                        break;
                }
            }

            if (intersectMesh == helm.visor1 || intersectMesh == helm.visor2) {
                delegate.interestChanged("visor");
            }
        }

        function onStart() {

            var self = this;

            window.addEventListener('keydown', function (e) {

                //f1
                if (e.keyCode == 112) {
                    self.takeSnapshot = true;
                }

                //f2
                if (e.keyCode == 113) {
                    console.log(camera.position);
                }

                //f3
                if (e.keyCode == 114) {
                    camera.position.set(startX, -2.7, startY);
                }

                //f4
                if (e.keyCode == 115) {
                    camera.position.set(356, -8, -52);
                }

                //f5
                if (e.keyCode == 116) {
                    camera.position.set(426, 66, -7);
                }

                //f6
                if (e.keyCode == 117) {
                    autoRotateRunning = true;
                }
            });

            frame.renderer.domElement.addEventListener('mousedown', onMouseDown, false);
            frame.renderer.domElement.addEventListener('mousemove', onMouseMove, false);
            frame.renderer.domElement.addEventListener('mouseup', onMouseUp, false);

            if (this.debug.showFPS)
                document.body.appendChild(debugDiv);

            projections = new Projections1(productModel);

            var renderer = frame.renderer;
            renderer.autoClear = false;
            renderer.setClearColor(0, 0);

            model = new ShellModel(productModel);

            //default
            model.layers[0].setColor(new THREE.Color(1, 1, 1)).makeNormal();
            model.layers[1].setColor(new THREE.Color(1, 1, 1)).makeNormal();
            model.layers[2].setColor(new THREE.Color(1, 1, 1)).makeNormal();
            model.layers[3].setColor(new THREE.Color(1, 1, 1)).makeNormal();
            model.layers[4].setColor(new THREE.Color(1, 1, 1)).makeNormal();

            shellMultiRTT = new ShellMultiRTT(model);

            snapShot = new SnapShot(6000);
            thumbnailCreator = new ThumbnailCreator(512 * 2 * 2);

            debugScene = new DebugScene();
            debugScene.addTexture(shellMultiRTT.diffuseRTT.texture, 90, 10, 200, 200);
            debugScene.addTexture(shellMultiRTT.surfaceRTT.texture, 300, 10, 200, 200);
            debugScene.addTexture(projections.texture, 510, 10, 400, 200);
            //debugScene.addTexture(snapShot.texture, 90, 310, 400, 400);

            glossyMaterial = new Glossy1(productModel, this.debug, shellMultiRTT, projections.texture);
            matteMaterial = new Matte1(productModel, this.debug, shellMultiRTT, projections.texture);

            scene = new THREE.Scene();
            scene.fog = new THREE.Fog(0x000000, 400, 570);

            camera = new THREE.PerspectiveCamera(45, 1, 1, 10000);
            camera.position.set(startX, -2.7, startY);
            camera.up = new THREE.Vector3(0, 1, 0);
            camera.lookAt(new THREE.Vector3(0, 0, 0));

            if (startWithAnim) {
                $(frame.renderer.domElement).css("opacity", 0.001);
            }
            else {
                controlsRunning = true;

                controls = new HelmControls(camera, frame.renderer.domElement);
                controls.enableDamping = true;

                controls.dampingFactor = 0.05; //damping for user interaction

                controls.rotateSpeed = 0.1;
                controls.zoomSpeed = 0.2;

                if (!self.debug.allowZoom) {
                    controls.maxPolarAngle = 2.3; //watching from bottom
                    controls.minPolarAngle = 0.3; //watching from top
                }
                else {
                    controls.enableZoom();
                }
            }

            hemiLight = new THREE.HemisphereLight(0xddddff, 0xffdddd, 0.8);
            scene.add(hemiLight);

            directionalLight = new THREE.DirectionalLight(0xffffff);
            directionalLight.intensity = 0.2;
            scene.add(directionalLight);

            shellMesh = new THREE.Mesh();

            helm = new Helm1(productModel, shellMesh, function (mesh, newShellMesh) {
                shellMesh = newShellMesh;

                console.log("All helm data is loaded ...");

                loaded = true;

                model.update();

                glossyMaterial.handleModelLoaded();
                matteMaterial.handleModelLoaded();

                var frontRot = new THREE.Object3D();
                frontRot.rotation.x = productModel.helmXRotation;

                frontRot.add(mesh);
                scene.add(frontRot);

                //assign logo color after load
                for (var layerIndex = 0; layerIndex < model.layers.length; layerIndex++) {
                    var layer = model.layers[layerIndex];

                    if (layerIndex == productModel.designJson.logo.layer) {
                        var logoColor = layer.currentColorConfig.logo_color == 1 ? new THREE.Color(0xffffff) : new THREE.Color(0x000000);
                        matteMaterial.setLogoColor(logoColor);
                        glossyMaterial.setLogoColor(logoColor);

                        delegate.setLogoColor(layer.currentColorConfig.logo_color);
                    }
                }

                matteMaterial.showProducerLogo(productModel.showProducerLogo);
                glossyMaterial.showProducerLogo(productModel.showProducerLogo);
                projections.renderCanvas();

                helm.updateVisor();
            });
        }

        function onLayout() {
            var size = frame.renderer.getSize();

            camera.aspect = size.width / size.height;
            camera.updateProjectionMatrix();

            debugScene.layout(size);
        }


        function onFrame(f) {

            if (loaded && !started && model.renderNeedsUpdate == 0 && startWithAnim) {
                started = true;

                var so = {};
                so.rad = 2.7;
                so.y = 370;

                var applyStartAnimValues = function () {
                    var l = Math.sqrt(startX * startX + startY * startY);
                    var rad = so.rad;
                    var sx = Math.sin(rad) * l;
                    var sy = Math.cos(rad) * l;

                    camera.position.set(sx, so.y - 2.7, sy);
                    camera.up = new THREE.Vector3(0, 1, 0);
                    camera.lookAt(new THREE.Vector3(0, so.y, 0));
                }

                TweenMax.to(so, 2, {
                    delay: 0.5, y: 0, rad: -2.14, ease: Quart.easeOut, onStart: function () {
                        applyStartAnimValues();
                        setTimeout(function () {
                            $(frame.renderer.domElement).css("opacity", 1);
                        }, 0);
                    }, onUpdate: function () {
                        applyStartAnimValues();
                    }, onComplete: function () {
                        controlsRunning = true;

                        controls = new HelmControls(camera, frame.renderer.domElement);
                        controls.enableDamping = true;

                        controls.dampingFactor = 0.05; //damping for user interaction

                        controls.rotateSpeed = 0.1;
                        controls.zoomSpeed = 0.2;

                        if (!self.debug.allowZoom) {
                            var l = Math.sqrt(startX * startX + startY * startY + 2.7 * 2.7);
                            controls.minDistance = controls.maxDistance = l;

                            controls.maxPolarAngle = 2.3; //watching from bottom
                            controls.minPolarAngle = 0.3; //watching from top
                        }
                        else {
                            controls.enableZoom();
                        }
                    }
                })
            }

            if (controlsRunning)
                controls.update();

            if (autoRotateRunning) {
                var l = Math.sqrt(startX * startX + startY * startY);

                var cx = camera.position.x;
                var cy = camera.position.z;
                var angle = Math.atan2(cx, cy);

                angle -= autoRotateAngleAdd;

                var maxSpeed = 0.005;
                autoRotateAngleAdd += maxSpeed / (60 * 2);
                if (autoRotateAngleAdd > maxSpeed)
                    autoRotateAngleAdd = maxSpeed;

                var rad = angle;
                var sx = Math.sin(rad) * l;
                var sy = Math.cos(rad) * l;

                camera.position.set(sx, camera.position.y, sy);
                camera.up = new THREE.Vector3(0, 1, 0);
                camera.lookAt(new THREE.Vector3(0, 0, 0));
            }

            scene.fog.near = camera.position.length() - 200;
            scene.fog.far = camera.position.length() + 250;

            //hemiLight.position.set(camera.position.x, camera.position.y, camera.position.z);
            directionalLight.position.set(camera.position.x, camera.position.y, camera.position.z);

            frame.renderer.clear();
            shellMultiRTT.render(frame.renderer);

            if (this.debug.showTextures)
                debugScene.render(frame.renderer);

            if (this.takeSnapshot) {
                snapShot.takeSnapshot(frame.renderer, camera, scene)
                this.takeSnapshot = false;
            }

            //console.log(camera.position);

            frame.renderer.render(scene, camera);

            if (this.debug.showFlicker)
                Ficker.render(frame.renderer);


            if (this.debug) {
                var tf0 = performance.now();
                frame.renderer.context.finish();
                var tf1 = performance.now();
                debugDiv.innerHTML = Math.round((tf1 - tf0) * 10) / 10;
            }
        }

        frame.onStart.add(onStart.bind(this));
        frame.onLayout.add(onLayout.bind(this));
        frame.onFrame.add(onFrame.bind(this));


        this.showProducerLogo = function (enabled) {
            console.log("showProducerLogo", enabled);

            productModel.showProducerLogo = enabled;

            matteMaterial.showProducerLogo(productModel.showProducerLogo);
            glossyMaterial.showProducerLogo(productModel.showProducerLogo);
            projections.renderCanvas();
        }

        this.toggleVisibility = function (name) {
            helm.toggleVisibility(name);
        }

        this.overrideMaterial = function (overrideMaterialName) {
            scene.overrideMaterial = DebugMaterials.get(overrideMaterialName);
        }

        this.setGlossy = function (glossy) {
            shellMesh.material = glossy ? glossyMaterial.material : matteMaterial.material;
        }

        this.setDriverName = function (driverName) {
            projections.setDriverName(driverName);
        }

        this.getGlossy = function () {
            return shellMesh.material == glossyMaterial.material;
        }

        this.setCamera = function (x, y, z) {
            camera.position.set(x, y, z);
        }

        this.setColor = function (layerIndex, sku) {
            var colorConfig = config.colors[sku];

            if (colorConfig == null) {
                console.error('Color ' + sku + ' not in config: ', config);
                return;
            }

            this.setColorConfig(layerIndex, colorConfig)
        }

        this.setColorConfig = function (layerIndex, colorConfig) {

            var layer = model.layers[layerIndex];
            if (layer == null) {
                console.warn("Layer " + layerIndex + " not implemented.");
                return;
            }

            layer.currentColorConfig = colorConfig;

            //thats where is logo is on
            if (productModel.designJson != null) {
                if (layerIndex == productModel.designJson.logo.layer) {
                    var logoColor = colorConfig.logo_color == 1 ? new THREE.Color(0xffffff) : new THREE.Color(0x000000);
                    matteMaterial.setLogoColor(logoColor);
                    glossyMaterial.setLogoColor(logoColor);

                    delegate.setLogoColor(colorConfig.logo_color);
                }
            }

            var type = colorConfig.type;

            if (colorConfig.gl) {
                if (colorConfig.gl.override_type != null) {
                    type = colorConfig.gl.override_type;
                }
            }

            switch (type) {
                case "metallic":
                    layer.makeMetallic();
                    layer.setColor(new THREE.Color(colorConfig.gl.diffuse));
                    break;
                case "metallic_gradient":
                    layer.makeMetallic();
                    layer.setGradient(
                        new THREE.Color(colorConfig.gl.diffuse0),
                        new THREE.Color(colorConfig.gl.diffuse1)
                    );
                    //layer.setColor(new THREE.Color(0xffffff));
                    break;
                case "flake":
                    layer.makeFlake();
                    layer.setColor(new THREE.Color(colorConfig.gl.diffuse));
                    break;
                case "holo-flake":
                    layer.makeHoloFlake();
                    layer.setColor(new THREE.Color(colorConfig.gl.diffuse));
                    break;
                case "gradient":
                    layer.makeNormal();
                    layer.setGradient(
                        new THREE.Color(colorConfig.gl.diffuse0),
                        new THREE.Color(colorConfig.gl.diffuse1)
                    );
                    break;
                default:
                    layer.makeNormal();
                    layer.setColor(new THREE.Color(colorConfig.hex));
            }
        }

        this.setInterest = function (interest) {
            try
            {
                switch (interest) {
                    case "color0":
                        model.layers[0].setHighlight();
                        break;
                    case "color1":
                        model.layers[1].setHighlight();
                        break;
                    case "color2":
                        model.layers[2].setHighlight();
                        break;
                    case "color3":
                        model.layers[3].setHighlight();
                        break;
                    case "color4":
                        model.layers[4].setHighlight();
                        break;
                    case "signature":
                        controls.setTargetMode(-1.6, 1.5);
                        break;
                    case "visor":
                        controls.setTargetMode(-2.3, 1.5);
                        break;
                }
            }
            catch(e)
            {
                console.warn("setInterest error:", e);
            }
        }

        this.setFont = function (font) {
            projections.setSignatureFont(font);
        }

        this.setVisorStickerColor = function (sku) {
            helm.setVisorStickerColor(sku);
        }

        this.setVisor = function (sku) {
          helm.setVisor(sku);
        }

        this.setSignatureColor = function (sku) {
            var colorConfig = config.colors[sku];

            if (colorConfig == null) {
                console.error('Color ' + sku + ' not in config: ', config);
                return;
            }

            glossyMaterial.setSignatureColor(colorConfig);
            matteMaterial.setSignatureColor(colorConfig);

        }

        this.setNumber = function (num) {
            projections.setNumber(num);
        }

        this.toggleDebugClick = function () {
            this.debug.highlightMeshClick = !this.debug.highlightMeshClick;
        }

        this.createThumbnail = function () {
            console.log("GLComponent Create thumbnail");

            var snapCamera = new THREE.PerspectiveCamera(45, 1, 1, 10000);
            snapCamera.position.set(startX, -2.7, startY);
            snapCamera.up = new THREE.Vector3(0, 1, 0);
            snapCamera.lookAt(new THREE.Vector3(0, 0, 0));

            return thumbnailCreator.takeSnapshot(frame.renderer, snapCamera, scene);
        }

    }

    return GLComponent;
});

define([
        "hr/CDN",
        "Frame",
        "hr/DefaultMaterialFactory",
        "hr/loader/OBJLoader",
        "text!app/TestTextureGradient/default.vert",
        "text!app/TestTextureGradient/diffuse.frag",
        "text!app/TestTextureGradient/shell.frag",
    ],
    function (CDN,
              Frame,
              MaterialFactory,
              ObjectLoader,
              defaultVertex,
              diffuseFrag,
              shellFrag) {

        var matFactory = new MaterialFactory();
        var frame = new Frame();

        var renderer = frame.renderer;
        renderer.autoClear = false;
        renderer.setClearColor(0, 0);

        var textureLoader = new THREE.TextureLoader();
        textureLoader.setCrossOrigin('');
        var metalTexture1 = textureLoader.load(CDN.path + "gl/mat/mirror/ModelTest/shape-assets-NEW/assets/shape-1.png");
        var metalTexture2 = textureLoader.load(CDN.path + "gl/mat/mirror/ModelTest/shape-assets-NEW/assets/shape-2.png");
        var metalTexture3 = textureLoader.load(CDN.path + "gl/mat/mirror/ModelTest/shape-assets-NEW/assets/stripes.png");

        var gradientMatrix = new THREE.Matrix4();
        //gradientMatrix.multiply(new THREE.Matrix4().makeScale(2, 1, 1));
        //gradientMatrix.multiply(new THREE.Matrix4().makeTranslation(-0.4,0,0));
        //gradientMatrix.multiply(new THREE.Matrix4().makeRotationZ(-0.9));

        //gradientMatrix.multiply(new THREE.Matrix4().makeScale(2, 1, 1));
        //gradientMatrix.multiply(new THREE.Matrix4().makeTranslation(-0.3,0,0));

        //gradientMatrix.multiply(new THREE.Matrix4().makeScale(2.5, 1, 1));
        //gradientMatrix.multiply(new THREE.Matrix4().makeTranslation(-0.2,0,0));
        //gradientMatrix.multiply(new THREE.Matrix4().makeRotationZ(-1.57));

        var materialShell = new THREE.ShaderMaterial({
            uniforms: {
                time: {type: "f", value: 0.1},
                m: {type: "m4", value: gradientMatrix},
                tDiffuse1: {type: "t", value: metalTexture1},
                tDiffuse2: {type: "t", value: metalTexture2},
                tDiffuse3: {type: "t", value: metalTexture3},
                c0: {type: "c", value: new THREE.Color(0x1a8f86)},
                c1: {type: "c", value: new THREE.Color(0x154d66)},
                c2: {type: "c", value: new THREE.Color(0xffffff)},
                c3: {type: "c", value: new THREE.Color(0.0, 0.0, 0.0)},
            },
            vertexShader: defaultVertex,
            fragmentShader: shellFrag,
            side: THREE.DoubleSide,
            depthWrite: false
        });

        var rtTexture = new THREE.WebGLRenderTarget(2048, 2048, {
            minFilter: THREE.LinearFilter,
            magFilter: THREE.LinearFilter,
            format: THREE.RGBFormat
        });
        rtTexture.texture.wrapS = rtTexture.texture.wrapT = THREE.RepeatWrapping;

        var textureScreen = new THREE.Scene();

        var plane2 = new THREE.PlaneBufferGeometry(1, 1);
        var quad2 = new THREE.Mesh(plane2, materialShell);

        textureScreen.add(quad2);

        var cameraTextureRTT = new THREE.OrthographicCamera(-0.5, 0.5, 0.5, -0.5, -10000, 10000);

        var sceneScreen = new THREE.Scene();

        var materialScreen2 = new THREE.ShaderMaterial({
            uniforms: {tDiffuse: {type: "t", value: rtTexture}},
            vertexShader: defaultVertex,
            fragmentShader: diffuseFrag,
            side: THREE.DoubleSide,
            depthWrite: false
        });


        var plane = new THREE.PlaneBufferGeometry(1, 1);
        quad = new THREE.Mesh(plane, materialScreen2);
        quad.position.x = 0.5;
        quad.position.y = 0.5;

        var sss = new THREE.Object3D();
        sss.add(quad);
        sceneScreen.add(sss);

        cameraRTT = new THREE.OrthographicCamera(0, 1, 0, 1, -10000, 10000);

        matFactory.mat('shellTexture2').map = rtTexture;
        //matFactory.mat('shellTexture').map = metalTexture;

        var scene = new THREE.Scene();

        var camera = new THREE.PerspectiveCamera(45, 1, 1, 10000);
        camera.position.x = -500;
        camera.position.z = -300;

        var hemiLight = new THREE.HemisphereLight(0xd2dbff, 0x9d8787, 1.2);
        hemiLight.position.set(0, 1, 0.1);
        scene.add(hemiLight);

        directionalLight = new THREE.DirectionalLight(0x777777);
        directionalLight.intensity = 0.3
        directionalLight.position.set(-1, 1, 0.3);
        scene.add(directionalLight);

        controls = new THREE.OrbitControls(camera, renderer.domElement);
        controls.enableDamping = true;
        controls.dampingFactor = 0.05;
        controls.enableZoom = true;
        controls.rotateSpeed = 0.1;
        controls.zoomSpeed = 0.2;

        var self = this;
        Object.defineProperty(THREE.Color.prototype, "datColor", {
            get: function datColor() {
                return this.getStyle()
            },
            set: function datColor(value) {
                this.set(value);
                self.textureNeedsUpdate = true;
            }
        });

        var gui = new dat.GUI({
            open: true
        });

        var gradient = {
            name: 'one',
            translate: 1.36,
            scale: 3,
            rotation: 211,
        };
        gui.add(gradient, 'translate', -3, 3).name("Translate");
        gui.add(gradient, 'scale', -3, 3).name("Scale");
        gui.add(gradient, 'rotation', 0, 360).name("Rotation");

        loader = new ObjectLoader();
        loader.load(CDN.path + "gl/mat/mirror/ModelTest/Helm_05.obj", function (s) {

            var m = s;
            s.position.z = 150;
            s.position.y = -150;
            console.log("m: " + m);

            m.traverse(function (x) {

                if (x.geometry != null)
                    switch (x.name) {
                        case 'Helmet_000':
                            x.material = matFactory.mat('shellTexture2');
                            break;
                    }
            });

            scene.add(m);
        });

        frame.onLayout.add(function () {
            var size = frame.renderer.getSize();

            camera.aspect = size.width / size.height;
            camera.updateProjectionMatrix();

            sss.scale.x = 200;
            sss.scale.y = 200;

            cameraRTT.top = 0;
            cameraRTT.bottom = size.height;

            cameraRTT.left = 0;
            cameraRTT.right = size.width;
            cameraRTT.updateProjectionMatrix();
        });

        var f = 0;
        this.textureNeedsUpdate = true;
        frame.onFrame.add(function () {
            controls.update();

            f++;

            gradientMatrix.identity();
            gradientMatrix.multiply(new THREE.Matrix4().makeTranslation(gradient.translate, 0, 0));
            gradientMatrix.multiply(new THREE.Matrix4().makeScale(gradient.scale, 1, 1));
            gradientMatrix.multiply(new THREE.Matrix4().makeRotationZ(gradient.rotation * Math.PI / 180));

            materialShell.uniforms.time.value = f * 0.1;

            frame.renderer.clear();
            frame.renderer.render(textureScreen, cameraTextureRTT, rtTexture, true);
            frame.renderer.render(scene, camera);
        });

        frame.start();
    }
)
;
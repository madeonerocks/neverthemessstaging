
        #define RECIPROCAL_PI2 0.15915494
        #define PI 3.141592653

        #define saturate(a) clamp( a, 0.0, 1.0 )

        uniform mat4 pmSide;
        uniform mat4 pmBack;

        uniform sampler2D tDiffuse;
        uniform sampler2D tAmbient;
        uniform sampler2D tNormal;
        uniform sampler2D tNormal2;
        uniform sampler2D tNormal3;
        uniform sampler2D tSpecular;
        uniform sampler2D tProject;
        uniform sampler2D tEnv;
        uniform float alpha;

        varying vec2 vUv;
        varying vec3 vViewPosition;
        varying vec3 vNormal;
        varying vec3 vWorldPosition;

        vec3 inverseTransformDirection( in vec3 normal, in mat4 matrix ) {

            return normalize( ( vec4( normal, 0.0 ) * matrix ).xyz );

        }

        vec3 F_Schlick( in vec3 specularColor, in float dotLH ) {

            // Original approximation by Christophe Schlick '94
        //	float fresnel = pow( 1.0 - dotLH, 5.0 );

            // Optimized variant (presented by Epic at SIGGRAPH '13)
            float fresnel = exp2( ( -5.55437 * dotLH - 6.98316 ) * dotLH );

            return ( 1.0 - specularColor ) * fresnel + specularColor;

        }

        float G_BlinnPhong_Implicit( /* in float dotNL, in float dotNV */ ) {

            // geometry term is (n⋅l)(n⋅v) / 4(n⋅l)(n⋅v)

            return 0.25;

        }

        float D_BlinnPhong( in float shininess, in float dotNH ) {

            // factor of 1/PI in distribution term omitted

            return ( shininess * 0.5 + 1.0 ) * pow( dotNH, shininess );

        }

        vec3 BRDF_BlinnPhong( in vec3 specularColor, in float shininess, in vec3 normal, in vec3 lightDir, in vec3 viewDir ) {

            vec3 halfDir = normalize( lightDir + viewDir );

        //	float dotNL = saturate( dot( normal, lightDir ) );
        //	float dotNV = saturate( dot( normal, viewDir ) );
            float dotNH = saturate( dot( normal, halfDir ) );
            float dotLH = saturate( dot( lightDir, halfDir ) );

            vec3 F = F_Schlick( specularColor, dotLH );

            float G = G_BlinnPhong_Implicit( /* dotNL, dotNV */ );

            float D = D_BlinnPhong( shininess, dotNH );

            return F * G * D;
        }

        vec3 perturbNormal2Arb( sampler2D map, vec3 eye_pos, vec3 surf_norm, vec2 normalScale, float scale) {

            vec3 q0 = dFdx( eye_pos.xyz );
            vec3 q1 = dFdy( eye_pos.xyz );
            vec2 st0 = dFdx( vUv.st );
            vec2 st1 = dFdy( vUv.st );

            vec3 S = normalize( q0 * st1.t - q1 * st0.t );
            vec3 T = normalize( -q0 * st1.s + q1 * st0.s );
            vec3 N = normalize( surf_norm );

            vec3 mapN = texture2D( map, vUv * scale ).xyz * 2.0 - 1.0;
            mapN.xy = normalScale * mapN.xy;
            mat3 tsn = mat3( S, T, N );
            return normalize( tsn * mapN );

        }

        void main() {

            vec3 viewDir = normalize( vViewPosition );

            vec3 result = vec3(0, 0, 0);
            vec3 diffuseMap = texture2D( tDiffuse, vUv).rgb;
            vec3 ambientMap = texture2D( tAmbient, vUv).rgb;
            vec3 specularMap = texture2D( tSpecular, vUv).rgb;

            vec3 specular = vec3(1, 1, 1);
            float shininess = 4.0;
            float specularStrength = 1.0;

            vec3 normal = normalize( vNormal );
            vec3 normal2 = perturbNormal2Arb(tNormal, -vViewPosition, normal, vec2(1, 1) , 1.0);
            vec3 normal3 = perturbNormal2Arb(tNormal2, -vViewPosition, normal, vec2(0.005, 0.005) , 30.0);
            normal3 = perturbNormal2Arb(tNormal3, -vViewPosition, normal3, vec2(0.01, 0.01) , 40.0);
            vec3 normal4 = perturbNormal2Arb(tNormal3, -vViewPosition, normal2, vec2(-0.3, -0.3) , 20.0);

            vec3 totalDiffuseLight = vec3( 0.0 );
            vec3 totalSpecularLight = vec3( 0.0 );

            vec3 lightColor = vec3(1.0, 1.0, 1.0);
    //		vec3 lightDir = normalize(vec3(-1.0, 1.0, -1.0));
            vec3 lightDir1 = normalize(vec3(0, 0.25, 1.0));
            vec3 lightDir2 = normalize(vec3(0, 0.25, 1.0));

            // diffuse

            //float cosineTerm = cos(saturate( dot( normal, lightDir ) ) * 16.8);
            float lambert1 = saturate( dot( normal4, lightDir1 ) );
            float lambert2 = saturate( dot( normal4, lightDir2 ) );
            float cosineTerm1 = saturate(sin(lambert1 * 12.6));
            float cosineTerm2 = saturate(sin(lambert2 * 24.0));

    		vec3 cameraToVertex = normalize( vWorldPosition - cameraPosition );
	    	vec3 worldNormal = inverseTransformDirection( normal3, viewMatrix );
			vec3 reflectVec = reflect( cameraToVertex, worldNormal );

		    vec2 sampleUV;
		    sampleUV.y = saturate(reflectVec.y * 0.5 + 0.5 );
		    sampleUV.x = atan(reflectVec.z,reflectVec.x ) * RECIPROCAL_PI2 + 0.5;
		    vec4 envColor = texture2D( tEnv, sampleUV );

            vec3 dirSide = normalize(vWorldPosition + vec3(0.0,52.0,53.0));
            dirSide = (pmSide * vec4(dirSide, 1.0)).xyz;

            vec2 uvProjectedSide = dirSide.xy;
            if (uvProjectedSide.x > 0.0 && uvProjectedSide.x < 1.0
                && uvProjectedSide.y > 0.0 && uvProjectedSide.y < 1.0)
            {
                uvProjectedSide.x = (dirSide.z > 0.0 ? 0.0 : 1.0) + uvProjectedSide.x * sign(dirSide.z);
                vec4 projectMap = texture2D(tProject, uvProjectedSide);
                diffuseMap = diffuseMap * (1.0 - projectMap.a) + projectMap.rgb * projectMap.a * 0.8;
            }

            vec3 dirBack = normalize(vWorldPosition + vec3(0.0,0,0));
            dirBack = (pmBack * vec4(dirBack, 1.0)).xyz;

            vec2 uvProjectedBack = dirBack.xy;
//            if (uvProjectedBack.x > 0.0 && uvProjectedBack.x < 1.0
//                && uvProjectedBack.y > 0.0 && uvProjectedBack.y < 1.0)
            {
                uvProjectedBack.x = (dirBack.z > 0.0 ? 0.0 : 1.0) + uvProjectedBack.x * sign(dirBack.z);
                vec4 projectMap = texture2D(tProject, uvProjectedBack);
//                diffuseMap = diffuseMap * (1.0 - projectMap.a) + projectMap.rgb * projectMap.a * 0.8;
            }


            result += envColor.xyz * lambert2 * 0.1;
            result += diffuseMap * 0.3;
            result += diffuseMap * lambert2 * 0.6 * (0.4 + ambientMap.r * 0.6);
            result += (specularMap * 2.0) * cosineTerm2 * 0.4;
            result += (vec3(0.4, 0.4, 0.4)) * cosineTerm1 * 0.3;



            gl_FragColor = vec4(result * 1.0, 1);

        }
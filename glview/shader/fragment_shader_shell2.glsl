varying vec2 vUv;

uniform sampler2D tDiffuse1;
uniform sampler2D tDiffuse2;
uniform sampler2D tDiffuse3;

uniform vec3 color1;
uniform vec3 color2;
uniform vec3 color3;
uniform vec3 color4;

uniform float time;

void main() {

	float t1 = texture2D( tDiffuse1, vUv ).r;
	float t2 = texture2D( tDiffuse2, vUv ).r;
	float t3 = texture2D( tDiffuse3, vUv ).r;

	vec3 rgb = color1;
	rgb = mix(rgb, color2, t1);
	rgb = mix(rgb, color3, t2);
	rgb = mix(rgb, color4, t3);

	gl_FragColor = vec4(rgb, 1);

}